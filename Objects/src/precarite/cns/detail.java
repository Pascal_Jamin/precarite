package precarite.cns;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class detail extends Activity implements B4AActivity{
	public static detail mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "precarite.cns", "precarite.cns.detail");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (detail).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "precarite.cns", "precarite.cns.detail");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "precarite.cns.detail", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (detail) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (detail) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return detail.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (detail) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (detail) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.net.FTPWrapper _v5 = null;
public static String _v6 = "";
public static String _v7 = "";
public static anywheresoftware.b4a.gps.GPS _v0 = null;
public static anywheresoftware.b4a.gps.LocationWrapper _vv1 = null;
public precarite.cns.cameraexclass _vvvvvvvvvvvvvvvvvvvvvvvvv3 = null;
public de.donmanfred.IconButtonWrapper _vvvvvvvvvvvvvvvvvvvvvvvvv1 = null;
public static int _vvvvvvvvvvvvvvvvvvv0 = 0;
public anywheresoftware.b4a.objects.JackcessDatabase _vvvvvvvvvvvvvvvvv2 = null;
public static boolean _vvvvvvvvvvvvvvvvvvvvvvvvvvv3 = false;
public static int _vvvvvvvvvvvvvvvvvvvv2 = 0;
public static boolean _vvvvvvvvvvvvvvvvvvv5 = false;
public static boolean _vvvvvvvvvvvvvvvvvvv6 = false;
public anywheresoftware.b4a.objects.PanelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _vvvvvvvvvvvvvvvvvvvvvv6 = null;
public static String _vvvvvvvvvvvvvvvvvvvvvv0 = "";
public static int _vvvvvvvvvvvvvvvvvvvvvvv6 = 0;
public static String _vvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvv7 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvv2 = "";
public static String _vvvvvvvvvvvvvvvvvvvv4 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvvvv7 = "";
public static String _vvvvvvvvvvvvvvvvvvvv3 = "";
public static String _vvvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvv7 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvv0 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv7 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv0 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv2 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv3 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvvv1 = "";
public anywheresoftware.b4a.objects.EditTextWrapper _commentaire = null;
public de.donmanfred.IconButtonWrapper _btn_next = null;
public de.donmanfred.IconButtonWrapper _btn_prev = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsave = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsaverec = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview1 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvv6 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbaccespcommunes = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview3 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview4 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview5 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview6 = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _vvvvvvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvv7 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvv0 = null;
public anywheresoftware.b4a.objects.LabelWrapper _options2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvv3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblplancher = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbllocal = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblisolant = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeporte = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeplancher = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypecloison = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypelocal = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeisolant = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcbacces = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblhspavec = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcbcollectif = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbcollectif = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txthspavecisolant = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txthspsansisolant = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblhspsans = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblnbbat = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnbbat = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblnbbatimm = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnbbatimm = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtaddrct1 = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtaddrct2 = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtaddrctcp = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtaddrctville = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnbcageesc = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblnbcageesc = null;
public anywheresoftware.b4a.objects.TabStripViewPager _tspreca = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypetoit = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypelogement = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnnatureisolant = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnmateriauprevu = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnetatplafond = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txthsousgoutiere = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtsurfcomble = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnbniveaux = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtsurfreelle = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbcombleamenag = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbparevapeur = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbtrappecomble = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbrehaussevmc = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbrehaussetrappe = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbevacisolant = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbspot = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbconduitcheminee = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbecartfeu = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbcombleencombre = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeplanchercbl = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypereseauplafond = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnetatisolantplb = null;
public anywheresoftware.b4a.objects.LabelWrapper _label1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv6 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp1 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp3 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp4 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp5 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewp6 = null;
public de.donmanfred.IconButtonWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsign = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageviewsignature = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnbsacsutilises = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imvcnslogo1 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbprgmage = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbcnxinternet = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcaddr1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcaddr2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvv0 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imvfs = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imvfsapres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp1apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp4apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp2apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp5apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp3apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp6apres = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp6 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbisolable = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbpresencevmc = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp7 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview7 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblp8 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview8 = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtcommentapres = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnresistanceisolant = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnaccespcommunes = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txthspporteisolant = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbenlevementneon = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbpointeauproche = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbaltluminaires = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbcaveencombree = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbavis1 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbavis2 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbrefusresident = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcblhide = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlplbhide = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcblhide = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtprenom = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtnom = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txttel1 = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txttel2 = null;
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bm = null;
anywheresoftware.b4a.objects.LabelWrapper _lbl = null;
String[] _colonnes = null;
String[] _coltypes = null;
int _i = 0;
int _position = 0;
 //BA.debugLineNum = 45;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 46;BA.debugLine="Activity.LoadLayout(\"Detail\")";
mostCurrent._activity.LoadLayout("Detail",mostCurrent.activityBA);
 //BA.debugLineNum = 48;BA.debugLine="If Main.TypeAppli.Contains(\"Comble\") Then";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvv6.contains("Comble")) { 
 //BA.debugLineNum = 49;BA.debugLine="tsPreca.LoadLayout(\"detail_cbl\", \"Combles\")";
mostCurrent._tspreca.LoadLayout("detail_cbl",BA.ObjectToCharSequence("Combles"));
 //BA.debugLineNum = 50;BA.debugLine="tsPreca.LoadLayout(\"photoAvant\", Chr(0xF030) & \"";
mostCurrent._tspreca.LoadLayout("photoAvant",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf030)))+" Avant"));
 //BA.debugLineNum = 51;BA.debugLine="tsPreca.LoadLayout(\"PhotoApres\", Chr(0xF030) & \"";
mostCurrent._tspreca.LoadLayout("PhotoApres",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf030)))+" Apres"));
 //BA.debugLineNum = 52;BA.debugLine="tsPreca.LoadLayout(\"detail_reception\", Chr(0xF00";
mostCurrent._tspreca.LoadLayout("detail_reception",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf007)))+" Réception"));
 //BA.debugLineNum = 53;BA.debugLine="tsPreca.LoadLayout(\"detail_plb\", \" \")";
mostCurrent._tspreca.LoadLayout("detail_plb",BA.ObjectToCharSequence(" "));
 };
 //BA.debugLineNum = 63;BA.debugLine="If Main.TypeAppli.StartsWith(\"Plancher\") Then";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvv6.startsWith("Plancher")) { 
 //BA.debugLineNum = 64;BA.debugLine="tsPreca.LoadLayout(\"detail_plb\", \"Plancher Bas\")";
mostCurrent._tspreca.LoadLayout("detail_plb",BA.ObjectToCharSequence("Plancher Bas"));
 //BA.debugLineNum = 65;BA.debugLine="tsPreca.LoadLayout(\"photoAvant\", Chr(0xF030) & \"";
mostCurrent._tspreca.LoadLayout("photoAvant",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf030)))+" Avant"));
 //BA.debugLineNum = 66;BA.debugLine="tsPreca.LoadLayout(\"PhotoApres\", Chr(0xF030) & \"";
mostCurrent._tspreca.LoadLayout("PhotoApres",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf030)))+" Apres"));
 //BA.debugLineNum = 67;BA.debugLine="tsPreca.LoadLayout(\"detail_reception\", Chr(0xF00";
mostCurrent._tspreca.LoadLayout("detail_reception",BA.ObjectToCharSequence(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf007)))+" Réception"));
 //BA.debugLineNum = 68;BA.debugLine="tsPreca.LoadLayout(\"detail_cbl\", \" \")";
mostCurrent._tspreca.LoadLayout("detail_cbl",BA.ObjectToCharSequence(" "));
 };
 //BA.debugLineNum = 79;BA.debugLine="Activity.Title=\"Relevé Précarité CNS (Détail)\"";
mostCurrent._activity.setTitle(BA.ObjectToCharSequence("Relevé Précarité CNS (Détail)"));
 //BA.debugLineNum = 82;BA.debugLine="CNSPath=File.DirRootExternal & \"/RelevePrecarite\"";
mostCurrent._vvvvvvvvvvvvvvvvv1 = anywheresoftware.b4a.keywords.Common.File.getDirRootExternal()+"/RelevePrecarite";
 //BA.debugLineNum = 83;BA.debugLine="iscamera=False";
_vvvvvvvvvvvvvvvvvvv5 = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 84;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 85;BA.debugLine="expectedPic=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 86;BA.debugLine="photoIdx=1";
_vvvvvvvvvvvvvvvvvvv0 = (int) (1);
 //BA.debugLineNum = 87;BA.debugLine="imvCNSLogo1.Bitmap=LoadBitmap(File.DirAssets,\"log";
mostCurrent._imvcnslogo1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"logocns.png").getObject()));
 //BA.debugLineNum = 88;BA.debugLine="btn_prev.Visible=False";
mostCurrent._btn_prev.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 89;BA.debugLine="btn_next.Visible=False";
mostCurrent._btn_next.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 93;BA.debugLine="Dim bm As BitmapDrawable";
_bm = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 94;BA.debugLine="bm.Initialize(LoadBitmap(File.DirAssets,\"313-arro";
_bm.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"313-arrow-left.png").getObject()));
 //BA.debugLineNum = 95;BA.debugLine="btn_prev.Text = \"\"";
mostCurrent._btn_prev.setText("");
 //BA.debugLineNum = 96;BA.debugLine="btn_prev.IconPadding = 0";
mostCurrent._btn_prev.setIconPadding((int) (0));
 //BA.debugLineNum = 97;BA.debugLine="btn_prev.setIcon(True,bm)";
mostCurrent._btn_prev.setIcon(anywheresoftware.b4a.keywords.Common.True,(android.graphics.drawable.Drawable)(_bm.getObject()));
 //BA.debugLineNum = 98;BA.debugLine="Dim bm As BitmapDrawable";
_bm = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 99;BA.debugLine="bm.Initialize(LoadBitmap(File.DirAssets,\"309-arro";
_bm.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"309-arrow-right.png").getObject()));
 //BA.debugLineNum = 100;BA.debugLine="btn_next.Text = \"\"";
mostCurrent._btn_next.setText("");
 //BA.debugLineNum = 101;BA.debugLine="btn_next.IconPadding = 0";
mostCurrent._btn_next.setIconPadding((int) (0));
 //BA.debugLineNum = 102;BA.debugLine="btn_next.setIcon(True,bm)";
mostCurrent._btn_next.setIcon(anywheresoftware.b4a.keywords.Common.True,(android.graphics.drawable.Drawable)(_bm.getObject()));
 //BA.debugLineNum = 103;BA.debugLine="Dim bm As BitmapDrawable";
_bm = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 104;BA.debugLine="bm.Initialize(LoadBitmap(File.DirAssets,\"099-flop";
_bm.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"099-floppy-disk.png").getObject()));
 //BA.debugLineNum = 108;BA.debugLine="For Each lbl As Label In GetAllTabLabels(tsPreca)";
_lbl = new anywheresoftware.b4a.objects.LabelWrapper();
{
final anywheresoftware.b4a.BA.IterableList group37 = _vvvvvvvvvvvvvvvvvvvv1(mostCurrent._tspreca);
final int groupLen37 = group37.getSize()
;int index37 = 0;
;
for (; index37 < groupLen37;index37++){
_lbl.setObject((android.widget.TextView)(group37.Get(index37)));
 //BA.debugLineNum = 109;BA.debugLine="lbl.Typeface = Typeface.FONTAWESOME";
_lbl.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.getFONTAWESOME());
 }
};
 //BA.debugLineNum = 113;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 114;BA.debugLine="If Not(File.Exists(CNSPath,\"\")) Then File.MakeDi";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,""))) { 
anywheresoftware.b4a.keywords.Common.File.MakeDir(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"/RelevePrecarite");};
 //BA.debugLineNum = 115;BA.debugLine="If Not(File.Exists(CNSPath, \"precarite.accdb\"))";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,"precarite.accdb"))) { 
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"precarite.accdb",mostCurrent._vvvvvvvvvvvvvvvvv1,"precarite.accdb");};
 //BA.debugLineNum = 116;BA.debugLine="If Not(File.Exists(CNSPath, \"vide.jpg\")) Then Fi";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg"))) { 
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"vide.jpg",mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg");};
 //BA.debugLineNum = 117;BA.debugLine="db.Open(CNSPath & \"/precarite.accdb\")";
mostCurrent._vvvvvvvvvvvvvvvvv2.Open(mostCurrent._vvvvvvvvvvvvvvvvv1+"/precarite.accdb");
 //BA.debugLineNum = 119;BA.debugLine="ftp.Initialize(\"ftp\", \"ftp.barrault-recherche.co";
_v5.Initialize(processBA,"ftp","ftp.barrault-recherche.com",(int) (21),"barrault","TH62JX14dL");
 //BA.debugLineNum = 120;BA.debugLine="Main.fTable.Initialize(db.GetTable(\"Detail\"))";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"));
 //BA.debugLineNum = 121;BA.debugLine="Main.fCursor.Initialize(db.GetTable(\"Detail\"),Ma";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 122;BA.debugLine="Main.fTableLieux.Initialize(db.GetTable(\"Lieux\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Lieux"));
 //BA.debugLineNum = 123;BA.debugLine="Main.fTableSite.Initialize(db.GetTable(\"Site\"))";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Site"));
 //BA.debugLineNum = 125;BA.debugLine="CampagneID=1  'A changer pour pouvoir selectionn";
_vvvvvvvvvvvvvvvvvvvv2 = (int) (1);
 //BA.debugLineNum = 126;BA.debugLine="curArea=\"Localisation\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv3 = "Localisation";
 //BA.debugLineNum = 127;BA.debugLine="curphotoName=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "";
 //BA.debugLineNum = 129;BA.debugLine="Dim colonnes() As String,coltypes() As String,i";
_colonnes = new String[(int) (0)];
java.util.Arrays.fill(_colonnes,"");
_coltypes = new String[(int) (0)];
java.util.Arrays.fill(_coltypes,"");
_i = 0;
 //BA.debugLineNum = 130;BA.debugLine="colonnes=Main.fTable.GetColumnNames";
_colonnes = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetColumnNames();
 //BA.debugLineNum = 131;BA.debugLine="coltypes=Main.fTable.GetColumnJavaTypes";
_coltypes = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetColumnJavaTypes();
 //BA.debugLineNum = 132;BA.debugLine="For i=0 To colonnes.Length -1";
{
final int step56 = 1;
final int limit56 = (int) (_colonnes.length-1);
_i = (int) (0) ;
for (;(step56 > 0 && _i <= limit56) || (step56 < 0 && _i >= limit56) ;_i = ((int)(0 + _i + step56))  ) {
 //BA.debugLineNum = 133;BA.debugLine="Log(\"Colonne \" & i & \" - \" & colonnes(i) & \" [\"";
anywheresoftware.b4a.keywords.Common.Log("Colonne "+BA.NumberToString(_i)+" - "+_colonnes[_i]+" ["+_coltypes[_i]+"]");
 }
};
 //BA.debugLineNum = 137;BA.debugLine="gpsClient.Initialize(\"gpsclient\")";
_v0.Initialize("gpsclient");
 //BA.debugLineNum = 138;BA.debugLine="userLocation.Initialize";
_vv1.Initialize();
 };
 //BA.debugLineNum = 142;BA.debugLine="If db.IsOpen=False Then";
if (mostCurrent._vvvvvvvvvvvvvvvvv2.IsOpen()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 143;BA.debugLine="db.Open(CNSPath & \"/precarite.accdb\")";
mostCurrent._vvvvvvvvvvvvvvvvv2.Open(mostCurrent._vvvvvvvvvvvvvvvvv1+"/precarite.accdb");
 //BA.debugLineNum = 144;BA.debugLine="Main.fTable.Initialize(db.GetTable(\"Detail\"))";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"));
 //BA.debugLineNum = 145;BA.debugLine="Main.fCursor.Initialize(db.GetTable(\"Detail\"),Ma";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 146;BA.debugLine="Main.fTableLieux.Initialize(db.GetTable(\"Lieux\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Lieux"));
 //BA.debugLineNum = 148;BA.debugLine="Main.fTableSite.Initialize(db.GetTable(\"Site\"))";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Site"));
 };
 //BA.debugLineNum = 152;BA.debugLine="CampagneID=1";
_vvvvvvvvvvvvvvvvvvvv2 = (int) (1);
 //BA.debugLineNum = 153;BA.debugLine="Main.fCursor.Initialize(db.GetTable(\"Detail\"),Mai";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 157;BA.debugLine="UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSp";
_vvvvvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 157;BA.debugLine="UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSp";
_vvvvvvvvvvvvvvvvvvvv6();
 //BA.debugLineNum = 157;BA.debugLine="UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSp";
_vvvvvvvvvvvvvvvvvvvv7();
 //BA.debugLineNum = 157;BA.debugLine="UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSp";
_vvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 157;BA.debugLine="UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSp";
_vvvvvvvvvvvvvvvvvvvvv1();
 //BA.debugLineNum = 158;BA.debugLine="UpdateSpnTypeReseauPlafond:UpdateSpnEtatIsolantPL";
_vvvvvvvvvvvvvvvvvvvvv2();
 //BA.debugLineNum = 158;BA.debugLine="UpdateSpnTypeReseauPlafond:UpdateSpnEtatIsolantPL";
_vvvvvvvvvvvvvvvvvvvvv3();
 //BA.debugLineNum = 158;BA.debugLine="UpdateSpnTypeReseauPlafond:UpdateSpnEtatIsolantPL";
_vvvvvvvvvvvvvvvvvvvvv4();
 //BA.debugLineNum = 158;BA.debugLine="UpdateSpnTypeReseauPlafond:UpdateSpnEtatIsolantPL";
_vvvvvvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvv6();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvv7();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvvv1();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvvv2();
 //BA.debugLineNum = 160;BA.debugLine="UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:Upd";
_vvvvvvvvvvvvvvvvvvvvvv3();
 //BA.debugLineNum = 163;BA.debugLine="Dim position As Int";
_position = 0;
 //BA.debugLineNum = 164;BA.debugLine="position=Main.gPosition";
_position = mostCurrent._vvvvvvvvvvvvvvv5._vvvv5;
 //BA.debugLineNum = 165;BA.debugLine="If position=0 Then gotoLastRow Else gotoRowPositi";
if (_position==0) { 
_vvvvvvvvvvvvvvvvvvvvvv4();}
else {
_vvvvvvvvvvvvvvvvvvvvvv5(_position);};
 //BA.debugLineNum = 167;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
String _smeg = "";
 //BA.debugLineNum = 1666;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 1667;BA.debugLine="Dim sMeg As String";
_smeg = "";
 //BA.debugLineNum = 1668;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 1670;BA.debugLine="If Main.fCursor.getColumnValue(\"timestamp\")=Null";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("timestamp")== null || (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("timestamp")).equals((Object)(""))) { 
 //BA.debugLineNum = 1671;BA.debugLine="If Msgbox2(\"La fiche n'a pas été sauvegardée, v";
if (anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("La fiche n'a pas été sauvegardée, voulez-vous sortir quand même ?"),BA.ObjectToCharSequence(""),"OUI","","NON",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA)==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1673;BA.debugLine="Return False";
if (true) return anywheresoftware.b4a.keywords.Common.False;
 }else {
 //BA.debugLineNum = 1677;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 };
 };
 };
 //BA.debugLineNum = 1681;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 269;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 271;BA.debugLine="Try";
try { //BA.debugLineNum = 272;BA.debugLine="StateManager.SetSetting(\"CameraRotation\",Main.ma";
mostCurrent._vvvvvvvvvvvvvvv0._vvv5(mostCurrent.activityBA,"CameraRotation",mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("Rotation"));
 //BA.debugLineNum = 273;BA.debugLine="StateManager.SetSetting(\"SurveyName\",Main.manage";
mostCurrent._vvvvvvvvvvvvvvv0._vvv5(mostCurrent.activityBA,"SurveyName",mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("cp1"));
 //BA.debugLineNum = 274;BA.debugLine="StateManager.SaveSettings";
mostCurrent._vvvvvvvvvvvvvvv0._vvv3(mostCurrent.activityBA);
 } 
       catch (Exception e6) {
			processBA.setLastException(e6); //BA.debugLineNum = 276;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 279;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
String _rot = "";
float _degre = 0f;
anywheresoftware.b4a.agraham.jpegutils.ExifUtils _exif = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _bmp2 = null;
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 179;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 180;BA.debugLine="iscamera=True";
_vvvvvvvvvvvvvvvvvvv5 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 183;BA.debugLine="If expectedPic<>\"\" And File.Exists(CNSPath,expect";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvv7).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7)) { 
 //BA.debugLineNum = 185;BA.debugLine="Try";
try { //BA.debugLineNum = 186;BA.debugLine="Dim rot As String,degre As Float";
_rot = "";
_degre = 0f;
 //BA.debugLineNum = 187;BA.debugLine="rot=Main.manager.GetString(\"Rotation\")";
_rot = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("Rotation");
 //BA.debugLineNum = 188;BA.debugLine="Dim exif As ExifData";
_exif = new anywheresoftware.b4a.agraham.jpegutils.ExifUtils();
 //BA.debugLineNum = 189;BA.debugLine="exif.Initialize(CNSPath,expectedPic)";
_exif.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7);
 //BA.debugLineNum = 190;BA.debugLine="Select rot";
switch (BA.switchObjectToInt(_rot,"90","180","270")) {
case 0: {
 //BA.debugLineNum = 192;BA.debugLine="exif.setAttribute(exif.TAG_ORIENTATION,exif.O";
_exif.setAttribute(_exif.TAG_ORIENTATION,BA.NumberToString(_exif.ORIENTATION_ROTATE_90));
 break; }
case 1: {
 //BA.debugLineNum = 194;BA.debugLine="exif.setAttribute(exif.TAG_ORIENTATION,exif.O";
_exif.setAttribute(_exif.TAG_ORIENTATION,BA.NumberToString(_exif.ORIENTATION_ROTATE_180));
 break; }
case 2: {
 //BA.debugLineNum = 196;BA.debugLine="exif.setAttribute(exif.TAG_ORIENTATION,exif.O";
_exif.setAttribute(_exif.TAG_ORIENTATION,BA.NumberToString(_exif.ORIENTATION_ROTATE_270));
 break; }
}
;
 //BA.debugLineNum = 198;BA.debugLine="degre=rot";
_degre = (float)(Double.parseDouble(_rot));
 //BA.debugLineNum = 199;BA.debugLine="bmp1.Initialize(CNSPath,expectedPic)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7);
 //BA.debugLineNum = 200;BA.debugLine="Dim bmp2 As Bitmap = LoadbitmapRotated(CNSPath,";
_bmp2 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_bmp2 = _vvvvvvvvvvvvvvvvvvvvvv7(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7,_degre);
 //BA.debugLineNum = 201;BA.debugLine="Dim out As OutputStream";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 202;BA.debugLine="out=File.OpenOutput(CNSPath,expectedPic,False)";
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 203;BA.debugLine="bmp2.WriteToStream(out,80,\"JPEG\")";
_bmp2.WriteToStream((java.io.OutputStream)(_out.getObject()),(int) (80),BA.getEnumFromString(android.graphics.Bitmap.CompressFormat.class,"JPEG"));
 //BA.debugLineNum = 204;BA.debugLine="out.Close";
_out.Close();
 } 
       catch (Exception e24) {
			processBA.setLastException(e24); //BA.debugLineNum = 206;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 207;BA.debugLine="ToastMessageShow(\"Photo rotate error\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Photo rotate error"),anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 210;BA.debugLine="If curphotoName<>\"\" Then";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("") == false) { 
 //BA.debugLineNum = 211;BA.debugLine="Main.fCursor.SetCurrentRowValue(curphotoName,ex";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue(mostCurrent._vvvvvvvvvvvvvvvvvvvv4,(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvv7));
 //BA.debugLineNum = 212;BA.debugLine="Select curphotoName";
switch (BA.switchObjectToInt(mostCurrent._vvvvvvvvvvvvvvvvvvvv4,"photo1","photo2","photo3","photo4","photo5","photo6","photo7","photo8","photo1apres","photo2apres","photo3apres","photo4apres","photo5apres","photo6apres","photoSignature")) {
case 0: {
 //BA.debugLineNum = 214;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 1: {
 //BA.debugLineNum = 216;BA.debugLine="ImageView2.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 2: {
 //BA.debugLineNum = 218;BA.debugLine="ImageView3.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 3: {
 //BA.debugLineNum = 220;BA.debugLine="ImageView4.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 4: {
 //BA.debugLineNum = 222;BA.debugLine="ImageView5.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 5: {
 //BA.debugLineNum = 224;BA.debugLine="ImageView6.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 6: {
 //BA.debugLineNum = 226;BA.debugLine="ImageView7.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview7.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 7: {
 //BA.debugLineNum = 228;BA.debugLine="ImageView8.Bitmap=LoadBitmap(CNSPath, expecte";
mostCurrent._imageview8.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 8: {
 //BA.debugLineNum = 234;BA.debugLine="ImageViewp1.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 9: {
 //BA.debugLineNum = 236;BA.debugLine="ImageViewp2.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 10: {
 //BA.debugLineNum = 238;BA.debugLine="ImageViewp3.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 11: {
 //BA.debugLineNum = 240;BA.debugLine="ImageViewp4.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 12: {
 //BA.debugLineNum = 242;BA.debugLine="ImageViewp5.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 13: {
 //BA.debugLineNum = 244;BA.debugLine="ImageViewp6.Bitmap=LoadBitmap(CNSPath, expect";
mostCurrent._imageviewp6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
case 14: {
 //BA.debugLineNum = 254;BA.debugLine="ImageViewSignature.Bitmap=LoadBitmap(CNSPath,";
mostCurrent._imageviewsignature.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvvvvvv7).getObject()));
 break; }
}
;
 };
 //BA.debugLineNum = 261;BA.debugLine="If photoIdx=1 Then curPhoto=expectedPic Else cur";
if (_vvvvvvvvvvvvvvvvvvv0==1) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;}
else {
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 264;BA.debugLine="expectedPic=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 265;BA.debugLine="curphotoName=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "";
 };
 //BA.debugLineNum = 267;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvv2() throws Exception{
 //BA.debugLineNum = 440;BA.debugLine="Sub AddFiche";
 //BA.debugLineNum = 442;BA.debugLine="Main.fCursor.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 443;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 444;BA.debugLine="AddRecord";
_vvvvvvvvvvvvvvvvvvvvvvv3();
 //BA.debugLineNum = 445;BA.debugLine="spnArea.SelectedIndex=spnArea.IndexOf(curArea)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.setSelectedIndex(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.IndexOf(mostCurrent._vvvvvvvvvvvvvvvvvvvv3));
 //BA.debugLineNum = 446;BA.debugLine="Main.fCursor.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 447;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvv3() throws Exception{
String[] _colonnes = null;
String[] _coltypes = null;
int _i = 0;
String _colname = "";
String _curtype = "";
String _curname = "";
int _maxcol = 0;
Object[] _record = null;
 //BA.debugLineNum = 449;BA.debugLine="Sub AddRecord";
 //BA.debugLineNum = 451;BA.debugLine="Dim colonnes() As String,coltypes() As String,i A";
_colonnes = new String[(int) (0)];
java.util.Arrays.fill(_colonnes,"");
_coltypes = new String[(int) (0)];
java.util.Arrays.fill(_coltypes,"");
_i = 0;
_colname = "";
_curtype = "";
_curname = "";
_maxcol = 0;
 //BA.debugLineNum = 452;BA.debugLine="colonnes=Main.fTable.GetColumnNames";
_colonnes = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetColumnNames();
 //BA.debugLineNum = 453;BA.debugLine="coltypes=Main.fTable.GetColumnJavaTypes";
_coltypes = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetColumnJavaTypes();
 //BA.debugLineNum = 454;BA.debugLine="maxcol=colonnes.Length -1";
_maxcol = (int) (_colonnes.length-1);
 //BA.debugLineNum = 455;BA.debugLine="Dim Record(maxcol) As Object";
_record = new Object[_maxcol];
{
int d0 = _record.length;
for (int i0 = 0;i0 < d0;i0++) {
_record[i0] = new Object();
}
}
;
 //BA.debugLineNum = 456;BA.debugLine="For i=0 To colonnes.Length -1";
{
final int step6 = 1;
final int limit6 = (int) (_colonnes.length-1);
_i = (int) (0) ;
for (;(step6 > 0 && _i <= limit6) || (step6 < 0 && _i >= limit6) ;_i = ((int)(0 + _i + step6))  ) {
 //BA.debugLineNum = 457;BA.debugLine="curtype=coltypes(i)";
_curtype = _coltypes[_i];
 //BA.debugLineNum = 458;BA.debugLine="curname=colonnes(i)";
_curname = _colonnes[_i];
 //BA.debugLineNum = 459;BA.debugLine="Log(\"Colonne \" & i & \" - \" & colonnes(i) & \" [\"";
anywheresoftware.b4a.keywords.Common.Log("Colonne "+BA.NumberToString(_i)+" - "+_colonnes[_i]+" ["+_coltypes[_i]+"]");
 //BA.debugLineNum = 460;BA.debugLine="Select Case curtype";
switch (BA.switchObjectToInt(_curtype,"String","integer","double")) {
case 0: {
 //BA.debugLineNum = 462;BA.debugLine="Record(i)=\"\"";
_record[_i] = (Object)("");
 break; }
case 1: {
 //BA.debugLineNum = 464;BA.debugLine="Record(i)=0";
_record[_i] = (Object)(0);
 break; }
case 2: {
 //BA.debugLineNum = 466;BA.debugLine="Record(i)=0";
_record[_i] = (Object)(0);
 break; }
default: {
 //BA.debugLineNum = 468;BA.debugLine="Record(i)=Null";
_record[_i] = anywheresoftware.b4a.keywords.Common.Null;
 break; }
}
;
 }
};
 //BA.debugLineNum = 471;BA.debugLine="Record(0)=CampagneID";
_record[(int) (0)] = (Object)(_vvvvvvvvvvvvvvvvvvvv2);
 //BA.debugLineNum = 472;BA.debugLine="Main.fTable.AddRow(Record)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.AddRow(_record);
 //BA.debugLineNum = 474;BA.debugLine="End Sub";
return "";
}
public static String  _btn_next_click() throws Exception{
int _nextid = 0;
 //BA.debugLineNum = 343;BA.debugLine="Sub btn_next_Click";
 //BA.debugLineNum = 345;BA.debugLine="Dim NextID As Int";
_nextid = 0;
 //BA.debugLineNum = 346;BA.debugLine="Try";
try { //BA.debugLineNum = 348;BA.debugLine="SaveFiche";
_vvvvvvvvvvvvvvvvvvvvvvv5();
 } 
       catch (Exception e5) {
			processBA.setLastException(e5); //BA.debugLineNum = 350;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 352;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 353;BA.debugLine="curID=Main.fCursor.GetColumnValue(\"ID\")";
_vvvvvvvvvvvvvvvvvvvvvvv6 = (int)(BA.ObjectToNumber(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("ID")));
 //BA.debugLineNum = 354;BA.debugLine="NextID=curID+1";
_nextid = (int) (_vvvvvvvvvvvvvvvvvvvvvvv6+1);
 //BA.debugLineNum = 355;BA.debugLine="Main.fCursor.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 356;BA.debugLine="If Main.fCursor.IsAfterLast=True Then";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.IsAfterLast()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 357;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 358;BA.debugLine="curPhoto=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = "";
 //BA.debugLineNum = 359;BA.debugLine="curphoto2=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 360;BA.debugLine="AddFiche";
_vvvvvvvvvvvvvvvvvvvvvvv2();
 //BA.debugLineNum = 361;BA.debugLine="Main.fCursor.FindFirstRow(\"ID\",NextID)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.FindFirstRow("ID",(Object)(_nextid));
 //BA.debugLineNum = 362;BA.debugLine="IDMsg(\"NEXT NOFOUND\")";
_vvvvvvvvvvvvvvvvvvvvvvv7("NEXT NOFOUND");
 //BA.debugLineNum = 363;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, \"vide.jpg\"";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 //BA.debugLineNum = 364;BA.debugLine="ImageView1.BringToFront";
mostCurrent._imageview1.BringToFront();
 //BA.debugLineNum = 365;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 368;BA.debugLine="spnArea.SelectedIndex=spnArea.IndexOf(curArea)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.setSelectedIndex(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.IndexOf(mostCurrent._vvvvvvvvvvvvvvvvvvvv3));
 }else {
 //BA.debugLineNum = 370;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 371;BA.debugLine="IDMsg(\"NEXT FOUND\")";
_vvvvvvvvvvvvvvvvvvvvvvv7("NEXT FOUND");
 };
 //BA.debugLineNum = 373;BA.debugLine="End Sub";
return "";
}
public static String  _btn_next_longclick() throws Exception{
 //BA.debugLineNum = 1291;BA.debugLine="Sub btn_next_LongClick()";
 //BA.debugLineNum = 1292;BA.debugLine="gotoLastRow";
_vvvvvvvvvvvvvvvvvvvvvv4();
 //BA.debugLineNum = 1293;BA.debugLine="End Sub";
return "";
}
public static void  _btn_prev_click() throws Exception{
ResumableSub_btn_prev_Click rsub = new ResumableSub_btn_prev_Click(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_btn_prev_Click extends BA.ResumableSub {
public ResumableSub_btn_prev_Click(precarite.cns.detail parent) {
this.parent = parent;
}
precarite.cns.detail parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
try {

        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 399;BA.debugLine="Try";
if (true) break;

case 1:
//try
this.state = 6;
this.catchState = 5;
this.state = 3;
if (true) break;

case 3:
//C
this.state = 6;
this.catchState = 5;
 //BA.debugLineNum = 401;BA.debugLine="SaveFiche";
_vvvvvvvvvvvvvvvvvvvvvvv5();
 if (true) break;

case 5:
//C
this.state = 6;
this.catchState = 0;
 //BA.debugLineNum = 403;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 if (true) break;
if (true) break;
;
 //BA.debugLineNum = 406;BA.debugLine="Try";

case 6:
//try
this.state = 17;
this.catchState = 0;
this.catchState = 16;
this.state = 8;
if (true) break;

case 8:
//C
this.state = 9;
this.catchState = 16;
 //BA.debugLineNum = 407;BA.debugLine="expectedPic=\"\"";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 408;BA.debugLine="Main.fCursor.GetPreviousRow";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetPreviousRow();
 //BA.debugLineNum = 409;BA.debugLine="If Main.fCursor.IsBeforeFirst=False Then";
if (true) break;

case 9:
//if
this.state = 14;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.IsBeforeFirst()==anywheresoftware.b4a.keywords.Common.False) { 
this.state = 11;
}else {
this.state = 13;
}if (true) break;

case 11:
//C
this.state = 14;
 //BA.debugLineNum = 410;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 411;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 412;BA.debugLine="IDMsg(\"PREV\")";
_vvvvvvvvvvvvvvvvvvvvvvv7("PREV");
 if (true) break;

case 13:
//C
this.state = 14;
 //BA.debugLineNum = 415;BA.debugLine="Main.fCursor.GetNextRow";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 416;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 417;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 if (true) break;

case 14:
//C
this.state = 17;
;
 if (true) break;

case 16:
//C
this.state = 17;
this.catchState = 0;
 //BA.debugLineNum = 422;BA.debugLine="Sleep(0)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (0));
this.state = 18;
return;
case 18:
//C
this.state = 17;
;
 if (true) break;
if (true) break;

case 17:
//C
this.state = -1;
this.catchState = 0;
;
 //BA.debugLineNum = 424;BA.debugLine="End Sub";
if (true) break;
}} 
       catch (Exception e0) {
			
if (catchState == 0)
    throw e0;
else {
    state = catchState;
processBA.setLastException(e0);}
            }
        }
    }
}
public static void  _btn_prev_longclick() throws Exception{
ResumableSub_btn_prev_LongClick rsub = new ResumableSub_btn_prev_LongClick(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_btn_prev_LongClick extends BA.ResumableSub {
public ResumableSub_btn_prev_LongClick(precarite.cns.detail parent) {
this.parent = parent;
}
precarite.cns.detail parent;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
try {

        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 1271;BA.debugLine="Try";
if (true) break;

case 1:
//try
this.state = 12;
this.catchState = 11;
this.state = 3;
if (true) break;

case 3:
//C
this.state = 4;
this.catchState = 11;
 //BA.debugLineNum = 1272;BA.debugLine="expectedPic=\"\"";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 1273;BA.debugLine="Main.fCursor.Reset()";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Reset();
 //BA.debugLineNum = 1274;BA.debugLine="If Main.fCursor.IsBeforeFirst=False Then";
if (true) break;

case 4:
//if
this.state = 9;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.IsBeforeFirst()==anywheresoftware.b4a.keywords.Common.False) { 
this.state = 6;
}else {
this.state = 8;
}if (true) break;

case 6:
//C
this.state = 9;
 //BA.debugLineNum = 1275;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 1276;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1277;BA.debugLine="IDMsg(\"FIRST\")";
_vvvvvvvvvvvvvvvvvvvvvvv7("FIRST");
 if (true) break;

case 8:
//C
this.state = 9;
 //BA.debugLineNum = 1280;BA.debugLine="Main.fCursor.GetNextRow";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 1281;BA.debugLine="RAZ_fiche";
_raz_fiche();
 //BA.debugLineNum = 1282;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 if (true) break;

case 9:
//C
this.state = 12;
;
 if (true) break;

case 11:
//C
this.state = 12;
this.catchState = 0;
 //BA.debugLineNum = 1287;BA.debugLine="Sleep(0)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (0));
this.state = 13;
return;
case 13:
//C
this.state = 12;
;
 if (true) break;
if (true) break;

case 12:
//C
this.state = -1;
this.catchState = 0;
;
 //BA.debugLineNum = 1289;BA.debugLine="End Sub";
if (true) break;
}} 
       catch (Exception e0) {
			
if (catchState == 0)
    throw e0;
else {
    state = catchState;
processBA.setLastException(e0);}
            }
        }
    }
}
public static String  _btnaddarea_click() throws Exception{
anywheresoftware.b4a.agraham.dialogs.InputDialog _dlgarea = null;
String _selarea = "";
int _ret = 0;
 //BA.debugLineNum = 1248;BA.debugLine="Sub btnAddArea_Click";
 //BA.debugLineNum = 1249;BA.debugLine="Dim dlgArea As InputDialog,selArea As String, ret";
_dlgarea = new anywheresoftware.b4a.agraham.dialogs.InputDialog();
_selarea = "";
_ret = 0;
 //BA.debugLineNum = 1250;BA.debugLine="dlgArea.Hint = \"Entrez la zone\"";
_dlgarea.setHint("Entrez la zone");
 //BA.debugLineNum = 1251;BA.debugLine="dlgArea.HintColor = Colors.ARGB(196, 255, 140, 0)";
_dlgarea.setHintColor(anywheresoftware.b4a.keywords.Common.Colors.ARGB((int) (196),(int) (255),(int) (140),(int) (0)));
 //BA.debugLineNum = 1252;BA.debugLine="ret = DialogResponse.CANCEL";
_ret = anywheresoftware.b4a.keywords.Common.DialogResponse.CANCEL;
 //BA.debugLineNum = 1253;BA.debugLine="ret = dlgArea.Show(\"Entrez le nom de la zone\", \"R";
_ret = _dlgarea.Show("Entrez le nom de la zone","Relevé Matelas","Yes","No","Maybe",mostCurrent.activityBA,(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null));
 //BA.debugLineNum = 1254;BA.debugLine="selArea=dlgArea.Input";
_selarea = _dlgarea.getInput();
 //BA.debugLineNum = 1255;BA.debugLine="If selArea<>\"\" Then";
if ((_selarea).equals("") == false) { 
 //BA.debugLineNum = 1256;BA.debugLine="Main.fTableLieux.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.GetNextRow();
 //BA.debugLineNum = 1257;BA.debugLine="Main.fTableLieux.AddRow(Array As Object(selArea,";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.AddRow(new Object[]{(Object)(_selarea),(Object)(1)});
 //BA.debugLineNum = 1258;BA.debugLine="curArea=selArea";
mostCurrent._vvvvvvvvvvvvvvvvvvvv3 = _selarea;
 //BA.debugLineNum = 1259;BA.debugLine="UpdateSpnArea";
_vvvvvvvvvvvvvvvvvvvvvvvv1();
 //BA.debugLineNum = 1260;BA.debugLine="spnArea.SelectedIndex=spnArea.IndexOf(selArea)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.setSelectedIndex(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.IndexOf(_selarea));
 };
 //BA.debugLineNum = 1262;BA.debugLine="End Sub";
return "";
}
public static String  _btnaddmateriel_click() throws Exception{
anywheresoftware.b4a.agraham.dialogs.InputDialog _dlgmateriel = null;
String _selmateriel = "";
int _ret = 0;
 //BA.debugLineNum = 1295;BA.debugLine="Sub btnAddMateriel_Click";
 //BA.debugLineNum = 1296;BA.debugLine="Dim dlgMateriel As InputDialog,selMateriel As Str";
_dlgmateriel = new anywheresoftware.b4a.agraham.dialogs.InputDialog();
_selmateriel = "";
_ret = 0;
 //BA.debugLineNum = 1297;BA.debugLine="dlgMateriel.Hint = \"Equipement\"";
_dlgmateriel.setHint("Equipement");
 //BA.debugLineNum = 1298;BA.debugLine="dlgMateriel.HintColor = Colors.ARGB(196, 255, 140";
_dlgmateriel.setHintColor(anywheresoftware.b4a.keywords.Common.Colors.ARGB((int) (196),(int) (255),(int) (140),(int) (0)));
 //BA.debugLineNum = 1299;BA.debugLine="ret = dlgMateriel.Show(\"Entrez l'équipement\", \"Re";
_ret = _dlgmateriel.Show("Entrez l'équipement","Relevé Matelas","Yes","No","Maybe",mostCurrent.activityBA,(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null));
 //BA.debugLineNum = 1300;BA.debugLine="selMateriel=dlgMateriel.Input";
_selmateriel = _dlgmateriel.getInput();
 //BA.debugLineNum = 1301;BA.debugLine="If selMateriel<>\"\" Then";
if ((_selmateriel).equals("") == false) { 
 //BA.debugLineNum = 1302;BA.debugLine="Main.fTableMateriel.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv7.GetNextRow();
 //BA.debugLineNum = 1303;BA.debugLine="Main.fTableMateriel.AddRow(Array As Object(selMa";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv7.AddRow(new Object[]{(Object)(_selmateriel),(Object)(1)});
 //BA.debugLineNum = 1304;BA.debugLine="curMateriel=selMateriel";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvv2 = _selmateriel;
 //BA.debugLineNum = 1305;BA.debugLine="RefreshspnType";
_vvvvvvvvvvvvvvvvvvvvvvvv3();
 };
 //BA.debugLineNum = 1307;BA.debugLine="End Sub";
return "";
}
public static String  _btnp1_click() throws Exception{
 //BA.debugLineNum = 887;BA.debugLine="Sub btnP1_Click";
 //BA.debugLineNum = 888;BA.debugLine="InitP1";
_vvvvvvvvvvvvvvvvvvvvvvvv4();
 //BA.debugLineNum = 889;BA.debugLine="End Sub";
return "";
}
public static String  _btnp2_click() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 891;BA.debugLine="Sub btnP2_Click";
 //BA.debugLineNum = 892;BA.debugLine="photoIdx=2";
_vvvvvvvvvvvvvvvvvvv0 = (int) (2);
 //BA.debugLineNum = 893;BA.debugLine="Try";
try { //BA.debugLineNum = 894;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 895;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo2\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2"));
 //BA.debugLineNum = 896;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 898;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 899;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 901;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, \"vide.jpg";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e12) {
			processBA.setLastException(e12); //BA.debugLineNum = 904;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 906;BA.debugLine="End Sub";
return "";
}
public static String  _btnsaverec_click() throws Exception{
int _isok = 0;
boolean _bvalide = false;
 //BA.debugLineNum = 376;BA.debugLine="Sub btnSaverec_Click";
 //BA.debugLineNum = 377;BA.debugLine="Dim isOK As Int,bValide As Boolean";
_isok = 0;
_bvalide = false;
 //BA.debugLineNum = 379;BA.debugLine="If Main.TypeAppli.Contains(\"Comble\") Then";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvv6.contains("Comble")) { 
 //BA.debugLineNum = 380;BA.debugLine="isOK=ValideCBL(True)";
_isok = _vvvvvvvvvvvvvvvvvvvvvvvv5(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 381;BA.debugLine="If isOK=1 Or isOK=2 Then";
if (_isok==1 || _isok==2) { 
 //BA.debugLineNum = 382;BA.debugLine="If isOK=1 Then bValide=True Else bValide=False";
if (_isok==1) { 
_bvalide = anywheresoftware.b4a.keywords.Common.True;}
else {
_bvalide = anywheresoftware.b4a.keywords.Common.False;};
 //BA.debugLineNum = 383;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLValide\",bVa";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLValide",(Object)(_bvalide));
 //BA.debugLineNum = 384;BA.debugLine="SaveFiche";
_vvvvvvvvvvvvvvvvvvvvvvv5();
 };
 };
 //BA.debugLineNum = 387;BA.debugLine="If Main.TypeAppli.StartsWith(\"Plancher\") Then";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvv6.startsWith("Plancher")) { 
 //BA.debugLineNum = 388;BA.debugLine="isOK=ValidePLB(True)";
_isok = _vvvvvvvvvvvvvvvvvvvvvvvv6(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 389;BA.debugLine="If isOK=1 Or isOK=2 Then";
if (_isok==1 || _isok==2) { 
 //BA.debugLineNum = 390;BA.debugLine="If isOK=1 Then bValide=True Else bValide=False";
if (_isok==1) { 
_bvalide = anywheresoftware.b4a.keywords.Common.True;}
else {
_bvalide = anywheresoftware.b4a.keywords.Common.False;};
 //BA.debugLineNum = 391;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBValide\",bVa";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBValide",(Object)(_bvalide));
 //BA.debugLineNum = 392;BA.debugLine="SaveFiche";
_vvvvvvvvvvvvvvvvvvvvvvv5();
 };
 };
 //BA.debugLineNum = 395;BA.debugLine="End Sub";
return "";
}
public static String  _btnsign_click() throws Exception{
String _filepic = "";
String _sid = "";
 //BA.debugLineNum = 1696;BA.debugLine="Sub btnSign_Click";
 //BA.debugLineNum = 1698;BA.debugLine="Dim filepic As String,sID As String";
_filepic = "";
_sid = "";
 //BA.debugLineNum = 1699;BA.debugLine="sID=Main.fCursor.getColumnValue(\"ID\")";
_sid = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("ID"));
 //BA.debugLineNum = 1700;BA.debugLine="filepic=\"Signature_\" & sID & getPhotoPrefix & \".j";
_filepic = "Signature_"+_sid+_vvvvvvvvvvvvvvvvvvvvvvvv7()+".jpg";
 //BA.debugLineNum = 1701;BA.debugLine="gPic=filepic";
_v7 = _filepic;
 //BA.debugLineNum = 1702;BA.debugLine="expectedPic=filepic";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = _filepic;
 //BA.debugLineNum = 1703;BA.debugLine="curphotoName=\"photoSignature\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photoSignature";
 //BA.debugLineNum = 1704;BA.debugLine="StartActivity(\"SignatureScreen\")";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)("SignatureScreen"));
 //BA.debugLineNum = 1705;BA.debugLine="End Sub";
return "";
}
public static String  _btntakepicture_click() throws Exception{
 //BA.debugLineNum = 321;BA.debugLine="Sub btnTakePicture_Click";
 //BA.debugLineNum = 322;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 323;BA.debugLine="End Sub";
return "";
}
public static String  _camera1_picturetaken(byte[] _data) throws Exception{
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
String _filepic = "";
String _filepicf = "";
String _dt = "";
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _bitmap_source = null;
com.rootsoft.bitmaplibrary.BitmapLibrary _bitmap_travail = null;
 //BA.debugLineNum = 281;BA.debugLine="Sub Camera1_PictureTaken (Data() As Byte)";
 //BA.debugLineNum = 283;BA.debugLine="Dim out As OutputStream,filePic As String,filePic";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
_filepic = "";
_filepicf = "";
 //BA.debugLineNum = 284;BA.debugLine="Dim dt As String";
_dt = "";
 //BA.debugLineNum = 285;BA.debugLine="DateTime.DateFormat = \"yyMMddHHmmss\" ' See this p";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyMMddHHmmss");
 //BA.debugLineNum = 286;BA.debugLine="dt = DateTime.Date(DateTime.Now)";
_dt = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 289;BA.debugLine="filePicF=\"preca_\" & dt & \"F.jpg\"";
_filepicf = "preca_"+_dt+"F.jpg";
 //BA.debugLineNum = 290;BA.debugLine="filePic=\"preca_\" & dt & \".jpg\"";
_filepic = "preca_"+_dt+".jpg";
 //BA.debugLineNum = 291;BA.debugLine="out = File.OpenOutput(CNSPath, filePicF, False)";
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(mostCurrent._vvvvvvvvvvvvvvvvv1,_filepicf,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 292;BA.debugLine="out.WriteBytes(Data, 0, Data.Length)";
_out.WriteBytes(_data,(int) (0),_data.length);
 //BA.debugLineNum = 293;BA.debugLine="out.Close";
_out.Close();
 //BA.debugLineNum = 295;BA.debugLine="btnTakePicture.Enabled = True";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv1.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 298;BA.debugLine="Dim BitMap_Source As Bitmap";
_bitmap_source = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 299;BA.debugLine="Dim BitMap_Travail As BitmapExtended";
_bitmap_travail = new com.rootsoft.bitmaplibrary.BitmapLibrary();
 //BA.debugLineNum = 300;BA.debugLine="out = File.OpenOutput (CNSPath, filePic, False)";
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(mostCurrent._vvvvvvvvvvvvvvvvv1,_filepic,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 301;BA.debugLine="BitMap_Source = LoadBitmap(CNSPath,filePicF)";
_bitmap_source = anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_filepicf);
 //BA.debugLineNum = 305;BA.debugLine="BitMap_Source.WriteToStream(out, 75, \"JPEG\")";
_bitmap_source.WriteToStream((java.io.OutputStream)(_out.getObject()),(int) (75),BA.getEnumFromString(android.graphics.Bitmap.CompressFormat.class,"JPEG"));
 //BA.debugLineNum = 306;BA.debugLine="out.Close";
_out.Close();
 //BA.debugLineNum = 310;BA.debugLine="If photoIdx=1 Then curPhoto=filePic";
if (_vvvvvvvvvvvvvvvvvvv0==1) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = _filepic;};
 //BA.debugLineNum = 311;BA.debugLine="If photoIdx=2 Then curphoto2=filePic";
if (_vvvvvvvvvvvvvvvvvvv0==2) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = _filepic;};
 //BA.debugLineNum = 312;BA.debugLine="Try";
try { //BA.debugLineNum = 313;BA.debugLine="SaveFiche";
_vvvvvvvvvvvvvvvvvvvvvvv5();
 } 
       catch (Exception e22) {
			processBA.setLastException(e22); //BA.debugLineNum = 315;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 318;BA.debugLine="ImageView1.Bitmap=LoadbitmapSample2(CNSPath, file";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(_vvvvvvvvvvvvvvvvvvvvvvvvv2(mostCurrent._vvvvvvvvvvvvvvvvv1,_filepic,anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.True).getObject()));
 //BA.debugLineNum = 319;BA.debugLine="End Sub";
return "";
}
public static String  _camera1_ready(boolean _success) throws Exception{
 //BA.debugLineNum = 169;BA.debugLine="Sub Camera1_Ready (Success As Boolean)";
 //BA.debugLineNum = 170;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 171;BA.debugLine="camera1.StartPreview";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv3._vvvvvvvvvvvvv4();
 }else {
 //BA.debugLineNum = 175;BA.debugLine="ToastMessageShow(\"Cannot open camera.\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Cannot open camera."),anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 177;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvv0() throws Exception{
String _sphoto = "";
Object _buf = null;
String _saddr1 = "";
String _saddr2 = "";
String _sphoto2 = "";
 //BA.debugLineNum = 658;BA.debugLine="Sub displayFiche";
 //BA.debugLineNum = 659;BA.debugLine="Dim sPhoto As String";
_sphoto = "";
 //BA.debugLineNum = 660;BA.debugLine="Try";
try { //BA.debugLineNum = 662;BA.debugLine="Dim buf As Object,sAddr1 As String,sAddr2 As Str";
_buf = new Object();
_saddr1 = "";
_saddr2 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 668;BA.debugLine="curphoto1=\"\":curphoto2=\"\":curphoto3=\"\":curphoto4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
 //BA.debugLineNum = 669;BA.debugLine="curphotop1=\"\":curphotop2=\"\":curphotop3=\"\":curpho";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
 //BA.debugLineNum = 673;BA.debugLine="If Main.fCursor.GetColumnValue(\"Commentaires\")<>";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("Commentaires")!= null) { 
mostCurrent._commentaire.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("Commentaires")));}
else {
mostCurrent._commentaire.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 674;BA.debugLine="If Main.fCursor.GetColumnValue(\"commentaires_apr";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("commentaires_apres")!= null) { 
mostCurrent._txtcommentapres.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("commentaires_apres")));}
else {
mostCurrent._txtcommentapres.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 678;BA.debugLine="sAddr1=Chr(0xF015) & \" \":sAddr2=\"\"";
_saddr1 = BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf015)))+" ";
 //BA.debugLineNum = 678;BA.debugLine="sAddr1=Chr(0xF015) & \" \":sAddr2=\"\"";
_saddr2 = "";
 //BA.debugLineNum = 679;BA.debugLine="If Main.fCursor.GetColumnValue(\"addrChantier1\")<";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier1")!= null) { 
 //BA.debugLineNum = 680;BA.debugLine="txtAddrCT1.Text=Main.fCursor.GetColumnValue(\"ad";
mostCurrent._txtaddrct1.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier1")));
 //BA.debugLineNum = 681;BA.debugLine="sAddr1=sAddr1 & Main.fCursor.GetColumnValue(\"ad";
_saddr1 = _saddr1+BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier1"));
 }else {
 //BA.debugLineNum = 683;BA.debugLine="txtAddrCT1.Text=\"\"";
mostCurrent._txtaddrct1.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 685;BA.debugLine="If Main.fCursor.GetColumnValue(\"addrChantier2\")<";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier2")!= null) { 
 //BA.debugLineNum = 686;BA.debugLine="txtAddrCT2.Text=Main.fCursor.GetColumnValue(\"ad";
mostCurrent._txtaddrct2.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier2")));
 //BA.debugLineNum = 687;BA.debugLine="sAddr1=sAddr1 & \" \" & Main.fCursor.GetColumnVal";
_saddr1 = _saddr1+" "+BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantier2"));
 }else {
 //BA.debugLineNum = 689;BA.debugLine="txtAddrCT2.Text=\"\"";
mostCurrent._txtaddrct2.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 691;BA.debugLine="If Main.fCursor.GetColumnValue(\"addrChantierCP\")";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierCP")!= null) { 
 //BA.debugLineNum = 692;BA.debugLine="txtAddrCTCP.Text=Main.fCursor.GetColumnValue(\"a";
mostCurrent._txtaddrctcp.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierCP")));
 //BA.debugLineNum = 693;BA.debugLine="sAddr2=sAddr2 & Main.fCursor.GetColumnValue(\"ad";
_saddr2 = _saddr2+BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierCP"));
 }else {
 //BA.debugLineNum = 695;BA.debugLine="txtAddrCTCP.Text=\"\"";
mostCurrent._txtaddrctcp.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 697;BA.debugLine="If Main.fCursor.GetColumnValue(\"addrChantierVill";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierVille")!= null) { 
 //BA.debugLineNum = 698;BA.debugLine="txtAddrCTVille.Text=Main.fCursor.GetColumnValue";
mostCurrent._txtaddrctville.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierVille")));
 //BA.debugLineNum = 699;BA.debugLine="sAddr2=sAddr2 & \" \" & Main.fCursor.GetColumnVal";
_saddr2 = _saddr2+" "+BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("addrChantierVille"));
 }else {
 //BA.debugLineNum = 701;BA.debugLine="txtAddrCTVille.Text=\"\"";
mostCurrent._txtaddrctville.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 703;BA.debugLine="lblcAddr1.Text=sAddr1";
mostCurrent._lblcaddr1.setText(BA.ObjectToCharSequence(_saddr1));
 //BA.debugLineNum = 704;BA.debugLine="lblcAddr2.Text=sAddr2";
mostCurrent._lblcaddr2.setText(BA.ObjectToCharSequence(_saddr2));
 //BA.debugLineNum = 706;BA.debugLine="If Main.fCursor.GetColumnValue(\"NomChantier\")<>N";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("NomChantier")!= null) { 
 //BA.debugLineNum = 708;BA.debugLine="txtNom.Text=Main.fCursor.GetColumnValue(\"NomCha";
mostCurrent._txtnom.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("NomChantier")));
 }else {
 //BA.debugLineNum = 712;BA.debugLine="txtNom.Text=\"\"";
mostCurrent._txtnom.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 714;BA.debugLine="If Main.fCursor.GetColumnValue(\"PrenomChantier\")";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PrenomChantier")!= null) { 
 //BA.debugLineNum = 716;BA.debugLine="txtPrenom.Text=Main.fCursor.GetColumnValue(\"Pre";
mostCurrent._txtprenom.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PrenomChantier")));
 }else {
 //BA.debugLineNum = 720;BA.debugLine="txtPrenom.Text=\"\"";
mostCurrent._txtprenom.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 722;BA.debugLine="If Main.fCursor.GetColumnValue(\"TelChantier\")<>N";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("TelChantier")!= null) { 
 //BA.debugLineNum = 724;BA.debugLine="txtTel1.Text=Main.fCursor.GetColumnValue(\"TelCh";
mostCurrent._txttel1.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("TelChantier")));
 }else {
 //BA.debugLineNum = 728;BA.debugLine="txtTel1.Text=\"\"";
mostCurrent._txttel1.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 730;BA.debugLine="If Main.fCursor.GetColumnValue(\"Tel2Chantier\")<>";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("Tel2Chantier")!= null) { 
 //BA.debugLineNum = 732;BA.debugLine="txtTel2.Text=Main.fCursor.GetColumnValue(\"Tel2C";
mostCurrent._txttel2.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("Tel2Chantier")));
 }else {
 //BA.debugLineNum = 736;BA.debugLine="txtTel2.Text=\"\"";
mostCurrent._txttel2.setText(BA.ObjectToCharSequence(""));
 };
 //BA.debugLineNum = 738;BA.debugLine="If Main.fCursor.GetColumnValue(\"EDLRefusLocatair";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLRefusLocataire")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLRefusLocataire")).equals((Object)(0)) == false) { 
mostCurrent._cbrefusresident.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbrefusresident.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 739;BA.debugLine="If Main.fCursor.GetColumnValue(\"EDLAvisPassage1\"";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLAvisPassage1")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLAvisPassage1")).equals((Object)(0)) == false) { 
mostCurrent._cbavis1.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbavis1.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 740;BA.debugLine="If Main.fCursor.GetColumnValue(\"EDLAvisPassage2\"";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLAvisPassage2")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("EDLAvisPassage2")).equals((Object)(0)) == false) { 
mostCurrent._cbavis2.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbavis2.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 745;BA.debugLine="spnTypeLogement.SelectedIndex=spnTypeLogement.In";
mostCurrent._spntypelogement.setSelectedIndex(mostCurrent._spntypelogement.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLTypeLogement"))));
 //BA.debugLineNum = 746;BA.debugLine="spnTypeToit.SelectedIndex=spnTypeToit.IndexOf(Ma";
mostCurrent._spntypetoit.setSelectedIndex(mostCurrent._spntypetoit.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLTypeToit"))));
 //BA.debugLineNum = 747;BA.debugLine="spnNatureIsolant.SelectedIndex=spnNatureIsolant.";
mostCurrent._spnnatureisolant.setSelectedIndex(mostCurrent._spnnatureisolant.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLNatureIsolantComble"))));
 //BA.debugLineNum = 748;BA.debugLine="spnMateriauPrevu.SelectedIndex=spnMateriauPrevu.";
mostCurrent._spnmateriauprevu.setSelectedIndex(mostCurrent._spnmateriauprevu.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLTypeMateriauPrevu"))));
 //BA.debugLineNum = 749;BA.debugLine="spnEtatPlafond.SelectedIndex=spnEtatPlafond.Inde";
mostCurrent._spnetatplafond.setSelectedIndex(mostCurrent._spnetatplafond.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLTypeEtatPlafond"))));
 //BA.debugLineNum = 750;BA.debugLine="spnTypePlancherCBL.SelectedIndex=spnTypePlancher";
mostCurrent._spntypeplanchercbl.setSelectedIndex(mostCurrent._spntypeplanchercbl.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLTypePlancher"))));
 //BA.debugLineNum = 752;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLhtsousGouttie";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLhtsousGouttiere")!= null) { 
mostCurrent._txthsousgoutiere.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLhtsousGouttiere")));}
else {
mostCurrent._txthsousgoutiere.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 753;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLnbNiveaux\")<>";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLnbNiveaux")!= null) { 
mostCurrent._txtnbniveaux.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLnbNiveaux")));}
else {
mostCurrent._txtnbniveaux.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 754;BA.debugLine="buf=Main.fCursor.GetColumnValue(\"CBLsurfaceCombl";
_buf = mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLsurfaceComble");
 //BA.debugLineNum = 755;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLsurfaceComble";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLsurfaceComble")!= null) { 
mostCurrent._txtsurfcomble.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLsurfaceComble")));}
else {
mostCurrent._txtsurfcomble.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 756;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLsurfaceComble";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLsurfaceCombleReelle")!= null) { 
mostCurrent._txtsurfreelle.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLsurfaceCombleReelle")));}
else {
mostCurrent._txtsurfreelle.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 757;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLnbSacsUtilise";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLnbSacsUtilises")!= null) { 
mostCurrent._txtnbsacsutilises.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLnbSacsUtilises")));}
else {
mostCurrent._txtnbsacsutilises.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 760;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkCombleAmen";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkCombleAmenageable")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkCombleAmenageable")).equals((Object)(0)) == false) { 
mostCurrent._cbcombleamenag.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbcombleamenag.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 761;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkPareVapeur";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkPareVapeur")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkPareVapeur")).equals((Object)(0)) == false) { 
mostCurrent._cbparevapeur.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbparevapeur.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 762;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkTrappeComb";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkTrappeCombles")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkTrappeCombles")).equals((Object)(0)) == false) { 
mostCurrent._cbtrappecomble.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbtrappecomble.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 763;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkRehausseVM";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkRehausseVMC")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkRehausseVMC")).equals((Object)(0)) == false) { 
mostCurrent._cbrehaussevmc.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbrehaussevmc.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 764;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkRehausseTr";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkRehausseTrappe")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkRehausseTrappe")).equals((Object)(0)) == false) { 
mostCurrent._cbrehaussetrappe.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbrehaussetrappe.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 765;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkEvacIsolan";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkEvacIsolant")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkEvacIsolant")).equals((Object)(0)) == false) { 
mostCurrent._cbevacisolant.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbevacisolant.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 766;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkSpot\")<>Nu";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkSpot")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkSpot")).equals((Object)(0)) == false) { 
mostCurrent._cbspot.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbspot.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 767;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkConduitChe";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkConduitCheminee")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkConduitCheminee")).equals((Object)(0)) == false) { 
mostCurrent._cbconduitcheminee.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbconduitcheminee.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 768;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkEcartAuFeu";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkEcartAuFeu")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkEcartAuFeu")).equals((Object)(0)) == false) { 
mostCurrent._cbecartfeu.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbecartfeu.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 769;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLchkComblesEnc";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkComblesEncombres")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLchkComblesEncombres")).equals((Object)(0)) == false) { 
mostCurrent._cbcombleencombre.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbcombleencombre.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 770;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLProgrammeMAGE";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLProgrammeMAGE")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLProgrammeMAGE")).equals((Object)(0)) == false) { 
mostCurrent._cbprgmage.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbprgmage.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 771;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLConnexionInte";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLConnexionInternet")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLConnexionInternet")).equals((Object)(0)) == false) { 
mostCurrent._cbcnxinternet.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbcnxinternet.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 772;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLIsolable\")<>N";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLIsolable")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLIsolable")).equals((Object)(0)) == false) { 
mostCurrent._cbisolable.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbisolable.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 773;BA.debugLine="If Main.fCursor.GetColumnValue(\"CBLPresenceVMC\")";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLPresenceVMC")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("CBLPresenceVMC")).equals((Object)(0)) == false) { 
mostCurrent._cbpresencevmc.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbpresencevmc.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 777;BA.debugLine="spnTypePorte.SelectedIndex=spnTypePorte.IndexOf(";
mostCurrent._spntypeporte.setSelectedIndex(mostCurrent._spntypeporte.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBTypePorte"))));
 //BA.debugLineNum = 778;BA.debugLine="spnTypePlancher.SelectedIndex=spnTypePlancher.In";
mostCurrent._spntypeplancher.setSelectedIndex(mostCurrent._spntypeplancher.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBTypePlancher"))));
 //BA.debugLineNum = 779;BA.debugLine="spnTypeCloison.SelectedIndex=spnTypeCloison.Inde";
mostCurrent._spntypecloison.setSelectedIndex(mostCurrent._spntypecloison.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBTypeCaveCloisonnement"))));
 //BA.debugLineNum = 780;BA.debugLine="spnTypeLocal.SelectedIndex=spnTypeLocal.IndexOf(";
mostCurrent._spntypelocal.setSelectedIndex(mostCurrent._spntypelocal.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBtypeLocal"))));
 //BA.debugLineNum = 781;BA.debugLine="spnTypeIsolant.SelectedIndex=spnTypeIsolant.Inde";
mostCurrent._spntypeisolant.setSelectedIndex(mostCurrent._spntypeisolant.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBNatureIsolant"))));
 //BA.debugLineNum = 782;BA.debugLine="spnEtatIsolantPLB.SelectedIndex=spnEtatIsolantPL";
mostCurrent._spnetatisolantplb.setSelectedIndex(mostCurrent._spnetatisolantplb.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBEtatIsolant"))));
 //BA.debugLineNum = 783;BA.debugLine="spnTypeReseauPlafond.SelectedIndex=spnTypeReseau";
mostCurrent._spntypereseauplafond.setSelectedIndex(mostCurrent._spntypereseauplafond.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBTypeReseauPlafond"))));
 //BA.debugLineNum = 785;BA.debugLine="spnAccesPCommunes.SelectedIndex=spnAccesPCommune";
mostCurrent._spnaccespcommunes.setSelectedIndex(mostCurrent._spnaccespcommunes.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBAccessPCommunes"))));
 //BA.debugLineNum = 787;BA.debugLine="spnResistanceIsolant.SelectedIndex=spnResistance";
mostCurrent._spnresistanceisolant.setSelectedIndex(mostCurrent._spnresistanceisolant.IndexOf(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBResistanceIsolant"))));
 //BA.debugLineNum = 790;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBHSPSansIsolan";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPSansIsolant")!= null) { 
mostCurrent._txthspsansisolant.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPSansIsolant")));}
else {
mostCurrent._txthspsansisolant.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 791;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBHSPAvecIsolan";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPAvecIsolant")!= null) { 
mostCurrent._txthspavecisolant.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPAvecIsolant")));}
else {
mostCurrent._txthspavecisolant.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 792;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBnbBatiment\")<";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbBatiment")!= null) { 
mostCurrent._txtnbbat.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbBatiment")));}
else {
mostCurrent._txtnbbat.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 793;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBnbLogementpar";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbLogementparBatiment")!= null) { 
mostCurrent._txtnbbatimm.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbLogementparBatiment")));}
else {
mostCurrent._txtnbbatimm.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 794;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBnbCageEscalie";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbCageEscalier")!= null) { 
mostCurrent._txtnbcageesc.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBnbCageEscalier")));}
else {
mostCurrent._txtnbcageesc.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 796;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBHSPPorteIsola";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPPorteIsolant")!= null) { 
mostCurrent._txthspporteisolant.setText(BA.ObjectToCharSequence(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBHSPPorteIsolant")));}
else {
mostCurrent._txthspporteisolant.setText(BA.ObjectToCharSequence(""));};
 //BA.debugLineNum = 800;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBbatCollectif\"";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBbatCollectif")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBbatCollectif")).equals((Object)(0)) == false) { 
mostCurrent._cbcollectif.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbcollectif.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 801;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBEnlevementNeo";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBEnlevementNeon")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBEnlevementNeon")).equals((Object)(0)) == false) { 
mostCurrent._cbenlevementneon.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbenlevementneon.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 802;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBPointEauProch";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBPointEauProche")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBPointEauProche")).equals((Object)(0)) == false) { 
mostCurrent._cbpointeauproche.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbpointeauproche.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 803;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBAltLuminaires";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBAltLuminaires")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBAltLuminaires")).equals((Object)(0)) == false) { 
mostCurrent._cbaltluminaires.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbaltluminaires.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 804;BA.debugLine="If Main.fCursor.GetColumnValue(\"PLBCaveEncombree";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBCaveEncombree")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("PLBCaveEncombree")).equals((Object)(0)) == false) { 
mostCurrent._cbcaveencombree.setChecked(anywheresoftware.b4a.keywords.Common.True);}
else {
mostCurrent._cbcaveencombree.setChecked(anywheresoftware.b4a.keywords.Common.False);};
 //BA.debugLineNum = 809;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo1\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo1"));
 //BA.debugLineNum = 810;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView1";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 810;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView1";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4 = _sphoto;
 //BA.debugLineNum = 811;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo2\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2"));
 //BA.debugLineNum = 812;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView2";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 812;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView2";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = _sphoto;
 //BA.debugLineNum = 813;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo3\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo3"));
 //BA.debugLineNum = 814;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView3";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 814;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView3";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5 = _sphoto;
 //BA.debugLineNum = 815;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo4\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo4"));
 //BA.debugLineNum = 816;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView4";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 816;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView4";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6 = _sphoto;
 //BA.debugLineNum = 817;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo5\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo5"));
 //BA.debugLineNum = 818;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView5";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 818;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView5";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7 = _sphoto;
 //BA.debugLineNum = 819;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo6\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo6"));
 //BA.debugLineNum = 820;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView6";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 820;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView6";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0 = _sphoto;
 //BA.debugLineNum = 821;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo7\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo7"));
 //BA.debugLineNum = 822;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView7";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview7.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 822;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView7";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7 = _sphoto;
 //BA.debugLineNum = 823;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo8\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo8"));
 //BA.debugLineNum = 824;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView8";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageview8.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 824;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageView8";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0 = _sphoto;
 //BA.debugLineNum = 829;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo1apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo1apres"));
 //BA.debugLineNum = 830;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 830;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1 = _sphoto;
 //BA.debugLineNum = 831;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo2apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2apres"));
 //BA.debugLineNum = 832;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 832;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2 = _sphoto;
 //BA.debugLineNum = 833;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo3apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo3apres"));
 //BA.debugLineNum = 834;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 834;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3 = _sphoto;
 //BA.debugLineNum = 835;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo4apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo4apres"));
 //BA.debugLineNum = 836;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 836;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4 = _sphoto;
 //BA.debugLineNum = 837;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo5apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo5apres"));
 //BA.debugLineNum = 838;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 838;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5 = _sphoto;
 //BA.debugLineNum = 839;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo6apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo6apres"));
 //BA.debugLineNum = 840;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewp6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 840;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewp";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6 = _sphoto;
 //BA.debugLineNum = 850;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photoSignatu";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photoSignature"));
 //BA.debugLineNum = 851;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewS";
if ((_sphoto).equals("null") == false && (_sphoto).equals("") == false) { 
mostCurrent._imageviewsignature.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));};
 //BA.debugLineNum = 851;BA.debugLine="If sPhoto<>\"null\" And sPhoto<>\"\" Then ImageViewS";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv1 = _sphoto;
 } 
       catch (Exception e161) {
			processBA.setLastException(e161); //BA.debugLineNum = 853;BA.debugLine="ToastMessageShow(\"Erreur affichage fiche\",False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Erreur affichage fiche"),anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 855;BA.debugLine="Try";
try { //BA.debugLineNum = 857;BA.debugLine="Log(\"DisplayFiche - Photo enter\")";
anywheresoftware.b4a.keywords.Common.Log("DisplayFiche - Photo enter");
 //BA.debugLineNum = 858;BA.debugLine="Dim sphoto2 As String,sPhoto As String";
_sphoto2 = "";
_sphoto = "";
 //BA.debugLineNum = 859;BA.debugLine="sPhoto=Main.fCursor.GetColumnValue(\"photo\" & pho";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo"+BA.NumberToString(_vvvvvvvvvvvvvvvvvvv0)));
 //BA.debugLineNum = 861;BA.debugLine="sphoto2=Main.fCursor.GetColumnValue(\"photo2\")";
_sphoto2 = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2"));
 //BA.debugLineNum = 863;BA.debugLine="If (File.Exists(CNSPath,sPhoto)) And sPhoto<>\"\"";
if ((anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) && (_sphoto).equals("") == false) { 
 //BA.debugLineNum = 864;BA.debugLine="Log(\"DisplayFiche - PhotoExists 1 enter\")";
anywheresoftware.b4a.keywords.Common.Log("DisplayFiche - PhotoExists 1 enter");
 //BA.debugLineNum = 865;BA.debugLine="curPhoto=sPhoto";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = _sphoto;
 //BA.debugLineNum = 866;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, sPhoto)";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 867;BA.debugLine="ImageView1.BringToFront";
mostCurrent._imageview1.BringToFront();
 //BA.debugLineNum = 868;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 870;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, \"vide.jpg";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 //BA.debugLineNum = 871;BA.debugLine="ImageView1.BringToFront";
mostCurrent._imageview1.BringToFront();
 //BA.debugLineNum = 872;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 874;BA.debugLine="If (File.Exists(CNSPath,sphoto2)) Then curphoto2";
if ((anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto2))) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = _sphoto2;};
 } 
       catch (Exception e181) {
			processBA.setLastException(e181); //BA.debugLineNum = 876;BA.debugLine="ToastMessageShow(\"Erreur affichage photo\",False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Erreur affichage photo"),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 877;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, \"vide.jpg\"";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 //BA.debugLineNum = 878;BA.debugLine="If iscamera Then";
if (_vvvvvvvvvvvvvvvvvvv5) { 
 //BA.debugLineNum = 879;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 };
 };
 //BA.debugLineNum = 883;BA.debugLine="InitP1";
_vvvvvvvvvvvvvvvvvvvvvvvv4();
 //BA.debugLineNum = 884;BA.debugLine="End Sub";
return "";
}
public static String  _ftp_uploadcompleted(String _serverpath,boolean _success) throws Exception{
 //BA.debugLineNum = 943;BA.debugLine="Sub ftp_UploadCompleted (ServerPath As String, Suc";
 //BA.debugLineNum = 944;BA.debugLine="Log(ServerPath & \", Success=\" & Success)";
anywheresoftware.b4a.keywords.Common.Log(_serverpath+", Success="+BA.ObjectToString(_success));
 //BA.debugLineNum = 945;BA.debugLine="If Success = False Then";
if (_success==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 946;BA.debugLine="Log(LastException.Message)";
anywheresoftware.b4a.keywords.Common.Log(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA).getMessage());
 //BA.debugLineNum = 947;BA.debugLine="ToastMessageShow(\"Envoi ZIP ECHEC : \" & LastExce";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Envoi ZIP ECHEC : "+anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA).getMessage()),anywheresoftware.b4a.keywords.Common.True);
 }else {
 //BA.debugLineNum = 949;BA.debugLine="ToastMessageShow(\"Envoi ZIP REUSSI\",True)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Envoi ZIP REUSSI"),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 951;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvvvvvvvvvvvv1(anywheresoftware.b4a.objects.TabStripViewPager _tabstrip) throws Exception{
anywheresoftware.b4j.object.JavaObject _jo = null;
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
anywheresoftware.b4a.objects.PanelWrapper _tc = null;
anywheresoftware.b4a.objects.collections.List _res = null;
anywheresoftware.b4a.objects.ConcreteViewWrapper _v = null;
 //BA.debugLineNum = 1343;BA.debugLine="Public Sub GetAllTabLabels (tabstrip As TabStrip)";
 //BA.debugLineNum = 1344;BA.debugLine="Dim jo As JavaObject = tabstrip";
_jo = new anywheresoftware.b4j.object.JavaObject();
_jo.setObject((java.lang.Object)(_tabstrip));
 //BA.debugLineNum = 1345;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 1346;BA.debugLine="r.Target = jo.GetField(\"tabStrip\")";
_r.Target = _jo.GetField("tabStrip");
 //BA.debugLineNum = 1347;BA.debugLine="Dim tc As Panel = r.GetField(\"tabsContainer\")";
_tc = new anywheresoftware.b4a.objects.PanelWrapper();
_tc.setObject((android.view.ViewGroup)(_r.GetField("tabsContainer")));
 //BA.debugLineNum = 1348;BA.debugLine="Dim res As List";
_res = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 1349;BA.debugLine="res.Initialize";
_res.Initialize();
 //BA.debugLineNum = 1350;BA.debugLine="For Each v As View In tc";
_v = new anywheresoftware.b4a.objects.ConcreteViewWrapper();
{
final anywheresoftware.b4a.BA.IterableList group7 = _tc;
final int groupLen7 = group7.getSize()
;int index7 = 0;
;
for (; index7 < groupLen7;index7++){
_v.setObject((android.view.View)(group7.Get(index7)));
 //BA.debugLineNum = 1351;BA.debugLine="If v Is Label Then res.Add(v)";
if (_v.getObjectOrNull() instanceof android.widget.TextView) { 
_res.Add((Object)(_v.getObject()));};
 }
};
 //BA.debugLineNum = 1353;BA.debugLine="Return res";
if (true) return _res;
 //BA.debugLineNum = 1355;BA.debugLine="End Sub";
return null;
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvv7() throws Exception{
String _result = "";
String _dt = "";
 //BA.debugLineNum = 2036;BA.debugLine="Sub getPhotoPrefix As String";
 //BA.debugLineNum = 2037;BA.debugLine="Dim result As String, dt As String";
_result = "";
_dt = "";
 //BA.debugLineNum = 2039;BA.debugLine="Try";
try { //BA.debugLineNum = 2040;BA.debugLine="DateTime.DateFormat = \"yyMMddHHmmss\" ' See this";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyMMddHHmmss");
 //BA.debugLineNum = 2041;BA.debugLine="dt = DateTime.Date(DateTime.Now)";
_dt = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 2042;BA.debugLine="result=\"_\" & Main.DeviceID & \"_\" & dt";
_result = "_"+mostCurrent._vvvvvvvvvvvvvvv5._vvvv7+"_"+_dt;
 //BA.debugLineNum = 2043;BA.debugLine="Return result";
if (true) return _result;
 } 
       catch (Exception e8) {
			processBA.setLastException(e8); //BA.debugLineNum = 2045;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 2046;BA.debugLine="Return \"\"";
if (true) return "";
 };
 //BA.debugLineNum = 2048;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvvvvv2() throws Exception{
String _dt = "";
 //BA.debugLineNum = 651;BA.debugLine="Sub getTimestamp As String";
 //BA.debugLineNum = 652;BA.debugLine="Dim dt As String";
_dt = "";
 //BA.debugLineNum = 653;BA.debugLine="DateTime.DateFormat = \"dd/MM/yyyy HH:mm:ss\" ' See";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("dd/MM/yyyy HH:mm:ss");
 //BA.debugLineNum = 654;BA.debugLine="dt = DateTime.Date(DateTime.Now)";
_dt = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 655;BA.debugLine="Return dt";
if (true) return _dt;
 //BA.debugLineNum = 656;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 12;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 13;BA.debugLine="Dim camera1 As CameraExClass,btnTakePicture As Ic";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv3 = new precarite.cns.cameraexclass();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv1 = new de.donmanfred.IconButtonWrapper();
_vvvvvvvvvvvvvvvvvvv0 = 0;
mostCurrent._vvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase();
_vvvvvvvvvvvvvvvvvvvvvvvvvvv3 = false;
_vvvvvvvvvvvvvvvvvvvv2 = 0;
_vvvvvvvvvvvvvvvvvvv5 = false;
_vvvvvvvvvvvvvvvvvvv6 = false;
 //BA.debugLineNum = 14;BA.debugLine="Dim Panel3 As Panel, bmp1 As Bitmap";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv6 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 15;BA.debugLine="Dim curPhoto As String,curID As Int,CNSPath As St";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = "";
_vvvvvvvvvvvvvvvvvvvvvvv6 = 0;
mostCurrent._vvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvv7 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvv2 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
 //BA.debugLineNum = 16;BA.debugLine="Dim curphoto2 As String,expectedPic As String,cur";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvv3 = "";
mostCurrent._vvvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0 = "";
 //BA.debugLineNum = 17;BA.debugLine="Dim curphotop1 As String,curphotop2 As String,cur";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 18;BA.debugLine="Private commentaire As EditText,btn_next As IconB";
mostCurrent._commentaire = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._btn_next = new de.donmanfred.IconButtonWrapper();
mostCurrent._btn_prev = new de.donmanfred.IconButtonWrapper();
mostCurrent._btnsave = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._btnsaverec = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private ImageView1 As ImageView, imgFullScreen As";
mostCurrent._imageview1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv6 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._cbaccespcommunes = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private ImageView2 As ImageView,ImageView3 As Ima";
mostCurrent._imageview2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageview3 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageview4 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageview5 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageview6 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private spnArea As Spinner,btnAddArea As Button,b";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv7 = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvv0 = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._options2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvv3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblplancher = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lbllocal = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblisolant = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private spnTypePorte As Spinner, spnTypePlancher";
mostCurrent._spntypeporte = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spntypeplancher = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spntypecloison = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spntypelocal = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spntypeisolant = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._lblcbacces = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Private lblHSPAvec As Label,lblcbCollectif As Lab";
mostCurrent._lblhspavec = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcbcollectif = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._cbcollectif = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._txthspavecisolant = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txthspsansisolant = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private lblHSPsans As Label,lblNbBat As Label, tx";
mostCurrent._lblhspsans = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblnbbat = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._txtnbbat = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._lblnbbatimm = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._txtnbbatimm = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtaddrct1 = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtaddrct2 = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private txtAddrCTCP As EditText, txtAddrCTVille A";
mostCurrent._txtaddrctcp = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtaddrctville = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtnbcageesc = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._lblnbcageesc = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._tspreca = new anywheresoftware.b4a.objects.TabStripViewPager();
mostCurrent._spntypetoit = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Private spnTypeLogement As Spinner,spnTypeIsolant";
mostCurrent._spntypelogement = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spnnatureisolant = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spnmateriauprevu = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spnetatplafond = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 27;BA.debugLine="Private txtHsousGoutiere As EditText, txtSurfComb";
mostCurrent._txthsousgoutiere = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtsurfcomble = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtnbniveaux = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._txtsurfreelle = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._cbcombleamenag = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbparevapeur = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Private cbTrappeComble As CheckBox, cbRehausseVMC";
mostCurrent._cbtrappecomble = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbrehaussevmc = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbrehaussetrappe = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbevacisolant = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbspot = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbconduitcheminee = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private cbEcartFeu As CheckBox, cbCombleEncombre";
mostCurrent._cbecartfeu = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbcombleencombre = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._spntypeplanchercbl = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spntypereseauplafond = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spnetatisolantplb = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Private Label1 As Label, Label2 As Label, Label3";
mostCurrent._label1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv6 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Private ImageViewp1 As ImageView, ImageViewp2 As";
mostCurrent._imageviewp1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageviewp2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageviewp3 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageviewp4 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageviewp5 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageviewp6 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Private btnSaverecPBL As IconButton,btnSign As Bu";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = new de.donmanfred.IconButtonWrapper();
mostCurrent._btnsign = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._imageviewsignature = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._txtnbsacsutilises = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._imvcnslogo1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._cbprgmage = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 33;BA.debugLine="Private cbcnxInternet As CheckBox, lblcAddr1 As L";
mostCurrent._cbcnxinternet = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._lblcaddr1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcaddr2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv0 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._imvfs = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imvfsapres = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 34;BA.debugLine="Private lblP1apres As Label, lblP4apres As Label,";
mostCurrent._lblp1apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp4apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp2apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp5apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp3apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp6apres = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Private lblP3 As Label, lblP4 As Label, lblP5 As";
mostCurrent._lblp3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblp6 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._cbisolable = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbpresencevmc = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._lblp7 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._imageview7 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private lblP8 As Label, ImageView8 As ImageView,";
mostCurrent._lblp8 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._imageview8 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._txtcommentapres = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._spnresistanceisolant = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._spnaccespcommunes = new anywheresoftware.b4a.objects.SpinnerWrapper();
mostCurrent._txthspporteisolant = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private cbEnlevementNeon As CheckBox, cbPointEauP";
mostCurrent._cbenlevementneon = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbpointeauproche = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbaltluminaires = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbcaveencombree = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbavis1 = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._cbavis2 = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private cbRefusResident As CheckBox, pnlCBLHide A";
mostCurrent._cbrefusresident = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
mostCurrent._pnlcblhide = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._pnlplbhide = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._lblcblhide = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private txtPrenom As EditText";
mostCurrent._txtprenom = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private txtNom As EditText";
mostCurrent._txtnom = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private txtTel1 As EditText";
mostCurrent._txttel1 = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private txtTel2 As EditText";
mostCurrent._txttel2 = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 43;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvv4() throws Exception{
int _nbrow = 0;
int _i = 0;
 //BA.debugLineNum = 908;BA.debugLine="Sub gotoLastRow";
 //BA.debugLineNum = 909;BA.debugLine="Dim nbrow As Int";
_nbrow = 0;
 //BA.debugLineNum = 910;BA.debugLine="nbrow=Main.fTable.GetRowCount";
_nbrow = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetRowCount();
 //BA.debugLineNum = 911;BA.debugLine="Log(\"STEP2\")";
anywheresoftware.b4a.keywords.Common.Log("STEP2");
 //BA.debugLineNum = 912;BA.debugLine="If nbrow>=1 Then";
if (_nbrow>=1) { 
 //BA.debugLineNum = 914;BA.debugLine="Main.fCursor.Reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Reset();
 //BA.debugLineNum = 915;BA.debugLine="For i=0 To nbrow -1";
{
final int step6 = 1;
final int limit6 = (int) (_nbrow-1);
_i = (int) (0) ;
for (;(step6 > 0 && _i <= limit6) || (step6 < 0 && _i >= limit6) ;_i = ((int)(0 + _i + step6))  ) {
 //BA.debugLineNum = 916;BA.debugLine="Main.fCursor.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 }
};
 //BA.debugLineNum = 918;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 };
 //BA.debugLineNum = 920;BA.debugLine="Log(\"STEP3\")";
anywheresoftware.b4a.keywords.Common.Log("STEP3");
 //BA.debugLineNum = 921;BA.debugLine="If nbrow<1 Then";
if (_nbrow<1) { 
 //BA.debugLineNum = 922;BA.debugLine="Main.fTable.addrow(Array As Object(1,1,1,2,\"\",\"\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.AddRow(new Object[]{(Object)(1),(Object)(1),(Object)(1),(Object)(2),(Object)(""),(Object)(""),(Object)(""),(Object)(""),(Object)(_vvvvvvvvvvvvvvvvvvvvvvvvvvv2())});
 //BA.debugLineNum = 924;BA.debugLine="Main.fCursor.Reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Reset();
 };
 //BA.debugLineNum = 926;BA.debugLine="Log(\"STEP4 Init end\")";
anywheresoftware.b4a.keywords.Common.Log("STEP4 Init end");
 //BA.debugLineNum = 928;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvv5(int _pos) throws Exception{
int _nbrow = 0;
int _i = 0;
 //BA.debugLineNum = 930;BA.debugLine="Sub gotoRowPosition(pos As Int)";
 //BA.debugLineNum = 931;BA.debugLine="Dim nbrow As Int";
_nbrow = 0;
 //BA.debugLineNum = 932;BA.debugLine="nbrow=Main.fTable.GetRowCount";
_nbrow = mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetRowCount();
 //BA.debugLineNum = 933;BA.debugLine="If pos>nbrow Then Return";
if (_pos>_nbrow) { 
if (true) return "";};
 //BA.debugLineNum = 934;BA.debugLine="If nbrow>=1 Then";
if (_nbrow>=1) { 
 //BA.debugLineNum = 935;BA.debugLine="Main.fCursor.Reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.Reset();
 //BA.debugLineNum = 936;BA.debugLine="For i=0 To pos-1";
{
final int step6 = 1;
final int limit6 = (int) (_pos-1);
_i = (int) (0) ;
for (;(step6 > 0 && _i <= limit6) || (step6 < 0 && _i >= limit6) ;_i = ((int)(0 + _i + step6))  ) {
 //BA.debugLineNum = 937;BA.debugLine="Main.fCursor.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetNextRow();
 }
};
 //BA.debugLineNum = 939;BA.debugLine="displayFiche";
_vvvvvvvvvvvvvvvvvvvvvvv0();
 };
 //BA.debugLineNum = 941;BA.debugLine="End Sub";
return "";
}
public static String  _gpsclient_locationchanged(anywheresoftware.b4a.gps.LocationWrapper _location1) throws Exception{
double _lat = 0;
double _lng = 0;
 //BA.debugLineNum = 1947;BA.debugLine="Sub gpsclient_LocationChanged(Location1 As Locatio";
 //BA.debugLineNum = 1948;BA.debugLine="Dim lat As Double,lng As Double";
_lat = 0;
_lng = 0;
 //BA.debugLineNum = 1949;BA.debugLine="userLocation=Location1";
_vv1 = _location1;
 //BA.debugLineNum = 1951;BA.debugLine="lat=userLocation.Latitude";
_lat = _vv1.getLatitude();
 //BA.debugLineNum = 1952;BA.debugLine="lng=userLocation.Longitude";
_lng = _vv1.getLongitude();
 //BA.debugLineNum = 1953;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrLat\",lat)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrLat",(Object)(_lat));
 //BA.debugLineNum = 1954;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrLng\",lng)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrLng",(Object)(_lng));
 //BA.debugLineNum = 1955;BA.debugLine="gpsClient.Stop";
_v0.Stop();
 //BA.debugLineNum = 1957;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvv7(String _act) throws Exception{
String _msg = "";
 //BA.debugLineNum = 426;BA.debugLine="Sub IDMsg(act As String)";
 //BA.debugLineNum = 427;BA.debugLine="Dim msg As String";
_msg = "";
 //BA.debugLineNum = 428;BA.debugLine="Try";
try { //BA.debugLineNum = 429;BA.debugLine="If Main.fCursor.IsAfterLast=False And Main.fCurs";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.IsAfterLast()==anywheresoftware.b4a.keywords.Common.False && mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.IsBeforeFirst()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 430;BA.debugLine="msg=Main.fCursor.GetColumnValue(\"ID\")";
_msg = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("ID"));
 }else {
 //BA.debugLineNum = 432;BA.debugLine="msg=\"#UNDEF#\"";
_msg = "#UNDEF#";
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 435;BA.debugLine="msg=\"ERROR\"";
_msg = "ERROR";
 };
 //BA.debugLineNum = 438;BA.debugLine="End Sub";
return "";
}
public static String  _imageview1_click() throws Exception{
 //BA.debugLineNum = 1357;BA.debugLine="Sub ImageView1_Click";
 //BA.debugLineNum = 1358;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1359;BA.debugLine="curphotoName=\"photo1\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo1";
 //BA.debugLineNum = 1360;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1362;BA.debugLine="If gpsClient.GPSEnabled=True Then gpsClient.Start";
if (_v0.getGPSEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
_v0.Start(processBA,(long) (0),(float) (0));};
 //BA.debugLineNum = 1363;BA.debugLine="End Sub";
return "";
}
public static String  _imageview1_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1323;BA.debugLine="Sub ImageView1_LongClick";
 //BA.debugLineNum = 1325;BA.debugLine="Try";
try { //BA.debugLineNum = 1326;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1327;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo1\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo1"));
 //BA.debugLineNum = 1328;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1330;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1331;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1332;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1335;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1337;BA.debugLine="End Sub";
return "";
}
public static String  _imageview2_click() throws Exception{
 //BA.debugLineNum = 1453;BA.debugLine="Sub ImageView2_Click";
 //BA.debugLineNum = 1454;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1455;BA.debugLine="curphotoName=\"photo2\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo2";
 //BA.debugLineNum = 1456;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1457;BA.debugLine="End Sub";
return "";
}
public static String  _imageview2_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1459;BA.debugLine="Sub ImageView2_LongClick";
 //BA.debugLineNum = 1461;BA.debugLine="Try";
try { //BA.debugLineNum = 1462;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1463;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo2\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2"));
 //BA.debugLineNum = 1464;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1466;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1467;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1468;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1471;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1473;BA.debugLine="End Sub";
return "";
}
public static String  _imageview3_click() throws Exception{
 //BA.debugLineNum = 1431;BA.debugLine="Sub ImageView3_Click";
 //BA.debugLineNum = 1432;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1433;BA.debugLine="curphotoName=\"photo3\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo3";
 //BA.debugLineNum = 1434;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1435;BA.debugLine="End Sub";
return "";
}
public static String  _imageview3_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1437;BA.debugLine="Sub ImageView3_LongClick";
 //BA.debugLineNum = 1439;BA.debugLine="Try";
try { //BA.debugLineNum = 1440;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1441;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo3\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo3"));
 //BA.debugLineNum = 1442;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1444;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1445;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1446;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1449;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1451;BA.debugLine="End Sub";
return "";
}
public static String  _imageview4_click() throws Exception{
 //BA.debugLineNum = 1409;BA.debugLine="Sub ImageView4_Click";
 //BA.debugLineNum = 1410;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1411;BA.debugLine="curphotoName=\"photo4\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo4";
 //BA.debugLineNum = 1412;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1413;BA.debugLine="End Sub";
return "";
}
public static String  _imageview4_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1415;BA.debugLine="Sub ImageView4_LongClick";
 //BA.debugLineNum = 1417;BA.debugLine="Try";
try { //BA.debugLineNum = 1418;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1419;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo4\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo4"));
 //BA.debugLineNum = 1420;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1422;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1423;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1424;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1427;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1429;BA.debugLine="End Sub";
return "";
}
public static String  _imageview5_click() throws Exception{
 //BA.debugLineNum = 1387;BA.debugLine="Sub ImageView5_Click";
 //BA.debugLineNum = 1388;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1389;BA.debugLine="curphotoName=\"photo5\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo5";
 //BA.debugLineNum = 1390;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1391;BA.debugLine="End Sub";
return "";
}
public static String  _imageview5_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1393;BA.debugLine="Sub ImageView5_LongClick";
 //BA.debugLineNum = 1395;BA.debugLine="Try";
try { //BA.debugLineNum = 1396;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1397;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo5\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo5"));
 //BA.debugLineNum = 1398;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1400;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1401;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1402;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1405;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1407;BA.debugLine="End Sub";
return "";
}
public static String  _imageview6_click() throws Exception{
 //BA.debugLineNum = 1365;BA.debugLine="Sub ImageView6_Click";
 //BA.debugLineNum = 1366;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1367;BA.debugLine="curphotoName=\"photo6\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo6";
 //BA.debugLineNum = 1368;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1369;BA.debugLine="End Sub";
return "";
}
public static String  _imageview6_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1371;BA.debugLine="Sub ImageView6_LongClick";
 //BA.debugLineNum = 1373;BA.debugLine="Try";
try { //BA.debugLineNum = 1374;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1375;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo6\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo6"));
 //BA.debugLineNum = 1376;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1378;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1379;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1380;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1383;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1385;BA.debugLine="End Sub";
return "";
}
public static String  _imageview7_click() throws Exception{
 //BA.debugLineNum = 1912;BA.debugLine="Sub ImageView7_Click";
 //BA.debugLineNum = 1913;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1914;BA.debugLine="curphotoName=\"photo7\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo7";
 //BA.debugLineNum = 1915;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1916;BA.debugLine="End Sub";
return "";
}
public static String  _imageview7_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1918;BA.debugLine="Sub ImageView7_LongClick";
 //BA.debugLineNum = 1920;BA.debugLine="Try";
try { //BA.debugLineNum = 1921;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1922;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo7\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo7"));
 //BA.debugLineNum = 1923;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1925;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1926;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1927;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1930;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1932;BA.debugLine="End Sub";
return "";
}
public static String  _imageview8_click() throws Exception{
 //BA.debugLineNum = 1877;BA.debugLine="Sub ImageView8_Click";
 //BA.debugLineNum = 1878;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1879;BA.debugLine="curphotoName=\"photo8\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo8";
 //BA.debugLineNum = 1880;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1881;BA.debugLine="End Sub";
return "";
}
public static String  _imageview8_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1883;BA.debugLine="Sub ImageView8_LongClick";
 //BA.debugLineNum = 1885;BA.debugLine="Try";
try { //BA.debugLineNum = 1886;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1887;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo8\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo8"));
 //BA.debugLineNum = 1888;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1890;BA.debugLine="imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfs.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1891;BA.debugLine="imvFS.Visible=True";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1892;BA.debugLine="imvFS.BringToFront";
mostCurrent._imvfs.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1895;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1897;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp1_click() throws Exception{
 //BA.debugLineNum = 1585;BA.debugLine="Sub ImageViewp1_Click";
 //BA.debugLineNum = 1586;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1587;BA.debugLine="curphotoName=\"photo1apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo1apres";
 //BA.debugLineNum = 1589;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"timestamp_apres\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("timestamp_apres",(Object)(_vvvvvvvvvvvvvvvvvvvvvvvvvvv2()));
 //BA.debugLineNum = 1590;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1591;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp1_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1593;BA.debugLine="Sub ImageViewp1_LongClick";
 //BA.debugLineNum = 1595;BA.debugLine="Try";
try { //BA.debugLineNum = 1596;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1597;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo1apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo1apres"));
 //BA.debugLineNum = 1598;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1600;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1601;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1602;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1605;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1607;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp2_click() throws Exception{
 //BA.debugLineNum = 1563;BA.debugLine="Sub ImageViewp2_Click";
 //BA.debugLineNum = 1564;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1565;BA.debugLine="curphotoName=\"photo2apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo2apres";
 //BA.debugLineNum = 1566;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1567;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp2_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1569;BA.debugLine="Sub ImageViewp2_LongClick";
 //BA.debugLineNum = 1571;BA.debugLine="Try";
try { //BA.debugLineNum = 1572;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1573;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo2apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo2apres"));
 //BA.debugLineNum = 1574;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1576;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1577;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1578;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1581;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1583;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp3_click() throws Exception{
 //BA.debugLineNum = 1541;BA.debugLine="Sub ImageViewp3_Click";
 //BA.debugLineNum = 1542;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1543;BA.debugLine="curphotoName=\"photo3apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo3apres";
 //BA.debugLineNum = 1544;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1545;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp3_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1547;BA.debugLine="Sub ImageViewp3_LongClick";
 //BA.debugLineNum = 1549;BA.debugLine="Try";
try { //BA.debugLineNum = 1550;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1551;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo3apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo3apres"));
 //BA.debugLineNum = 1552;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1554;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1555;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1556;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1559;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1561;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp4_click() throws Exception{
 //BA.debugLineNum = 1519;BA.debugLine="Sub ImageViewp4_Click";
 //BA.debugLineNum = 1520;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1521;BA.debugLine="curphotoName=\"photo4apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo4apres";
 //BA.debugLineNum = 1522;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1523;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp4_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1525;BA.debugLine="Sub ImageViewp4_LongClick";
 //BA.debugLineNum = 1527;BA.debugLine="Try";
try { //BA.debugLineNum = 1528;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1529;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo4apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo4apres"));
 //BA.debugLineNum = 1530;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1532;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1533;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1534;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1537;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1539;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp5_click() throws Exception{
 //BA.debugLineNum = 1497;BA.debugLine="Sub ImageViewp5_Click";
 //BA.debugLineNum = 1498;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1499;BA.debugLine="curphotoName=\"photo5apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo5apres";
 //BA.debugLineNum = 1500;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1501;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp5_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1503;BA.debugLine="Sub ImageViewp5_LongClick";
 //BA.debugLineNum = 1505;BA.debugLine="Try";
try { //BA.debugLineNum = 1506;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1507;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo5apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo5apres"));
 //BA.debugLineNum = 1508;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1510;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1511;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1512;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1515;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1517;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp6_click() throws Exception{
 //BA.debugLineNum = 1475;BA.debugLine="Sub ImageViewp6_Click";
 //BA.debugLineNum = 1476;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 1477;BA.debugLine="curphotoName=\"photo6apres\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "photo6apres";
 //BA.debugLineNum = 1478;BA.debugLine="RunCamera";
_vvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 1479;BA.debugLine="End Sub";
return "";
}
public static String  _imageviewp6_longclick() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1481;BA.debugLine="Sub ImageViewp6_LongClick";
 //BA.debugLineNum = 1483;BA.debugLine="Try";
try { //BA.debugLineNum = 1484;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1485;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo6apres\"";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo6apres"));
 //BA.debugLineNum = 1486;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1488;BA.debugLine="imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imvfsapres.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1489;BA.debugLine="imvFSApres.Visible=True";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 1490;BA.debugLine="imvFSApres.BringToFront";
mostCurrent._imvfsapres.BringToFront();
 };
 } 
       catch (Exception e10) {
			processBA.setLastException(e10); //BA.debugLineNum = 1493;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1495;BA.debugLine="End Sub";
return "";
}
public static String  _imgfullscreen_longclick() throws Exception{
 //BA.debugLineNum = 1339;BA.debugLine="Sub imgFullScreen_LongClick";
 //BA.debugLineNum = 1341;BA.debugLine="End Sub";
return "";
}
public static String  _imvfs_longclick() throws Exception{
 //BA.debugLineNum = 1707;BA.debugLine="Sub imvFS_LongClick";
 //BA.debugLineNum = 1708;BA.debugLine="imvFS.Visible=False";
mostCurrent._imvfs.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1709;BA.debugLine="imvFS.SendToBack";
mostCurrent._imvfs.SendToBack();
 //BA.debugLineNum = 1710;BA.debugLine="End Sub";
return "";
}
public static String  _imvfsapres_longclick() throws Exception{
 //BA.debugLineNum = 1712;BA.debugLine="Sub imvFSApres_LongClick";
 //BA.debugLineNum = 1713;BA.debugLine="imvFSApres.Visible=False";
mostCurrent._imvfsapres.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 1714;BA.debugLine="imvFSApres.SendToBack";
mostCurrent._imvfsapres.SendToBack();
 //BA.debugLineNum = 1715;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvv4() throws Exception{
String _sphoto = "";
 //BA.debugLineNum = 1011;BA.debugLine="Sub InitP1";
 //BA.debugLineNum = 1012;BA.debugLine="Try";
try { //BA.debugLineNum = 1013;BA.debugLine="photoIdx=1";
_vvvvvvvvvvvvvvvvvvv0 = (int) (1);
 //BA.debugLineNum = 1014;BA.debugLine="Dim sphoto As String";
_sphoto = "";
 //BA.debugLineNum = 1015;BA.debugLine="sphoto=Main.fCursor.GetColumnValue(\"photo1\")";
_sphoto = BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.GetColumnValue("photo1"));
 //BA.debugLineNum = 1016;BA.debugLine="If sphoto<>\"\" And File.Exists(CNSPath,sphoto) Th";
if ((_sphoto).equals("") == false && anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto)) { 
 //BA.debugLineNum = 1018;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,_sphoto).getObject()));
 //BA.debugLineNum = 1019;BA.debugLine="canTakePic=True";
_vvvvvvvvvvvvvvvvvvv6 = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 1021;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath, \"vide.jpg";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e12) {
			processBA.setLastException(e12); //BA.debugLineNum = 1024;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1026;BA.debugLine="End Sub";
return "";
}
public static String  _lblp1_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1864;BA.debugLine="Sub lblP1_LongClick";
 //BA.debugLineNum = 1865;BA.debugLine="Try";
try { //BA.debugLineNum = 1866;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1867;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 1"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1868;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1869;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo1\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo1",(Object)(""));
 //BA.debugLineNum = 1870;BA.debugLine="ImageView1.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1873;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1875;BA.debugLine="End Sub";
return "";
}
public static String  _lblp1apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1782;BA.debugLine="Sub lblP1apres_LongClick";
 //BA.debugLineNum = 1783;BA.debugLine="Try";
try { //BA.debugLineNum = 1784;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1785;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 1 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1786;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1787;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo1apres\",\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo1apres",(Object)(""));
 //BA.debugLineNum = 1788;BA.debugLine="ImageViewp1.Bitmap=LoadBitmap(CNSPath,\"vide.jpg";
mostCurrent._imageviewp1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1791;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1793;BA.debugLine="End Sub";
return "";
}
public static String  _lblp2_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1850;BA.debugLine="Sub lblP2_LongClick";
 //BA.debugLineNum = 1851;BA.debugLine="Try";
try { //BA.debugLineNum = 1852;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1853;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 2"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1854;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1855;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo2\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo2",(Object)(""));
 //BA.debugLineNum = 1856;BA.debugLine="ImageView2.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1859;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1862;BA.debugLine="End Sub";
return "";
}
public static String  _lblp2apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1756;BA.debugLine="Sub lblP2apres_LongClick";
 //BA.debugLineNum = 1757;BA.debugLine="Try";
try { //BA.debugLineNum = 1758;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1759;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 2 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1760;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1761;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo2apres\",\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo2apres",(Object)(""));
 //BA.debugLineNum = 1762;BA.debugLine="ImageViewp2.Bitmap=LoadBitmap(CNSPath,\"vide.jpg";
mostCurrent._imageviewp2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1765;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1767;BA.debugLine="End Sub";
return "";
}
public static String  _lblp3_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1836;BA.debugLine="Sub lblP3_LongClick";
 //BA.debugLineNum = 1837;BA.debugLine="Try";
try { //BA.debugLineNum = 1838;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1839;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 3"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1840;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1841;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo3\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo3",(Object)(""));
 //BA.debugLineNum = 1842;BA.debugLine="ImageView3.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1845;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1848;BA.debugLine="End Sub";
return "";
}
public static String  _lblp3apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1730;BA.debugLine="Sub lblP3apres_LongClick";
 //BA.debugLineNum = 1731;BA.debugLine="Try";
try { //BA.debugLineNum = 1732;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1733;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 3 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1734;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1735;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo3apres\",\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo3apres",(Object)(""));
 //BA.debugLineNum = 1736;BA.debugLine="ImageViewp3.Bitmap=LoadBitmap(CNSPath,\"vide.jpg";
mostCurrent._imageviewp3.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1739;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1741;BA.debugLine="End Sub";
return "";
}
public static String  _lblp4_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1822;BA.debugLine="Sub lblP4_LongClick";
 //BA.debugLineNum = 1823;BA.debugLine="Try";
try { //BA.debugLineNum = 1824;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1825;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 4"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1826;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1827;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo4\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo4",(Object)(""));
 //BA.debugLineNum = 1828;BA.debugLine="ImageView4.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1831;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1834;BA.debugLine="End Sub";
return "";
}
public static String  _lblp4apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1769;BA.debugLine="Sub lblP4apres_LongClick";
 //BA.debugLineNum = 1770;BA.debugLine="Try";
try { //BA.debugLineNum = 1771;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1772;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 4 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1773;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1774;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo4apres\",\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo4apres",(Object)(""));
 //BA.debugLineNum = 1775;BA.debugLine="ImageViewp4.Bitmap=LoadBitmap(CNSPath,\"vide.jpg";
mostCurrent._imageviewp4.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1778;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1780;BA.debugLine="End Sub";
return "";
}
public static String  _lblp5_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1808;BA.debugLine="Sub lblP5_LongClick";
 //BA.debugLineNum = 1809;BA.debugLine="Try";
try { //BA.debugLineNum = 1810;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1811;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 5"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1812;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1813;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo5\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo5",(Object)(""));
 //BA.debugLineNum = 1814;BA.debugLine="ImageView5.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1817;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1820;BA.debugLine="End Sub";
return "";
}
public static String  _lblp5apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1743;BA.debugLine="Sub lblP5apres_LongClick";
 //BA.debugLineNum = 1744;BA.debugLine="Try";
try { //BA.debugLineNum = 1745;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1746;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 5 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1747;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1748;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo5apres\",\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo5apres",(Object)(""));
 //BA.debugLineNum = 1749;BA.debugLine="ImageViewp5.Bitmap=LoadBitmap(CNSPath,\"vide.jpg";
mostCurrent._imageviewp5.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1752;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1754;BA.debugLine="End Sub";
return "";
}
public static String  _lblp6_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1795;BA.debugLine="Sub lblP6_LongClick";
 //BA.debugLineNum = 1796;BA.debugLine="Try";
try { //BA.debugLineNum = 1797;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1798;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 6"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1799;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1800;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo6\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo6",(Object)(""));
 //BA.debugLineNum = 1801;BA.debugLine="ImageView6.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1804;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1806;BA.debugLine="End Sub";
return "";
}
public static String  _lblp6apres_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1717;BA.debugLine="Sub lblP6apres_LongClick";
 //BA.debugLineNum = 1718;BA.debugLine="Try";
try { //BA.debugLineNum = 1719;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1720;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo  ?";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 6 après"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1721;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1722;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo6apres\",\"\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo6apres",(Object)(""));
 //BA.debugLineNum = 1723;BA.debugLine="ImageViewp6.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageviewp6.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1726;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1728;BA.debugLine="End Sub";
return "";
}
public static String  _lblp7_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1934;BA.debugLine="Sub lblP7_LongClick";
 //BA.debugLineNum = 1935;BA.debugLine="Try";
try { //BA.debugLineNum = 1936;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1937;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 7"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1938;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1939;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo7\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo7",(Object)(""));
 //BA.debugLineNum = 1940;BA.debugLine="ImageView7.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview7.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1943;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1945;BA.debugLine="End Sub";
return "";
}
public static String  _lblp8_longclick() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1899;BA.debugLine="Sub lblP8_LongClick";
 //BA.debugLineNum = 1900;BA.debugLine="Try";
try { //BA.debugLineNum = 1901;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1902;BA.debugLine="i = Msgbox2(\"Voulez-vous supprimer cette photo";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous supprimer cette photo  ?"),BA.ObjectToCharSequence("Supprimer photo 8"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1903;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 1904;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"photo8\",\"\")";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo8",(Object)(""));
 //BA.debugLineNum = 1905;BA.debugLine="ImageView8.Bitmap=LoadBitmap(CNSPath,\"vide.jpg\"";
mostCurrent._imageview8.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg").getObject()));
 };
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 1908;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1910;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper  _vvvvvvvvvvvvvvvvvvvvvv7(String _dir,String _filename,float _bmprotation) throws Exception{
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _bm = null;
com.rootsoft.bitmaplibrary.BitmapLibrary _bm2 = null;
 //BA.debugLineNum = 999;BA.debugLine="Sub LoadbitmapRotated(Dir As String,Filename As St";
 //BA.debugLineNum = 1000;BA.debugLine="Dim bm As Bitmap";
_bm = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 1001;BA.debugLine="bm=LoadBitmapSample(Dir,Filename,100%x,100%y)";
_bm = anywheresoftware.b4a.keywords.Common.LoadBitmapSample(_dir,_filename,anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA));
 //BA.debugLineNum = 1002;BA.debugLine="Dim bm2 As BitmapExtended";
_bm2 = new com.rootsoft.bitmaplibrary.BitmapLibrary();
 //BA.debugLineNum = 1003;BA.debugLine="Try";
try { //BA.debugLineNum = 1004;BA.debugLine="bm=bm2.rotateBitmap(bm,bmprotation)";
_bm.setObject((android.graphics.Bitmap)(_bm2.rotateBitmap((android.graphics.Bitmap)(_bm.getObject()),_bmprotation)));
 } 
       catch (Exception e7) {
			processBA.setLastException(e7); //BA.debugLineNum = 1006;BA.debugLine="Log(\"LoadBitmapRotated failed\")";
anywheresoftware.b4a.keywords.Common.Log("LoadBitmapRotated failed");
 };
 //BA.debugLineNum = 1008;BA.debugLine="Return bm";
if (true) return _bm;
 //BA.debugLineNum = 1009;BA.debugLine="End Sub";
return null;
}
public static anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper  _vvvvvvvvvvvvvvvvvvvvvvvvv2(String _dir,String _filename,int _maxwidth,int _maxheight,boolean _autorotate) throws Exception{
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _bm = null;
com.rootsoft.bitmaplibrary.BitmapLibrary _bm2 = null;
anywheresoftware.b4a.agraham.jpegutils.ExifUtils _exifdata1 = null;
 //BA.debugLineNum = 967;BA.debugLine="Sub LoadbitmapSample2(Dir As String,Filename As St";
 //BA.debugLineNum = 968;BA.debugLine="Dim bm As Bitmap";
_bm = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 969;BA.debugLine="bm=LoadBitmapSample(Dir,Filename,100%x,100%y)";
_bm = anywheresoftware.b4a.keywords.Common.LoadBitmapSample(_dir,_filename,anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA));
 //BA.debugLineNum = 970;BA.debugLine="If AutoRotate Then";
if (_autorotate) { 
 //BA.debugLineNum = 971;BA.debugLine="Dim bm2 As BitmapExtended";
_bm2 = new com.rootsoft.bitmaplibrary.BitmapLibrary();
 //BA.debugLineNum = 972;BA.debugLine="Dim exifdata1 As ExifData";
_exifdata1 = new anywheresoftware.b4a.agraham.jpegutils.ExifUtils();
 //BA.debugLineNum = 973;BA.debugLine="Try";
try { //BA.debugLineNum = 974;BA.debugLine="exifdata1.Initialize(Dir,Filename)";
_exifdata1.Initialize(_dir,_filename);
 //BA.debugLineNum = 984;BA.debugLine="Select Case exifdata1.getAttribute(exifda";
switch (BA.switchObjectToInt(_exifdata1.getAttribute(_exifdata1.TAG_ORIENTATION),BA.NumberToString(_exifdata1.ORIENTATION_ROTATE_180),BA.NumberToString(_exifdata1.ORIENTATION_ROTATE_90),BA.NumberToString(_exifdata1.ORIENTATION_ROTATE_270))) {
case 0: {
 //BA.debugLineNum = 986;BA.debugLine="bm=bm2.rotateBitmap(bm,180)";
_bm.setObject((android.graphics.Bitmap)(_bm2.rotateBitmap((android.graphics.Bitmap)(_bm.getObject()),(float) (180))));
 break; }
case 1: {
 //BA.debugLineNum = 988;BA.debugLine="bm=bm2.rotateBitmap(bm,90)";
_bm.setObject((android.graphics.Bitmap)(_bm2.rotateBitmap((android.graphics.Bitmap)(_bm.getObject()),(float) (90))));
 break; }
case 2: {
 //BA.debugLineNum = 990;BA.debugLine="bm=bm2.rotateBitmap(bm,270)";
_bm.setObject((android.graphics.Bitmap)(_bm2.rotateBitmap((android.graphics.Bitmap)(_bm.getObject()),(float) (270))));
 break; }
}
;
 } 
       catch (Exception e17) {
			processBA.setLastException(e17); //BA.debugLineNum = 993;BA.debugLine="Log(\"LoadBitmapSample failed\")";
anywheresoftware.b4a.keywords.Common.Log("LoadBitmapSample failed");
 };
 };
 //BA.debugLineNum = 996;BA.debugLine="Return bm";
if (true) return _bm;
 //BA.debugLineNum = 997;BA.debugLine="End Sub";
return null;
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv3(String _dir,String _fn) throws Exception{
anywheresoftware.b4a.objects.IntentWrapper _camintent = null;
 //BA.debugLineNum = 959;BA.debugLine="Sub OpenCam(dir As String, fn As String)";
 //BA.debugLineNum = 960;BA.debugLine="Dim camintent As Intent";
_camintent = new anywheresoftware.b4a.objects.IntentWrapper();
 //BA.debugLineNum = 961;BA.debugLine="camintent.Initialize(\"android.media.action.IMA";
_camintent.Initialize("android.media.action.IMAGE_CAPTURE","");
 //BA.debugLineNum = 962;BA.debugLine="camintent.PutExtra(\"output\",ParseUri(\"file://\"";
_camintent.PutExtra("output",_vvvvvvvvvvvvvvvvvvvvvvvvvvvvv4("file://"+anywheresoftware.b4a.keywords.Common.File.Combine(_dir,_fn)));
 //BA.debugLineNum = 963;BA.debugLine="StartActivity(camintent)";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)(_camintent.getObject()));
 //BA.debugLineNum = 964;BA.debugLine="End Sub";
return "";
}
public static Object  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv4(String _s) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
 //BA.debugLineNum = 954;BA.debugLine="Sub ParseUri(s As String) As Object";
 //BA.debugLineNum = 955;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 956;BA.debugLine="Return r.RunStaticMethod(\"android.net.Uri\", \"p";
if (true) return _r.RunStaticMethod("android.net.Uri","parse",new Object[]{(Object)(_s)},new String[]{"java.lang.String"});
 //BA.debugLineNum = 957;BA.debugLine="End Sub";
return null;
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim ftp As FTP,gPath As String,gPic As String,gps";
_v5 = new anywheresoftware.b4a.net.FTPWrapper();
_v6 = "";
_v7 = "";
_v0 = new anywheresoftware.b4a.gps.GPS();
_vv1 = new anywheresoftware.b4a.gps.LocationWrapper();
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return "";
}
public static String  _raz_fiche() throws Exception{
 //BA.debugLineNum = 325;BA.debugLine="Sub RAZ_fiche";
 //BA.debugLineNum = 327;BA.debugLine="commentaire.Text=\"\"";
mostCurrent._commentaire.setText(BA.ObjectToCharSequence(""));
 //BA.debugLineNum = 328;BA.debugLine="curPhoto=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvv0 = "";
 //BA.debugLineNum = 329;BA.debugLine="curphoto2=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 330;BA.debugLine="expectedPic=\"\"";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = "";
 //BA.debugLineNum = 341;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvv3() throws Exception{
String _selequipment = "";
 //BA.debugLineNum = 1311;BA.debugLine="Sub RefreshspnType";
 //BA.debugLineNum = 1313;BA.debugLine="Dim selEquipment As String";
_selequipment = "";
 //BA.debugLineNum = 1321;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv5(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _original,float _degree) throws Exception{
anywheresoftware.b4j.object.JavaObject _matrix = null;
anywheresoftware.b4j.object.JavaObject _bmp = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _newimage = null;
 //BA.debugLineNum = 1684;BA.debugLine="Sub RotateImage(original As Bitmap, degree As Floa";
 //BA.debugLineNum = 1685;BA.debugLine="Dim matrix As JavaObject";
_matrix = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 1686;BA.debugLine="matrix.InitializeNewInstance(\"android.graphics.Ma";
_matrix.InitializeNewInstance("android.graphics.Matrix",(Object[])(anywheresoftware.b4a.keywords.Common.Null));
 //BA.debugLineNum = 1687;BA.debugLine="matrix.RunMethod(\"postRotate\", Array(degree))";
_matrix.RunMethod("postRotate",new Object[]{(Object)(_degree)});
 //BA.debugLineNum = 1688;BA.debugLine="Dim bmp As JavaObject";
_bmp = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 1689;BA.debugLine="bmp.InitializeStatic(\"android.graphics.Bitmap\")";
_bmp.InitializeStatic("android.graphics.Bitmap");
 //BA.debugLineNum = 1690;BA.debugLine="Dim NewImage As Bitmap = bmp.RunMethod(\"createBit";
_newimage = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_newimage.setObject((android.graphics.Bitmap)(_bmp.RunMethod("createBitmap",new Object[]{(Object)(_original.getObject()),(Object)(0),(Object)(0),(Object)(_original.getWidth()),(Object)(_original.getHeight()),(Object)(_matrix.getObject()),(Object)(anywheresoftware.b4a.keywords.Common.True)})));
 //BA.debugLineNum = 1692;BA.debugLine="Return NewImage";
if (true) return _newimage;
 //BA.debugLineNum = 1693;BA.debugLine="End Sub";
return null;
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvv0() throws Exception{
String _dt = "";
String _filepic = "";
 //BA.debugLineNum = 1039;BA.debugLine="Sub RunCamera";
 //BA.debugLineNum = 1040;BA.debugLine="Dim dt As String,filepic As String";
_dt = "";
_filepic = "";
 //BA.debugLineNum = 1041;BA.debugLine="DateTime.DateFormat = \"yyMMddHHmmss\" ' See this p";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyMMddHHmmss");
 //BA.debugLineNum = 1042;BA.debugLine="dt = DateTime.Date(DateTime.Now)";
_dt = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 1043;BA.debugLine="If curphotoName=\"\" Or curphotoName=Null Or curpho";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("") || mostCurrent._vvvvvvvvvvvvvvvvvvvv4== null || (mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("null")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvv4 = "Unknown";};
 //BA.debugLineNum = 1044;BA.debugLine="filepic=curphotoName & getPhotoPrefix & \".jpg\"";
_filepic = mostCurrent._vvvvvvvvvvvvvvvvvvvv4+_vvvvvvvvvvvvvvvvvvvvvvvv7()+".jpg";
 //BA.debugLineNum = 1045;BA.debugLine="expectedPic=filepic";
mostCurrent._vvvvvvvvvvvvvvvvvvv7 = _filepic;
 //BA.debugLineNum = 1046;BA.debugLine="If curphotoName=\"photo1\" Then curphoto1=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo1")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1047;BA.debugLine="If curphotoName=\"photo2\" Then curphoto2=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo2")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1048;BA.debugLine="If curphotoName=\"photo3\" Then curphoto3=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo3")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1049;BA.debugLine="If curphotoName=\"photo4\" Then curphoto4=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo4")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1050;BA.debugLine="If curphotoName=\"photo5\" Then curphoto5=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo5")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1051;BA.debugLine="If curphotoName=\"photo6\" Then curphoto6=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo6")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1052;BA.debugLine="If curphotoName=\"photo7\" Then curphoto7=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo7")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1053;BA.debugLine="If curphotoName=\"photo8\" Then curphoto8=expectedP";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo8")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1058;BA.debugLine="If curphotoName=\"photo1apres\" Then curphotop1=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo1apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1059;BA.debugLine="If curphotoName=\"photo2apres\" Then curphotop2=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo2apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1060;BA.debugLine="If curphotoName=\"photo3apres\" Then curphotop3=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo3apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1061;BA.debugLine="If curphotoName=\"photo4apres\" Then curphotop4=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo4apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1062;BA.debugLine="If curphotoName=\"photo5apres\" Then curphotop5=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo5apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1063;BA.debugLine="If curphotoName=\"photo6apres\" Then curphotop6=exp";
if ((mostCurrent._vvvvvvvvvvvvvvvvvvvv4).equals("photo6apres")) { 
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6 = mostCurrent._vvvvvvvvvvvvvvvvvvv7;};
 //BA.debugLineNum = 1068;BA.debugLine="OpenCam(CNSPath,filepic)";
_vvvvvvvvvvvvvvvvvvvvvvvvvvvvv3(mostCurrent._vvvvvvvvvvvvvvvvv1,_filepic);
 //BA.debugLineNum = 1071;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvv5() throws Exception{
String _ccomment = "";
String _cphoto1 = "";
String _cphoto2 = "";
String _ctimestamp = "";
String _snom = "";
String _sprenom = "";
String _stel1 = "";
String _stel2 = "";
String _csite = "";
String _clocalisation = "";
String _cfluide = "";
String _cvaleurn = "";
String _cmateriel = "";
int _cnb = 0;
int _cdn = 0;
int _dsacsutilises = 0;
String _saddrct1 = "";
String _saddrct2 = "";
String _saddrcp = "";
String _saddrville = "";
String _saddrloc1 = "";
String _saddrloc2 = "";
String _saddrloccp = "";
String _saddrlocville = "";
String _stypeporte = "";
String _stypeplancher = "";
String _stypelocal = "";
String _stypecloison = "";
String _snatureisolant = "";
String _stypeisolantcomble = "";
int _nbbatiment = 0;
int _nblogement = 0;
int _hspsansisolant = 0;
int _hspavecisolant = 0;
int _chkaccespc = 0;
int _chkbatcollectif = 0;
int _chkcombleamenag = 0;
int _chkparevapeur = 0;
int _chkrehaussevmc = 0;
int _chkrehaussetrappe = 0;
int _chkevacisolant = 0;
int _chkspot = 0;
int _chkconduitcheminee = 0;
int _chkecartfeu = 0;
int _chkcombleencombre = 0;
int _chkmage = 0;
int _chkcnxinternet = 0;
String _stypetoit = "";
String _stypelogement = "";
String _stypeisolantcbl = "";
String _smateriauprevu = "";
String _snatureisolantcbl = "";
String _stypeetatplafond = "";
String _stypeplanchercbl = "";
int _dhsousgouttiere = 0;
int _nnbniveaux = 0;
int _dsurfcomble = 0;
int _dsurfreelle = 0;
int _dsurfplb = 0;
String _stypereseauplafond = "";
String _setatisolantplb = "";
String _scomment = "";
String _scomment2 = "";
int _nnbcageesc = 0;
String _sresistanceisolant = "";
String _saccespcommunes = "";
int _hspporteisolant = 0;
 //BA.debugLineNum = 477;BA.debugLine="Sub SaveFiche";
 //BA.debugLineNum = 479;BA.debugLine="Dim ccomment As String ,cphoto1 As String,cphoto2";
_ccomment = "";
_cphoto1 = "";
_cphoto2 = "";
_ctimestamp = "";
_snom = "";
_sprenom = "";
_stel1 = "";
_stel2 = "";
 //BA.debugLineNum = 480;BA.debugLine="Dim cSite As String,cLocalisation As String,cFlui";
_csite = "";
_clocalisation = "";
_cfluide = "";
_cvaleurn = "";
_cmateriel = "";
_cnb = 0;
_cdn = 0;
_dsacsutilises = 0;
 //BA.debugLineNum = 481;BA.debugLine="Dim sAddrCT1 As String,sAddrCT2 As String,sAddrCP";
_saddrct1 = "";
_saddrct2 = "";
_saddrcp = "";
_saddrville = "";
_saddrloc1 = "";
_saddrloc2 = "";
_saddrloccp = "";
_saddrlocville = "";
 //BA.debugLineNum = 482;BA.debugLine="Dim sTypePorte As String,sTypePlancher As String,";
_stypeporte = "";
_stypeplancher = "";
_stypelocal = "";
_stypecloison = "";
_snatureisolant = "";
_stypeisolantcomble = "";
 //BA.debugLineNum = 483;BA.debugLine="Dim nbBatiment As Int,nbLogement As Int,HSPsansIs";
_nbbatiment = 0;
_nblogement = 0;
_hspsansisolant = 0;
_hspavecisolant = 0;
_chkaccespc = 0;
_chkbatcollectif = 0;
_chkcombleamenag = 0;
_chkparevapeur = 0;
 //BA.debugLineNum = 484;BA.debugLine="Dim chkRehausseVMC As Int,chkRehausseTrappe As In";
_chkrehaussevmc = 0;
_chkrehaussetrappe = 0;
_chkevacisolant = 0;
_chkspot = 0;
_chkconduitcheminee = 0;
_chkecartfeu = 0;
_chkcombleencombre = 0;
_chkmage = 0;
_chkcnxinternet = 0;
 //BA.debugLineNum = 485;BA.debugLine="Dim sTypeToit As String,sTypeLogement As String,s";
_stypetoit = "";
_stypelogement = "";
_stypeisolantcbl = "";
_smateriauprevu = "";
_snatureisolantcbl = "";
_stypeetatplafond = "";
_stypeplanchercbl = "";
 //BA.debugLineNum = 486;BA.debugLine="Dim dHsousGouttiere As Int,nNbNiveaux As Int,dSur";
_dhsousgouttiere = 0;
_nnbniveaux = 0;
_dsurfcomble = 0;
_dsurfreelle = 0;
_dsurfplb = 0;
_stypereseauplafond = "";
_setatisolantplb = "";
_scomment = "";
_scomment2 = "";
 //BA.debugLineNum = 487;BA.debugLine="Dim nNbCageEsc As Int,sResistanceIsolant As Strin";
_nnbcageesc = 0;
_sresistanceisolant = "";
_saccespcommunes = "";
_hspporteisolant = 0;
 //BA.debugLineNum = 489;BA.debugLine="Try";
try { //BA.debugLineNum = 491;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"IDCampagne\",1)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("IDCampagne",(Object)(1));
 //BA.debugLineNum = 492;BA.debugLine="cSite=\"Site\"";
_csite = "Site";
 //BA.debugLineNum = 493;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"Site\",cSite)";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("Site",(Object)(_csite));
 //BA.debugLineNum = 495;BA.debugLine="cLocalisation=\"Localisation\"";
_clocalisation = "Localisation";
 //BA.debugLineNum = 496;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"Localisation\",c";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("Localisation",(Object)(_clocalisation));
 //BA.debugLineNum = 498;BA.debugLine="ctimestamp = getTimestamp";
_ctimestamp = _vvvvvvvvvvvvvvvvvvvvvvvvvvv2();
 //BA.debugLineNum = 499;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"timestamp\",ctim";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("timestamp",(Object)(_ctimestamp));
 //BA.debugLineNum = 503;BA.debugLine="If txtNom.Text<>Null And txtNom.Text<>\"\" Then sN";
if (mostCurrent._txtnom.getText()!= null && (mostCurrent._txtnom.getText()).equals("") == false) { 
_snom = mostCurrent._txtnom.getText();}
else {
_snom = "";};
 //BA.debugLineNum = 504;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"NomChantier\",sN";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("NomChantier",(Object)(_snom));
 //BA.debugLineNum = 505;BA.debugLine="If txtPrenom.Text<>Null And txtPrenom.Text<>\"\" T";
if (mostCurrent._txtprenom.getText()!= null && (mostCurrent._txtprenom.getText()).equals("") == false) { 
_sprenom = mostCurrent._txtprenom.getText();}
else {
_sprenom = "";};
 //BA.debugLineNum = 506;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PrenomChantier\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PrenomChantier",(Object)(_sprenom));
 //BA.debugLineNum = 507;BA.debugLine="If txtTel1.Text<>Null And txtTel1.Text<>\"\" Then";
if (mostCurrent._txttel1.getText()!= null && (mostCurrent._txttel1.getText()).equals("") == false) { 
_stel1 = mostCurrent._txttel1.getText();}
else {
_stel1 = "";};
 //BA.debugLineNum = 508;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"TelChantier\",sT";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("TelChantier",(Object)(_stel1));
 //BA.debugLineNum = 509;BA.debugLine="If txtTel2.Text<>Null And txtTel2.Text<>\"\" Then";
if (mostCurrent._txttel2.getText()!= null && (mostCurrent._txttel2.getText()).equals("") == false) { 
_stel2 = mostCurrent._txttel2.getText();}
else {
_stel2 = "";};
 //BA.debugLineNum = 510;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"Tel2Chantier\",s";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("Tel2Chantier",(Object)(_stel2));
 //BA.debugLineNum = 512;BA.debugLine="If txtAddrCT1.Text<>Null And txtAddrCT1.Text<>\"\"";
if (mostCurrent._txtaddrct1.getText()!= null && (mostCurrent._txtaddrct1.getText()).equals("") == false) { 
_saddrct1 = mostCurrent._txtaddrct1.getText();}
else {
_saddrct1 = "";};
 //BA.debugLineNum = 513;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrChantier1\",";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrChantier1",(Object)(_saddrct1));
 //BA.debugLineNum = 514;BA.debugLine="If txtAddrCT2.Text<>Null And txtAddrCT2.Text<>\"\"";
if (mostCurrent._txtaddrct2.getText()!= null && (mostCurrent._txtaddrct2.getText()).equals("") == false) { 
_saddrct2 = mostCurrent._txtaddrct2.getText();}
else {
_saddrct2 = "";};
 //BA.debugLineNum = 515;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrChantier2\",";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrChantier2",(Object)(_saddrct2));
 //BA.debugLineNum = 516;BA.debugLine="If txtAddrCTCP.Text<>Null And txtAddrCTCP.Text<>";
if (mostCurrent._txtaddrctcp.getText()!= null && (mostCurrent._txtaddrctcp.getText()).equals("") == false) { 
_saddrcp = mostCurrent._txtaddrctcp.getText();}
else {
_saddrcp = "";};
 //BA.debugLineNum = 517;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrChantierCP\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrChantierCP",(Object)(_saddrcp));
 //BA.debugLineNum = 518;BA.debugLine="If txtAddrCTVille.Text<>Null And txtAddrCTVille.";
if (mostCurrent._txtaddrctville.getText()!= null && (mostCurrent._txtaddrctville.getText()).equals("") == false) { 
_saddrville = mostCurrent._txtaddrctville.getText();}
else {
_saddrville = "";};
 //BA.debugLineNum = 519;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"addrChantierVil";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("addrChantierVille",(Object)(_saddrville));
 //BA.debugLineNum = 521;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"EDLAvisPassage1";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("EDLAvisPassage1",(Object)(mostCurrent._cbavis1.getChecked()));
 //BA.debugLineNum = 522;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"EDLAvisPassage2";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("EDLAvisPassage2",(Object)(mostCurrent._cbavis2.getChecked()));
 //BA.debugLineNum = 523;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"EDLRefusLocatai";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("EDLRefusLocataire",(Object)(mostCurrent._cbrefusresident.getChecked()));
 //BA.debugLineNum = 527;BA.debugLine="If spnTypeToit.SelectedItem<>Null And spnTypeToi";
if (mostCurrent._spntypetoit.getSelectedItem()!= null && (mostCurrent._spntypetoit.getSelectedItem()).equals("") == false) { 
_stypetoit = mostCurrent._spntypetoit.getSelectedItem();}
else {
_stypetoit = "";};
 //BA.debugLineNum = 528;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLTypeToit\",sT";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLTypeToit",(Object)(_stypetoit));
 //BA.debugLineNum = 529;BA.debugLine="If spnTypeLogement.SelectedItem<>Null And spnTyp";
if (mostCurrent._spntypelogement.getSelectedItem()!= null && (mostCurrent._spntypelogement.getSelectedItem()).equals("") == false) { 
_stypelogement = mostCurrent._spntypelogement.getSelectedItem();}
else {
_stypelogement = "";};
 //BA.debugLineNum = 530;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLTypeLogement";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLTypeLogement",(Object)(_stypelogement));
 //BA.debugLineNum = 533;BA.debugLine="If spnNatureIsolant.SelectedItem<>Null And spnNa";
if (mostCurrent._spnnatureisolant.getSelectedItem()!= null && (mostCurrent._spnnatureisolant.getSelectedItem()).equals("") == false) { 
_snatureisolantcbl = mostCurrent._spnnatureisolant.getSelectedItem();}
else {
_snatureisolantcbl = "";};
 //BA.debugLineNum = 534;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLNatureIsolan";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLNatureIsolantComble",(Object)(_snatureisolantcbl));
 //BA.debugLineNum = 535;BA.debugLine="If spnMateriauPrevu.SelectedItem<>Null And spnMa";
if (mostCurrent._spnmateriauprevu.getSelectedItem()!= null && (mostCurrent._spnmateriauprevu.getSelectedItem()).equals("") == false) { 
_smateriauprevu = mostCurrent._spnmateriauprevu.getSelectedItem();}
else {
_smateriauprevu = "";};
 //BA.debugLineNum = 536;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLtypeMateriau";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLtypeMateriauPrevu",(Object)(_smateriauprevu));
 //BA.debugLineNum = 537;BA.debugLine="If spnEtatPlafond.SelectedItem<>Null And spnEtat";
if (mostCurrent._spnetatplafond.getSelectedItem()!= null && (mostCurrent._spnetatplafond.getSelectedItem()).equals("") == false) { 
_stypeetatplafond = mostCurrent._spnetatplafond.getSelectedItem();}
else {
_stypeetatplafond = "";};
 //BA.debugLineNum = 538;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLtypeEtatPlaf";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLtypeEtatPlafond",(Object)(_stypeetatplafond));
 //BA.debugLineNum = 539;BA.debugLine="If spnTypePlancherCBL.SelectedItem<>Null And spn";
if (mostCurrent._spntypeplanchercbl.getSelectedItem()!= null && (mostCurrent._spntypeplanchercbl.getSelectedItem()).equals("") == false) { 
_stypeplanchercbl = mostCurrent._spntypeplanchercbl.getSelectedItem();}
else {
_stypeplanchercbl = "";};
 //BA.debugLineNum = 540;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLtypePlancher";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLtypePlancher",(Object)(_stypeplanchercbl));
 //BA.debugLineNum = 543;BA.debugLine="If txtHsousGoutiere.Text<>Null And txtHsousGouti";
if (mostCurrent._txthsousgoutiere.getText()!= null && (mostCurrent._txthsousgoutiere.getText()).equals("") == false) { 
_dhsousgouttiere = (int)(Double.parseDouble(mostCurrent._txthsousgoutiere.getText()));}
else {
_dhsousgouttiere = (int) (0);};
 //BA.debugLineNum = 544;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLhtsousGoutti";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLhtsousGouttiere",(Object)(_dhsousgouttiere));
 //BA.debugLineNum = 545;BA.debugLine="If txtNbNiveaux.Text<>Null And txtNbNiveaux.Text";
if (mostCurrent._txtnbniveaux.getText()!= null && (mostCurrent._txtnbniveaux.getText()).equals("") == false) { 
_nnbniveaux = (int)(Double.parseDouble(mostCurrent._txtnbniveaux.getText()));}
else {
_nnbniveaux = (int) (0);};
 //BA.debugLineNum = 546;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLnbNiveaux\",n";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLnbNiveaux",(Object)(_nnbniveaux));
 //BA.debugLineNum = 547;BA.debugLine="If txtSurfComble.Text<>Null And txtSurfComble.Te";
if (mostCurrent._txtsurfcomble.getText()!= null && (mostCurrent._txtsurfcomble.getText()).equals("") == false) { 
_dsurfcomble = (int)(Double.parseDouble(mostCurrent._txtsurfcomble.getText()));}
else {
_dsurfcomble = (int) (0);};
 //BA.debugLineNum = 548;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLSurfaceCombl";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLSurfaceComble",(Object)(_dsurfcomble));
 //BA.debugLineNum = 549;BA.debugLine="If txtSurfReelle.Text<>Null And txtSurfReelle.Te";
if (mostCurrent._txtsurfreelle.getText()!= null && (mostCurrent._txtsurfreelle.getText()).equals("") == false) { 
_dsurfreelle = (int)(Double.parseDouble(mostCurrent._txtsurfreelle.getText()));}
else {
_dsurfreelle = (int) (0);};
 //BA.debugLineNum = 550;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLSurfaceCombl";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLSurfaceCombleReelle",(Object)(_dsurfreelle));
 //BA.debugLineNum = 551;BA.debugLine="If txtnbSacsUtilises.Text<>Null And txtnbSacsUti";
if (mostCurrent._txtnbsacsutilises.getText()!= null && (mostCurrent._txtnbsacsutilises.getText()).equals("") == false) { 
_dsacsutilises = (int)(Double.parseDouble(mostCurrent._txtnbsacsutilises.getText()));}
else {
_dsacsutilises = (int) (0);};
 //BA.debugLineNum = 552;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLnbSacsUtilis";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLnbSacsUtilises",(Object)(_dsacsutilises));
 //BA.debugLineNum = 553;BA.debugLine="If commentaire.Text<>Null And commentaire.Text<>";
if (mostCurrent._commentaire.getText()!= null && (mostCurrent._commentaire.getText()).equals("") == false) { 
_scomment = mostCurrent._commentaire.getText();}
else {
_scomment = "";};
 //BA.debugLineNum = 554;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"Commentaires\",s";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("Commentaires",(Object)(_scomment));
 //BA.debugLineNum = 555;BA.debugLine="If txtCommentApres.Text<>Null And txtCommentApre";
if (mostCurrent._txtcommentapres.getText()!= null && (mostCurrent._txtcommentapres.getText()).equals("") == false) { 
_scomment2 = mostCurrent._txtcommentapres.getText();}
else {
_scomment2 = "";};
 //BA.debugLineNum = 556;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"commentaires_ap";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("commentaires_apres",(Object)(_scomment2));
 //BA.debugLineNum = 559;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkCombleAme";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkCombleAmenageable",(Object)(mostCurrent._cbcombleamenag.getChecked()));
 //BA.debugLineNum = 560;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkPareVapeu";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkPareVapeur",(Object)(mostCurrent._cbparevapeur.getChecked()));
 //BA.debugLineNum = 561;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkTrappeCom";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkTrappeCombles",(Object)(mostCurrent._cbtrappecomble.getChecked()));
 //BA.debugLineNum = 562;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkRehausseV";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkRehausseVMC",(Object)(mostCurrent._cbrehaussevmc.getChecked()));
 //BA.debugLineNum = 563;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkRehausseT";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkRehausseTrappe",(Object)(mostCurrent._cbrehaussetrappe.getChecked()));
 //BA.debugLineNum = 564;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkEvacIsola";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkEvacIsolant",(Object)(mostCurrent._cbevacisolant.getChecked()));
 //BA.debugLineNum = 565;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkSpot\",cbS";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkSpot",(Object)(mostCurrent._cbspot.getChecked()));
 //BA.debugLineNum = 566;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkConduitCh";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkConduitCheminee",(Object)(mostCurrent._cbconduitcheminee.getChecked()));
 //BA.debugLineNum = 567;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkEcartAuFe";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkEcartAuFeu",(Object)(mostCurrent._cbecartfeu.getChecked()));
 //BA.debugLineNum = 568;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLchkComblesEn";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLchkComblesEncombres",(Object)(mostCurrent._cbcombleencombre.getChecked()));
 //BA.debugLineNum = 569;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLProgrammeMAG";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLProgrammeMAGE",(Object)(mostCurrent._cbprgmage.getChecked()));
 //BA.debugLineNum = 570;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLConnexionInt";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLConnexionInternet",(Object)(mostCurrent._cbcnxinternet.getChecked()));
 //BA.debugLineNum = 571;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLIsolable\",cb";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLIsolable",(Object)(mostCurrent._cbisolable.getChecked()));
 //BA.debugLineNum = 572;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"CBLPresenceVMC\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("CBLPresenceVMC",(Object)(mostCurrent._cbpresencevmc.getChecked()));
 //BA.debugLineNum = 576;BA.debugLine="If spnTypePorte.SelectedItem<>Null And spnTypePo";
if (mostCurrent._spntypeporte.getSelectedItem()!= null && (mostCurrent._spntypeporte.getSelectedItem()).equals("") == false) { 
_stypeporte = mostCurrent._spntypeporte.getSelectedItem();}
else {
_stypeporte = "";};
 //BA.debugLineNum = 577;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBTypePorte\",s";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBTypePorte",(Object)(_stypeporte));
 //BA.debugLineNum = 578;BA.debugLine="If spnTypePlancher.SelectedItem<>Null And spnTyp";
if (mostCurrent._spntypeplancher.getSelectedItem()!= null && (mostCurrent._spntypeplancher.getSelectedItem()).equals("") == false) { 
_stypeplancher = mostCurrent._spntypeplancher.getSelectedItem();}
else {
_stypeplancher = "";};
 //BA.debugLineNum = 579;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBTypePlancher";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBTypePlancher",(Object)(_stypeplancher));
 //BA.debugLineNum = 580;BA.debugLine="If spnTypeCloison.SelectedItem<>Null And spnType";
if (mostCurrent._spntypecloison.getSelectedItem()!= null && (mostCurrent._spntypecloison.getSelectedItem()).equals("") == false) { 
_stypecloison = mostCurrent._spntypecloison.getSelectedItem();}
else {
_stypecloison = "";};
 //BA.debugLineNum = 581;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBTypeCaveCloi";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBTypeCaveCloisonnement",(Object)(_stypecloison));
 //BA.debugLineNum = 582;BA.debugLine="If spnTypeLocal.SelectedItem<>Null And spnTypeLo";
if (mostCurrent._spntypelocal.getSelectedItem()!= null && (mostCurrent._spntypelocal.getSelectedItem()).equals("") == false) { 
_stypelocal = mostCurrent._spntypelocal.getSelectedItem();}
else {
_stypelocal = "";};
 //BA.debugLineNum = 583;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBtypeLocal\",s";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBtypeLocal",(Object)(_stypelocal));
 //BA.debugLineNum = 584;BA.debugLine="If spnTypeIsolant.SelectedItem<>Null And spnType";
if (mostCurrent._spntypeisolant.getSelectedItem()!= null && (mostCurrent._spntypeisolant.getSelectedItem()).equals("") == false) { 
_snatureisolant = mostCurrent._spntypeisolant.getSelectedItem();}
else {
_snatureisolant = "";};
 //BA.debugLineNum = 585;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBNatureIsolan";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBNatureIsolant",(Object)(_snatureisolant));
 //BA.debugLineNum = 586;BA.debugLine="If spnTypeReseauPlafond.SelectedItem<>Null And s";
if (mostCurrent._spntypereseauplafond.getSelectedItem()!= null && (mostCurrent._spntypereseauplafond.getSelectedItem()).equals("") == false) { 
_stypereseauplafond = mostCurrent._spntypereseauplafond.getSelectedItem();}
else {
_stypereseauplafond = "";};
 //BA.debugLineNum = 587;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBTypeReseauPl";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBTypeReseauPlafond",(Object)(_stypereseauplafond));
 //BA.debugLineNum = 588;BA.debugLine="If spnEtatIsolantPLB.SelectedItem<>Null And spnE";
if (mostCurrent._spnetatisolantplb.getSelectedItem()!= null && (mostCurrent._spnetatisolantplb.getSelectedItem()).equals("") == false) { 
_setatisolantplb = mostCurrent._spnetatisolantplb.getSelectedItem();}
else {
_setatisolantplb = "";};
 //BA.debugLineNum = 589;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBEtatIsolant\"";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBEtatIsolant",(Object)(_setatisolantplb));
 //BA.debugLineNum = 590;BA.debugLine="If spnResistanceIsolant.SelectedItem<>Null And s";
if (mostCurrent._spnresistanceisolant.getSelectedItem()!= null && (mostCurrent._spnresistanceisolant.getSelectedItem()).equals("") == false) { 
_sresistanceisolant = mostCurrent._spnresistanceisolant.getSelectedItem();}
else {
_sresistanceisolant = "";};
 //BA.debugLineNum = 591;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBResistanceIs";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBResistanceIsolant",(Object)(_sresistanceisolant));
 //BA.debugLineNum = 592;BA.debugLine="If spnAccesPCommunes.SelectedItem<>Null And spnA";
if (mostCurrent._spnaccespcommunes.getSelectedItem()!= null && (mostCurrent._spnaccespcommunes.getSelectedItem()).equals("") == false) { 
_saccespcommunes = mostCurrent._spnaccespcommunes.getSelectedItem();}
else {
_saccespcommunes = "";};
 //BA.debugLineNum = 593;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBAccessPCommu";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBAccessPCommunes",(Object)(_saccespcommunes));
 //BA.debugLineNum = 596;BA.debugLine="If txtHSPAvecIsolant.Text<>Null And txtHSPAvecIs";
if (mostCurrent._txthspavecisolant.getText()!= null && (mostCurrent._txthspavecisolant.getText()).equals("") == false) { 
_hspavecisolant = (int)(Double.parseDouble(mostCurrent._txthspavecisolant.getText()));}
else {
_hspavecisolant = (int) (0);};
 //BA.debugLineNum = 597;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBHSPAvecIsola";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBHSPAvecIsolant",(Object)(_hspavecisolant));
 //BA.debugLineNum = 598;BA.debugLine="If txtHSPsansIsolant.Text<>Null And txtHSPsansIs";
if (mostCurrent._txthspsansisolant.getText()!= null && (mostCurrent._txthspsansisolant.getText()).equals("") == false) { 
_hspsansisolant = (int)(Double.parseDouble(mostCurrent._txthspsansisolant.getText()));}
else {
_hspsansisolant = (int) (0);};
 //BA.debugLineNum = 599;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBHSPSansIsola";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBHSPSansIsolant",(Object)(_hspsansisolant));
 //BA.debugLineNum = 600;BA.debugLine="If txtHSPPorteIsolant.Text<>Null And txtHSPPorte";
if (mostCurrent._txthspporteisolant.getText()!= null && (mostCurrent._txthspporteisolant.getText()).equals("") == false) { 
_hspporteisolant = (int)(Double.parseDouble(mostCurrent._txthspporteisolant.getText()));}
else {
_hspporteisolant = (int) (0);};
 //BA.debugLineNum = 601;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBHSPPorteIsol";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBHSPPorteIsolant",(Object)(_hspporteisolant));
 //BA.debugLineNum = 602;BA.debugLine="If txtNbBat.Text<>Null And txtNbBat.Text<>\"\" The";
if (mostCurrent._txtnbbat.getText()!= null && (mostCurrent._txtnbbat.getText()).equals("") == false) { 
_nbbatiment = (int)(Double.parseDouble(mostCurrent._txtnbbat.getText()));}
else {
_nbbatiment = (int) (0);};
 //BA.debugLineNum = 603;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBnbBatiment\",";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBnbBatiment",(Object)(_nbbatiment));
 //BA.debugLineNum = 604;BA.debugLine="If txtNbBatImm.Text<>Null And txtNbBatImm.Text<>";
if (mostCurrent._txtnbbatimm.getText()!= null && (mostCurrent._txtnbbatimm.getText()).equals("") == false) { 
_nblogement = (int)(Double.parseDouble(mostCurrent._txtnbbatimm.getText()));}
else {
_nblogement = (int) (0);};
 //BA.debugLineNum = 605;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBnbLogementpa";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBnbLogementparBatiment",(Object)(_nblogement));
 //BA.debugLineNum = 606;BA.debugLine="If txtNbCageEsc.Text<>Null And txtNbCageEsc.Text";
if (mostCurrent._txtnbcageesc.getText()!= null && (mostCurrent._txtnbcageesc.getText()).equals("") == false) { 
_nnbcageesc = (int)(Double.parseDouble(mostCurrent._txtnbcageesc.getText()));}
else {
_nnbcageesc = (int) (0);};
 //BA.debugLineNum = 607;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBnbCageEscali";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBnbCageEscalier",(Object)(_nnbcageesc));
 //BA.debugLineNum = 611;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBbatCollectif";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBbatCollectif",(Object)(mostCurrent._cbcollectif.getChecked()));
 //BA.debugLineNum = 612;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBPointEauProc";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBPointEauProche",(Object)(mostCurrent._cbpointeauproche.getChecked()));
 //BA.debugLineNum = 613;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBAltLuminaire";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBAltLuminaires",(Object)(mostCurrent._cbaltluminaires.getChecked()));
 //BA.debugLineNum = 614;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBEnlevementNe";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBEnlevementNeon",(Object)(mostCurrent._cbenlevementneon.getChecked()));
 //BA.debugLineNum = 615;BA.debugLine="Main.fCursor.SetCurrentRowValue(\"PLBCaveEncombre";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("PLBCaveEncombree",(Object)(mostCurrent._cbcaveencombree.getChecked()));
 //BA.debugLineNum = 618;BA.debugLine="If curphoto1<>Null And curphoto1<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo1",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv4));};
 //BA.debugLineNum = 619;BA.debugLine="If curphoto2<>Null And curphoto2<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo2",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv1));};
 //BA.debugLineNum = 620;BA.debugLine="If curphoto3<>Null And curphoto3<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo3",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv5));};
 //BA.debugLineNum = 621;BA.debugLine="If curphoto4<>Null And curphoto4<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo4",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv6));};
 //BA.debugLineNum = 622;BA.debugLine="If curphoto5<>Null And curphoto5<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo5",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv7));};
 //BA.debugLineNum = 623;BA.debugLine="If curphoto6<>Null And curphoto6<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo6",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvv0));};
 //BA.debugLineNum = 624;BA.debugLine="If curphoto7<>Null And curphoto7<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo7",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv7));};
 //BA.debugLineNum = 625;BA.debugLine="If curphoto8<>Null And curphoto8<>\"\" Then Main.f";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo8",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv0));};
 //BA.debugLineNum = 629;BA.debugLine="If curphotop1<>Null And curphotop1<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo1apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv1));};
 //BA.debugLineNum = 630;BA.debugLine="If curphotop2<>Null And curphotop2<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo2apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv2));};
 //BA.debugLineNum = 631;BA.debugLine="If curphotop3<>Null And curphotop3<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo3apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv3));};
 //BA.debugLineNum = 632;BA.debugLine="If curphotop4<>Null And curphotop4<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo4apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv4));};
 //BA.debugLineNum = 633;BA.debugLine="If curphotop5<>Null And curphotop5<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo5apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv5));};
 //BA.debugLineNum = 634;BA.debugLine="If curphotop6<>Null And curphotop6<>\"\" Then Main";
if (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6!= null && (mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("photo6apres",(Object)(mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvv6));};
 //BA.debugLineNum = 641;BA.debugLine="If Main.manager.Getstring(\"user1\")<>Null And Mai";
if (mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("user1")!= null && (mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("user1")).equals("") == false) { 
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv2.SetCurrentRowValue("Operateur",(Object)(mostCurrent._vvvvvvvvvvvvvvv5._vvvvv1.GetString("user1")));};
 //BA.debugLineNum = 643;BA.debugLine="ToastMessageShow(\"Fiche SAVE OK\",False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Fiche SAVE OK"),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 644;BA.debugLine="IDMsg(\"SAVE\")";
_vvvvvvvvvvvvvvvvvvvvvvv7("SAVE");
 } 
       catch (Exception e130) {
			processBA.setLastException(e130); //BA.debugLineNum = 646;BA.debugLine="ToastMessageShow(\"Fiche SAVE ERROR\",False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Fiche SAVE ERROR"),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 647;BA.debugLine="Log(\"SaveFiche Update failed\")";
anywheresoftware.b4a.keywords.Common.Log("SaveFiche Update failed");
 };
 //BA.debugLineNum = 649;BA.debugLine="End Sub";
return "";
}
public static String  _spnarea_itemclick(int _position,Object _value) throws Exception{
 //BA.debugLineNum = 1264;BA.debugLine="Sub spnArea_ItemClick (Position As Int, Value As O";
 //BA.debugLineNum = 1265;BA.debugLine="curArea=spnArea.SelectedItem";
mostCurrent._vvvvvvvvvvvvvvvvvvvv3 = mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.getSelectedItem();
 //BA.debugLineNum = 1266;BA.debugLine="End Sub";
return "";
}
public static String  _tspreca_pageselected(int _position) throws Exception{
 //BA.debugLineNum = 2050;BA.debugLine="Sub tsPreca_PageSelected (Position As Int)";
 //BA.debugLineNum = 2051;BA.debugLine="If Position=4 Then";
if (_position==4) { 
 //BA.debugLineNum = 2053;BA.debugLine="tsPreca.ScrollTo(0,False)";
mostCurrent._tspreca.ScrollTo((int) (0),anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 2055;BA.debugLine="End Sub";
return "";
}
public static String  _updatephoto(String _tpath,String _tpic) throws Exception{
 //BA.debugLineNum = 1029;BA.debugLine="public Sub updatePhoto(tpath As String,tPic As Str";
 //BA.debugLineNum = 1030;BA.debugLine="Try";
try { //BA.debugLineNum = 1031;BA.debugLine="ImageView1.Bitmap=LoadBitmap(tpath, tPic)";
mostCurrent._imageview1.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(_tpath,_tpic).getObject()));
 } 
       catch (Exception e4) {
			processBA.setLastException(e4); //BA.debugLineNum = 1033;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 1036;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv5() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1166;BA.debugLine="Sub UpdateSpnAccesPCommunes";
 //BA.debugLineNum = 1167;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1168;BA.debugLine="spnAccesPCommunes.Clear";
mostCurrent._spnaccespcommunes.Clear();
 //BA.debugLineNum = 1169;BA.debugLine="Main.fTableAccesPCommunes.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv2.Reset();
 //BA.debugLineNum = 1170;BA.debugLine="For i=0 To Main.fTableAccesPCommunes.GetRowCount";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv2.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1171;BA.debugLine="Main.fTableAccesPCommunes.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv2.GetNextRow();
 //BA.debugLineNum = 1172;BA.debugLine="spnAccesPCommunes.Add(Main.fTableAccesPCommunes.";
mostCurrent._spnaccespcommunes.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv2.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1174;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvv1() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1074;BA.debugLine="Sub UpdateSpnArea";
 //BA.debugLineNum = 1076;BA.debugLine="spnArea.Clear";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.Clear();
 //BA.debugLineNum = 1077;BA.debugLine="Main.fTableLieux.Reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.Reset();
 //BA.debugLineNum = 1078;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1079;BA.debugLine="For i=0 To Main.fTableLieux.GetRowCount - 1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1080;BA.debugLine="Main.fTableLieux.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.GetNextRow();
 //BA.debugLineNum = 1081;BA.debugLine="spnArea.Add(Main.fTableLieux.GetColumnValue(\"Lib";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvv4.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvv3.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1083;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv3() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1146;BA.debugLine="Sub UpdateSpnEtatIsolantPLB";
 //BA.debugLineNum = 1147;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1148;BA.debugLine="spnEtatIsolantPLB.Clear";
mostCurrent._spnetatisolantplb.Clear();
 //BA.debugLineNum = 1149;BA.debugLine="Main.fTableEtatIsolant.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv0.Reset();
 //BA.debugLineNum = 1150;BA.debugLine="For i=0 To Main.fTableEtatIsolant.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv0.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1151;BA.debugLine="Main.fTableEtatIsolant.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv0.GetNextRow();
 //BA.debugLineNum = 1152;BA.debugLine="spnEtatIsolantPLB.Add(Main.fTableEtatIsolant.Get";
mostCurrent._spnetatisolantplb.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv0.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1154;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvv3() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1237;BA.debugLine="Sub UpdateSpnEtatPlafondCBL";
 //BA.debugLineNum = 1238;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1239;BA.debugLine="spnEtatPlafond.Clear";
mostCurrent._spnetatplafond.Clear();
 //BA.debugLineNum = 1240;BA.debugLine="Main.fTableEtatPlafond.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv2.Reset();
 //BA.debugLineNum = 1241;BA.debugLine="For i=0 To Main.fTableEtatPlafond.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv2.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1242;BA.debugLine="Main.fTableEtatPlafond.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv2.GetNextRow();
 //BA.debugLineNum = 1243;BA.debugLine="spnEtatPlafond.Add(Main.fTableEtatPlafond.GetCol";
mostCurrent._spnetatplafond.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv2.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1245;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvv2() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1227;BA.debugLine="Sub UpdateSpnMateriauPrevuCBL";
 //BA.debugLineNum = 1228;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1229;BA.debugLine="spnMateriauPrevu.Clear";
mostCurrent._spnmateriauprevu.Clear();
 //BA.debugLineNum = 1230;BA.debugLine="Main.fTableMateriauPrevu.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv0.Reset();
 //BA.debugLineNum = 1231;BA.debugLine="For i=0 To Main.fTableMateriauPrevu.GetRowCount -";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv0.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1232;BA.debugLine="Main.fTableMateriauPrevu.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv0.GetNextRow();
 //BA.debugLineNum = 1233;BA.debugLine="spnMateriauPrevu.Add(Main.fTableMateriauPrevu.Ge";
mostCurrent._spnmateriauprevu.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv0.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1235;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvv1() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1217;BA.debugLine="Sub UpdateSpnNatureIsolantCBL";
 //BA.debugLineNum = 1218;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1219;BA.debugLine="spnNatureIsolant.Clear";
mostCurrent._spnnatureisolant.Clear();
 //BA.debugLineNum = 1220;BA.debugLine="Main.fTableNatureIsolant.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv1.Reset();
 //BA.debugLineNum = 1221;BA.debugLine="For i=0 To Main.fTableNatureIsolant.GetRowCount -";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv1.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1222;BA.debugLine="Main.fTableNatureIsolant.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv1.GetNextRow();
 //BA.debugLineNum = 1223;BA.debugLine="spnNatureIsolant.Add(Main.fTableNatureIsolant.Ge";
mostCurrent._spnnatureisolant.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv1.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1225;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv4() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1156;BA.debugLine="Sub UpdateSpnResistanceThermique";
 //BA.debugLineNum = 1157;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1158;BA.debugLine="spnResistanceIsolant.Clear";
mostCurrent._spnresistanceisolant.Clear();
 //BA.debugLineNum = 1159;BA.debugLine="Main.fTableResistanceThermique.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv1.Reset();
 //BA.debugLineNum = 1160;BA.debugLine="For i=0 To Main.fTableResistanceThermique.GetRowC";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv1.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1161;BA.debugLine="Main.fTableResistanceThermique.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv1.GetNextRow();
 //BA.debugLineNum = 1162;BA.debugLine="spnResistanceIsolant.Add(Main.fTableResistanceTh";
mostCurrent._spnresistanceisolant.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv1.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1164;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvv7() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1126;BA.debugLine="Sub UpdateSpnTypeCloisonnement";
 //BA.debugLineNum = 1127;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1128;BA.debugLine="spnTypeCloison.Clear";
mostCurrent._spntypecloison.Clear();
 //BA.debugLineNum = 1129;BA.debugLine="Main.fTableTypeCloisonnement.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv6.Reset();
 //BA.debugLineNum = 1130;BA.debugLine="For i=0 To Main.fTableTypeCloisonnement.GetRowCou";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv6.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1131;BA.debugLine="Main.fTableTypeCloisonnement.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv6.GetNextRow();
 //BA.debugLineNum = 1132;BA.debugLine="spnTypeCloison.Add(Main.fTableTypeCloisonnement.";
mostCurrent._spntypecloison.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv6.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1134;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv1() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1116;BA.debugLine="Sub UpdateSpnTypeIsolant";
 //BA.debugLineNum = 1117;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1118;BA.debugLine="spnTypeIsolant.Clear";
mostCurrent._spntypeisolant.Clear();
 //BA.debugLineNum = 1119;BA.debugLine="Main.ftableTypeIsolant.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv3.Reset();
 //BA.debugLineNum = 1120;BA.debugLine="For i=0 To Main.ftableTypeIsolant.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv3.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1121;BA.debugLine="Main.ftableTypeIsolant.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv3.GetNextRow();
 //BA.debugLineNum = 1122;BA.debugLine="spnTypeIsolant.Add(Main.ftableTypeIsolant.GetCol";
mostCurrent._spntypeisolant.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv3.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1124;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv6() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1207;BA.debugLine="Sub UpdateSpnTypeIsolantCBL";
 //BA.debugLineNum = 1208;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1209;BA.debugLine="spnTypeIsolantComble.Clear";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv1.Clear();
 //BA.debugLineNum = 1210;BA.debugLine="Main.fTableCBLIsolant.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv5.Reset();
 //BA.debugLineNum = 1211;BA.debugLine="For i=0 To Main.fTableCBLIsolant.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv5.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1212;BA.debugLine="Main.fTableCBLIsolant.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv5.GetNextRow();
 //BA.debugLineNum = 1213;BA.debugLine="spnTypeIsolantComble.Add(Main.fTableCBLIsolant.G";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvv1.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv5.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1215;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvv0() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1106;BA.debugLine="Sub UpdateSpnTypeLocal";
 //BA.debugLineNum = 1107;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1108;BA.debugLine="spnTypeLocal.Clear";
mostCurrent._spntypelocal.Clear();
 //BA.debugLineNum = 1109;BA.debugLine="Main.ftableTypeLocal.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv7.Reset();
 //BA.debugLineNum = 1110;BA.debugLine="For i=0 To Main.ftableTypeLocal.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv7.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1111;BA.debugLine="Main.ftableTypeLocal.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv7.GetNextRow();
 //BA.debugLineNum = 1112;BA.debugLine="spnTypeLocal.Add(Main.ftableTypeLocal.GetColumnV";
mostCurrent._spntypelocal.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv7.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1114;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv6() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1177;BA.debugLine="Sub UpdateSpnTypeLogementCBL";
 //BA.debugLineNum = 1178;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1179;BA.debugLine="spnTypeLogement.Clear";
mostCurrent._spntypelogement.Clear();
 //BA.debugLineNum = 1180;BA.debugLine="Main.fTableTypeLogement.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv4.Reset();
 //BA.debugLineNum = 1181;BA.debugLine="For i=0 To Main.fTableTypeLogement.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv4.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1182;BA.debugLine="Main.fTableTypeLogement.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv4.GetNextRow();
 //BA.debugLineNum = 1183;BA.debugLine="spnTypeLogement.Add(Main.fTableTypeLogement.GetC";
mostCurrent._spntypelogement.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv4.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1185;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvv6() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1096;BA.debugLine="Sub UpdateSpnTypePlancher";
 //BA.debugLineNum = 1097;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1098;BA.debugLine="spnTypePlancher.Clear";
mostCurrent._spntypeplancher.Clear();
 //BA.debugLineNum = 1099;BA.debugLine="Main.ftableTypePlancher.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv5.Reset();
 //BA.debugLineNum = 1100;BA.debugLine="For i=0 To Main.ftableTypePlancher.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv5.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1101;BA.debugLine="Main.ftableTypePlancher.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv5.GetNextRow();
 //BA.debugLineNum = 1102;BA.debugLine="spnTypePlancher.Add(Main.ftableTypePlancher.GetC";
mostCurrent._spntypeplancher.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv5.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1104;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv0() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1197;BA.debugLine="Sub UpdateSpnTypePlancherCBL";
 //BA.debugLineNum = 1199;BA.debugLine="spnTypePlancherCBL.Clear";
mostCurrent._spntypeplanchercbl.Clear();
 //BA.debugLineNum = 1200;BA.debugLine="Main.fTableCBLPlancher.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv6.Reset();
 //BA.debugLineNum = 1201;BA.debugLine="For i=0 To Main.fTableCBLPlancher.GetRowCount -1";
{
final int step3 = 1;
final int limit3 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv6.GetRowCount()-1);
_i = (int) (0) ;
for (;(step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3) ;_i = ((int)(0 + _i + step3))  ) {
 //BA.debugLineNum = 1202;BA.debugLine="Main.fTableCBLPlancher.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv6.GetNextRow();
 //BA.debugLineNum = 1203;BA.debugLine="spnTypePlancherCBL.Add(Main.fTableCBLPlancher.Ge";
mostCurrent._spntypeplanchercbl.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv6.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1205;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvv5() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1086;BA.debugLine="Sub UpdateSpnTypePorte";
 //BA.debugLineNum = 1087;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1088;BA.debugLine="spnTypePorte.Clear";
mostCurrent._spntypeporte.Clear();
 //BA.debugLineNum = 1089;BA.debugLine="Main.fTableTypePorte.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv4.Reset();
 //BA.debugLineNum = 1090;BA.debugLine="For i=0 To Main.fTableTypePorte.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv4.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1091;BA.debugLine="Main.fTableTypePorte.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv4.GetNextRow();
 //BA.debugLineNum = 1092;BA.debugLine="spnTypePorte.Add(Main.fTableTypePorte.GetColumnV";
mostCurrent._spntypeporte.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvv4.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1094;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv2() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1136;BA.debugLine="Sub UpdateSpnTypeReseauPlafond";
 //BA.debugLineNum = 1137;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1138;BA.debugLine="spnTypeReseauPlafond.Clear";
mostCurrent._spntypereseauplafond.Clear();
 //BA.debugLineNum = 1139;BA.debugLine="Main.fTabletypeReseauPlafond.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv7.Reset();
 //BA.debugLineNum = 1140;BA.debugLine="For i=0 To Main.fTabletypeReseauPlafond.GetRowCou";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv7.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1141;BA.debugLine="Main.fTabletypeReseauPlafond.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv7.GetNextRow();
 //BA.debugLineNum = 1142;BA.debugLine="spnTypeReseauPlafond.Add(Main.fTabletypeReseauPl";
mostCurrent._spntypereseauplafond.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv7.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1144;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvvvvv7() throws Exception{
int _i = 0;
 //BA.debugLineNum = 1187;BA.debugLine="Sub UpdateSpnTypeToitCBL";
 //BA.debugLineNum = 1188;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1189;BA.debugLine="spnTypeToit.Clear";
mostCurrent._spntypetoit.Clear();
 //BA.debugLineNum = 1190;BA.debugLine="Main.fTableTypeToit.reset";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv3.Reset();
 //BA.debugLineNum = 1191;BA.debugLine="For i=0 To Main.fTableTypeToit.GetRowCount -1";
{
final int step4 = 1;
final int limit4 = (int) (mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv3.GetRowCount()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 1192;BA.debugLine="Main.fTableTypeToit.GetNextRow";
mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv3.GetNextRow();
 //BA.debugLineNum = 1193;BA.debugLine="spnTypeToit.Add(Main.fTableTypeToit.GetColumnVal";
mostCurrent._spntypetoit.Add(BA.ObjectToString(mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvv3.GetColumnValue("Libelle")));
 }
};
 //BA.debugLineNum = 1195;BA.debugLine="End Sub";
return "";
}
public static int  _vvvvvvvvvvvvvvvvvvvvvvvv5(boolean _showmsg) throws Exception{
int _isvalide = 0;
String _smsg = "";
int _i = 0;
 //BA.debugLineNum = 1609;BA.debugLine="Sub ValideCBL(showMsg As Boolean) As Int";
 //BA.debugLineNum = 1612;BA.debugLine="Dim isValide As Int ,sMsg As String";
_isvalide = 0;
_smsg = "";
 //BA.debugLineNum = 1613;BA.debugLine="isValide=1";
_isvalide = (int) (1);
 //BA.debugLineNum = 1614;BA.debugLine="sMsg=\"\"";
_smsg = "";
 //BA.debugLineNum = 1616;BA.debugLine="If spnTypeLogement.SelectedItem=Null Or spnTypeLo";
if (mostCurrent._spntypelogement.getSelectedItem()== null || (mostCurrent._spntypelogement.getSelectedItem()).equals("") || (mostCurrent._spntypelogement.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1617;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1618;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type logement non rens";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type logement non renseigné";
 };
 //BA.debugLineNum = 1620;BA.debugLine="If spnNatureIsolant.SelectedItem=Null Or spnNatur";
if (mostCurrent._spnnatureisolant.getSelectedItem()== null || (mostCurrent._spnnatureisolant.getSelectedItem()).equals("") || (mostCurrent._spnnatureisolant.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1621;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1622;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Isolant en place non r";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Isolant en place non renseigné";
 };
 //BA.debugLineNum = 1624;BA.debugLine="If spnTypeToit.SelectedItem=Null Or spnTypeToit.S";
if (mostCurrent._spntypetoit.getSelectedItem()== null || (mostCurrent._spntypetoit.getSelectedItem()).equals("") || (mostCurrent._spntypetoit.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1625;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1626;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type toit non renseign";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type toit non renseigné";
 };
 //BA.debugLineNum = 1628;BA.debugLine="If spnMateriauPrevu.SelectedItem=Null Or spnMater";
if (mostCurrent._spnmateriauprevu.getSelectedItem()== null || (mostCurrent._spnmateriauprevu.getSelectedItem()).equals("") || (mostCurrent._spnmateriauprevu.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1629;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1630;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Matériau prévu non ren";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Matériau prévu non renseigné";
 };
 //BA.debugLineNum = 1632;BA.debugLine="If spnEtatPlafond.SelectedItem=Null Or spnEtatPla";
if (mostCurrent._spnetatplafond.getSelectedItem()== null || (mostCurrent._spnetatplafond.getSelectedItem()).equals("") || (mostCurrent._spnetatplafond.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1633;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1634;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Etat plafond non rense";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Etat plafond non renseigné";
 };
 //BA.debugLineNum = 1636;BA.debugLine="If spnTypePlancherCBL.SelectedItem=Null Or spnTyp";
if (mostCurrent._spntypeplanchercbl.getSelectedItem()== null || (mostCurrent._spntypeplanchercbl.getSelectedItem()).equals("") || (mostCurrent._spntypeplanchercbl.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1637;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1638;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type plancher non rens";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type plancher non renseigné";
 };
 //BA.debugLineNum = 1642;BA.debugLine="If txtNbNiveaux.Text=Null Or txtNbNiveaux.Text=\"\"";
if (mostCurrent._txtnbniveaux.getText()== null || (mostCurrent._txtnbniveaux.getText()).equals("") || (mostCurrent._txtnbniveaux.getText()).equals("0")) { 
 //BA.debugLineNum = 1643;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1644;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Nb niveaux vide ou 0\"";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Nb niveaux vide ou 0";
 };
 //BA.debugLineNum = 1646;BA.debugLine="If txtHsousGoutiere.Text=Null Or txtHsousGoutiere";
if (mostCurrent._txthsousgoutiere.getText()== null || (mostCurrent._txthsousgoutiere.getText()).equals("") || (mostCurrent._txthsousgoutiere.getText()).equals("0")) { 
 //BA.debugLineNum = 1647;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1648;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Ht ss gouttière vide o";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Ht ss gouttière vide ou 0";
 };
 //BA.debugLineNum = 1650;BA.debugLine="If txtSurfComble.Text=Null Or txtSurfComble.Text=";
if (mostCurrent._txtsurfcomble.getText()== null || (mostCurrent._txtsurfcomble.getText()).equals("") || (mostCurrent._txtsurfcomble.getText()).equals("0")) { 
 //BA.debugLineNum = 1651;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1652;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Surface combles vide o";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Surface combles vide ou 0";
 };
 //BA.debugLineNum = 1654;BA.debugLine="If txtSurfReelle.Text=Null Or txtSurfReelle.Text=";
if (mostCurrent._txtsurfreelle.getText()== null || (mostCurrent._txtsurfreelle.getText()).equals("") || (mostCurrent._txtsurfreelle.getText()).equals("0")) { 
 //BA.debugLineNum = 1655;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1656;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Surface réelle vide ou";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Surface réelle vide ou 0";
 };
 //BA.debugLineNum = 1658;BA.debugLine="If isValide=0 And showMsg=True Then";
if (_isvalide==0 && _showmsg==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 1659;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 1660;BA.debugLine="i = Msgbox2(\"Certains champs sont erronés : \" &";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Certains champs sont erronés : "+_smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+"Voulez-vous enregistrer quand même ?"),BA.ObjectToCharSequence("Validation du relevé"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 1661;BA.debugLine="If i=DialogResponse.POSITIVE Then isValide=2";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
_isvalide = (int) (2);};
 };
 //BA.debugLineNum = 1663;BA.debugLine="Return isValide";
if (true) return _isvalide;
 //BA.debugLineNum = 1664;BA.debugLine="End Sub";
return 0;
}
public static int  _vvvvvvvvvvvvvvvvvvvvvvvv6(boolean _showmsg) throws Exception{
int _isvalide = 0;
String _smsg = "";
int _i = 0;
 //BA.debugLineNum = 1959;BA.debugLine="Sub ValidePLB(showMsg As Boolean) As Int";
 //BA.debugLineNum = 1962;BA.debugLine="Dim isValide As Int ,sMsg As String";
_isvalide = 0;
_smsg = "";
 //BA.debugLineNum = 1963;BA.debugLine="isValide=1";
_isvalide = (int) (1);
 //BA.debugLineNum = 1964;BA.debugLine="sMsg=\"\"";
_smsg = "";
 //BA.debugLineNum = 1966;BA.debugLine="If spnTypePorte.SelectedItem=Null Or spnTypePorte";
if (mostCurrent._spntypeporte.getSelectedItem()== null || (mostCurrent._spntypeporte.getSelectedItem()).equals("") || (mostCurrent._spntypeporte.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1967;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1968;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type porte non renseig";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type porte non renseigné";
 };
 //BA.debugLineNum = 1970;BA.debugLine="If spnTypePlancher.SelectedItem=Null Or spnTypePl";
if (mostCurrent._spntypeplancher.getSelectedItem()== null || (mostCurrent._spntypeplancher.getSelectedItem()).equals("") || (mostCurrent._spntypeplancher.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1971;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1972;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type plancher non rens";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type plancher non renseigné";
 };
 //BA.debugLineNum = 1974;BA.debugLine="If spnTypeCloison.SelectedItem=Null Or spnTypeClo";
if (mostCurrent._spntypecloison.getSelectedItem()== null || (mostCurrent._spntypecloison.getSelectedItem()).equals("") || (mostCurrent._spntypecloison.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1975;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1976;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type cloison non rense";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type cloison non renseigné";
 };
 //BA.debugLineNum = 1978;BA.debugLine="If spnTypeLocal.SelectedItem=Null Or spnTypeLocal";
if (mostCurrent._spntypelocal.getSelectedItem()== null || (mostCurrent._spntypelocal.getSelectedItem()).equals("") || (mostCurrent._spntypelocal.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1979;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1980;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type local non renseig";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type local non renseigné";
 };
 //BA.debugLineNum = 1982;BA.debugLine="If spnTypeIsolant.SelectedItem=Null Or spnTypeIso";
if (mostCurrent._spntypeisolant.getSelectedItem()== null || (mostCurrent._spntypeisolant.getSelectedItem()).equals("") || (mostCurrent._spntypeisolant.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1983;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1984;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Type isolant non rense";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Type isolant non renseigné";
 };
 //BA.debugLineNum = 1986;BA.debugLine="If spnEtatIsolantPLB.SelectedItem=Null Or spnEtat";
if (mostCurrent._spnetatisolantplb.getSelectedItem()== null || (mostCurrent._spnetatisolantplb.getSelectedItem()).equals("") || (mostCurrent._spnetatisolantplb.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1987;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1988;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Etat isolant non rense";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Etat isolant non renseigné";
 };
 //BA.debugLineNum = 1990;BA.debugLine="If spnResistanceIsolant.SelectedItem=Null Or spnR";
if (mostCurrent._spnresistanceisolant.getSelectedItem()== null || (mostCurrent._spnresistanceisolant.getSelectedItem()).equals("") || (mostCurrent._spnresistanceisolant.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1991;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1992;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Résistance isolant non";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Résistance isolant non renseignée";
 };
 //BA.debugLineNum = 1994;BA.debugLine="If spnAccesPCommunes.SelectedItem=Null Or spnAcce";
if (mostCurrent._spnaccespcommunes.getSelectedItem()== null || (mostCurrent._spnaccespcommunes.getSelectedItem()).equals("") || (mostCurrent._spnaccespcommunes.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1995;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 1996;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Parties communes non r";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Parties communes non renseigné";
 };
 //BA.debugLineNum = 1998;BA.debugLine="If spnTypeReseauPlafond.SelectedItem=Null Or spnT";
if (mostCurrent._spntypereseauplafond.getSelectedItem()== null || (mostCurrent._spntypereseauplafond.getSelectedItem()).equals("") || (mostCurrent._spntypereseauplafond.getSelectedItem()).equals(" ")) { 
 //BA.debugLineNum = 1999;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2000;BA.debugLine="sMsg=sMsg & Chr(10) & \" - Réseau plafond non ren";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - Réseau plafond non renseigné";
 };
 //BA.debugLineNum = 2004;BA.debugLine="If txtHSPsansIsolant.Text=Null Or txtHSPsansIsola";
if (mostCurrent._txthspsansisolant.getText()== null || (mostCurrent._txthspsansisolant.getText()).equals("") || (mostCurrent._txthspsansisolant.getText()).equals("0")) { 
 //BA.debugLineNum = 2005;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2006;BA.debugLine="sMsg=sMsg & Chr(10) & \" - HSP sans isolant vide";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - HSP sans isolant vide ou 0";
 };
 //BA.debugLineNum = 2008;BA.debugLine="If txtHSPAvecIsolant.Text=Null Or txtHSPAvecIsola";
if (mostCurrent._txthspavecisolant.getText()== null || (mostCurrent._txthspavecisolant.getText()).equals("") || (mostCurrent._txthspavecisolant.getText()).equals("0")) { 
 //BA.debugLineNum = 2009;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2010;BA.debugLine="sMsg=sMsg & Chr(10) & \" - HSP avec isolant vide";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - HSP avec isolant vide ou 0";
 };
 //BA.debugLineNum = 2012;BA.debugLine="If txtHSPPorteIsolant.Text=Null Or txtHSPPorteIso";
if (mostCurrent._txthspporteisolant.getText()== null || (mostCurrent._txthspporteisolant.getText()).equals("") || (mostCurrent._txthspporteisolant.getText()).equals("0")) { 
 //BA.debugLineNum = 2013;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2014;BA.debugLine="sMsg=sMsg & Chr(10) & \" - HSP plafond porte vide";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - HSP plafond porte vide ou 0";
 };
 //BA.debugLineNum = 2016;BA.debugLine="If txtNbCageEsc.Text=Null Or txtNbCageEsc.Text=\"\"";
if (mostCurrent._txtnbcageesc.getText()== null || (mostCurrent._txtnbcageesc.getText()).equals("") || (mostCurrent._txtnbcageesc.getText()).equals("0")) { 
 //BA.debugLineNum = 2017;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2018;BA.debugLine="sMsg=sMsg & Chr(10) & \" - nb cages escalier vide";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - nb cages escalier vide ou 0";
 };
 //BA.debugLineNum = 2020;BA.debugLine="If txtNbBat.Text=Null Or txtNbBat.Text=\"\" Or txtN";
if (mostCurrent._txtnbbat.getText()== null || (mostCurrent._txtnbbat.getText()).equals("") || (mostCurrent._txtnbbat.getText()).equals("0")) { 
 //BA.debugLineNum = 2021;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2022;BA.debugLine="sMsg=sMsg & Chr(10) & \" - nb batiments vide ou 0";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - nb batiments vide ou 0";
 };
 //BA.debugLineNum = 2024;BA.debugLine="If txtNbBatImm.Text=Null Or txtNbBatImm.Text=\"\" O";
if (mostCurrent._txtnbbatimm.getText()== null || (mostCurrent._txtnbbatimm.getText()).equals("") || (mostCurrent._txtnbbatimm.getText()).equals("0")) { 
 //BA.debugLineNum = 2025;BA.debugLine="isValide=0";
_isvalide = (int) (0);
 //BA.debugLineNum = 2026;BA.debugLine="sMsg=sMsg & Chr(10) & \" - nb logements/batiment";
_smsg = _smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+" - nb logements/batiment vide ou 0";
 };
 //BA.debugLineNum = 2028;BA.debugLine="If isValide=0 And showMsg=True Then";
if (_isvalide==0 && _showmsg==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 2029;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 2030;BA.debugLine="i = Msgbox2(\"Certains champs sont erronés : \" &";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Certains champs sont erronés : "+_smsg+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (10)))+"Voulez-vous enregistrer quand même ?"),BA.ObjectToCharSequence("Validation du relevé"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 2031;BA.debugLine="If i=DialogResponse.POSITIVE Then isValide=2";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
_isvalide = (int) (2);};
 };
 //BA.debugLineNum = 2033;BA.debugLine="Return isValide";
if (true) return _isvalide;
 //BA.debugLineNum = 2034;BA.debugLine="End Sub";
return 0;
}
}
