package precarite.cns.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_dessin{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
//BA.debugLineNum = 2;BA.debugLine="If ActivitySize < 3.2 Then"[dessin/General script]
if ((anywheresoftware.b4a.keywords.LayoutBuilder.getScreenSize()<3.2d)) { 
;
//BA.debugLineNum = 3;BA.debugLine="AutoScaleRate(1)"[dessin/General script]
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(1d);
//BA.debugLineNum = 4;BA.debugLine="End If"[dessin/General script]
;};
//BA.debugLineNum = 6;BA.debugLine="AutoScaleAll"[dessin/General script]
anywheresoftware.b4a.keywords.LayoutBuilder.scaleAll(views);

}
}