package precarite.cns;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class cameraexclass extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    private static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new BA(_ba, this, htSubs, "precarite.cns.cameraexclass");
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            
        }
        if (BA.isShellModeRuntimeCheck(ba)) 
			   this.getClass().getMethod("_class_globals", precarite.cns.cameraexclass.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 public anywheresoftware.b4a.keywords.Common __c = null;
public Object _vvvvvvvvvvvvvv6 = null;
public anywheresoftware.b4a.objects.CameraW _vvvvvvvvvvvvvv7 = null;
public anywheresoftware.b4a.agraham.reflection.Reflection _vvvvvvvvvvvvvv0 = null;
public Object _vvvvvvvvvvvvvvv1 = null;
public String _vvvvvvvvvvvvvvv2 = "";
public boolean _vvvvvvvvvvvvvvv3 = false;
public Object _vvvvvvvvvvvvvvv4 = null;
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;
public static class _camerainfoandid{
public boolean IsInitialized;
public Object CameraInfo;
public int Id;
public void Initialize() {
IsInitialized = true;
CameraInfo = new Object();
Id = 0;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static class _camerasize{
public boolean IsInitialized;
public int Width;
public int Height;
public void Initialize() {
IsInitialized = true;
Width = 0;
Height = 0;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public String  _camera_focusdone(boolean _success) throws Exception{
 //BA.debugLineNum = 409;BA.debugLine="Private Sub Camera_FocusDone (Success As Boolean)";
 //BA.debugLineNum = 410;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 411;BA.debugLine="TakePicture";
_vvvvvvvvvvvvv6();
 }else {
 //BA.debugLineNum = 413;BA.debugLine="Log(\"AutoFocus error.\")";
__c.Log("AutoFocus error.");
 };
 //BA.debugLineNum = 415;BA.debugLine="End Sub";
return "";
}
public String  _camera_picturetaken(byte[] _data) throws Exception{
 //BA.debugLineNum = 109;BA.debugLine="Private Sub Camera_PictureTaken (Data() As Byte)";
 //BA.debugLineNum = 110;BA.debugLine="CallSub2(target, event & \"_PictureTaken\", Data)";
__c.CallSubNew2(ba,_vvvvvvvvvvvvvvv1,_vvvvvvvvvvvvvvv2+"_PictureTaken",(Object)(_data));
 //BA.debugLineNum = 111;BA.debugLine="End Sub";
return "";
}
public String  _camera_preview(byte[] _data) throws Exception{
 //BA.debugLineNum = 99;BA.debugLine="Sub Camera_Preview (Data() As Byte)";
 //BA.debugLineNum = 100;BA.debugLine="If SubExists(target, event & \"_preview\") Then";
if (__c.SubExists(ba,_vvvvvvvvvvvvvvv1,_vvvvvvvvvvvvvvv2+"_preview")) { 
 //BA.debugLineNum = 101;BA.debugLine="CallSub2(target, event & \"_preview\", Data)";
__c.CallSubNew2(ba,_vvvvvvvvvvvvvvv1,_vvvvvvvvvvvvvvv2+"_preview",(Object)(_data));
 };
 //BA.debugLineNum = 103;BA.debugLine="End Sub";
return "";
}
public String  _camera_ready(boolean _success) throws Exception{
 //BA.debugLineNum = 86;BA.debugLine="Private Sub Camera_Ready (Success As Boolean)";
 //BA.debugLineNum = 87;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 88;BA.debugLine="r.target = cam";
_vvvvvvvvvvvvvv0.Target = (Object)(_vvvvvvvvvvvvvv7);
 //BA.debugLineNum = 89;BA.debugLine="nativeCam = r.GetField(\"camera\")";
_vvvvvvvvvvvvvv6 = _vvvvvvvvvvvvvv0.GetField("camera");
 //BA.debugLineNum = 90;BA.debugLine="r.target = nativeCam";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv6;
 //BA.debugLineNum = 91;BA.debugLine="parameters = r.RunMethod(\"getParameters\")";
_vvvvvvvvvvvvvvv4 = _vvvvvvvvvvvvvv0.RunMethod("getParameters");
 //BA.debugLineNum = 92;BA.debugLine="SetDisplayOrientation";
_vvvvvvvvvvvv2();
 }else {
 //BA.debugLineNum = 94;BA.debugLine="Log(\"success = false, \" & LastException)";
__c.Log("success = false, "+BA.ObjectToString(__c.LastException(ba)));
 };
 //BA.debugLineNum = 96;BA.debugLine="CallSub2(target, event & \"_ready\", Success)";
__c.CallSubNew2(ba,_vvvvvvvvvvvvvvv1,_vvvvvvvvvvvvvvv2+"_ready",(Object)(_success));
 //BA.debugLineNum = 97;BA.debugLine="End Sub";
return "";
}
public String  _class_globals() throws Exception{
 //BA.debugLineNum = 10;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 11;BA.debugLine="Private nativeCam As Object";
_vvvvvvvvvvvvvv6 = new Object();
 //BA.debugLineNum = 12;BA.debugLine="Private cam As Camera";
_vvvvvvvvvvvvvv7 = new anywheresoftware.b4a.objects.CameraW();
 //BA.debugLineNum = 13;BA.debugLine="Private r As Reflector";
_vvvvvvvvvvvvvv0 = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 14;BA.debugLine="Private target As Object";
_vvvvvvvvvvvvvvv1 = new Object();
 //BA.debugLineNum = 15;BA.debugLine="Private event As String";
_vvvvvvvvvvvvvvv2 = "";
 //BA.debugLineNum = 16;BA.debugLine="Public Front As Boolean";
_vvvvvvvvvvvvvvv3 = false;
 //BA.debugLineNum = 17;BA.debugLine="Type CameraInfoAndId (CameraInfo As Object, Id As";
;
 //BA.debugLineNum = 18;BA.debugLine="Type CameraSize (Width As Int, Height As Int)";
;
 //BA.debugLineNum = 19;BA.debugLine="Private parameters As Object";
_vvvvvvvvvvvvvvv4 = new Object();
 //BA.debugLineNum = 20;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvv6() throws Exception{
 //BA.debugLineNum = 398;BA.debugLine="Public Sub CloseNow";
 //BA.debugLineNum = 399;BA.debugLine="cam.Release";
_vvvvvvvvvvvvvv7.Release();
 //BA.debugLineNum = 400;BA.debugLine="r.target = cam";
_vvvvvvvvvvvvvv0.Target = (Object)(_vvvvvvvvvvvvvv7);
 //BA.debugLineNum = 401;BA.debugLine="r.RunMethod2(\"releaseCameras\", True, \"java.lang.b";
_vvvvvvvvvvvvvv0.RunMethod2("releaseCameras",BA.ObjectToString(__c.True),"java.lang.boolean");
 //BA.debugLineNum = 402;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvv7() throws Exception{
 //BA.debugLineNum = 142;BA.debugLine="Public Sub CommitParameters";
 //BA.debugLineNum = 143;BA.debugLine="Try";
try { //BA.debugLineNum = 144;BA.debugLine="r.target = nativeCam";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv6;
 //BA.debugLineNum = 145;BA.debugLine="r.RunMethod4(\"setParameters\", Array As Object(pa";
_vvvvvvvvvvvvvv0.RunMethod4("setParameters",new Object[]{_vvvvvvvvvvvvvvv4},new String[]{"android.hardware.Camera$Parameters"});
 } 
       catch (Exception e5) {
			ba.setLastException(e5); //BA.debugLineNum = 147;BA.debugLine="Log(\"Error setting parameters.\")";
__c.Log("Error setting parameters.");
 //BA.debugLineNum = 148;BA.debugLine="Log(LastException)";
__c.Log(BA.ObjectToString(__c.LastException(ba)));
 };
 //BA.debugLineNum = 150;BA.debugLine="End Sub";
return "";
}
public precarite.cns.cameraexclass._camerainfoandid  _vvvvvvvv0(boolean _frontcamera) throws Exception{
precarite.cns.cameraexclass._camerainfoandid _ci = null;
Object _camerainfo = null;
int _cameravalue = 0;
int _numberofcameras = 0;
int _i = 0;
 //BA.debugLineNum = 39;BA.debugLine="Private Sub FindCamera (frontCamera As Boolean) As";
 //BA.debugLineNum = 40;BA.debugLine="Dim ci As CameraInfoAndId";
_ci = new precarite.cns.cameraexclass._camerainfoandid();
 //BA.debugLineNum = 41;BA.debugLine="Dim cameraInfo As Object";
_camerainfo = new Object();
 //BA.debugLineNum = 42;BA.debugLine="Dim cameraValue As Int";
_cameravalue = 0;
 //BA.debugLineNum = 43;BA.debugLine="If frontCamera Then cameraValue = 1 Else cameraVa";
if (_frontcamera) { 
_cameravalue = (int) (1);}
else {
_cameravalue = (int) (0);};
 //BA.debugLineNum = 44;BA.debugLine="cameraInfo = r.CreateObject(\"android.hardware.Cam";
_camerainfo = _vvvvvvvvvvvvvv0.CreateObject("android.hardware.Camera$CameraInfo");
 //BA.debugLineNum = 45;BA.debugLine="Dim numberOfCameras As Int = r.RunStaticMethod(\"a";
_numberofcameras = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunStaticMethod("android.hardware.Camera","getNumberOfCameras",(Object[])(__c.Null),(String[])(__c.Null))));
 //BA.debugLineNum = 46;BA.debugLine="For i = 0 To numberOfCameras - 1";
{
final int step7 = 1;
final int limit7 = (int) (_numberofcameras-1);
_i = (int) (0) ;
for (;(step7 > 0 && _i <= limit7) || (step7 < 0 && _i >= limit7) ;_i = ((int)(0 + _i + step7))  ) {
 //BA.debugLineNum = 47;BA.debugLine="r.RunStaticMethod(\"android.hardware.Camera\", \"ge";
_vvvvvvvvvvvvvv0.RunStaticMethod("android.hardware.Camera","getCameraInfo",new Object[]{(Object)(_i),_camerainfo},new String[]{"java.lang.int","android.hardware.Camera$CameraInfo"});
 //BA.debugLineNum = 49;BA.debugLine="r.target = cameraInfo";
_vvvvvvvvvvvvvv0.Target = _camerainfo;
 //BA.debugLineNum = 50;BA.debugLine="If r.GetField(\"facing\") = cameraValue Then";
if ((_vvvvvvvvvvvvvv0.GetField("facing")).equals((Object)(_cameravalue))) { 
 //BA.debugLineNum = 51;BA.debugLine="ci.cameraInfo = r.target";
_ci.CameraInfo = _vvvvvvvvvvvvvv0.Target;
 //BA.debugLineNum = 52;BA.debugLine="ci.Id = i";
_ci.Id = _i;
 //BA.debugLineNum = 53;BA.debugLine="Return ci";
if (true) return _ci;
 };
 }
};
 //BA.debugLineNum = 56;BA.debugLine="ci.id = -1";
_ci.Id = (int) (-1);
 //BA.debugLineNum = 57;BA.debugLine="Return ci";
if (true) return _ci;
 //BA.debugLineNum = 58;BA.debugLine="End Sub";
return null;
}
public String  _vvvvvvvvv1() throws Exception{
 //BA.debugLineNum = 405;BA.debugLine="Public Sub FocusAndTakePicture";
 //BA.debugLineNum = 406;BA.debugLine="cam.Autofocus";
_vvvvvvvvvvvvvv7.AutoFocus();
 //BA.debugLineNum = 407;BA.debugLine="End Sub";
return "";
}
public String  _getvvvvvvvvvvvvv7() throws Exception{
 //BA.debugLineNum = 154;BA.debugLine="Public Sub getColorEffect As String";
 //BA.debugLineNum = 155;BA.debugLine="Return GetParameter(\"effect\")";
if (true) return _vvvvvvvvv0("effect");
 //BA.debugLineNum = 156;BA.debugLine="End Sub";
return "";
}
public String  _getvvvvvvvvvvvvv0() throws Exception{
 //BA.debugLineNum = 276;BA.debugLine="Public Sub getFlashMode As String";
 //BA.debugLineNum = 277;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 278;BA.debugLine="Return r.RunMethod(\"getFlashMode\")";
if (true) return BA.ObjectToString(_vvvvvvvvvvvvvv0.RunMethod("getFlashMode"));
 //BA.debugLineNum = 279;BA.debugLine="End Sub";
return "";
}
public float[]  _vvvvvvvvv4() throws Exception{
float[] _f = null;
 //BA.debugLineNum = 389;BA.debugLine="Public Sub GetFocusDistances As Float()";
 //BA.debugLineNum = 390;BA.debugLine="Dim F(3) As Float";
_f = new float[(int) (3)];
;
 //BA.debugLineNum = 391;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 392;BA.debugLine="r.RunMethod4(\"getFocusDistances\", Array As Object";
_vvvvvvvvvvvvvv0.RunMethod4("getFocusDistances",new Object[]{(Object)(_f)},new String[]{"[F"});
 //BA.debugLineNum = 393;BA.debugLine="Return F";
if (true) return _f;
 //BA.debugLineNum = 394;BA.debugLine="End Sub";
return null;
}
public String  _getvvvvvvvvvvvvvv1() throws Exception{
 //BA.debugLineNum = 354;BA.debugLine="Public Sub getFocusMode As String";
 //BA.debugLineNum = 355;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 356;BA.debugLine="Return r.RunMethod(\"getFocusMode\")";
if (true) return BA.ObjectToString(_vvvvvvvvvvvvvv0.RunMethod("getFocusMode"));
 //BA.debugLineNum = 357;BA.debugLine="End Sub";
return "";
}
public int  _getvvvvvvvvvvvvvv2() throws Exception{
 //BA.debugLineNum = 220;BA.debugLine="Public Sub getJpegQuality() As Int";
 //BA.debugLineNum = 221;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 222;BA.debugLine="Return r.RunMethod(\"getJpegQuality\")";
if (true) return (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunMethod("getJpegQuality")));
 //BA.debugLineNum = 223;BA.debugLine="End Sub";
return 0;
}
public int  _vvvvvvvvv7() throws Exception{
 //BA.debugLineNum = 258;BA.debugLine="Public Sub GetMaxZoom() As Int";
 //BA.debugLineNum = 259;BA.debugLine="If isZoomSupported Then";
if (_vvvvvvvvvvv4()) { 
 //BA.debugLineNum = 260;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 261;BA.debugLine="Return r.RunMethod(\"getMaxZoom\")";
if (true) return (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunMethod("getMaxZoom")));
 }else {
 //BA.debugLineNum = 264;BA.debugLine="Return 0";
if (true) return (int) (0);
 };
 //BA.debugLineNum = 266;BA.debugLine="End Sub";
return 0;
}
public String  _vvvvvvvvv0(String _key) throws Exception{
 //BA.debugLineNum = 132;BA.debugLine="Public Sub GetParameter(Key As String) As String";
 //BA.debugLineNum = 133;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 134;BA.debugLine="Return r.RunMethod2(\"get\", Key, \"java.lang.String";
if (true) return BA.ObjectToString(_vvvvvvvvvvvvvv0.RunMethod2("get",_key,"java.lang.String"));
 //BA.debugLineNum = 135;BA.debugLine="End Sub";
return "";
}
public precarite.cns.cameraexclass._camerasize  _vvvvvvvvvv1() throws Exception{
precarite.cns.cameraexclass._camerasize _cs = null;
 //BA.debugLineNum = 190;BA.debugLine="Public Sub GetPictureSize As CameraSize";
 //BA.debugLineNum = 191;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 192;BA.debugLine="r.target = r.RunMethod(\"getPictureSize\")";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv0.RunMethod("getPictureSize");
 //BA.debugLineNum = 193;BA.debugLine="Dim cs As CameraSize";
_cs = new precarite.cns.cameraexclass._camerasize();
 //BA.debugLineNum = 194;BA.debugLine="cs.Width = r.GetField(\"width\")";
_cs.Width = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("width")));
 //BA.debugLineNum = 195;BA.debugLine="cs.Height = r.GetField(\"height\")";
_cs.Height = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("height")));
 //BA.debugLineNum = 196;BA.debugLine="Return cs";
if (true) return _cs;
 //BA.debugLineNum = 197;BA.debugLine="End Sub";
return null;
}
public precarite.cns.cameraexclass._camerasize  _vvvvvvvvvv2() throws Exception{
precarite.cns.cameraexclass._camerasize _cs = null;
 //BA.debugLineNum = 314;BA.debugLine="Public Sub GetPreviewSize As CameraSize";
 //BA.debugLineNum = 315;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 316;BA.debugLine="r.target = r.RunMethod(\"getPreviewSize\")";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv0.RunMethod("getPreviewSize");
 //BA.debugLineNum = 317;BA.debugLine="Dim cs As CameraSize";
_cs = new precarite.cns.cameraexclass._camerasize();
 //BA.debugLineNum = 318;BA.debugLine="cs.Width = r.GetField(\"width\")";
_cs.Width = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("width")));
 //BA.debugLineNum = 319;BA.debugLine="cs.Height = r.GetField(\"height\")";
_cs.Height = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("height")));
 //BA.debugLineNum = 320;BA.debugLine="Return cs";
if (true) return _cs;
 //BA.debugLineNum = 321;BA.debugLine="End Sub";
return null;
}
public String  _getvvvvvvvvvvvvvv3() throws Exception{
 //BA.debugLineNum = 172;BA.debugLine="Public Sub getSceneMode As String";
 //BA.debugLineNum = 173;BA.debugLine="Return GetParameter(\"mode\")";
if (true) return _vvvvvvvvv0("mode");
 //BA.debugLineNum = 174;BA.debugLine="End Sub";
return "";
}
public anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvv4() throws Exception{
 //BA.debugLineNum = 165;BA.debugLine="Public Sub GetSupportedColorEffects As List";
 //BA.debugLineNum = 166;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 167;BA.debugLine="Return r.RunMethod(\"getSupportedColorEffects\")";
if (true) return (anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedColorEffects")));
 //BA.debugLineNum = 168;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvv5() throws Exception{
 //BA.debugLineNum = 288;BA.debugLine="Public Sub GetSupportedFlashModes As List";
 //BA.debugLineNum = 289;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 290;BA.debugLine="Return r.RunMethod(\"getSupportedFlashModes\")";
if (true) return (anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedFlashModes")));
 //BA.debugLineNum = 291;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvv6() throws Exception{
 //BA.debugLineNum = 367;BA.debugLine="Public Sub GetSupportedFocusModes As List";
 //BA.debugLineNum = 368;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 369;BA.debugLine="Return r.RunMethod(\"getSupportedFocusModes\")";
if (true) return (anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedFocusModes")));
 //BA.debugLineNum = 370;BA.debugLine="End Sub";
return null;
}
public precarite.cns.cameraexclass._camerasize[]  _vvvvvvvvvv7() throws Exception{
anywheresoftware.b4a.objects.collections.List _list1 = null;
precarite.cns.cameraexclass._camerasize[] _cs = null;
int _i = 0;
 //BA.debugLineNum = 206;BA.debugLine="Public Sub GetSupportedPicturesSizes As CameraSize";
 //BA.debugLineNum = 207;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 208;BA.debugLine="Dim list1 As List = r.RunMethod(\"getSupportedPict";
_list1 = new anywheresoftware.b4a.objects.collections.List();
_list1.setObject((java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedPictureSizes")));
 //BA.debugLineNum = 209;BA.debugLine="Dim cs(list1.Size) As CameraSize";
_cs = new precarite.cns.cameraexclass._camerasize[_list1.getSize()];
{
int d0 = _cs.length;
for (int i0 = 0;i0 < d0;i0++) {
_cs[i0] = new precarite.cns.cameraexclass._camerasize();
}
}
;
 //BA.debugLineNum = 210;BA.debugLine="For i = 0 To list1.Size - 1";
{
final int step4 = 1;
final int limit4 = (int) (_list1.getSize()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 211;BA.debugLine="r.target = list1.Get(i)";
_vvvvvvvvvvvvvv0.Target = _list1.Get(_i);
 //BA.debugLineNum = 212;BA.debugLine="cs(i).Width = r.GetField(\"width\")";
_cs[_i].Width = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("width")));
 //BA.debugLineNum = 213;BA.debugLine="cs(i).Height = r.GetField(\"height\")";
_cs[_i].Height = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("height")));
 //BA.debugLineNum = 214;BA.debugLine="Log(cs(i).Width + \"x\" +cs(i).Height)";
__c.Log(BA.NumberToString(_cs[_i].Width+(double)(Double.parseDouble("x"))+_cs[_i].Height));
 }
};
 //BA.debugLineNum = 216;BA.debugLine="Return cs";
if (true) return _cs;
 //BA.debugLineNum = 217;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvv0() throws Exception{
 //BA.debugLineNum = 184;BA.debugLine="Public Sub GetSupportedSceneModes As List";
 //BA.debugLineNum = 185;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 186;BA.debugLine="Return r.RunMethod(\"getSupportedSceneModes\")";
if (true) return (anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedSceneModes")));
 //BA.debugLineNum = 187;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.collections.List  _vvvvvvvvvvv1() throws Exception{
 //BA.debugLineNum = 308;BA.debugLine="Public Sub GetSupportedWhiteBalance  As List";
 //BA.debugLineNum = 309;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 310;BA.debugLine="Return r.RunMethod(\"getSupportedWhiteBalance\")";
if (true) return (anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(_vvvvvvvvvvvvvv0.RunMethod("getSupportedWhiteBalance")));
 //BA.debugLineNum = 311;BA.debugLine="End Sub";
return null;
}
public String  _getvvvvvvvvvvvvvv4() throws Exception{
 //BA.debugLineNum = 295;BA.debugLine="Public Sub getWhiteBalance As String";
 //BA.debugLineNum = 296;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 297;BA.debugLine="Return r.RunMethod(\"getWhiteBalance\")";
if (true) return BA.ObjectToString(_vvvvvvvvvvvvvv0.RunMethod("getWhiteBalance"));
 //BA.debugLineNum = 298;BA.debugLine="End Sub";
return "";
}
public int  _getvvvvvvvvvvvvvv5() throws Exception{
 //BA.debugLineNum = 235;BA.debugLine="Public Sub getZoom() As Int";
 //BA.debugLineNum = 236;BA.debugLine="If isZoomSupported Then";
if (_vvvvvvvvvvv4()) { 
 //BA.debugLineNum = 237;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 238;BA.debugLine="Return r.RunMethod(\"getZoom\")";
if (true) return (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunMethod("getZoom")));
 }else {
 //BA.debugLineNum = 241;BA.debugLine="Return 0";
if (true) return (int) (0);
 };
 //BA.debugLineNum = 243;BA.debugLine="End Sub";
return 0;
}
public String  _initialize(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.PanelWrapper _panel1,boolean _frontcamera,Object _targetmodule,String _eventname) throws Exception{
innerInitialize(_ba);
int _id = 0;
 //BA.debugLineNum = 22;BA.debugLine="Public Sub Initialize (Panel1 As Panel, FrontCamer";
 //BA.debugLineNum = 23;BA.debugLine="target = TargetModule";
_vvvvvvvvvvvvvvv1 = _targetmodule;
 //BA.debugLineNum = 24;BA.debugLine="event = EventName";
_vvvvvvvvvvvvvvv2 = _eventname;
 //BA.debugLineNum = 25;BA.debugLine="Front = FrontCamera";
_vvvvvvvvvvvvvvv3 = _frontcamera;
 //BA.debugLineNum = 26;BA.debugLine="Dim id As Int";
_id = 0;
 //BA.debugLineNum = 27;BA.debugLine="id = FindCamera(Front).id";
_id = _vvvvvvvv0(_vvvvvvvvvvvvvvv3).Id;
 //BA.debugLineNum = 28;BA.debugLine="If id = -1 Then";
if (_id==-1) { 
 //BA.debugLineNum = 29;BA.debugLine="Front = Not(Front) 'try different camera";
_vvvvvvvvvvvvvvv3 = __c.Not(_vvvvvvvvvvvvvvv3);
 //BA.debugLineNum = 30;BA.debugLine="id = FindCamera(Front).id";
_id = _vvvvvvvv0(_vvvvvvvvvvvvvvv3).Id;
 //BA.debugLineNum = 31;BA.debugLine="If id = -1 Then";
if (_id==-1) { 
 //BA.debugLineNum = 32;BA.debugLine="ToastMessageShow(\"No camera found.\", True)";
__c.ToastMessageShow(BA.ObjectToCharSequence("No camera found."),__c.True);
 //BA.debugLineNum = 33;BA.debugLine="Return";
if (true) return "";
 };
 };
 //BA.debugLineNum = 36;BA.debugLine="cam.Initialize2(Panel1, \"camera\", id)";
_vvvvvvvvvvvvvv7.Initialize2(ba,(android.view.ViewGroup)(_panel1.getObject()),"camera",_id);
 //BA.debugLineNum = 37;BA.debugLine="End Sub";
return "";
}
public boolean  _vvvvvvvvvvv4() throws Exception{
 //BA.debugLineNum = 269;BA.debugLine="Public Sub isZoomSupported() As Boolean";
 //BA.debugLineNum = 270;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 271;BA.debugLine="Return r.RunMethod(\"isZoomSupported\")";
if (true) return BA.ObjectToBoolean(_vvvvvvvvvvvvvv0.RunMethod("isZoomSupported"));
 //BA.debugLineNum = 272;BA.debugLine="End Sub";
return false;
}
public byte[]  _vvvvvvvvvvv5(byte[] _data,int _quality) throws Exception{
Object _size = null;
Object _previewformat = null;
int _width = 0;
int _height = 0;
Object _yuvimage = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.RectWrapper _rect1 = null;
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 332;BA.debugLine="Public Sub PreviewImageToJpeg(data() As Byte, qual";
 //BA.debugLineNum = 333;BA.debugLine="Dim size, previewFormat As Object";
_size = new Object();
_previewformat = new Object();
 //BA.debugLineNum = 334;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 335;BA.debugLine="size = r.RunMethod(\"getPreviewSize\")";
_size = _vvvvvvvvvvvvvv0.RunMethod("getPreviewSize");
 //BA.debugLineNum = 336;BA.debugLine="previewFormat = r.RunMethod(\"getPreviewFormat\")";
_previewformat = _vvvvvvvvvvvvvv0.RunMethod("getPreviewFormat");
 //BA.debugLineNum = 337;BA.debugLine="r.target = size";
_vvvvvvvvvvvvvv0.Target = _size;
 //BA.debugLineNum = 338;BA.debugLine="Dim width = r.GetField(\"width\"), height = r.GetFi";
_width = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("width")));
_height = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("height")));
 //BA.debugLineNum = 339;BA.debugLine="Dim yuvImage As Object = r.CreateObject2(\"android";
_yuvimage = _vvvvvvvvvvvvvv0.CreateObject2("android.graphics.YuvImage",new Object[]{(Object)(_data),_previewformat,(Object)(_width),(Object)(_height),__c.Null},new String[]{"[B","java.lang.int","java.lang.int","java.lang.int","[I"});
 //BA.debugLineNum = 342;BA.debugLine="r.target = yuvImage";
_vvvvvvvvvvvvvv0.Target = _yuvimage;
 //BA.debugLineNum = 343;BA.debugLine="Dim rect1 As Rect";
_rect1 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.RectWrapper();
 //BA.debugLineNum = 344;BA.debugLine="rect1.Initialize(0, 0, r.RunMethod(\"getWidth\"), r";
_rect1.Initialize((int) (0),(int) (0),(int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunMethod("getWidth"))),(int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.RunMethod("getHeight"))));
 //BA.debugLineNum = 345;BA.debugLine="Dim out As OutputStream";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 346;BA.debugLine="out.InitializeToBytesArray(100)";
_out.InitializeToBytesArray((int) (100));
 //BA.debugLineNum = 347;BA.debugLine="r.RunMethod4(\"compressToJpeg\", Array As Object(re";
_vvvvvvvvvvvvvv0.RunMethod4("compressToJpeg",new Object[]{(Object)(_rect1.getObject()),(Object)(_quality),(Object)(_out.getObject())},new String[]{"android.graphics.Rect","java.lang.int","java.io.OutputStream"});
 //BA.debugLineNum = 349;BA.debugLine="Return out.ToBytesArray";
if (true) return _out.ToBytesArray();
 //BA.debugLineNum = 350;BA.debugLine="End Sub";
return null;
}
public String  _vvvvvvvvvvv6() throws Exception{
 //BA.debugLineNum = 121;BA.debugLine="Public Sub Release";
 //BA.debugLineNum = 122;BA.debugLine="cam.Release";
_vvvvvvvvvvvvvv7.Release();
 //BA.debugLineNum = 123;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvv7(byte[] _data,String _dir,String _filename) throws Exception{
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 126;BA.debugLine="Public Sub SavePictureToFile(Data() As Byte, Dir A";
 //BA.debugLineNum = 127;BA.debugLine="Dim out As OutputStream = File.OpenOutput(Dir, Fi";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
_out = __c.File.OpenOutput(_dir,_filename,__c.False);
 //BA.debugLineNum = 128;BA.debugLine="out.WriteBytes(Data, 0, Data.Length)";
_out.WriteBytes(_data,(int) (0),_data.length);
 //BA.debugLineNum = 129;BA.debugLine="out.Close";
_out.Close();
 //BA.debugLineNum = 130;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvv7(String _effect) throws Exception{
 //BA.debugLineNum = 158;BA.debugLine="Public Sub setColorEffect(Effect As String)";
 //BA.debugLineNum = 159;BA.debugLine="SetParameter(\"effect\", Effect)";
_vvvvvvvvvvvv6("effect",_effect);
 //BA.debugLineNum = 160;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvv1() throws Exception{
anywheresoftware.b4a.objects.collections.List _modes = null;
 //BA.debugLineNum = 374;BA.debugLine="Public Sub SetContinuousAutoFocus";
 //BA.debugLineNum = 375;BA.debugLine="Dim modes As List = GetSupportedFocusModes";
_modes = new anywheresoftware.b4a.objects.collections.List();
_modes = _vvvvvvvvvv6();
 //BA.debugLineNum = 376;BA.debugLine="If modes.IndexOf(\"continuous-picture\") > -1 Th";
if (_modes.IndexOf((Object)("continuous-picture"))>-1) { 
 //BA.debugLineNum = 377;BA.debugLine="setFocusMode(\"continuous-picture\")";
_setvvvvvvvvvvvvvv1("continuous-picture");
 }else if(_modes.IndexOf((Object)("continuous-video"))>-1) { 
 //BA.debugLineNum = 379;BA.debugLine="setFocusMode(\"continuous-video\")";
_setvvvvvvvvvvvvvv1("continuous-video");
 }else if(_modes.IndexOf((Object)("continuous"))>-1) { 
 //BA.debugLineNum = 381;BA.debugLine="setFocusMode(\"continuous\")";
_setvvvvvvvvvvvvvv1("continuous");
 }else {
 //BA.debugLineNum = 383;BA.debugLine="Log(\"Continuous focus mode is not availabl";
__c.Log("Continuous focus mode is not available");
 };
 //BA.debugLineNum = 385;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvv2() throws Exception{
int _previewresult = 0;
int _result = 0;
int _degrees = 0;
precarite.cns.cameraexclass._camerainfoandid _ci = null;
int _orientation = 0;
 //BA.debugLineNum = 60;BA.debugLine="Private Sub SetDisplayOrientation";
 //BA.debugLineNum = 61;BA.debugLine="r.target = r.GetActivity";
_vvvvvvvvvvvvvv0.Target = (Object)(_vvvvvvvvvvvvvv0.GetActivity(ba));
 //BA.debugLineNum = 62;BA.debugLine="r.target = r.RunMethod(\"getWindowManager\")";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv0.RunMethod("getWindowManager");
 //BA.debugLineNum = 63;BA.debugLine="r.target = r.RunMethod(\"getDefaultDisplay\")";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv0.RunMethod("getDefaultDisplay");
 //BA.debugLineNum = 64;BA.debugLine="r.target = r.RunMethod(\"getRotation\")";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv0.RunMethod("getRotation");
 //BA.debugLineNum = 65;BA.debugLine="Dim previewResult, result, degrees As Int = r.tar";
_previewresult = 0;
_result = 0;
_degrees = (int) ((double)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.Target))*90);
 //BA.debugLineNum = 66;BA.debugLine="Dim ci As CameraInfoAndId = FindCamera(Front)";
_ci = _vvvvvvvv0(_vvvvvvvvvvvvvvv3);
 //BA.debugLineNum = 67;BA.debugLine="r.target = ci.CameraInfo";
_vvvvvvvvvvvvvv0.Target = _ci.CameraInfo;
 //BA.debugLineNum = 68;BA.debugLine="Dim orientation As Int = r.GetField(\"orientation\"";
_orientation = (int)(BA.ObjectToNumber(_vvvvvvvvvvvvvv0.GetField("orientation")));
 //BA.debugLineNum = 69;BA.debugLine="If Front Then";
if (_vvvvvvvvvvvvvvv3) { 
 //BA.debugLineNum = 70;BA.debugLine="previewResult = (orientation + degrees) Mod 360";
_previewresult = (int) ((_orientation+_degrees)%360);
 //BA.debugLineNum = 71;BA.debugLine="result = previewResult";
_result = _previewresult;
 //BA.debugLineNum = 72;BA.debugLine="previewResult = (360 - previewResult) Mod 360";
_previewresult = (int) ((360-_previewresult)%360);
 }else {
 //BA.debugLineNum = 74;BA.debugLine="previewResult = (orientation - degrees + 360) Mo";
_previewresult = (int) ((_orientation-_degrees+360)%360);
 //BA.debugLineNum = 75;BA.debugLine="result = previewResult";
_result = _previewresult;
 //BA.debugLineNum = 76;BA.debugLine="Log(previewResult)";
__c.Log(BA.NumberToString(_previewresult));
 };
 //BA.debugLineNum = 78;BA.debugLine="r.target = nativeCam";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvv6;
 //BA.debugLineNum = 79;BA.debugLine="r.RunMethod2(\"setDisplayOrientation\", previewResu";
_vvvvvvvvvvvvvv0.RunMethod2("setDisplayOrientation",BA.NumberToString(_previewresult),"java.lang.int");
 //BA.debugLineNum = 80;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 82;BA.debugLine="r.RunMethod2(\"setRotation\", 90, \"java.lang.int\")";
_vvvvvvvvvvvvvv0.RunMethod2("setRotation",BA.NumberToString(90),"java.lang.int");
 //BA.debugLineNum = 83;BA.debugLine="CommitParameters";
_vvvvvvvv7();
 //BA.debugLineNum = 84;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvv0(String _mode) throws Exception{
 //BA.debugLineNum = 281;BA.debugLine="Public Sub setFlashMode(Mode As String)";
 //BA.debugLineNum = 282;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 283;BA.debugLine="r.RunMethod2(\"setFlashMode\", Mode, \"java.lang.Str";
_vvvvvvvvvvvvvv0.RunMethod2("setFlashMode",_mode,"java.lang.String");
 //BA.debugLineNum = 284;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvvv1(String _mode) throws Exception{
 //BA.debugLineNum = 359;BA.debugLine="Public Sub setFocusMode(Mode As String)";
 //BA.debugLineNum = 360;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 361;BA.debugLine="r.RunMethod2(\"setFocusMode\", Mode, \"java.lang.";
_vvvvvvvvvvvvvv0.RunMethod2("setFocusMode",_mode,"java.lang.String");
 //BA.debugLineNum = 362;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvvv2(int _quality) throws Exception{
 //BA.debugLineNum = 225;BA.debugLine="Public Sub setJpegQuality(Quality As Int)";
 //BA.debugLineNum = 226;BA.debugLine="If Quality<1 Then Quality=1";
if (_quality<1) { 
_quality = (int) (1);};
 //BA.debugLineNum = 227;BA.debugLine="If Quality>100 Then Quality=100";
if (_quality>100) { 
_quality = (int) (100);};
 //BA.debugLineNum = 228;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 229;BA.debugLine="r.RunMethod2(\"setJpegQuality\", Quality, \"java.lan";
_vvvvvvvvvvvvvv0.RunMethod2("setJpegQuality",BA.NumberToString(_quality),"java.lang.int");
 //BA.debugLineNum = 230;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvv6(String _key,String _value) throws Exception{
 //BA.debugLineNum = 137;BA.debugLine="Public Sub SetParameter(Key As String, Value As St";
 //BA.debugLineNum = 138;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 139;BA.debugLine="r.RunMethod3(\"set\", Key, \"java.lang.String\", Valu";
_vvvvvvvvvvvvvv0.RunMethod3("set",_key,"java.lang.String",_value,"java.lang.String");
 //BA.debugLineNum = 140;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvv7(int _width,int _height) throws Exception{
 //BA.debugLineNum = 200;BA.debugLine="Public Sub SetPictureSize(Width As Int, Height As";
 //BA.debugLineNum = 201;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 202;BA.debugLine="r.RunMethod3(\"setPictureSize\", Width, \"java.lang.";
_vvvvvvvvvvvvvv0.RunMethod3("setPictureSize",BA.NumberToString(_width),"java.lang.int",BA.NumberToString(_height),"java.lang.int");
 //BA.debugLineNum = 203;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvv0(int _width,int _height) throws Exception{
 //BA.debugLineNum = 325;BA.debugLine="Public Sub SetPreviewSize(Width As Int, Height As";
 //BA.debugLineNum = 326;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 327;BA.debugLine="r.RunMethod3(\"setPreviewSize\", Width, \"java.lang.";
_vvvvvvvvvvvvvv0.RunMethod3("setPreviewSize",BA.NumberToString(_width),"java.lang.int",BA.NumberToString(_height),"java.lang.int");
 //BA.debugLineNum = 328;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvvv3(String _mode) throws Exception{
 //BA.debugLineNum = 176;BA.debugLine="Public Sub setSceneMode(Mode As String)";
 //BA.debugLineNum = 177;BA.debugLine="SetParameter(\"mode\", Mode)";
_vvvvvvvvvvvv6("mode",_mode);
 //BA.debugLineNum = 178;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvvv4(String _mode) throws Exception{
 //BA.debugLineNum = 300;BA.debugLine="Public Sub setWhiteBalance (Mode As String)";
 //BA.debugLineNum = 301;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 302;BA.debugLine="r.RunMethod2(\"setWhiteBalance\", Mode, \"java.lan";
_vvvvvvvvvvvvvv0.RunMethod2("setWhiteBalance",_mode,"java.lang.String");
 //BA.debugLineNum = 303;BA.debugLine="End Sub";
return "";
}
public String  _setvvvvvvvvvvvvvv5(int _value) throws Exception{
 //BA.debugLineNum = 245;BA.debugLine="Public Sub setZoom(Value As Int)";
 //BA.debugLineNum = 246;BA.debugLine="Value=Max(Value,GetMaxZoom)";
_value = (int) (__c.Max(_value,_vvvvvvvvv7()));
 //BA.debugLineNum = 247;BA.debugLine="If isZoomSupported Then";
if (_vvvvvvvvvvv4()) { 
 //BA.debugLineNum = 248;BA.debugLine="r.target = parameters";
_vvvvvvvvvvvvvv0.Target = _vvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 249;BA.debugLine="r.RunMethod2(\"setZoom\", Value, \"java.lang.int\")";
_vvvvvvvvvvvvvv0.RunMethod2("setZoom",BA.NumberToString(_value),"java.lang.int");
 }else {
 //BA.debugLineNum = 251;BA.debugLine="Log(\"Zoom not supported.\")";
__c.Log("Zoom not supported.");
 };
 //BA.debugLineNum = 253;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvvv4() throws Exception{
 //BA.debugLineNum = 113;BA.debugLine="Public Sub StartPreview";
 //BA.debugLineNum = 114;BA.debugLine="cam.StartPreview";
_vvvvvvvvvvvvvv7.StartPreview();
 //BA.debugLineNum = 115;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvvv5() throws Exception{
 //BA.debugLineNum = 117;BA.debugLine="Public Sub StopPreview";
 //BA.debugLineNum = 118;BA.debugLine="cam.StopPreview";
_vvvvvvvvvvvvvv7.StopPreview();
 //BA.debugLineNum = 119;BA.debugLine="End Sub";
return "";
}
public String  _vvvvvvvvvvvvv6() throws Exception{
 //BA.debugLineNum = 105;BA.debugLine="Public Sub TakePicture";
 //BA.debugLineNum = 106;BA.debugLine="cam.TakePicture";
_vvvvvvvvvvvvvv7.TakePicture();
 //BA.debugLineNum = 107;BA.debugLine="End Sub";
return "";
}
public Object callSub(String sub, Object sender, Object[] args) throws Exception {
BA.senderHolder.set(sender);
return BA.SubDelegator.SubNotFound;
}
}
