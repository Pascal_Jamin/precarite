package precarite.cns;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class signaturecapture {
private static signaturecapture mostCurrent = new signaturecapture();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 public anywheresoftware.b4a.keywords.Common __c = null;
public static int _vvv6 = 0;
public static int _vvv7 = 0;
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;
public static class _signaturedata{
public boolean IsInitialized;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper Canvas;
public anywheresoftware.b4a.objects.PanelWrapper Panel;
public int SignatureColor;
public int SignatureWidth;
public void Initialize() {
IsInitialized = true;
Canvas = new anywheresoftware.b4a.objects.drawable.CanvasWrapper();
Panel = new anywheresoftware.b4a.objects.PanelWrapper();
SignatureColor = 0;
SignatureWidth = 0;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _vvv0(anywheresoftware.b4a.BA _ba,precarite.cns.signaturecapture._signaturedata _sd) throws Exception{
 //BA.debugLineNum = 18;BA.debugLine="Sub Clear(SD As SignatureData)";
 //BA.debugLineNum = 19;BA.debugLine="SD.Canvas.DrawColor(Colors.White)";
_sd.Canvas.DrawColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 21;BA.debugLine="SD.Panel.Invalidate";
_sd.Panel.Invalidate();
 //BA.debugLineNum = 22;BA.debugLine="End Sub";
return "";
}
public static String  _panel_touch(anywheresoftware.b4a.BA _ba,precarite.cns.signaturecapture._signaturedata _sd,int _x,int _y,int _action) throws Exception{
 //BA.debugLineNum = 7;BA.debugLine="Sub Panel_Touch(SD As SignatureData, x As Int,y As";
 //BA.debugLineNum = 8;BA.debugLine="If Action = 0 Then 'mouse down constant";
if (_action==0) { 
 //BA.debugLineNum = 9;BA.debugLine="px = x";
_vvv6 = _x;
 //BA.debugLineNum = 10;BA.debugLine="py = y";
_vvv7 = _y;
 }else {
 //BA.debugLineNum = 12;BA.debugLine="SD.Canvas.DrawLine(px, py, x, y, SD.SignatureCol";
_sd.Canvas.DrawLine((float) (_vvv6),(float) (_vvv7),(float) (_x),(float) (_y),_sd.SignatureColor,(float) (_sd.SignatureWidth));
 //BA.debugLineNum = 13;BA.debugLine="SD.Panel.Invalidate";
_sd.Panel.Invalidate();
 //BA.debugLineNum = 14;BA.debugLine="px = x";
_vvv6 = _x;
 //BA.debugLineNum = 15;BA.debugLine="py = y";
_vvv7 = _y;
 };
 //BA.debugLineNum = 17;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Dim px, py As Int";
_vvv6 = 0;
_vvv7 = 0;
 //BA.debugLineNum = 4;BA.debugLine="Type SignatureData (Canvas As Canvas, Panel As Pa";
;
 //BA.debugLineNum = 5;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv1(anywheresoftware.b4a.BA _ba,precarite.cns.signaturecapture._signaturedata _sd,String _dir,String _name) throws Exception{
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 23;BA.debugLine="Sub Save(SD As SignatureData, Dir As String, Name";
 //BA.debugLineNum = 24;BA.debugLine="Dim out As OutputStream";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 25;BA.debugLine="out = File.OpenOutput(Dir, Name, False)";
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(_dir,_name,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 26;BA.debugLine="SD.Canvas.Bitmap.WriteToStream(out, 100, \"JPEG\")";
_sd.Canvas.getBitmap().WriteToStream((java.io.OutputStream)(_out.getObject()),(int) (100),BA.getEnumFromString(android.graphics.Bitmap.CompressFormat.class,"JPEG"));
 //BA.debugLineNum = 27;BA.debugLine="out.Close";
_out.Close();
 //BA.debugLineNum = 28;BA.debugLine="End Sub";
return "";
}
}
