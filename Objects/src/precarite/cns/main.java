package precarite.cns;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "precarite.cns", "precarite.cns.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "precarite.cns", "precarite.cns.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "precarite.cns.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static int _vvvv5 = 0;
public static String _vvvv6 = "";
public static String _vvvv7 = "";
public static anywheresoftware.b4a.objects.preferenceactivity.PreferenceScreenWrapper _vvvv0 = null;
public static anywheresoftware.b4a.objects.preferenceactivity.PreferenceManager _vvvvv1 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv2 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv3 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv4 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv5 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv6 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv7 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvv0 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv1 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv2 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv3 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv4 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv5 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv6 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv7 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvv0 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv1 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv2 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv3 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv4 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv5 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv6 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv7 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvv0 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvvv1 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessIndexCursor _vvvvvvvv2 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessCursor _vvvvvvvv3 = null;
public static anywheresoftware.b4a.objects.JackcessDatabase.JackcessIndexCursor _vvvvvvvv4 = null;
public static String _vvvvvvvv5 = "";
public static String _vvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvv7 = "";
public static String _vvvvvvvvvvvvvvvv5 = "";
public static String _vvvvvvvvvvvvvvvvv0 = "";
public static String _vvvvvvvvvvvvvvvvvv2 = "";
public static String _vvvvvvvvvvvvvvvvvv1 = "";
public static String _vvvvvvvvvvvvvvvvvv3 = "";
public anywheresoftware.b4a.objects.JackcessDatabase _vvvvvvvvvvvvvvvvv2 = null;
public anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable _vvvvvvvvvvvvvvvvvvv1 = null;
public anywheresoftware.b4a.objects.ListViewWrapper _lsmaint = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _vvvvvvvvvvvvvvvvvvv2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvv3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblplancher = null;
public anywheresoftware.b4a.objects.LabelWrapper _vvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbllocal = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblisolant = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper _cbaccespcommunes = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeporte = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeplancher = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypecloison = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypelocal = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spntypeisolant = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblfiltre = null;
public anywheresoftware.b4a.objects.EditTextWrapper _txtfiltre = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnfiltre = null;
public anywheresoftware.b4a.objects.LabelWrapper _btnquit = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblappname = null;
public anywheresoftware.b4a.objects.ProgressBarWrapper _pbzip = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblwait = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
vis = vis | (detail.mostCurrent != null);
vis = vis | (signaturescreen.mostCurrent != null);
vis = vis | (gmaps.mostCurrent != null);
vis = vis | (draw1.mostCurrent != null);
return vis;}
public static String  _activity_create(boolean _firsttime) throws Exception{
String _encrypteddata = "";
String _licencecode = "";
String _readcode = "";
anywheresoftware.b4a.objects.drawable.ColorDrawable _cd = null;
int _nbrow = 0;
String _curadresse = "";
 //BA.debugLineNum = 59;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 60;BA.debugLine="Activity.LoadLayout(\"Main\")";
mostCurrent._activity.LoadLayout("Main",mostCurrent.activityBA);
 //BA.debugLineNum = 61;BA.debugLine="gPosition=0";
_vvvv5 = (int) (0);
 //BA.debugLineNum = 62;BA.debugLine="curFilter=\"\"";
mostCurrent._vvvvvvvvvvvvvvvv5 = "";
 //BA.debugLineNum = 64;BA.debugLine="DeviceID = GetDeviceId";
_vvvv7 = _vvvvvvvvvvvvvvvv6();
 //BA.debugLineNum = 65;BA.debugLine="Log(\"DeviceID : \" & DeviceID )";
anywheresoftware.b4a.keywords.Common.Log("DeviceID : "+_vvvv7);
 //BA.debugLineNum = 66;BA.debugLine="Dim EncryptedData As String = EncryptText(DeviceI";
_encrypteddata = _vvvvvvvvvvvvvvvv7(_vvvv7);
 //BA.debugLineNum = 67;BA.debugLine="Log(\"Encrypted : \" & EncryptedData)";
anywheresoftware.b4a.keywords.Common.Log("Encrypted : "+_encrypteddata);
 //BA.debugLineNum = 68;BA.debugLine="Log(\"Decrypted : \" & DecryptText(EncryptedData))";
anywheresoftware.b4a.keywords.Common.Log("Decrypted : "+_vvvvvvvvvvvvvvvv0(_encrypteddata));
 //BA.debugLineNum = 70;BA.debugLine="Activity.SetBackgroundImage(LoadBitmap(File.DirAs";
mostCurrent._activity.SetBackgroundImage((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"backmain2.jpg").getObject()));
 //BA.debugLineNum = 73;BA.debugLine="CNSPath=File.DirRootExternal & \"/RelevePrecarite\"";
mostCurrent._vvvvvvvvvvvvvvvvv1 = anywheresoftware.b4a.keywords.Common.File.getDirRootExternal()+"/RelevePrecarite";
 //BA.debugLineNum = 74;BA.debugLine="If Not(File.Exists(CNSPath,\"\")) Then File.MakeDir";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,""))) { 
anywheresoftware.b4a.keywords.Common.File.MakeDir(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"/RelevePrecarite");};
 //BA.debugLineNum = 75;BA.debugLine="If Not(File.Exists(CNSPath, \"precarite.accdb\")) T";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,"precarite.accdb"))) { 
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"precarite.accdb",mostCurrent._vvvvvvvvvvvvvvvvv1,"precarite.accdb");};
 //BA.debugLineNum = 76;BA.debugLine="If Not(File.Exists(CNSPath, \"vide.jpg\")) Then Fil";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg"))) { 
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"vide.jpg",mostCurrent._vvvvvvvvvvvvvvvvv1,"vide.jpg");};
 //BA.debugLineNum = 77;BA.debugLine="db.Open(CNSPath & \"/precarite.accdb\")";
mostCurrent._vvvvvvvvvvvvvvvvv2.Open(mostCurrent._vvvvvvvvvvvvvvvvv1+"/precarite.accdb");
 //BA.debugLineNum = 78;BA.debugLine="fTable.Initialize(db.GetTable(\"Detail\"))";
_vvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"));
 //BA.debugLineNum = 79;BA.debugLine="fCursor.Initialize(db.GetTable(\"Detail\"),fTable.G";
_vvvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),_vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 80;BA.debugLine="ProximCursor.Initialize(db.GetTable(\"Detail\"),fTa";
_vvvvvvvv4.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),_vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 81;BA.debugLine="fTableSite.Initialize(db.GetTable(\"Site\"))";
_vvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Site"));
 //BA.debugLineNum = 82;BA.debugLine="fTableCampagne.Initialize(db.GetTable(\"Campagnes\"";
_vvvvv5.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Campagnes"));
 //BA.debugLineNum = 84;BA.debugLine="fTableTypePorte.Initialize(db.GetTable(\"PLBTypePo";
_vvvvvv4.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypePorte"));
 //BA.debugLineNum = 85;BA.debugLine="fTableTypePlancher.Initialize(db.GetTable(\"PLBTyp";
_vvvvvv5.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypePlancher"));
 //BA.debugLineNum = 86;BA.debugLine="fTableTypeCloisonnement.Initialize(db.GetTable(\"P";
_vvvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypeCloisonnement"));
 //BA.debugLineNum = 87;BA.debugLine="fTableTypeLocal.Initialize(db.GetTable(\"PLBTypeLo";
_vvvvvv7.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypeLocal"));
 //BA.debugLineNum = 88;BA.debugLine="fTableTypeIsolant.Initialize(db.GetTable(\"PLBType";
_vvvvvv3.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypeIsolant"));
 //BA.debugLineNum = 89;BA.debugLine="fTableEtatIsolant.Initialize(db.GetTable(\"PLBType";
_vvvvvvv0.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypeEtatIsolant"));
 //BA.debugLineNum = 90;BA.debugLine="fTableResistanceThermique.Initialize(db.GetTable(";
_vvvvvvvv1.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBResistanceThermique"));
 //BA.debugLineNum = 91;BA.debugLine="fTabletypeReseauPlafond.Initialize(db.GetTable(\"P";
_vvvvvvv7.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBTypeReseauPlafond"));
 //BA.debugLineNum = 92;BA.debugLine="fTableAccesPCommunes.Initialize(db.GetTable(\"PLBA";
_vvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("PLBAccesPCommunes"));
 //BA.debugLineNum = 94;BA.debugLine="fTableMateriauPrevu.Initialize(db.GetTable(\"CBLMa";
_vvvvvv0.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLMateriauPrevu"));
 //BA.debugLineNum = 95;BA.debugLine="fTableTypeToit.Initialize(db.GetTable(\"CBLTypeToi";
_vvvvvvv3.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLTypeToit"));
 //BA.debugLineNum = 96;BA.debugLine="fTableNatureIsolant.Initialize(db.GetTable(\"CBLNa";
_vvvvvvv1.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLNatureIsolant"));
 //BA.debugLineNum = 97;BA.debugLine="fTableTypeLogement.Initialize(db.GetTable(\"CBLTyp";
_vvvvvvv4.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLTypeLogement"));
 //BA.debugLineNum = 98;BA.debugLine="fTableEtatPlafond.Initialize(db.GetTable(\"CBLEtat";
_vvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLEtatPlafond"));
 //BA.debugLineNum = 99;BA.debugLine="fTableCBLIsolant.Initialize(db.GetTable(\"CBLTypeI";
_vvvvvvv5.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLTypeIsolant"));
 //BA.debugLineNum = 100;BA.debugLine="fTableCBLPlancher.Initialize(db.GetTable(\"CBLType";
_vvvvvvv6.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("CBLTypePlancher"));
 //BA.debugLineNum = 104;BA.debugLine="campcursor.Initialize(db.GetTable(\"Campagnes\"))";
_vvvvvvvv3.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Campagnes"));
 //BA.debugLineNum = 105;BA.debugLine="campcursor.Reset";
_vvvvvvvv3.Reset();
 //BA.debugLineNum = 106;BA.debugLine="campcursor.GetNextRow";
_vvvvvvvv3.GetNextRow();
 //BA.debugLineNum = 107;BA.debugLine="campcursor.SetCurrentRowValue(\"UniqueID\",DeviceID";
_vvvvvvvv3.SetCurrentRowValue("UniqueID",(Object)(_vvvv7));
 //BA.debugLineNum = 109;BA.debugLine="Dim LicenceCode As String = EncryptText(DeviceID)";
_licencecode = _vvvvvvvvvvvvvvvv7(_vvvv7);
_readcode = "";
 //BA.debugLineNum = 111;BA.debugLine="readCode=ReadLicenceCode";
_readcode = _vvvvvvvvvvvvvvvvv3();
 //BA.debugLineNum = 112;BA.debugLine="Log(\"readCode : \" & readCode)";
anywheresoftware.b4a.keywords.Common.Log("readCode : "+_readcode);
 //BA.debugLineNum = 114;BA.debugLine="If readCode.EqualsIgnoreCase(LicenceCode)=False T";
if (_readcode.equalsIgnoreCase(_licencecode)==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 115;BA.debugLine="ValidateLicence";
_vvvvvvvvvvvvvvvvv4();
 };
 //BA.debugLineNum = 118;BA.debugLine="Dim cd As ColorDrawable";
_cd = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 119;BA.debugLine="cd.Initialize(Colors.LightGray, 0dip)";
_cd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.LightGray,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (0)));
 //BA.debugLineNum = 120;BA.debugLine="lsMaint.TwoLinesLayout.Background=cd";
mostCurrent._lsmaint.getTwoLinesLayout().Background = (android.graphics.drawable.Drawable)(_cd.getObject());
 //BA.debugLineNum = 121;BA.debugLine="lsMaint.TwoLinesLayout.ItemHeight=135";
mostCurrent._lsmaint.getTwoLinesLayout().setItemHeight((int) (135));
 //BA.debugLineNum = 122;BA.debugLine="lsMaint.ScrollingBackgroundColor = Colors.DarkGra";
mostCurrent._lsmaint.setScrollingBackgroundColor(anywheresoftware.b4a.keywords.Common.Colors.DarkGray);
 //BA.debugLineNum = 123;BA.debugLine="lsMaint.TwoLinesLayout.Label.TextSize=13";
mostCurrent._lsmaint.getTwoLinesLayout().Label.setTextSize((float) (13));
 //BA.debugLineNum = 124;BA.debugLine="lsMaint.TwoLinesLayout.Label.TextColor=Colors.Blu";
mostCurrent._lsmaint.getTwoLinesLayout().Label.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.Blue);
 //BA.debugLineNum = 125;BA.debugLine="lsMaint.TwoLinesLayout.SecondLabel.TextSize=12";
mostCurrent._lsmaint.getTwoLinesLayout().SecondLabel.setTextSize((float) (12));
 //BA.debugLineNum = 126;BA.debugLine="lsMaint.TwoLinesLayout.SecondLabel.TextColor=Colo";
mostCurrent._lsmaint.getTwoLinesLayout().SecondLabel.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.DarkGray);
 //BA.debugLineNum = 127;BA.debugLine="lsMaint.TwoLinesLayout.SecondLabel.Typeface=Typef";
mostCurrent._lsmaint.getTwoLinesLayout().SecondLabel.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.getFONTAWESOME());
 //BA.debugLineNum = 129;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 131;BA.debugLine="Dim nbrow As Int,curAdresse As String";
_nbrow = 0;
_curadresse = "";
 //BA.debugLineNum = 132;BA.debugLine="CreatePreferenceScreen";
_vvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 133;BA.debugLine="If manager.GetAll.Size = 0 Then SetDefaults";
if (_vvvvv1.GetAll().getSize()==0) { 
_vvvvvvvvvvvvvvvvv6();};
 //BA.debugLineNum = 134;BA.debugLine="curTC=\"Type Capteur\"";
mostCurrent._vvvvvvvvvvvvvvvvv7 = "Type Capteur";
 //BA.debugLineNum = 135;BA.debugLine="curAdresse=\"\"";
_curadresse = "";
 };
 //BA.debugLineNum = 137;BA.debugLine="Activity.AddMenuItem(\"Paramètres campagne\",\"OptCa";
mostCurrent._activity.AddMenuItem(BA.ObjectToCharSequence("Paramètres campagne"),"OptCampagne");
 //BA.debugLineNum = 138;BA.debugLine="Activity.AddMenuItem(\"Reset des données\",\"ResetDB";
mostCurrent._activity.AddMenuItem(BA.ObjectToCharSequence("Reset des données"),"ResetDB");
 //BA.debugLineNum = 140;BA.debugLine="rotation=manager.Getstring(\"Rotation\")";
mostCurrent._vvvvvvvvvvvvvvvvv0 = _vvvvv1.GetString("Rotation");
 //BA.debugLineNum = 141;BA.debugLine="If rotation=\"\" Then rotation=\"0\"";
if ((mostCurrent._vvvvvvvvvvvvvvvvv0).equals("")) { 
mostCurrent._vvvvvvvvvvvvvvvvv0 = "0";};
 //BA.debugLineNum = 142;BA.debugLine="curUser=manager.Getstring(\"user1\")";
mostCurrent._vvvvvvvvvvvvvvvvvv1 = _vvvvv1.GetString("user1");
 //BA.debugLineNum = 143;BA.debugLine="If curUser=\"\" Then curUser=\"CNS\"";
if ((mostCurrent._vvvvvvvvvvvvvvvvvv1).equals("")) { 
mostCurrent._vvvvvvvvvvvvvvvvvv1 = "CNS";};
 //BA.debugLineNum = 144;BA.debugLine="campagne=manager.Getstring(\"cp1\")";
mostCurrent._vvvvvvvvvvvvvvvvvv2 = _vvvvv1.GetString("cp1");
 //BA.debugLineNum = 145;BA.debugLine="If campagne=\"\" Then campagne=\"Campagne1\"";
if ((mostCurrent._vvvvvvvvvvvvvvvvvv2).equals("")) { 
mostCurrent._vvvvvvvvvvvvvvvvvv2 = "Campagne1";};
 //BA.debugLineNum = 146;BA.debugLine="typeCP=manager.GetString(\"TypeCampagne\")";
mostCurrent._vvvvvvvvvvvvvvvvvv3 = _vvvvv1.GetString("TypeCampagne");
 //BA.debugLineNum = 147;BA.debugLine="If typeCP=\"\" Then typeCP=\"Combles\"";
if ((mostCurrent._vvvvvvvvvvvvvvvvvv3).equals("")) { 
mostCurrent._vvvvvvvvvvvvvvvvvv3 = "Combles";};
 //BA.debugLineNum = 149;BA.debugLine="ReadCampagneSettings";
_vvvvvvvvvvvvvvvvvv4();
 //BA.debugLineNum = 150;BA.debugLine="End Sub";
return "";
}
public static void  _activity_resume() throws Exception{
ResumableSub_Activity_Resume rsub = new ResumableSub_Activity_Resume(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_Activity_Resume extends BA.ResumableSub {
public ResumableSub_Activity_Resume(precarite.cns.main parent) {
this.parent = parent;
}
precarite.cns.main parent;
String _licencecode = "";
String _readcode = "";

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 226;BA.debugLine="ProgressDialogHide";
anywheresoftware.b4a.keywords.Common.ProgressDialogHide();
 //BA.debugLineNum = 227;BA.debugLine="Sleep(0)";
anywheresoftware.b4a.keywords.Common.Sleep(mostCurrent.activityBA,this,(int) (0));
this.state = 5;
return;
case 5:
//C
this.state = 1;
;
 //BA.debugLineNum = 229;BA.debugLine="Dim LicenceCode As String = EncryptText(GetDevice";
_licencecode = _vvvvvvvvvvvvvvvv7(_vvvvvvvvvvvvvvvv6());
_readcode = "";
 //BA.debugLineNum = 230;BA.debugLine="readCode=ReadLicenceCode";
_readcode = _vvvvvvvvvvvvvvvvv3();
 //BA.debugLineNum = 231;BA.debugLine="If readCode.EqualsIgnoreCase(LicenceCode)=False T";
if (true) break;

case 1:
//if
this.state = 4;
if (_readcode.equalsIgnoreCase(_licencecode)==anywheresoftware.b4a.keywords.Common.False) { 
this.state = 3;
}if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 232;BA.debugLine="ValidateLicence";
_vvvvvvvvvvvvvvvvv4();
 if (true) break;

case 4:
//C
this.state = -1;
;
 //BA.debugLineNum = 235;BA.debugLine="creeListe";
_vvvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 236;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _btnfiltre_click() throws Exception{
 //BA.debugLineNum = 373;BA.debugLine="Sub btnFiltre_Click";
 //BA.debugLineNum = 374;BA.debugLine="creeListe";
_vvvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 375;BA.debugLine="lsMaint.SetSelection(0) 'scroll to top of the lis";
mostCurrent._lsmaint.SetSelection((int) (0));
 //BA.debugLineNum = 376;BA.debugLine="End Sub";
return "";
}
public static String  _btnquit_click() throws Exception{
com.AB.ABZipUnzip.ABZipUnzip _rlvzip = null;
String _zipname = "";
String _dt = "";
int _i = 0;
 //BA.debugLineNum = 404;BA.debugLine="Sub btnQuit_Click";
 //BA.debugLineNum = 409;BA.debugLine="Try";
try { //BA.debugLineNum = 410;BA.debugLine="Dim rlvZip As ABZipUnzip,zipName As String";
_rlvzip = new com.AB.ABZipUnzip.ABZipUnzip();
_zipname = "";
 //BA.debugLineNum = 411;BA.debugLine="Dim dt As String";
_dt = "";
 //BA.debugLineNum = 412;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 413;BA.debugLine="i = Msgbox2(\"Voulez-vous quitter ?\", \"Quitter et";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous quitter ?"),BA.ObjectToCharSequence("Quitter et archiver le relevé"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 414;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 415;BA.debugLine="startWait(\"zip des données en cours...\")";
_vvvvvvvvvvvvvvvvvv6("zip des données en cours...");
 //BA.debugLineNum = 417;BA.debugLine="DoEvents";
anywheresoftware.b4a.keywords.Common.DoEvents();
 //BA.debugLineNum = 419;BA.debugLine="If manager.Getstring(\"user1\")<>Null And manager";
if (_vvvvv1.GetString("user1")!= null && (_vvvvv1.GetString("user1")).equals("") == false) { 
_vvvvvvvv3.SetCurrentRowValue("Operateur",(Object)(_vvvvv1.GetString("user1")));};
 //BA.debugLineNum = 420;BA.debugLine="If manager.Getstring(\"TypeCP\")<>Null And manage";
if (_vvvvv1.GetString("TypeCP")!= null && (_vvvvv1.GetString("TypeCP")).equals("") == false) { 
_vvvvvvvv3.SetCurrentRowValue("Type",(Object)(_vvvvv1.GetString("TypeCP")));};
 //BA.debugLineNum = 421;BA.debugLine="DateTime.DateFormat = \"yyyyMMdd\" ' See this pag";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyyyMMdd");
 //BA.debugLineNum = 422;BA.debugLine="dt = DateTime.Date(DateTime.Now)";
_dt = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 425;BA.debugLine="If File.Exists(File.DirRootExternal , \"RelevePr";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite_prev.zip")) { 
anywheresoftware.b4a.keywords.Common.File.Delete(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite_prev.zip");};
 //BA.debugLineNum = 426;BA.debugLine="If File.Exists(File.DirRootExternal , \"RelevePr";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite.zip")) { 
 //BA.debugLineNum = 427;BA.debugLine="File.Copy(File.DirRootExternal , \"RelevePrecar";
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite.zip",anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite_prev.zip");
 //BA.debugLineNum = 428;BA.debugLine="File.Delete(File.DirRootExternal , \"RelevePrec";
anywheresoftware.b4a.keywords.Common.File.Delete(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"RelevePrecarite.zip");
 };
 //BA.debugLineNum = 430;BA.debugLine="rlvZip.ABZipDirectory(File.DirRootExternal & \"/";
_rlvzip.ABZipDirectory(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal()+"/RelevePrecarite",anywheresoftware.b4a.keywords.Common.File.getDirRootExternal()+"/RelevePrecarite.zip");
 //BA.debugLineNum = 432;BA.debugLine="DoEvents";
anywheresoftware.b4a.keywords.Common.DoEvents();
 //BA.debugLineNum = 433;BA.debugLine="ToastMessageShow(\"FICHIER RelevePrecarite.zip c";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("FICHIER RelevePrecarite.zip créé"),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 436;BA.debugLine="stopWait";
_vvvvvvvvvvvvvvvvvv7();
 //BA.debugLineNum = 437;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 };
 } 
       catch (Exception e25) {
			processBA.setLastException(e25); //BA.debugLineNum = 440;BA.debugLine="ToastMessageShow(\"Erreur dans la création du fic";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Erreur dans la création du fichier ZIP !"),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 442;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvv5() throws Exception{
anywheresoftware.b4a.objects.preferenceactivity.PreferenceCategoryWrapper _cat1 = null;
anywheresoftware.b4a.objects.preferenceactivity.PreferenceCategoryWrapper _cat2 = null;
 //BA.debugLineNum = 387;BA.debugLine="Sub CreatePreferenceScreen";
 //BA.debugLineNum = 388;BA.debugLine="screen.Initialize(\"Paramètres de la campagne\", \"\"";
_vvvv0.Initialize("Paramètres de la campagne","");
 //BA.debugLineNum = 390;BA.debugLine="Dim cat1, cat2 As PreferenceCategory";
_cat1 = new anywheresoftware.b4a.objects.preferenceactivity.PreferenceCategoryWrapper();
_cat2 = new anywheresoftware.b4a.objects.preferenceactivity.PreferenceCategoryWrapper();
 //BA.debugLineNum = 391;BA.debugLine="cat1.Initialize(\"Infos générales\")";
_cat1.Initialize("Infos générales");
 //BA.debugLineNum = 392;BA.debugLine="cat1.AddList(\"TypeCP\", \"TypeCampagne\", \"Type de c";
_cat1.AddList("TypeCP","TypeCampagne","Type de campagne","Combles",anywheresoftware.b4a.keywords.Common.ArrayToList(new String[]{"Combles","Plancher bas"}));
 //BA.debugLineNum = 393;BA.debugLine="cat1.AddEditText(\"cp1\", \"Campagne\", \"Nom de la ca";
_cat1.AddEditText("cp1","Campagne","Nom de la campagne précarité","");
 //BA.debugLineNum = 394;BA.debugLine="cat1.AddEditText(\"user1\", \"Operateur\", \"Operateur";
_cat1.AddEditText("user1","Operateur","Operateur du relevé","CNS");
 //BA.debugLineNum = 395;BA.debugLine="cat1.AddEditText(\"abrev\", \"Abreviation\", \"Abrévia";
_cat1.AddEditText("abrev","Abreviation","Abréviation de la campagne","CBL");
 //BA.debugLineNum = 396;BA.debugLine="cat2.Initialize(\"Photos\")";
_cat2.Initialize("Photos");
 //BA.debugLineNum = 397;BA.debugLine="cat2.AddList(\"Rotation\", \"Rotation\", \"Rotation au";
_cat2.AddList("Rotation","Rotation","Rotation auto des photos","",anywheresoftware.b4a.keywords.Common.ArrayToList(new String[]{"0","90","180","270"}));
 //BA.debugLineNum = 400;BA.debugLine="screen.AddPreferenceCategory(cat1)";
_vvvv0.AddPreferenceCategory(_cat1);
 //BA.debugLineNum = 401;BA.debugLine="screen.AddPreferenceCategory(cat2)";
_vvvv0.AddPreferenceCategory(_cat2);
 //BA.debugLineNum = 402;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvv5() throws Exception{
int _nbrow = 0;
String _curadresse = "";
boolean _isfilter = false;
String _stimestampcbl = "";
int _i = 0;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _bm1 = null;
String _ficphoto = "";
String _libphoto = "";
 //BA.debugLineNum = 172;BA.debugLine="Sub creeListe()";
 //BA.debugLineNum = 174;BA.debugLine="Dim nbrow As Int,curAdresse As String,isfilter As";
_nbrow = 0;
_curadresse = "";
_isfilter = false;
_stimestampcbl = "";
 //BA.debugLineNum = 177;BA.debugLine="If curTC=\"\" Or curTC=Null Then curTC=\"Type Capteu";
if ((mostCurrent._vvvvvvvvvvvvvvvvv7).equals("") || mostCurrent._vvvvvvvvvvvvvvvvv7== null) { 
mostCurrent._vvvvvvvvvvvvvvvvv7 = "Type Capteur";};
 //BA.debugLineNum = 178;BA.debugLine="isfilter=False";
_isfilter = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 179;BA.debugLine="lsMaint.Clear";
mostCurrent._lsmaint.Clear();
 //BA.debugLineNum = 180;BA.debugLine="fTable.Initialize(db.GetTable(\"Detail\"))";
_vvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"));
 //BA.debugLineNum = 181;BA.debugLine="nbrow=fTable.GetRowCount";
_nbrow = _vvvvv2.GetRowCount();
 //BA.debugLineNum = 182;BA.debugLine="If nbrow>=1 Then";
if (_nbrow>=1) { 
 //BA.debugLineNum = 184;BA.debugLine="startWait(\"creation de la liste des adresses...\"";
_vvvvvvvvvvvvvvvvvv6("creation de la liste des adresses...");
 //BA.debugLineNum = 185;BA.debugLine="fCursor.Initialize(db.GetTable(\"Detail\"),fTable.";
_vvvvvvvv2.Initialize(mostCurrent._vvvvvvvvvvvvvvvvv2.GetTable("Detail"),_vvvvv2.GetIndex("PrimaryKey"));
 //BA.debugLineNum = 186;BA.debugLine="fCursor.Reset";
_vvvvvvvv2.Reset();
 //BA.debugLineNum = 187;BA.debugLine="For i=0 To nbrow -1";
{
final int step11 = 1;
final int limit11 = (int) (_nbrow-1);
_i = (int) (0) ;
for (;(step11 > 0 && _i <= limit11) || (step11 < 0 && _i >= limit11) ;_i = ((int)(0 + _i + step11))  ) {
 //BA.debugLineNum = 188;BA.debugLine="fCursor.GetNextRow";
_vvvvvvvv2.GetNextRow();
 //BA.debugLineNum = 190;BA.debugLine="Dim bm1 As Bitmap,ficphoto As String,libPhoto A";
_bm1 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_ficphoto = "";
_libphoto = "";
 //BA.debugLineNum = 196;BA.debugLine="If fCursor.GetColumnValue(\"addrChantier1\")<>Nul";
if (_vvvvvvvv2.GetColumnValue("addrChantier1")!= null && (_vvvvvvvv2.GetColumnValue("addrChantier1")).equals((Object)("")) == false) { 
_curadresse = BA.ObjectToString(_vvvvvvvv2.GetColumnValue("addrChantier1"));}
else {
_curadresse = "(NoAdress)";};
 //BA.debugLineNum = 198;BA.debugLine="If fCursor.GetColumnValue(\"addrChantierVille\")<";
if (_vvvvvvvv2.GetColumnValue("addrChantierVille")!= null && (_vvvvvvvv2.GetColumnValue("addrChantierVille")).equals((Object)("")) == false) { 
_curadresse = _curadresse+" - "+BA.ObjectToString(_vvvvvvvv2.GetColumnValue("addrChantierVille"));}
else {
_curadresse = _curadresse+" (NoVille)";};
 //BA.debugLineNum = 200;BA.debugLine="If fCursor.GetColumnValue(\"timestamp\")<>Null Th";
if (_vvvvvvvv2.GetColumnValue("timestamp")!= null) { 
_stimestampcbl = BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (0xf0c7)))+" "+BA.ObjectToString(_vvvvvvvv2.GetColumnValue("timestamp"));}
else {
_stimestampcbl = "(pas encore saisi)";};
 //BA.debugLineNum = 203;BA.debugLine="If txtFiltre.Text<>Null And txtFiltre.Text<>\"\"";
if (mostCurrent._txtfiltre.getText()!= null && (mostCurrent._txtfiltre.getText()).equals("") == false) { 
 //BA.debugLineNum = 204;BA.debugLine="isfilter=True";
_isfilter = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 205;BA.debugLine="curFilter=txtFiltre.Text";
mostCurrent._vvvvvvvvvvvvvvvv5 = mostCurrent._txtfiltre.getText();
 }else {
 //BA.debugLineNum = 207;BA.debugLine="curFilter=\"\"";
mostCurrent._vvvvvvvvvvvvvvvv5 = "";
 };
 //BA.debugLineNum = 210;BA.debugLine="If (isfilter=True And curAdresse.ToUpperCase.Co";
if ((_isfilter==anywheresoftware.b4a.keywords.Common.True && _curadresse.toUpperCase().contains(mostCurrent._vvvvvvvvvvvvvvvv5.toUpperCase())) || _isfilter==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 211;BA.debugLine="lsMaint.AddTwoLines2(\"[\" & fCursor.GetColumnVa";
mostCurrent._lsmaint.AddTwoLines2(BA.ObjectToCharSequence("["+BA.ObjectToString(_vvvvvvvv2.GetColumnValue("ID"))+"] - "+_curadresse),BA.ObjectToCharSequence(_stimestampcbl),_vvvvvvvv2.GetColumnValue("ID"));
 };
 }
};
 //BA.debugLineNum = 217;BA.debugLine="lsMaint.SetSelection(0)";
mostCurrent._lsmaint.SetSelection((int) (0));
 //BA.debugLineNum = 218;BA.debugLine="stopWait";
_vvvvvvvvvvvvvvvvvv7();
 };
 //BA.debugLineNum = 222;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvv0(String _text) throws Exception{
int _i = 0;
int _curval = 0;
int _curpval = 0;
String _pass = "";
String _res = "";
String _curspval = "";
 //BA.debugLineNum = 314;BA.debugLine="Sub DecryptText(text As String) As String";
 //BA.debugLineNum = 315;BA.debugLine="Dim i As Int,curval As Int,curpval As Int,pass As";
_i = 0;
_curval = 0;
_curpval = 0;
_pass = "";
_res = "";
_curspval = "";
 //BA.debugLineNum = 317;BA.debugLine="res=\"\"";
_res = "";
 //BA.debugLineNum = 327;BA.debugLine="Return res";
if (true) return _res;
 //BA.debugLineNum = 328;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvv7(String _text) throws Exception{
int _i = 0;
int _curval = 0;
int _curpval = 0;
String _pass = "";
String _res = "";
String _curspval = "";
 //BA.debugLineNum = 301;BA.debugLine="Sub EncryptText(text As String) As String";
 //BA.debugLineNum = 302;BA.debugLine="Dim i As Int,curval As Int,curpval As Int,pass As";
_i = 0;
_curval = 0;
_curpval = 0;
_pass = "";
_res = "";
_curspval = "";
 //BA.debugLineNum = 303;BA.debugLine="res=\"\"";
_res = "";
 //BA.debugLineNum = 304;BA.debugLine="pass=PASSKEY";
_pass = _vvvvvvvv5;
 //BA.debugLineNum = 305;BA.debugLine="For i=0 To text.Length -1";
{
final int step4 = 1;
final int limit4 = (int) (_text.length()-1);
_i = (int) (0) ;
for (;(step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4) ;_i = ((int)(0 + _i + step4))  ) {
 //BA.debugLineNum = 306;BA.debugLine="curval=Asc(text.CharAt(i))";
_curval = anywheresoftware.b4a.keywords.Common.Asc(_text.charAt(_i));
 //BA.debugLineNum = 308;BA.debugLine="curpval=Asc(pass.CharAt(i))";
_curpval = anywheresoftware.b4a.keywords.Common.Asc(_pass.charAt(_i));
 //BA.debugLineNum = 309;BA.debugLine="res=res & Chr(48 + (curval+curpval) Mod 10)";
_res = _res+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Chr((int) (48+(_curval+_curpval)%10)));
 }
};
 //BA.debugLineNum = 311;BA.debugLine="Return res";
if (true) return _res;
 //BA.debugLineNum = 312;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvv6() throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
int _api = 0;
int _id = 0;
 //BA.debugLineNum = 280;BA.debugLine="public Sub GetDeviceId As String";
 //BA.debugLineNum = 282;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 283;BA.debugLine="Dim Api As Int";
_api = 0;
 //BA.debugLineNum = 284;BA.debugLine="Api = r.GetStaticField(\"android.os.Build$VERSION\"";
_api = (int)(BA.ObjectToNumber(_r.GetStaticField("android.os.Build$VERSION","SDK_INT")));
 //BA.debugLineNum = 285;BA.debugLine="If Api < 9 Then";
if (_api<9) { 
 //BA.debugLineNum = 287;BA.debugLine="If File.Exists(File.DirInternal, \"__id\") Then";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"__id")) { 
 //BA.debugLineNum = 288;BA.debugLine="Return File.ReadString(File.DirInternal, \"__id\"";
if (true) return anywheresoftware.b4a.keywords.Common.File.ReadString(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"__id");
 }else {
 //BA.debugLineNum = 290;BA.debugLine="Dim id As Int";
_id = 0;
 //BA.debugLineNum = 291;BA.debugLine="id = Rnd(0x10000000, 0x7FFFFFFF)";
_id = anywheresoftware.b4a.keywords.Common.Rnd((int) (0x10000000),(int) (0x7fffffff));
 //BA.debugLineNum = 292;BA.debugLine="File.WriteString(File.DirInternal, \"__id\", id)";
anywheresoftware.b4a.keywords.Common.File.WriteString(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"__id",BA.NumberToString(_id));
 //BA.debugLineNum = 293;BA.debugLine="Return id";
if (true) return BA.NumberToString(_id);
 };
 }else {
 //BA.debugLineNum = 297;BA.debugLine="Return r.GetStaticField(\"android.os.Build\", \"SER";
if (true) return BA.ObjectToString(_r.GetStaticField("android.os.Build","SERIAL"));
 };
 //BA.debugLineNum = 299;BA.debugLine="End Sub";
return "";
}
public static int  _vvvvvvvvvvvvvvvvvv0(String _svalue) throws Exception{
int _p1 = 0;
int _p2 = 0;
String _res1 = "";
int _res = 0;
 //BA.debugLineNum = 159;BA.debugLine="Sub getID(sValue As String) As Int";
 //BA.debugLineNum = 160;BA.debugLine="Try";
try { //BA.debugLineNum = 161;BA.debugLine="Dim p1 As Int,p2 As Int,res1 As String,res As In";
_p1 = 0;
_p2 = 0;
_res1 = "";
_res = 0;
 //BA.debugLineNum = 162;BA.debugLine="p1=sValue.IndexOf(\"[\")";
_p1 = _svalue.indexOf("[");
 //BA.debugLineNum = 163;BA.debugLine="p2=sValue.IndexOf(\"]\")";
_p2 = _svalue.indexOf("]");
 //BA.debugLineNum = 164;BA.debugLine="res1=sValue.SubString2(p1+1,p2)";
_res1 = _svalue.substring((int) (_p1+1),_p2);
 //BA.debugLineNum = 165;BA.debugLine="res=res1";
_res = (int)(Double.parseDouble(_res1));
 //BA.debugLineNum = 166;BA.debugLine="Return res";
if (true) return _res;
 } 
       catch (Exception e9) {
			processBA.setLastException(e9); //BA.debugLineNum = 168;BA.debugLine="Return 1";
if (true) return (int) (1);
 };
 //BA.debugLineNum = 170;BA.debugLine="End Sub";
return 0;
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 34;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 35;BA.debugLine="Dim CNSPath As String,curTC As String,curFilter A";
mostCurrent._vvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvv7 = "";
mostCurrent._vvvvvvvvvvvvvvvv5 = "";
mostCurrent._vvvvvvvvvvvvvvvvv0 = "";
mostCurrent._vvvvvvvvvvvvvvvvvv2 = "";
mostCurrent._vvvvvvvvvvvvvvvvvv1 = "";
mostCurrent._vvvvvvvvvvvvvvvvvv3 = "";
 //BA.debugLineNum = 36;BA.debugLine="Dim db As JackcessDatabase,fCampagne As JackcessT";
mostCurrent._vvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase();
mostCurrent._vvvvvvvvvvvvvvvvvvv1 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 37;BA.debugLine="Dim lsMaint As ListView";
mostCurrent._lsmaint = new anywheresoftware.b4a.objects.ListViewWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private spnVal2 As Spinner";
mostCurrent._vvvvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private lblPorte As Label";
mostCurrent._vvvvvvvvvvvvvvvvvvv3 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private lblPlancher As Label";
mostCurrent._lblplancher = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private lblCloison As Label";
mostCurrent._vvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private lblLocal As Label";
mostCurrent._lbllocal = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private lblIsolant As Label";
mostCurrent._lblisolant = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private cbAccesPCommunes As CheckBox";
mostCurrent._cbaccespcommunes = new anywheresoftware.b4a.objects.CompoundButtonWrapper.CheckBoxWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private spnTypePorte As Spinner";
mostCurrent._spntypeporte = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Private spnTypePlancher As Spinner";
mostCurrent._spntypeplancher = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private spnTypeCloison As Spinner";
mostCurrent._spntypecloison = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Private spnTypeLocal As Spinner";
mostCurrent._spntypelocal = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 49;BA.debugLine="Private spnTypeIsolant As Spinner";
mostCurrent._spntypeisolant = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 50;BA.debugLine="Private lblFiltre As Label";
mostCurrent._lblfiltre = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 51;BA.debugLine="Private txtFiltre As EditText";
mostCurrent._txtfiltre = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 52;BA.debugLine="Private btnFiltre As Button";
mostCurrent._btnfiltre = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 53;BA.debugLine="Private btnQuit As Label";
mostCurrent._btnquit = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 54;BA.debugLine="Private lblAppName As Label";
mostCurrent._lblappname = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 55;BA.debugLine="Private PbZip As ProgressBar";
mostCurrent._pbzip = new anywheresoftware.b4a.objects.ProgressBarWrapper();
 //BA.debugLineNum = 56;BA.debugLine="Private lblWait As Label";
mostCurrent._lblwait = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 57;BA.debugLine="End Sub";
return "";
}
public static String  _lblappli_longclick() throws Exception{
 //BA.debugLineNum = 460;BA.debugLine="Sub lblAppli_LongClick";
 //BA.debugLineNum = 462;BA.debugLine="End Sub";
return "";
}
public static String  _lsmaint_itemclick(int _position,Object _value) throws Exception{
 //BA.debugLineNum = 153;BA.debugLine="Sub lsMaint_ItemClick (Position As Int, Value As O";
 //BA.debugLineNum = 155;BA.debugLine="gPosition=Value";
_vvvv5 = (int)(BA.ObjectToNumber(_value));
 //BA.debugLineNum = 156;BA.debugLine="StartActivity(\"Detail\")";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)("Detail"));
 //BA.debugLineNum = 157;BA.debugLine="End Sub";
return "";
}
public static String  _optcampagne_click() throws Exception{
 //BA.debugLineNum = 266;BA.debugLine="Sub OptCampagne_click";
 //BA.debugLineNum = 268;BA.debugLine="Try";
try { //BA.debugLineNum = 269;BA.debugLine="StartActivity(screen.CreateIntent)";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)(_vvvv0.CreateIntent()));
 } 
       catch (Exception e4) {
			processBA.setLastException(e4); //BA.debugLineNum = 271;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 273;BA.debugLine="End Sub";
return "";
}
public static String  _options_click() throws Exception{
 //BA.debugLineNum = 275;BA.debugLine="Sub Options_Click";
 //BA.debugLineNum = 276;BA.debugLine="Activity.OpenMenu";
mostCurrent._activity.OpenMenu();
 //BA.debugLineNum = 277;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
starter._process_globals();
detail._process_globals();
statemanager._process_globals();
signaturecapture._process_globals();
signaturescreen._process_globals();
gmaps._process_globals();
draw1._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 19;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 22;BA.debugLine="Dim gPosition As Int,TypeAppli As String,DeviceID";
_vvvv5 = 0;
_vvvv6 = "";
_vvvv7 = "";
 //BA.debugLineNum = 23;BA.debugLine="Public screen As PreferenceScreen, manager As Pre";
_vvvv0 = new anywheresoftware.b4a.objects.preferenceactivity.PreferenceScreenWrapper();
_vvvvv1 = new anywheresoftware.b4a.objects.preferenceactivity.PreferenceManager();
 //BA.debugLineNum = 24;BA.debugLine="Public fTable As JackcessTable,fTableLieux As Jac";
_vvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvv3 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvv4 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvv5 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 25;BA.debugLine="Public fTableSite As JackcessTable,fTableMateriel";
_vvvvv6 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvv7 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvv0 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv1 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 26;BA.debugLine="Public fTableTypeIsolant As JackcessTable,fTableT";
_vvvvvv3 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv4 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv5 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv6 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvv7 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 27;BA.debugLine="Public fTableMateriauPrevu As JackcessTable,fTabl";
_vvvvvv0 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv1 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv3 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv4 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 28;BA.debugLine="Public fTableCBLIsolant As JackcessTable,fTableCB";
_vvvvvvv5 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv6 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv7 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvv0 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
_vvvvvvvv1 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessTable();
 //BA.debugLineNum = 29;BA.debugLine="Public fCursor As JackcessIndexCursor,campcursor";
_vvvvvvvv2 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessIndexCursor();
_vvvvvvvv3 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessCursor();
_vvvvvvvv4 = new anywheresoftware.b4a.objects.JackcessDatabase.JackcessIndexCursor();
 //BA.debugLineNum = 31;BA.debugLine="Public PASSKEY As String = \"854741759688667123652";
_vvvvvvvv5 = BA.__b (new byte[] {121,107,36,-44,120,112,40,-35,110,40,49,-43,116,106,48,-44,124,97,50,-64,116,96,114,-42,108,109}, 578586);
 //BA.debugLineNum = 32;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvv4() throws Exception{
 //BA.debugLineNum = 444;BA.debugLine="Sub ReadCampagneSettings";
 //BA.debugLineNum = 446;BA.debugLine="If campcursor.GetColumnValue(\"Nom\")<>Null And cam";
if (_vvvvvvvv3.GetColumnValue("Nom")!= null && (_vvvvvvvv3.GetColumnValue("Nom")).equals((Object)("")) == false) { 
_vvvvv1.SetString("cp1",BA.ObjectToString(_vvvvvvvv3.GetColumnValue("Nom")));}
else {
_vvvvv1.SetString("cp1","CampagnePreca1");};
 //BA.debugLineNum = 447;BA.debugLine="If campcursor.GetColumnValue(\"Type\")<>Null And ca";
if (_vvvvvvvv3.GetColumnValue("Type")!= null && (_vvvvvvvv3.GetColumnValue("Type")).equals((Object)("")) == false) { 
 //BA.debugLineNum = 448;BA.debugLine="manager.SetString(\"TypeCP\", campcursor.GetColumn";
_vvvvv1.SetString("TypeCP",BA.ObjectToString(_vvvvvvvv3.GetColumnValue("Type")));
 //BA.debugLineNum = 449;BA.debugLine="TypeAppli=campcursor.GetColumnValue(\"Type\")";
_vvvv6 = BA.ObjectToString(_vvvvvvvv3.GetColumnValue("Type"));
 }else {
 //BA.debugLineNum = 451;BA.debugLine="manager.SetString(\"TypeCP\", \"Combles\")";
_vvvvv1.SetString("TypeCP","Combles");
 //BA.debugLineNum = 452;BA.debugLine="TypeAppli=\"Combles\"";
_vvvv6 = "Combles";
 };
 //BA.debugLineNum = 454;BA.debugLine="lblAppName.Text=\"Visite technique \" & TypeAppli.T";
mostCurrent._lblappname.setText(BA.ObjectToCharSequence("Visite technique "+_vvvv6.toLowerCase()));
 //BA.debugLineNum = 455;BA.debugLine="If campcursor.GetColumnValue(\"Abrev\")<>Null And c";
if (_vvvvvvvv3.GetColumnValue("Abrev")!= null && (_vvvvvvvv3.GetColumnValue("Abrev")).equals((Object)("")) == false) { 
_vvvvv1.SetString("abrev",BA.ObjectToString(_vvvvvvvv3.GetColumnValue("Abrev")));}
else {
_vvvvv1.SetString("abrev","CBL");};
 //BA.debugLineNum = 456;BA.debugLine="If campcursor.GetColumnValue(\"Operateur\")<>Null A";
if (_vvvvvvvv3.GetColumnValue("Operateur")!= null && (_vvvvvvvv3.GetColumnValue("Operateur")).equals((Object)("")) == false) { 
_vvvvv1.SetString("user1",BA.ObjectToString(_vvvvvvvv3.GetColumnValue("Operateur")));}
else {
_vvvvv1.SetString("user1","Operateur");};
 //BA.debugLineNum = 458;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvv3() throws Exception{
 //BA.debugLineNum = 331;BA.debugLine="Sub ReadLicenceCode As String";
 //BA.debugLineNum = 334;BA.debugLine="Try";
try { //BA.debugLineNum = 335;BA.debugLine="If Not(File.Exists(File.DirRootExternal, \"cbl.li";
if (anywheresoftware.b4a.keywords.Common.Not(anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"cbl.lic"))) { 
 //BA.debugLineNum = 336;BA.debugLine="Return \"\"";
if (true) return "";
 };
 //BA.debugLineNum = 338;BA.debugLine="Return File.ReadString(File.DirRootExternal, \"cb";
if (true) return anywheresoftware.b4a.keywords.Common.File.ReadString(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"cbl.lic");
 } 
       catch (Exception e7) {
			processBA.setLastException(e7); //BA.debugLineNum = 340;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 341;BA.debugLine="Return \"\"";
if (true) return "";
 };
 //BA.debugLineNum = 343;BA.debugLine="End Sub";
return "";
}
public static String  _resetdb_click() throws Exception{
int _i = 0;
anywheresoftware.b4a.objects.collections.List _filestodelete = null;
 //BA.debugLineNum = 242;BA.debugLine="Sub ResetDB_click";
 //BA.debugLineNum = 244;BA.debugLine="Try";
try { //BA.debugLineNum = 245;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 246;BA.debugLine="i = Msgbox2(\"Voulez-vous repartir de zéro et qui";
_i = anywheresoftware.b4a.keywords.Common.Msgbox2(BA.ObjectToCharSequence("Voulez-vous repartir de zéro et quitter ?  (Cela va effacer toutes les données et photos en cours)"),BA.ObjectToCharSequence("Réinitialiser le relevé"),"Oui","","Non",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 247;BA.debugLine="If i=DialogResponse.POSITIVE Then";
if (_i==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 248;BA.debugLine="If db.IsOpen Then db.Close";
if (mostCurrent._vvvvvvvvvvvvvvvvv2.IsOpen()) { 
mostCurrent._vvvvvvvvvvvvvvvvv2.Close();};
 //BA.debugLineNum = 250;BA.debugLine="Dim FilesToDelete As List";
_filestodelete = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 251;BA.debugLine="FilesToDelete.Initialize";
_filestodelete.Initialize();
 //BA.debugLineNum = 252;BA.debugLine="FilesToDelete.AddAll(File.ListFiles(CNSPath))";
_filestodelete.AddAll(anywheresoftware.b4a.keywords.Common.File.ListFiles(mostCurrent._vvvvvvvvvvvvvvvvv1));
 //BA.debugLineNum = 253;BA.debugLine="For I = 0 To FilesToDelete.Size -1";
{
final int step9 = 1;
final int limit9 = (int) (_filestodelete.getSize()-1);
_i = (int) (0) ;
for (;(step9 > 0 && _i <= limit9) || (step9 < 0 && _i >= limit9) ;_i = ((int)(0 + _i + step9))  ) {
 //BA.debugLineNum = 254;BA.debugLine="File.Delete(CNSPath, FilesToDelete.Get(I))";
anywheresoftware.b4a.keywords.Common.File.Delete(mostCurrent._vvvvvvvvvvvvvvvvv1,BA.ObjectToString(_filestodelete.Get(_i)));
 }
};
 //BA.debugLineNum = 256;BA.debugLine="File.Delete(CNSPath,\"\")";
anywheresoftware.b4a.keywords.Common.File.Delete(mostCurrent._vvvvvvvvvvvvvvvvv1,"");
 //BA.debugLineNum = 257;BA.debugLine="ToastMessageShow(\"Relevé Precarité supprimé\",Tr";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Relevé Precarité supprimé"),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 258;BA.debugLine="ExitApplication";
anywheresoftware.b4a.keywords.Common.ExitApplication();
 };
 } 
       catch (Exception e17) {
			processBA.setLastException(e17); //BA.debugLineNum = 262;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 264;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvv6() throws Exception{
 //BA.debugLineNum = 378;BA.debugLine="Sub SetDefaults";
 //BA.debugLineNum = 380;BA.debugLine="manager.SetString(\"cp1\", \"CampagnePreca1\")";
_vvvvv1.SetString("cp1","CampagnePreca1");
 //BA.debugLineNum = 381;BA.debugLine="manager.SetString(\"Rotation\", \"0\")";
_vvvvv1.SetString("Rotation","0");
 //BA.debugLineNum = 382;BA.debugLine="manager.SetString(\"TypeCP\", \"Combles\") 'A modifie";
_vvvvv1.SetString("TypeCP","Combles");
 //BA.debugLineNum = 383;BA.debugLine="manager.SetString(\"abrev\", \"CBL\")  'A modifier pa";
_vvvvv1.SetString("abrev","CBL");
 //BA.debugLineNum = 384;BA.debugLine="manager.SetString(\"user1\", \"OperateurPreca\")";
_vvvvv1.SetString("user1","OperateurPreca");
 //BA.debugLineNum = 385;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvv6(String _msg) throws Exception{
 //BA.debugLineNum = 464;BA.debugLine="Sub startWait(msg As String)";
 //BA.debugLineNum = 465;BA.debugLine="PbZip.Visible=True";
mostCurrent._pbzip.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 466;BA.debugLine="lblWait.Text=msg";
mostCurrent._lblwait.setText(BA.ObjectToCharSequence(_msg));
 //BA.debugLineNum = 467;BA.debugLine="lblWait.Visible=True";
mostCurrent._lblwait.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 468;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvvv7() throws Exception{
 //BA.debugLineNum = 470;BA.debugLine="Sub stopWait";
 //BA.debugLineNum = 471;BA.debugLine="PbZip.Visible=False";
mostCurrent._pbzip.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 472;BA.debugLine="lblWait.Visible=False";
mostCurrent._lblwait.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 473;BA.debugLine="End Sub";
return "";
}
public static String  _tgvalide_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 238;BA.debugLine="Sub tgValide_CheckedChange(Checked As Boolean)";
 //BA.debugLineNum = 239;BA.debugLine="creeListe";
_vvvvvvvvvvvvvvvvvv5();
 //BA.debugLineNum = 240;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvvvvvvvvvvvvvv4() throws Exception{
anywheresoftware.b4a.agraham.dialogs.InputDialog _dlglicence = null;
String _sellicence = "";
int _ret = 0;
String _licencecode = "";
 //BA.debugLineNum = 345;BA.debugLine="Sub ValidateLicence";
 //BA.debugLineNum = 347;BA.debugLine="Dim dlgLicence As InputDialog,selLicence As Strin";
_dlglicence = new anywheresoftware.b4a.agraham.dialogs.InputDialog();
_sellicence = "";
_ret = 0;
 //BA.debugLineNum = 348;BA.debugLine="Dim DeviceID As String = GetDeviceId, LicenceCode";
_vvvv7 = _vvvvvvvvvvvvvvvv6();
_licencecode = "";
 //BA.debugLineNum = 349;BA.debugLine="LicenceCode=EncryptText(DeviceID)";
_licencecode = _vvvvvvvvvvvvvvvv7(_vvvv7);
 //BA.debugLineNum = 350;BA.debugLine="dlgLicence.Hint = \"DeviceID : \" & DeviceID";
_dlglicence.setHint("DeviceID : "+_vvvv7);
 //BA.debugLineNum = 351;BA.debugLine="dlgLicence.HintColor = Colors.ARGB(196, 255, 140,";
_dlglicence.setHintColor(anywheresoftware.b4a.keywords.Common.Colors.ARGB((int) (196),(int) (255),(int) (140),(int) (0)));
 //BA.debugLineNum = 352;BA.debugLine="dlgLicence.InputType=dlgLicence.INPUT_TYPE_NUMBER";
_dlglicence.setInputType(_dlglicence.INPUT_TYPE_NUMBERS);
 //BA.debugLineNum = 353;BA.debugLine="ret = DialogResponse.CANCEL";
_ret = anywheresoftware.b4a.keywords.Common.DialogResponse.CANCEL;
 //BA.debugLineNum = 354;BA.debugLine="ret = dlgLicence.Show(\"le code de licence est fou";
_ret = _dlglicence.Show("le code de licence est fourni par CNSolutions","Entrez le code de licence","Oui","Non",BA.ObjectToString(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA,(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null));
 //BA.debugLineNum = 355;BA.debugLine="selLicence=dlgLicence.Input";
_sellicence = _dlglicence.getInput();
 //BA.debugLineNum = 356;BA.debugLine="If selLicence<>\"\" And selLicence.Length>2   Then";
if ((_sellicence).equals("") == false && _sellicence.length()>2) { 
 //BA.debugLineNum = 357;BA.debugLine="If selLicence.EqualsIgnoreCase(LicenceCode) Then";
if (_sellicence.equalsIgnoreCase(_licencecode)) { 
 //BA.debugLineNum = 360;BA.debugLine="File.WriteString(File.DirRootExternal, \"cbl.lic";
anywheresoftware.b4a.keywords.Common.File.WriteString(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"cbl.lic",_sellicence);
 }else {
 //BA.debugLineNum = 362;BA.debugLine="ToastMessageShow(\"Le code de licence est invali";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Le code de licence est invalide : Application terminée"),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 363;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 };
 }else {
 //BA.debugLineNum = 366;BA.debugLine="ToastMessageShow(\"Le code de licence est invalid";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Le code de licence est invalide : Application terminée"),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 367;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 };
 //BA.debugLineNum = 369;BA.debugLine="End Sub";
return "";
}
}
