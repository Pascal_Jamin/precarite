package precarite.cns;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class signaturescreen extends Activity implements B4AActivity{
	public static signaturescreen mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "precarite.cns", "precarite.cns.signaturescreen");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (signaturescreen).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "precarite.cns", "precarite.cns.signaturescreen");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "precarite.cns.signaturescreen", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (signaturescreen) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (signaturescreen) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return signaturescreen.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (signaturescreen) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (signaturescreen) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.PanelWrapper _panelsign = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = null;
public precarite.cns.signaturecapture._signaturedata _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsignclear = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsignsave = null;
public static String _vvvvvvvvvvvvvvvvv1 = "";
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 25;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 29;BA.debugLine="Activity.LoadLayout(\"signature\")";
mostCurrent._activity.LoadLayout("signature",mostCurrent.activityBA);
 //BA.debugLineNum = 30;BA.debugLine="Activity.Title=\"Signature du résident\"";
mostCurrent._activity.setTitle(BA.ObjectToCharSequence("Signature du résident"));
 //BA.debugLineNum = 31;BA.debugLine="CNSPath=File.DirRootExternal & \"/RelevePrecarite\"";
mostCurrent._vvvvvvvvvvvvvvvvv1 = anywheresoftware.b4a.keywords.Common.File.getDirRootExternal()+"/RelevePrecarite";
 //BA.debugLineNum = 32;BA.debugLine="Canvas1.Initialize(PanelSign)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4.Initialize((android.view.View)(mostCurrent._panelsign.getObject()));
 //BA.debugLineNum = 33;BA.debugLine="SD.Initialize";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Initialize();
 //BA.debugLineNum = 34;BA.debugLine="SD.Canvas = Canvas1";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Canvas = mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 35;BA.debugLine="SD.Panel = PanelSign";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Panel = mostCurrent._panelsign;
 //BA.debugLineNum = 36;BA.debugLine="SD.SignatureColor = Colors.Black";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.SignatureColor = anywheresoftware.b4a.keywords.Common.Colors.Black;
 //BA.debugLineNum = 37;BA.debugLine="SD.SignatureWidth = 3dip 'Stroke width";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.SignatureWidth = anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (3));
 //BA.debugLineNum = 41;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 47;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 49;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 43;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 45;BA.debugLine="End Sub";
return "";
}
public static String  _btnsignclear_click() throws Exception{
 //BA.debugLineNum = 70;BA.debugLine="Sub btnSignClear_Click";
 //BA.debugLineNum = 71;BA.debugLine="SignatureCapture.Clear(SD)";
mostCurrent._vvvvvvvvvvvvvvvv1._vvv0(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5);
 //BA.debugLineNum = 72;BA.debugLine="End Sub";
return "";
}
public static String  _btnsignsave_click() throws Exception{
String _dt = "";
String _filepic = "";
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imgrotated = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imgsource = null;
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _outbmp = null;
 //BA.debugLineNum = 74;BA.debugLine="Sub btnSignSave_Click";
 //BA.debugLineNum = 75;BA.debugLine="Dim dt As String,filepic As String,imgRotated As";
_dt = "";
_filepic = "";
_imgrotated = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_imgsource = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_outbmp = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 79;BA.debugLine="If Detail.gPic<>\"\" And Detail.gPic<>\"null\" And De";
if ((mostCurrent._vvvvvvvvvvvvvvv7._v7).equals("") == false && (mostCurrent._vvvvvvvvvvvvvvv7._v7).equals("null") == false && mostCurrent._vvvvvvvvvvvvvvv7._v7.contains("Signature_")) { 
 //BA.debugLineNum = 81;BA.debugLine="SignatureCapture.Save(SD, CNSPath,\"tempsign.jpg\"";
mostCurrent._vvvvvvvvvvvvvvvv1._vvvv1(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5,mostCurrent._vvvvvvvvvvvvvvvvv1,"tempsign.jpg");
 //BA.debugLineNum = 83;BA.debugLine="imgSource=LoadBitmap(CNSPath,\"tempsign.jpg\")";
_imgsource = anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvvvv1,"tempsign.jpg");
 //BA.debugLineNum = 84;BA.debugLine="imgRotated=RotateImage(imgSource,270)";
_imgrotated = _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv5(_imgsource,(float) (270));
 //BA.debugLineNum = 85;BA.debugLine="outBmp=File.OpenOutput(CNSPath,Detail.gPic,True)";
_outbmp = anywheresoftware.b4a.keywords.Common.File.OpenOutput(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvv7._v7,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 86;BA.debugLine="imgRotated.WriteToStream(outBmp,90,\"JPEG\")";
_imgrotated.WriteToStream((java.io.OutputStream)(_outbmp.getObject()),(int) (90),BA.getEnumFromString(android.graphics.Bitmap.CompressFormat.class,"JPEG"));
 //BA.debugLineNum = 87;BA.debugLine="outBmp.Flush";
_outbmp.Flush();
 //BA.debugLineNum = 88;BA.debugLine="outBmp.Close";
_outbmp.Close();
 //BA.debugLineNum = 89;BA.debugLine="ToastMessageShow(\"Signature saved to: \" & File.C";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Signature saved to: "+anywheresoftware.b4a.keywords.Common.File.Combine(mostCurrent._vvvvvvvvvvvvvvvvv1,mostCurrent._vvvvvvvvvvvvvvv7._v7)),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 90;BA.debugLine="Detail.gPic=\"\"";
mostCurrent._vvvvvvvvvvvvvvv7._v7 = "";
 };
 //BA.debugLineNum = 92;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 93;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 13;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 16;BA.debugLine="Private PanelSign As Panel";
mostCurrent._panelsign = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Dim Canvas1 As Canvas";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Dim SD As SignatureData 'This object holds the da";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = new precarite.cns.signaturecapture._signaturedata();
 //BA.debugLineNum = 19;BA.debugLine="Private btnSignClear As Button";
mostCurrent._btnsignclear = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private btnSignSave As Button";
mostCurrent._btnsignsave = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private CNSPath As String";
mostCurrent._vvvvvvvvvvvvvvvvv1 = "";
 //BA.debugLineNum = 23;BA.debugLine="End Sub";
return "";
}
public static String  _panelsign_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 51;BA.debugLine="Sub PanelSign_Touch (Action As Int, X As Float, Y";
 //BA.debugLineNum = 52;BA.debugLine="SignatureCapture.Panel_Touch(SD, x, y, Action)";
mostCurrent._vvvvvvvvvvvvvvvv1._panel_touch(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5,(int) (_x),(int) (_y),_action);
 //BA.debugLineNum = 53;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvv5(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _original,float _degree) throws Exception{
anywheresoftware.b4j.object.JavaObject _matrix = null;
anywheresoftware.b4j.object.JavaObject _bmp = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _newimage = null;
 //BA.debugLineNum = 60;BA.debugLine="Sub RotateImage(original As Bitmap, degree As Floa";
 //BA.debugLineNum = 61;BA.debugLine="Dim matrix As JavaObject";
_matrix = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 62;BA.debugLine="matrix.InitializeNewInstance(\"android.graphics.Ma";
_matrix.InitializeNewInstance("android.graphics.Matrix",(Object[])(anywheresoftware.b4a.keywords.Common.Null));
 //BA.debugLineNum = 63;BA.debugLine="matrix.RunMethod(\"postRotate\", Array(degree))";
_matrix.RunMethod("postRotate",new Object[]{(Object)(_degree)});
 //BA.debugLineNum = 64;BA.debugLine="Dim bmp As JavaObject";
_bmp = new anywheresoftware.b4j.object.JavaObject();
 //BA.debugLineNum = 65;BA.debugLine="bmp.InitializeStatic(\"android.graphics.Bitmap\")";
_bmp.InitializeStatic("android.graphics.Bitmap");
 //BA.debugLineNum = 66;BA.debugLine="Dim NewImage As Bitmap = bmp.RunMethod(\"createBit";
_newimage = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
_newimage.setObject((android.graphics.Bitmap)(_bmp.RunMethod("createBitmap",new Object[]{(Object)(_original.getObject()),(Object)(0),(Object)(0),(Object)(_original.getWidth()),(Object)(_original.getHeight()),(Object)(_matrix.getObject()),(Object)(anywheresoftware.b4a.keywords.Common.True)})));
 //BA.debugLineNum = 67;BA.debugLine="Return NewImage";
if (true) return _newimage;
 //BA.debugLineNum = 68;BA.debugLine="End Sub";
return null;
}
public static String  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv6(anywheresoftware.b4a.objects.ConcreteViewWrapper _v,float _angle) throws Exception{
anywheresoftware.b4j.object.JavaObject _jo = null;
 //BA.debugLineNum = 55;BA.debugLine="Sub setRotationX(v As View, Angle As Float)";
 //BA.debugLineNum = 56;BA.debugLine="Dim jo = v As JavaObject";
_jo = new anywheresoftware.b4j.object.JavaObject();
_jo.setObject((java.lang.Object)(_v.getObject()));
 //BA.debugLineNum = 57;BA.debugLine="jo.RunMethod(\"setRotationX\", Array As Object(Angl";
_jo.RunMethod("setRotationX",new Object[]{(Object)(_angle)});
 //BA.debugLineNum = 58;BA.debugLine="End Sub";
return "";
}
}
