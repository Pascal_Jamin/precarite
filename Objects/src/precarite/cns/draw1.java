package precarite.cns;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class draw1 extends Activity implements B4AActivity{
	public static draw1 mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "precarite.cns", "precarite.cns.draw1");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (draw1).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "precarite.cns", "precarite.cns.draw1");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "precarite.cns.draw1", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (draw1) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (draw1) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return draw1.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (draw1) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (draw1) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnret = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview3 = null;
public precarite.cns.signaturecapture._signaturedata _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = null;
public anywheresoftware.b4a.objects.drawable.CanvasWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = null;
public anywheresoftware.b4a.objects.PanelWrapper _panel2 = null;
public de.donmanfred.IconButtonWrapper _btnsave = null;
public de.donmanfred.IconButtonWrapper _btnclear = null;
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.gmaps _vvvvvvvvvvvvvvvv3 = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 27;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 29;BA.debugLine="Activity.LoadLayout(\"Dessin\")";
mostCurrent._activity.LoadLayout("Dessin",mostCurrent.activityBA);
 //BA.debugLineNum = 30;BA.debugLine="Try";
try { //BA.debugLineNum = 31;BA.debugLine="If Detail.gPic<>\"\" Then";
if ((mostCurrent._vvvvvvvvvvvvvvv7._v7).equals("") == false) { 
 //BA.debugLineNum = 32;BA.debugLine="ImageView2.Bitmap=LoadBitmap(Detail.gPath,Detai";
mostCurrent._imageview2.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(mostCurrent._vvvvvvvvvvvvvvv7._v6,mostCurrent._vvvvvvvvvvvvvvv7._v7).getObject()));
 };
 } 
       catch (Exception e7) {
			processBA.setLastException(e7); //BA.debugLineNum = 35;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 };
 //BA.debugLineNum = 39;BA.debugLine="Canvas1.Initialize(Panel2)";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4.Initialize((android.view.View)(mostCurrent._panel2.getObject()));
 //BA.debugLineNum = 40;BA.debugLine="SD.Initialize";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Initialize();
 //BA.debugLineNum = 41;BA.debugLine="SD.Canvas = Canvas1";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Canvas = mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4;
 //BA.debugLineNum = 42;BA.debugLine="SD.Panel = Panel2";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.Panel = mostCurrent._panel2;
 //BA.debugLineNum = 43;BA.debugLine="SD.SignatureColor = Colors.Red";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.SignatureColor = anywheresoftware.b4a.keywords.Common.Colors.Red;
 //BA.debugLineNum = 44;BA.debugLine="SD.SignatureWidth = 4dip 'Stroke width";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5.SignatureWidth = anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4));
 //BA.debugLineNum = 46;BA.debugLine="btnSave.Text=\"SAVE\"";
mostCurrent._btnsave.setText("SAVE");
 //BA.debugLineNum = 47;BA.debugLine="btnClear.Text=\"CLEAR\"";
mostCurrent._btnclear.setText("CLEAR");
 //BA.debugLineNum = 49;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 55;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 57;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 51;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 53;BA.debugLine="End Sub";
return "";
}
public static String  _btnclear_click() throws Exception{
 //BA.debugLineNum = 127;BA.debugLine="Sub btnClear_Click";
 //BA.debugLineNum = 128;BA.debugLine="SignatureCapture.Clear(SD)";
mostCurrent._vvvvvvvvvvvvvvvv1._vvv0(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5);
 //BA.debugLineNum = 129;BA.debugLine="Panel2.Invalidate";
mostCurrent._panel2.Invalidate();
 //BA.debugLineNum = 130;BA.debugLine="End Sub";
return "";
}
public static String  _btnret_click() throws Exception{
 //BA.debugLineNum = 60;BA.debugLine="Sub btnRet_Click";
 //BA.debugLineNum = 61;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 62;BA.debugLine="StartActivity(Detail)";
anywheresoftware.b4a.keywords.Common.StartActivity(processBA,(Object)(mostCurrent._vvvvvvvvvvvvvvv7.getObject()));
 //BA.debugLineNum = 63;BA.debugLine="End Sub";
return "";
}
public static String  _btnsave_click() throws Exception{
anywheresoftware.b4a.objects.drawable.BitmapDrawable _imgforeground = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _imgbackground = null;
anywheresoftware.b4a.agraham.reflection.Reflection _obj1 = null;
anywheresoftware.b4a.objects.ImageViewWrapper _displayimg = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.RectWrapper _destrect = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper _cannewimg = null;
anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _out = null;
 //BA.debugLineNum = 71;BA.debugLine="Sub btnSave_Click";
 //BA.debugLineNum = 73;BA.debugLine="SignatureCapture.Save(SD, File.DirRootExternal, \"";
mostCurrent._vvvvvvvvvvvvvvvv1._vvvv1(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5,anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"sign.png");
 //BA.debugLineNum = 77;BA.debugLine="DoEvents";
anywheresoftware.b4a.keywords.Common.DoEvents();
 //BA.debugLineNum = 78;BA.debugLine="Try";
try { //BA.debugLineNum = 79;BA.debugLine="Dim imgForeGround As BitmapDrawable";
_imgforeground = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 80;BA.debugLine="imgForeGround.Initialize(LoadBitmap(File.DirR";
_imgforeground.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirRootExternal(),"sign.png").getObject()));
 //BA.debugLineNum = 82;BA.debugLine="Dim imgBackGround As Bitmap";
_imgbackground = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 83;BA.debugLine="imgBackGround.Initialize(Detail.gPath,Detail.gPi";
_imgbackground.Initialize(mostCurrent._vvvvvvvvvvvvvvv7._v6,mostCurrent._vvvvvvvvvvvvvvv7._v7);
 //BA.debugLineNum = 85;BA.debugLine="Dim obj1 As Reflector";
_obj1 = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 86;BA.debugLine="obj1.Target = imgForeGround";
_obj1.Target = (Object)(_imgforeground.getObject());
 //BA.debugLineNum = 87;BA.debugLine="obj1.RunMethod2(\"setAlpha\", 255, \"java.lang.int\"";
_obj1.RunMethod2("setAlpha",BA.NumberToString(255),"java.lang.int");
 //BA.debugLineNum = 89;BA.debugLine="Dim displayImg As ImageView";
_displayimg = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 90;BA.debugLine="displayImg.Initialize(\"displayImg\")";
_displayimg.Initialize(mostCurrent.activityBA,"displayImg");
 //BA.debugLineNum = 91;BA.debugLine="displayImg.Bitmap = imgBackGround";
_displayimg.setBitmap((android.graphics.Bitmap)(_imgbackground.getObject()));
 //BA.debugLineNum = 92;BA.debugLine="displayImg.Gravity = Gravity.FILL";
_displayimg.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.FILL);
 //BA.debugLineNum = 93;BA.debugLine="displayImg.Visible=False";
_displayimg.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 94;BA.debugLine="Activity.AddView(displayImg,0dip,50dip,100%x-0di";
mostCurrent._activity.AddView((android.view.View)(_displayimg.getObject()),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (0)),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50)),(int) (anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (0))),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (50))));
 //BA.debugLineNum = 96;BA.debugLine="Dim destRect As Rect";
_destrect = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.RectWrapper();
 //BA.debugLineNum = 97;BA.debugLine="destRect.Initialize(0,0,displayImg.Width,display";
_destrect.Initialize((int) (0),(int) (0),_displayimg.getWidth(),_displayimg.getHeight());
 //BA.debugLineNum = 99;BA.debugLine="Dim canNewImg As Canvas";
_cannewimg = new anywheresoftware.b4a.objects.drawable.CanvasWrapper();
 //BA.debugLineNum = 100;BA.debugLine="canNewImg.Initialize(displayImg)";
_cannewimg.Initialize((android.view.View)(_displayimg.getObject()));
 //BA.debugLineNum = 101;BA.debugLine="canNewImg.DrawDrawable(imgForeGround,destRect)";
_cannewimg.DrawDrawable((android.graphics.drawable.Drawable)(_imgforeground.getObject()),(android.graphics.Rect)(_destrect.getObject()));
 //BA.debugLineNum = 102;BA.debugLine="displayImg.Invalidate";
_displayimg.Invalidate();
 //BA.debugLineNum = 104;BA.debugLine="obj1.RunMethod2(\"setAlpha\", 255, \"java.lang.int\"";
_obj1.RunMethod2("setAlpha",BA.NumberToString(255),"java.lang.int");
 //BA.debugLineNum = 105;BA.debugLine="canNewImg.DrawBitmap(imgBackGround,Null,destR";
_cannewimg.DrawBitmap((android.graphics.Bitmap)(_imgbackground.getObject()),(android.graphics.Rect)(anywheresoftware.b4a.keywords.Common.Null),(android.graphics.Rect)(_destrect.getObject()));
 //BA.debugLineNum = 106;BA.debugLine="canNewImg.DrawDrawable(imgForeGround,destRect";
_cannewimg.DrawDrawable((android.graphics.drawable.Drawable)(_imgforeground.getObject()),(android.graphics.Rect)(_destrect.getObject()));
 //BA.debugLineNum = 107;BA.debugLine="displayImg.Bitmap = canNewImg.Bitmap";
_displayimg.setBitmap((android.graphics.Bitmap)(_cannewimg.getBitmap().getObject()));
 //BA.debugLineNum = 113;BA.debugLine="Dim Out As OutputStream";
_out = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 114;BA.debugLine="Out = File.OpenOutput(Detail.gPath, Detail.gPic,";
_out = anywheresoftware.b4a.keywords.Common.File.OpenOutput(mostCurrent._vvvvvvvvvvvvvvv7._v6,mostCurrent._vvvvvvvvvvvvvvv7._v7,anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 115;BA.debugLine="canNewImg.Bitmap.WriteToStream(Out, 100, \"JPEG\")";
_cannewimg.getBitmap().WriteToStream((java.io.OutputStream)(_out.getObject()),(int) (100),BA.getEnumFromString(android.graphics.Bitmap.CompressFormat.class,"JPEG"));
 //BA.debugLineNum = 117;BA.debugLine="ToastMessageShow(\"Picture saved to: \" & File.Com";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Picture saved to: "+anywheresoftware.b4a.keywords.Common.File.Combine(mostCurrent._vvvvvvvvvvvvvvv7._v6,mostCurrent._vvvvvvvvvvvvvvv7._v7)),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 118;BA.debugLine="CallSubDelayed3(Detail,\"updatePhoto\",Detail.gPat";
anywheresoftware.b4a.keywords.Common.CallSubDelayed3(processBA,(Object)(mostCurrent._vvvvvvvvvvvvvvv7.getObject()),"updatePhoto",(Object)(mostCurrent._vvvvvvvvvvvvvvv7._v6),(Object)(mostCurrent._vvvvvvvvvvvvvvv7._v7));
 //BA.debugLineNum = 119;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 } 
       catch (Exception e34) {
			processBA.setLastException(e34); //BA.debugLineNum = 121;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 122;BA.debugLine="ToastMessageShow(\"Save Image failed\",True)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Save Image failed"),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 125;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv6(anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _original,int _newwidth,int _newheight) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper _b = null;
 //BA.debugLineNum = 132;BA.debugLine="Sub CreateScaledBitmap(Original As Bitmap, NewWidt";
 //BA.debugLineNum = 133;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 134;BA.debugLine="Dim b As Bitmap";
_b = new anywheresoftware.b4a.objects.drawable.CanvasWrapper.BitmapWrapper();
 //BA.debugLineNum = 136;BA.debugLine="b = r.RunStaticMethod(\"android.graphics.Bitmap";
_b.setObject((android.graphics.Bitmap)(_r.RunStaticMethod("android.graphics.Bitmap","createScaledBitmap",new Object[]{(Object)(_original.getObject()),(Object)(_newwidth),(Object)(_newheight),(Object)(anywheresoftware.b4a.keywords.Common.True)},new String[]{"android.graphics.Bitmap","java.lang.int","java.lang.int","java.lang.boolean"})));
 //BA.debugLineNum = 139;BA.debugLine="Return b";
if (true) return _b;
 //BA.debugLineNum = 140;BA.debugLine="End Sub";
return null;
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 12;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 16;BA.debugLine="Private btnRet As Button";
mostCurrent._btnret = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Private ImageView2 As ImageView,imageview3 As Ima";
mostCurrent._imageview2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
mostCurrent._imageview3 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Dim SD As SignatureData 'This object holds the da";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = new precarite.cns.signaturecapture._signaturedata();
 //BA.debugLineNum = 19;BA.debugLine="Dim Canvas1 As Canvas, canvas2 As Canvas";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper();
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = new anywheresoftware.b4a.objects.drawable.CanvasWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private Panel2 As Panel";
mostCurrent._panel2 = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private btnSave As IconButton";
mostCurrent._btnsave = new de.donmanfred.IconButtonWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private btnClear As IconButton";
mostCurrent._btnclear = new de.donmanfred.IconButtonWrapper();
 //BA.debugLineNum = 25;BA.debugLine="End Sub";
return "";
}
public static String  _panel2_touch(int _action,float _x,float _y) throws Exception{
 //BA.debugLineNum = 65;BA.debugLine="Sub Panel2_Touch (Action As Int, X As Float, Y As";
 //BA.debugLineNum = 66;BA.debugLine="SignatureCapture.Panel_Touch(SD, X, Y, Action)";
mostCurrent._vvvvvvvvvvvvvvvv1._panel_touch(mostCurrent.activityBA,mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5,(int) (_x),(int) (_y),_action);
 //BA.debugLineNum = 67;BA.debugLine="Panel2.Invalidate";
mostCurrent._panel2.Invalidate();
 //BA.debugLineNum = 68;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return "";
}
}
