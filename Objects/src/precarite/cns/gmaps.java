package precarite.cns;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class gmaps extends Activity implements B4AActivity{
	public static gmaps mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = true;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "precarite.cns", "precarite.cns.gmaps");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (gmaps).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "precarite.cns", "precarite.cns.gmaps");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "precarite.cns.gmaps", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (gmaps) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (gmaps) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEventFromUI(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return gmaps.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeydown", this, new Object[] {keyCode, event}))
            return true;
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (processBA.runHook("onkeyup", this, new Object[] {keyCode, event}))
            return true;
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (gmaps) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (gmaps) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        for (int i = 0;i < permissions.length;i++) {
            Object[] o = new Object[] {permissions[i], grantResults[i] == 0};
            processBA.raiseEventFromDifferentThread(null,null, 0, "activity_permissionresult", true, o);
        }
            
    }

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.RuntimePermissions _vvvv2 = null;
public static anywheresoftware.b4a.gps.GPS _vvvv3 = null;
public static anywheresoftware.b4a.gps.LocationWrapper _vvvv4 = null;
public anywheresoftware.b4a.objects.MapFragmentWrapper.GoogleMapWrapper _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = null;
public anywheresoftware.b4a.objects.MapFragmentWrapper _mapfragment1 = null;
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv3 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
public static String _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
public anywheresoftware.b4a.objects.ListViewWrapper _lstproximity = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblrlvmode = null;
public anywheresoftware.b4a.objects.SpinnerWrapper _spnrlvmode = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblsearchproximity = null;
public anywheresoftware.b4a.objects.collections.List _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = null;
public static int _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = 0;
public anywheresoftware.b4a.objects.ProgressBarWrapper _progressbar1 = null;
public precarite.cns.main _vvvvvvvvvvvvvvv5 = null;
public precarite.cns.starter _vvvvvvvvvvvvvvv6 = null;
public precarite.cns.detail _vvvvvvvvvvvvvvv7 = null;
public precarite.cns.statemanager _vvvvvvvvvvvvvvv0 = null;
public precarite.cns.signaturecapture _vvvvvvvvvvvvvvvv1 = null;
public precarite.cns.signaturescreen _vvvvvvvvvvvvvvvv2 = null;
public precarite.cns.draw1 _vvvvvvvvvvvvvvvv4 = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static class _address{
public boolean IsInitialized;
public long ID;
public String Adress1;
public double lat;
public double lng;
public float dist;
public void Initialize() {
IsInitialized = true;
ID = 0L;
Adress1 = "";
lat = 0;
lng = 0;
dist = 0f;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 28;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 31;BA.debugLine="Activity.LoadLayout(\"gMaps\")";
mostCurrent._activity.LoadLayout("gMaps",mostCurrent.activityBA);
 //BA.debugLineNum = 32;BA.debugLine="If MapFragment1.IsGooglePlayServicesAvailable = F";
if (mostCurrent._mapfragment1.IsGooglePlayServicesAvailable(mostCurrent.activityBA)==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 33;BA.debugLine="ToastMessageShow(\"Please install Google Play Ser";
anywheresoftware.b4a.keywords.Common.ToastMessageShow(BA.ObjectToCharSequence("Please install Google Play Services."),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 37;BA.debugLine="lstProximity.TwoLinesLayout.ItemHeight=135";
mostCurrent._lstproximity.getTwoLinesLayout().setItemHeight((int) (135));
 //BA.debugLineNum = 38;BA.debugLine="lstProximity.ScrollingBackgroundColor = Colors.Da";
mostCurrent._lstproximity.setScrollingBackgroundColor(anywheresoftware.b4a.keywords.Common.Colors.DarkGray);
 //BA.debugLineNum = 39;BA.debugLine="lstProximity.TwoLinesLayout.Label.TextSize=13";
mostCurrent._lstproximity.getTwoLinesLayout().Label.setTextSize((float) (13));
 //BA.debugLineNum = 40;BA.debugLine="lstProximity.TwoLinesLayout.Label.TextColor=Color";
mostCurrent._lstproximity.getTwoLinesLayout().Label.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.Blue);
 //BA.debugLineNum = 41;BA.debugLine="lstProximity.TwoLinesLayout.SecondLabel.TextSize=";
mostCurrent._lstproximity.getTwoLinesLayout().SecondLabel.setTextSize((float) (12));
 //BA.debugLineNum = 42;BA.debugLine="lstProximity.TwoLinesLayout.SecondLabel.TextColor";
mostCurrent._lstproximity.getTwoLinesLayout().SecondLabel.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.DarkGray);
 //BA.debugLineNum = 43;BA.debugLine="lstProximity.TwoLinesLayout.SecondLabel.Typeface=";
mostCurrent._lstproximity.getTwoLinesLayout().SecondLabel.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.getFONTAWESOME());
 //BA.debugLineNum = 44;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 45;BA.debugLine="spnRlvMode.Add(\"Avant\")";
mostCurrent._spnrlvmode.Add("Avant");
 //BA.debugLineNum = 46;BA.debugLine="spnRlvMode.Add(\"Après\")";
mostCurrent._spnrlvmode.Add("Après");
 //BA.debugLineNum = 47;BA.debugLine="spnRlvMode.SelectedIndex=spnRlvMode.IndexOf(\"Ava";
mostCurrent._spnrlvmode.setSelectedIndex(mostCurrent._spnrlvmode.IndexOf("Avant"));
 //BA.debugLineNum = 48;BA.debugLine="gps.Initialize(\"gps\")";
_vvvv3.Initialize("gps");
 };
 //BA.debugLineNum = 51;BA.debugLine="gLocation.Initialize";
_vvvv4.Initialize();
 //BA.debugLineNum = 52;BA.debugLine="lstAdresses.Initialize";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7.Initialize();
 //BA.debugLineNum = 55;BA.debugLine="CreeListeProximity";
_vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv0();
 //BA.debugLineNum = 56;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 120;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 122;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 116;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 118;BA.debugLine="End Sub";
return "";
}
public static void  _vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv0() throws Exception{
ResumableSub_CreeListeProximity rsub = new ResumableSub_CreeListeProximity(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_CreeListeProximity extends BA.ResumableSub {
public ResumableSub_CreeListeProximity(precarite.cns.gmaps parent) {
this.parent = parent;
}
precarite.cns.gmaps parent;
String _curdoneavant = "";
String _curdoneapres = "";
double _curlng = 0;
double _curlat = 0;
double _lstlng = 0;
double _lstlat = 0;
double _slstlng = 0;
double _slstlat = 0;
double _ecart = 0;
long _nbrow = 0L;
String _curadresse = "";
long _curid = 0L;
String _curmode = "";
double _curdist = 0;
float _distance = 0f;
int _cpt = 0;
anywheresoftware.b4a.gps.LocationWrapper _plist = null;
anywheresoftware.b4a.gps.LocationWrapper _location1 = null;
int _i = 0;
precarite.cns.gmaps._address _adr = null;
int _j = 0;
precarite.cns.gmaps._address _a = null;
int step11;
int limit11;
int step40;
int limit40;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = 1;
 //BA.debugLineNum = 60;BA.debugLine="Dim curDoneAvant As String,curDoneApres As String";
_curdoneavant = "";
_curdoneapres = "";
_curlng = 0;
_curlat = 0;
_lstlng = 0;
_lstlat = 0;
_slstlng = 0;
_slstlat = 0;
 //BA.debugLineNum = 61;BA.debugLine="Dim ecart As Double,nbrow As Long,curAdresse As S";
_ecart = 0;
_nbrow = 0L;
_curadresse = "";
_curid = 0L;
_curmode = "";
_curdist = 0;
_distance = 0f;
_cpt = 0;
 //BA.debugLineNum = 62;BA.debugLine="Dim pList As  Location";
_plist = new anywheresoftware.b4a.gps.LocationWrapper();
 //BA.debugLineNum = 65;BA.debugLine="gps.Start(0,0)";
parent._vvvv3.Start(processBA,(long) (0),(float) (0));
 //BA.debugLineNum = 66;BA.debugLine="Wait For gps_LocationChanged (Location1 As Locati";
anywheresoftware.b4a.keywords.Common.WaitFor("gps_locationchanged", processBA, this, null);
this.state = 51;
return;
case 51:
//C
this.state = 1;
_location1 = (anywheresoftware.b4a.gps.LocationWrapper) result[0];
;
 //BA.debugLineNum = 68;BA.debugLine="curMode=\"Avant\"     'TODO : se baser sur le spinn";
_curmode = "Avant";
 //BA.debugLineNum = 69;BA.debugLine="Main.ProximCursor.Reset";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.Reset();
 //BA.debugLineNum = 70;BA.debugLine="nbrow=Main.fTable.GetRowCount";
_nbrow = (long) (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvv2.GetRowCount());
 //BA.debugLineNum = 71;BA.debugLine="ProgressBar1.Visible=True";
parent.mostCurrent._progressbar1.setVisible(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 72;BA.debugLine="cpt=0";
_cpt = (int) (0);
 //BA.debugLineNum = 73;BA.debugLine="For i=0 To nbrow -1";
if (true) break;

case 1:
//for
this.state = 40;
step11 = 1;
limit11 = (int) (_nbrow-1);
_i = (int) (0) ;
this.state = 52;
if (true) break;

case 52:
//C
this.state = 40;
if ((step11 > 0 && _i <= limit11) || (step11 < 0 && _i >= limit11)) this.state = 3;
if (true) break;

case 53:
//C
this.state = 52;
_i = ((int)(0 + _i + step11)) ;
if (true) break;

case 3:
//C
this.state = 4;
 //BA.debugLineNum = 74;BA.debugLine="Main.ProximCursor.GetNextRow";
parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetNextRow();
 //BA.debugLineNum = 75;BA.debugLine="ProgressBar1.progress=(i/nbrow)*100";
parent.mostCurrent._progressbar1.setProgress((int) ((_i/(double)_nbrow)*100));
 //BA.debugLineNum = 76;BA.debugLine="ProgressBar1.Invalidate";
parent.mostCurrent._progressbar1.Invalidate();
 //BA.debugLineNum = 77;BA.debugLine="curID=Main.ProximCursor.GetColumnValue(\"ID\")";
_curid = BA.ObjectToLongNumber(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("ID"));
 //BA.debugLineNum = 78;BA.debugLine="Log(curID)";
anywheresoftware.b4a.keywords.Common.Log(BA.NumberToString(_curid));
 //BA.debugLineNum = 79;BA.debugLine="curDoneAvant=Main.ProximCursor.GetColumnValue(\"t";
_curdoneavant = BA.ObjectToString(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("timestamp"));
 //BA.debugLineNum = 80;BA.debugLine="curDoneApres=Main.ProximCursor.GetColumnValue(\"t";
_curdoneapres = BA.ObjectToString(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("timestamp_apres"));
 //BA.debugLineNum = 81;BA.debugLine="If Main.ProximCursor.GetColumnValue(\"addrLat\")<>";
if (true) break;

case 4:
//if
this.state = 11;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrLat")!= null) { 
this.state = 6;
;}
else {
this.state = 8;
;}if (true) break;

case 6:
//C
this.state = 11;
_lstlat = (double)(BA.ObjectToNumber(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrLat")));
if (true) break;

case 8:
//C
this.state = 11;
_lstlat = 0;
if (true) break;

case 11:
//C
this.state = 12;
;
 //BA.debugLineNum = 82;BA.debugLine="If Main.ProximCursor.GetColumnValue(\"addrLng\")<>";
if (true) break;

case 12:
//if
this.state = 19;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrLng")!= null) { 
this.state = 14;
;}
else {
this.state = 16;
;}if (true) break;

case 14:
//C
this.state = 19;
_lstlng = (double)(BA.ObjectToNumber(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrLng")));
if (true) break;

case 16:
//C
this.state = 19;
_lstlng = 0;
if (true) break;

case 19:
//C
this.state = 20;
;
 //BA.debugLineNum = 84;BA.debugLine="If (lstLat<>0 And lstLng<>0) And (curDoneAvant=\"";
if (true) break;

case 20:
//if
this.state = 39;
if ((_lstlat!=0 && _lstlng!=0) && ((_curdoneavant).equals("null") || (_curdoneavant).equals(""))) { 
this.state = 22;
}if (true) break;

case 22:
//C
this.state = 23;
 //BA.debugLineNum = 85;BA.debugLine="slstLat=lstLat";
_slstlat = _lstlat;
 //BA.debugLineNum = 86;BA.debugLine="slstLng=lstLng";
_slstlng = _lstlng;
 //BA.debugLineNum = 87;BA.debugLine="pList.Initialize2(slstLat,slstLng)";
_plist.Initialize2(BA.NumberToString(_slstlat),BA.NumberToString(_slstlng));
 //BA.debugLineNum = 89;BA.debugLine="If Main.ProximCursor.GetColumnValue(\"addrChanti";
if (true) break;

case 23:
//if
this.state = 30;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantier1")!= null && (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantier1")).equals((Object)("")) == false) { 
this.state = 25;
;}
else {
this.state = 27;
;}if (true) break;

case 25:
//C
this.state = 30;
_curadresse = BA.ObjectToString(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantier1"));
if (true) break;

case 27:
//C
this.state = 30;
_curadresse = "(NoAdress)";
if (true) break;

case 30:
//C
this.state = 31;
;
 //BA.debugLineNum = 90;BA.debugLine="If Main.ProximCursor.GetColumnValue(\"addrChanti";
if (true) break;

case 31:
//if
this.state = 38;
if (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantierVille")!= null && (parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantierVille")).equals((Object)("")) == false) { 
this.state = 33;
;}
else {
this.state = 35;
;}if (true) break;

case 33:
//C
this.state = 38;
_curadresse = _curadresse+" - "+BA.ObjectToString(parent.mostCurrent._vvvvvvvvvvvvvvv5._vvvvvvvv4.GetColumnValue("addrChantierVille"));
if (true) break;

case 35:
//C
this.state = 38;
_curadresse = _curadresse+" (NoVille)";
if (true) break;

case 38:
//C
this.state = 39;
;
 //BA.debugLineNum = 92;BA.debugLine="Distance = gLocation.DistanceTo(pList)";
_distance = parent._vvvv4.DistanceTo((android.location.Location)(_plist.getObject()));
 //BA.debugLineNum = 93;BA.debugLine="Dim adr As Address";
_adr = new precarite.cns.gmaps._address();
 //BA.debugLineNum = 94;BA.debugLine="adr.Adress1=curAdresse";
_adr.Adress1 = _curadresse;
 //BA.debugLineNum = 95;BA.debugLine="adr.ID=curID";
_adr.ID = _curid;
 //BA.debugLineNum = 96;BA.debugLine="adr.lat=lstLat";
_adr.lat = _lstlat;
 //BA.debugLineNum = 97;BA.debugLine="adr.lng=lstLng";
_adr.lng = _lstlng;
 //BA.debugLineNum = 98;BA.debugLine="adr.dist=Distance";
_adr.dist = _distance;
 //BA.debugLineNum = 99;BA.debugLine="lstAdresses.Add(adr)";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7.Add((Object)(_adr));
 //BA.debugLineNum = 100;BA.debugLine="cpt=cpt+1";
_cpt = (int) (_cpt+1);
 if (true) break;

case 39:
//C
this.state = 53;
;
 if (true) break;
if (true) break;

case 40:
//C
this.state = 41;
;
 //BA.debugLineNum = 104;BA.debugLine="ProgressBar1.Visible=False";
parent.mostCurrent._progressbar1.setVisible(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 106;BA.debugLine="lstAdresses.SortType(\"dist\", True) 'Sort the list";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7.SortType("dist",anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 107;BA.debugLine="For j = 0 To lstAdresses.Size - 1";
if (true) break;

case 41:
//for
this.state = 50;
step40 = 1;
limit40 = (int) (parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7.getSize()-1);
_j = (int) (0) ;
this.state = 54;
if (true) break;

case 54:
//C
this.state = 50;
if ((step40 > 0 && _j <= limit40) || (step40 < 0 && _j >= limit40)) this.state = 43;
if (true) break;

case 55:
//C
this.state = 54;
_j = ((int)(0 + _j + step40)) ;
if (true) break;

case 43:
//C
this.state = 44;
 //BA.debugLineNum = 108;BA.debugLine="Dim a As Address";
_a = new precarite.cns.gmaps._address();
 //BA.debugLineNum = 109;BA.debugLine="If i>=ListLimit Then Exit";
if (true) break;

case 44:
//if
this.state = 49;
if (_i>=parent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv1) { 
this.state = 46;
;}if (true) break;

case 46:
//C
this.state = 49;
this.state = 50;
if (true) break;
if (true) break;

case 49:
//C
this.state = 55;
;
 //BA.debugLineNum = 110;BA.debugLine="a = lstAdresses.Get(i)";
_a = (precarite.cns.gmaps._address)(parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7.Get(_i));
 //BA.debugLineNum = 112;BA.debugLine="lstProximity.AddTwoLines2(a.Adress1, a.dist & \"";
parent.mostCurrent._lstproximity.AddTwoLines2(BA.ObjectToCharSequence(_a.Adress1),BA.ObjectToCharSequence(BA.NumberToString(_a.dist)+" m"),(Object)(_a.ID));
 if (true) break;
if (true) break;

case 50:
//C
this.state = -1;
;
 //BA.debugLineNum = 114;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 17;BA.debugLine="Private gmap As GoogleMap";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = new anywheresoftware.b4a.objects.MapFragmentWrapper.GoogleMapWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private MapFragment1 As MapFragment";
mostCurrent._mapfragment1 = new anywheresoftware.b4a.objects.MapFragmentWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private rlvMode As String,sAddr1 As String,sName1";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv3 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv4 = "";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv5 = "";
 //BA.debugLineNum = 20;BA.debugLine="Private lstProximity As ListView";
mostCurrent._lstproximity = new anywheresoftware.b4a.objects.ListViewWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private lblRlvMode As Label";
mostCurrent._lblrlvmode = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private spnRlvMode As Spinner";
mostCurrent._spnrlvmode = new anywheresoftware.b4a.objects.SpinnerWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Private lblSearchProximity As Label";
mostCurrent._lblsearchproximity = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Dim lstAdresses As List,ListLimit As Int = 10";
mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv7 = new anywheresoftware.b4a.objects.collections.List();
_vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv1 = (int) (10);
 //BA.debugLineNum = 25;BA.debugLine="Private ProgressBar1 As ProgressBar";
mostCurrent._progressbar1 = new anywheresoftware.b4a.objects.ProgressBarWrapper();
 //BA.debugLineNum = 26;BA.debugLine="End Sub";
return "";
}
public static String  _gps_locationchanged(anywheresoftware.b4a.gps.LocationWrapper _location1) throws Exception{
 //BA.debugLineNum = 133;BA.debugLine="Sub gps_LocationChanged(Location1 As Location)";
 //BA.debugLineNum = 134;BA.debugLine="gLocation=Location1";
_vvvv4 = _location1;
 //BA.debugLineNum = 135;BA.debugLine="gps.Stop";
_vvvv3.Stop();
 //BA.debugLineNum = 136;BA.debugLine="Msgbox(Location1.Latitude & \" Lat \" & Location1.L";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToCharSequence(BA.NumberToString(_location1.getLatitude())+" Lat "+BA.NumberToString(_location1.getLongitude())+" long1"),BA.ObjectToCharSequence("Loc"),mostCurrent.activityBA);
 //BA.debugLineNum = 137;BA.debugLine="End Sub";
return "";
}
public static void  _mapfragment1_ready() throws Exception{
ResumableSub_MapFragment1_Ready rsub = new ResumableSub_MapFragment1_Ready(null);
rsub.resume(processBA, null);
}
public static class ResumableSub_MapFragment1_Ready extends BA.ResumableSub {
public ResumableSub_MapFragment1_Ready(precarite.cns.gmaps parent) {
this.parent = parent;
}
precarite.cns.gmaps parent;
String _permission = "";
boolean _result = false;
anywheresoftware.b4a.objects.MapFragmentWrapper.MarkerWrapper _m1 = null;

@Override
public void resume(BA ba, Object[] result) throws Exception{

    while (true) {
        switch (state) {
            case -1:
return;

case 0:
//C
this.state = -1;
 //BA.debugLineNum = 125;BA.debugLine="gmap = MapFragment1.GetMap";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv2 = parent.mostCurrent._mapfragment1.GetMap();
 //BA.debugLineNum = 126;BA.debugLine="rp.CheckAndRequest(rp.PERMISSION_ACCESS_FINE_LOCA";
parent._vvvv2.CheckAndRequest(processBA,parent._vvvv2.PERMISSION_ACCESS_FINE_LOCATION);
 //BA.debugLineNum = 127;BA.debugLine="Wait For Activity_PermissionResult (Permission As";
anywheresoftware.b4a.keywords.Common.WaitFor("activity_permissionresult", processBA, this, null);
this.state = 1;
return;
case 1:
//C
this.state = -1;
_permission = (String) result[0];
_result = (Boolean) result[1];
;
 //BA.debugLineNum = 128;BA.debugLine="gmap.MyLocationEnabled = Result";
parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv2.setMyLocationEnabled(_result);
 //BA.debugLineNum = 129;BA.debugLine="Dim m1 As Marker = gmap.AddMarker(10, 30, \"test\")";
_m1 = new anywheresoftware.b4a.objects.MapFragmentWrapper.MarkerWrapper();
_m1 = parent.mostCurrent._vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv2.AddMarker(10,30,"test");
 //BA.debugLineNum = 130;BA.debugLine="m1.Snippet = \"Test CNS\"";
_m1.setSnippet("Test CNS");
 //BA.debugLineNum = 131;BA.debugLine="End Sub";
if (true) break;

            }
        }
    }
}
public static void  _activity_permissionresult(String _permission,boolean _result) throws Exception{
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim rp As RuntimePermissions";
_vvvv2 = new anywheresoftware.b4a.objects.RuntimePermissions();
 //BA.debugLineNum = 10;BA.debugLine="Type Address(ID As Long, Adress1 As String, lat A";
;
 //BA.debugLineNum = 11;BA.debugLine="Dim gps As GPS,gLocation As Location";
_vvvv3 = new anywheresoftware.b4a.gps.GPS();
_vvvv4 = new anywheresoftware.b4a.gps.LocationWrapper();
 //BA.debugLineNum = 12;BA.debugLine="End Sub";
return "";
}
}
