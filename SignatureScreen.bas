﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.


End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private PanelSign As Panel
	Dim Canvas1 As Canvas
	Dim SD As SignatureData 'This object holds the data required for SignatureCapture
	Private btnSignClear As Button
	Private btnSignSave As Button
	Private CNSPath As String
	
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	'Activity.LoadLayout("Layout1")
	'Signature
	Activity.LoadLayout("signature")
	Activity.Title="Signature du résident"
	CNSPath=File.DirRootExternal & "/RelevePrecarite"
	Canvas1.Initialize(PanelSign)
	SD.Initialize
	SD.Canvas = Canvas1
	SD.Panel = PanelSign
	SD.SignatureColor = Colors.Black
	SD.SignatureWidth = 3dip 'Stroke width
	'setRotationX(btnSignClear,90)
	'setRotationX(btnSignSave,90)
	
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub PanelSign_Touch (Action As Int, X As Float, Y As Float)
	SignatureCapture.Panel_Touch(SD, x, y, Action)
End Sub

Sub setRotationX(v As View, Angle As Float)
	Dim jo = v As JavaObject
	jo.RunMethod("setRotationX", Array As Object(Angle))
End Sub

Sub RotateImage(original As Bitmap, degree As Float) As Bitmap
	Dim matrix As JavaObject
	matrix.InitializeNewInstance("android.graphics.Matrix", Null)
	matrix.RunMethod("postRotate", Array(degree))
	Dim bmp As JavaObject
	bmp.InitializeStatic("android.graphics.Bitmap")
	Dim NewImage As Bitmap = bmp.RunMethod("createBitmap", Array(original, 0, 0, original.Width, original.Height, matrix, True))
	Return NewImage
End Sub

Sub btnSignClear_Click
	SignatureCapture.Clear(SD)
End Sub

Sub btnSignSave_Click
	Dim dt As String,filepic As String,imgRotated As Bitmap,imgSource As Bitmap,outBmp As OutputStream
	'DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
	'dt = DateTime.Date(DateTime.Now)
	'filepic="Signature_" & dt & ".png"
	If Detail.gPic<>"" And Detail.gPic<>"null" And Detail.gPic.Contains("Signature_") Then
		'SignatureCapture.Save(SD, CNSPath,Detail.gPic)
		SignatureCapture.Save(SD, CNSPath,"tempsign.jpg")
		'Rotation de l'image
		imgSource=LoadBitmap(CNSPath,"tempsign.jpg")
		imgRotated=RotateImage(imgSource,270)
		outBmp=File.OpenOutput(CNSPath,Detail.gPic,True)
		imgRotated.WriteToStream(outBmp,90,"JPEG")
		outBmp.Flush
		outBmp.Close
		ToastMessageShow("Signature saved to: " & File.Combine(CNSPath, Detail.gPic), True)
		Detail.gPic=""
	End If
	Activity.Finish
End Sub