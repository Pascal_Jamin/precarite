﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim ftp As FTP,gPath As String,gPic As String,gpsClient As GPS,userLocation As Location
End Sub

Sub Globals
	Dim camera1 As CameraExClass,btnTakePicture As IconButton,photoIdx As Int,db As JackcessDatabase,Found As Boolean,CampagneID As Int,iscamera As Boolean,canTakePic As Boolean
	Dim Panel3 As Panel, bmp1 As Bitmap
	Dim curPhoto As String,curID As Int,CNSPath As String,curTC As String,curSite As String,curMateriel As String,curphotoName As String,curphoto1 As String
	Dim curphoto2 As String,expectedPic As String,curArea As String,curUser As String,curphoto3 As String,curphoto4 As String,curphoto5 As String,curphoto6 As String,curphoto7 As String,curphoto8 As String
	Dim curphotop1 As String,curphotop2 As String,curphotop3 As String,curphotop4 As String,curphotop5 As String,curphotop6 As String,curphotosignature As String
	Private commentaire As EditText,btn_next As IconButton,btn_prev As IconButton, btnSave As Button, btnSaverec As Button
	Private ImageView1 As ImageView, imgFullScreen As ImageView,cbAccesPCommunes As CheckBox
	Private ImageView2 As ImageView,ImageView3 As ImageView,ImageView4 As ImageView,ImageView5 As ImageView,ImageView6 As ImageView
	Private spnArea As Spinner,btnAddArea As Button,btnAddMateriel As Button,Options2 As Label,lblPorte As Label,lblPlancher As Label,lblCloison As Label,lblLocal As Label,lblIsolant As Label
	Private spnTypePorte As Spinner, spnTypePlancher As Spinner, spnTypeCloison As Spinner,spnTypeLocal As Spinner, spnTypeIsolant As Spinner, lblcbAcces As Label
	Private lblHSPAvec As Label,lblcbCollectif As Label, cbCollectif As CheckBox, txtHSPAvecIsolant As EditText, txtHSPsansIsolant As EditText
	Private lblHSPsans As Label,lblNbBat As Label, txtNbBat As EditText,lblNBBatImm As Label, txtNbBatImm As EditText,txtAddrCT1 As EditText,txtAddrCT2 As EditText
	Private txtAddrCTCP As EditText, txtAddrCTVille As EditText, txtNbCageEsc As EditText, lblnbCageEsc As Label, tsPreca As TabStrip,spnTypeToit As Spinner
	Private spnTypeLogement As Spinner,spnTypeIsolantComble As Spinner, spnNatureIsolant As Spinner, spnMateriauPrevu As Spinner, spnEtatPlafond As Spinner
	Private txtHsousGoutiere As EditText, txtSurfComble As EditText, txtNbNiveaux As EditText, txtSurfReelle As EditText, cbCombleAmenag As CheckBox, cbPareVapeur As CheckBox
	Private cbTrappeComble As CheckBox, cbRehausseVMC As CheckBox, cbRehausseTrappe As CheckBox, cbEvacIsolant As CheckBox, cbSpot As CheckBox,cbConduitCheminee As CheckBox
	Private cbEcartFeu As CheckBox, cbCombleEncombre As CheckBox, spnTypePlancherCBL As Spinner, spnTypeReseauPlafond As Spinner, spnEtatIsolantPLB As Spinner
	Private Label1 As Label, Label2 As Label, Label3 As Label, Label4 As Label,Label5 As Label, Label6 As Label
	Private ImageViewp1 As ImageView, ImageViewp2 As ImageView, ImageViewp3 As ImageView, ImageViewp4 As ImageView, ImageViewp5 As ImageView,ImageViewp6 As ImageView
	Private btnSaverecPBL As IconButton,btnSign As Button, ImageViewSignature As ImageView, txtnbSacsUtilises As EditText, imvCNSLogo1 As ImageView, cbPrgMage As CheckBox
	Private cbcnxInternet As CheckBox, lblcAddr1 As Label, lblcAddr2 As Label,lblcPrenom As Label, lblcNom As Label, lblcTel1 As Label, imvFS As ImageView, imvFSApres As ImageView
	Private lblP1apres As Label, lblP4apres As Label, lblP2apres As Label, lblP5apres As Label, lblP3apres As Label, lblP6apres As Label, lblP1 As Label, lblP2 As Label
	Private lblP3 As Label, lblP4 As Label, lblP5 As Label, lblP6 As Label, cbIsolable As CheckBox, cbPresenceVMC As CheckBox, lblP7 As Label, ImageView7 As ImageView
	Private lblP8 As Label, ImageView8 As ImageView, txtCommentApres As EditText, spnResistanceIsolant As Spinner, spnAccesPCommunes As Spinner, txtHSPPorteIsolant As EditText
	Private cbEnlevementNeon As CheckBox, cbPointEauProche As CheckBox, cbAltLuminaires As CheckBox, cbCaveEncombree As CheckBox, cbAvis1 As CheckBox, cbAvis2 As CheckBox
	Private cbRefusResident As CheckBox, pnlCBLHide As Panel, pnlPLBHide As Panel, lblCBLHide As Label
	Private txtPrenom As EditText
	Private txtNom As EditText
	Private txtTel1 As EditText
	Private txtTel2 As EditText
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Activity.LoadLayout("Detail")
	'TODO : gerer les onglets selon le type d'appli (combles / plancher bas / ...)
	If Main.TypeAppli.Contains("Comble") Then 
		tsPreca.LoadLayout("detail_cbl", "Combles")
		tsPreca.LoadLayout("photoAvant", Chr(0xF030) & " Avant")
		tsPreca.LoadLayout("PhotoApres", Chr(0xF030) & " Apres")
		tsPreca.LoadLayout("detail_reception", Chr(0xF007) & " Réception")
		tsPreca.LoadLayout("detail_plb", " ")
		'Resize panel to hide controls of the other application
'		pnlPLBHide.Top=0
'		pnlPLBHide.Width=450dip
'		pnlPLBHide.Height=650dip
'		pnlPLBHide.Visible=True
'		pnlPLBHide.Enabled=False
'		pnlPLBHide.BringToFront
		'tsPreca.
	End If
	If Main.TypeAppli.StartsWith("Plancher") Then 
		tsPreca.LoadLayout("detail_plb", "Plancher Bas")
		tsPreca.LoadLayout("photoAvant", Chr(0xF030) & " Avant")
		tsPreca.LoadLayout("PhotoApres", Chr(0xF030) & " Apres")
		tsPreca.LoadLayout("detail_reception", Chr(0xF007) & " Réception")
		tsPreca.LoadLayout("detail_cbl", " ")
		'Resize panel to hide controls of the other application
'		lblCBLHide.Top=0
'		'pnlCBLHide.Top=0
'		lblCBLHide.Width=450dip
'		lblCBLHide.Height=650dip
'		lblCBLHide.Visible=True
'		lblCBLHide.Enabled=False
'		lblCBLHide.BringToFront
	End If
	
	Activity.Title="Relevé Précarité CNS (Détail)"
	'Activity.AddMenuItem("Options","Options")
	'Activity.AddMenuItem("Supprimer et quitter","ResetDB")
	CNSPath=File.DirRootExternal & "/RelevePrecarite"
	iscamera=False
	canTakePic=True
	expectedPic=""
	photoIdx=1	
	imvCNSLogo1.Bitmap=LoadBitmap(File.DirAssets,"logocns.png")
	btn_prev.Visible=False
	btn_next.Visible=False
	

	'init des boutons de controle
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"313-arrow-left.png"))
	btn_prev.Text = ""
	btn_prev.IconPadding = 0
	btn_prev.setIcon(True,bm)
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"309-arrow-right.png"))
	btn_next.Text = ""
	btn_next.IconPadding = 0
	btn_next.setIcon(True,bm)
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"099-floppy-disk.png"))
	'btnSaverec.Text = ""
	'btnSaverec.IconPadding = 0
	'btnSaverec.setIcon(True,bm)
	For Each lbl As Label In GetAllTabLabels(tsPreca)
		lbl.Typeface = Typeface.FONTAWESOME
	Next

	'Ouvrir base Access et initialiser fichiers
	If FirstTime Then
		If Not(File.Exists(CNSPath,"")) Then File.MakeDir(File.DirRootExternal, "/RelevePrecarite")
		If Not(File.Exists(CNSPath, "precarite.accdb")) Then File.Copy(File.DirAssets, "precarite.accdb", CNSPath, "precarite.accdb")
		If Not(File.Exists(CNSPath, "vide.jpg")) Then File.Copy(File.DirAssets, "vide.jpg", CNSPath, "vide.jpg") 
		db.Open(CNSPath & "/precarite.accdb")
		
		ftp.Initialize("ftp", "ftp.barrault-recherche.com", 21, "barrault", "TH62JX14dL")
		Main.fTable.Initialize(db.GetTable("Detail"))
		Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
		Main.fTableLieux.Initialize(db.GetTable("Lieux"))
		Main.fTableSite.Initialize(db.GetTable("Site"))

		CampagneID=1  'A changer pour pouvoir selectionner la campagne en amont
		curArea="Localisation"
		curphotoName=""
		'Afficher l'ordre des colonnes
		Dim colonnes() As String,coltypes() As String,i As Int
		colonnes=Main.fTable.GetColumnNames
		coltypes=Main.fTable.GetColumnJavaTypes
		For i=0 To colonnes.Length -1
			Log("Colonne " & i & " - " & colonnes(i) & " [" & coltypes(i) & "]")
		Next		
		'CreatePreferenceScreen
		'If manager.GetAll.Size = 0 Then SetDefaults
		gpsClient.Initialize("gpsclient")
		userLocation.Initialize
	End If		
	
	'TEST : Afficher Libelle de la campagne 1
	If db.IsOpen=False Then
		db.Open(CNSPath & "/precarite.accdb")
		Main.fTable.Initialize(db.GetTable("Detail"))
		Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
		Main.fTableLieux.Initialize(db.GetTable("Lieux"))
		'Main.fTableTypeCapteur.Initialize(db.GetTable("TypeCapteur"))
		Main.fTableSite.Initialize(db.GetTable("Site"))
		'Main.fTableMateriel.Initialize(db.GetTable("Materiel"))
		'Main.fTableFluide.Initialize(db.GetTable("TypeFluide"))
	End If
	CampagneID=1
	Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
	
	'initialiser les spinners
	'Spinners Plancher bas
	UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSpnTypeCloisonnement:UpdateSpnTypeLocal:UpdateSpnTypeIsolant
	UpdateSpnTypeReseauPlafond:UpdateSpnEtatIsolantPLB:UpdateSpnResistanceThermique:UpdateSpnAccesPCommunes
	'Spinners Combles
	UpdateSpnTypeLogementCBL:UpdateSpnTypeToitCBL:UpdateSpnTypePlancherCBL:UpdateSpnNatureIsolantCBL:UpdateSpnMateriauPrevuCBL:UpdateSpnEtatPlafondCBL

	'aller à la dernière fiche (ou position dans la liste)
	Dim position As Int
	position=Main.gPosition
	If position=0 Then gotoLastRow Else gotoRowPosition(position)	
	
End Sub

Sub Camera1_Ready (Success As Boolean)
	If Success Then
		camera1.StartPreview
		'limiter la taille des photos
		'camera1.GetSupportedPicturesSizes
	Else
		ToastMessageShow("Cannot open camera.", False)
	End If
End Sub

Sub Activity_Resume
	iscamera=True
	
	'si retour app photo : sauvegarder photo
	If expectedPic<>"" And File.Exists(CNSPath,expectedPic) Then
		'Gerer rotation auto 
		Try
			Dim rot As String,degre As Float
			rot=Main.manager.GetString("Rotation")
			Dim exif As ExifData
			exif.Initialize(CNSPath,expectedPic)
			Select rot
				Case "90"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_90)
				Case "180"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_180)
				Case "270"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_270)
			End Select
			degre=rot
			bmp1.Initialize(CNSPath,expectedPic)
			Dim bmp2 As Bitmap = LoadbitmapRotated(CNSPath,expectedPic,degre)
			Dim out As OutputStream
			out=File.OpenOutput(CNSPath,expectedPic,False)
			bmp2.WriteToStream(out,80,"JPEG")
			out.Close
		Catch
			Log(LastException)
			ToastMessageShow("Photo rotate error", False)
		End Try
		'réafficher la photo dans son cadre respectif
		If curphotoName<>"" Then			
			Main.fCursor.SetCurrentRowValue(curphotoName,expectedPic)
			Select curphotoName
				Case "photo1"
					ImageView1.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo2"
					ImageView2.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo3"
					ImageView3.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo4"
					ImageView4.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo5"
					ImageView5.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo6"
					ImageView6.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo7"
					ImageView7.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo8"
					ImageView8.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo9"
'					ImageView9.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo10"
'					ImageView10.Bitmap=LoadBitmap(CNSPath, expectedPic)					
				Case "photo1apres"
					ImageViewp1.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo2apres"
					ImageViewp2.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo3apres"
					ImageViewp3.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo4apres"
					ImageViewp4.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo5apres"
					ImageViewp5.Bitmap=LoadBitmap(CNSPath, expectedPic)
				Case "photo6apres"
					ImageViewp6.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo7apres"
'					ImageViewp7.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo8apres"
'					ImageViewp8.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo9apres"
'					ImageViewp9.Bitmap=LoadBitmap(CNSPath, expectedPic)
'				Case "photo10apres"
'					ImageViewp10.Bitmap=LoadBitmap(CNSPath, expectedPic)						
				Case "photoSignature"
					ImageViewSignature.Bitmap=LoadBitmap(CNSPath, expectedPic)
			End Select
			
		End If
		'Main.fCursor.SetCurrentRowValue("photo" & photoIdx,expectedPic)
		'ImageView1.Bitmap=LoadBitmap(CNSPath, expectedPic)
		'ImageView1.BringToFront
		If photoIdx=1 Then curPhoto=expectedPic Else curphoto2=expectedPic
		
		'SaveFiche
		expectedPic=""
		curphotoName=""
	End If
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	'//camera1.Release
	Try
		StateManager.SetSetting("CameraRotation",Main.manager.GetString("Rotation"))
		StateManager.SetSetting("SurveyName",Main.manager.GetString("cp1"))
		StateManager.SaveSettings
	Catch
		Log(LastException)
	End Try

End Sub

Sub Camera1_PictureTaken (Data() As Byte)
	'camera1.StartPreview
	Dim out As OutputStream,filePic As String,filePicF As String
	Dim dt As String
	DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	'photoIdx=photoIdx+1
	
	filePicF="preca_" & dt & "F.jpg"
	filePic="preca_" & dt & ".jpg"
	out = File.OpenOutput(CNSPath, filePicF, False)
	out.WriteBytes(Data, 0, Data.Length)
	out.Close
	'ToastMessageShow("Image saved: " & File.Combine(CNSPath, filePic),False)
	btnTakePicture.Enabled = True

	'recompresse l'image
	Dim BitMap_Source As Bitmap
	Dim BitMap_Travail As BitmapExtended
	out = File.OpenOutput (CNSPath, filePic, False)
	BitMap_Source = LoadBitmap(CNSPath,filePicF)
	'BitMap_Travail.compress (BitMap_Source, "JPEG", 75, out)
	'BitMap_Travail.
	
	BitMap_Source.WriteToStream(out, 75, "JPEG")
	out.Close


	'HasPhoto=True
	If photoIdx=1 Then curPhoto=filePic
	If photoIdx=2 Then curphoto2=filePic	
	Try
		SaveFiche
	Catch
		Log(LastException)
	End Try
	'Associe l'image au canvas	
	ImageView1.Bitmap=LoadbitmapSample2(CNSPath, filePic,100%x,100%y,True)
End Sub

Sub btnTakePicture_Click
	RunCamera
End Sub

Sub RAZ_fiche
	'Remise à zero de la fiche
	commentaire.Text=""
	curPhoto=""
	curphoto2=""
	expectedPic=""	
	'Spinners
	
	'textboxs
	
	'CheckBoxs
	
	'ImageViews
	
	
	'spnArea.SelectedIndex=spnArea.IndexOf("Localisation")
End Sub

Sub btn_next_Click
	'passe à la fiche suivante / nouvelle fiche
	Dim NextID As Int
	Try
		'essayer d'enregistrer la fiche
		SaveFiche
	Catch
		Log(LastException)
	End Try
	RAZ_fiche
	curID=Main.fCursor.GetColumnValue("ID")
	NextID=curID+1	
	Main.fCursor.GetNextRow
	If Main.fCursor.IsAfterLast=True Then
		RAZ_fiche
		curPhoto=""
		curphoto2=""
		AddFiche
		Main.fCursor.FindFirstRow("ID",NextID)
		IDMsg("NEXT NOFOUND")
		ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		ImageView1.BringToFront
		canTakePic=True
		'displayFiche
		'reprendre la precedente localisation
		spnArea.SelectedIndex=spnArea.IndexOf(curArea)
	Else
		displayFiche
		IDMsg("NEXT FOUND")
	End If	
End Sub


Sub btnSaverec_Click
Dim isOK As Int,bValide As Boolean
'selon le type d'appli : valider les champs erronés
	If Main.TypeAppli.Contains("Comble") Then
		isOK=ValideCBL(True)
		If isOK=1 Or isOK=2 Then 
			If isOK=1 Then bValide=True Else bValide=False
			Main.fCursor.SetCurrentRowValue("CBLValide",bValide)
			SaveFiche
		End If
	End If
	If Main.TypeAppli.StartsWith("Plancher") Then
		isOK=ValidePLB(True)
		If isOK=1 Or isOK=2 Then 
			If isOK=1 Then bValide=True Else bValide=False
			Main.fCursor.SetCurrentRowValue("PLBValide",bValide)
			SaveFiche
		End If
	End If
End Sub

Sub btn_prev_Click
	'passe à la fiche precedente
	Try
		'essayer d'enregistrer la fiche
		SaveFiche
	Catch
		Log(LastException)
	End Try
	
	Try 
		expectedPic=""
		Main.fCursor.GetPreviousRow
		If Main.fCursor.IsBeforeFirst=False Then
			RAZ_fiche
			displayFiche
			IDMsg("PREV")
		Else
			'ToastMessageShow("Debut de liste atteint !",False)
			Main.fCursor.GetNextRow
			RAZ_fiche
			displayFiche
			'Sleep(0)
		End If	
	Catch
		'ToastMessageShow("Debut de liste atteint !",False)
		Sleep(0)
	End Try		
End Sub

Sub IDMsg(act As String)
	Dim msg As String
	Try
		If Main.fCursor.IsAfterLast=False And Main.fCursor.IsBeforeFirst=False Then
			msg=Main.fCursor.GetColumnValue("ID")
		Else
			msg="#UNDEF#"
		End If	
	Catch
		msg="ERROR"
	End Try	
	'ToastMessageShow("ACTION=" & act & " ID=" & msg,False)
End Sub

Sub AddFiche
	'Ajouter une fiche vierge dans un nouvel enregistrement
	Main.fCursor.GetNextRow
	RAZ_fiche
	AddRecord
	spnArea.SelectedIndex=spnArea.IndexOf(curArea)
	Main.fCursor.GetNextRow
End Sub

Sub AddRecord
	'Afficher l'ordre des colonnes
	Dim colonnes() As String,coltypes() As String,i As Int,colname As String,curtype As String,curname As String,maxcol As Int
	colonnes=Main.fTable.GetColumnNames
	coltypes=Main.fTable.GetColumnJavaTypes
	maxcol=colonnes.Length -1
	Dim Record(maxcol) As Object
	For i=0 To colonnes.Length -1
		curtype=coltypes(i)
		curname=colonnes(i)
		Log("Colonne " & i & " - " & colonnes(i) & " [" & coltypes(i) & "]")
		Select Case curtype
			Case "String"
				Record(i)=""
			Case "integer"
				Record(i)=0
			Case "double"
				Record(i)=0
			Case Else
				Record(i)=Null
		End Select
	Next
	Record(0)=CampagneID
	Main.fTable.AddRow(Record)
	
End Sub


Sub SaveFiche
	'enregistrer la fiche
	Dim ccomment As String ,cphoto1 As String,cphoto2 As String,ctimestamp As String,sNom As String,sPrenom As String, sTel1 As String,sTel2 As String
	Dim cSite As String,cLocalisation As String,cFluide As String,cValeurn As String,cMateriel As String,cNB As Int,cDN As Int,dSacsUtilises As Int
	Dim sAddrCT1 As String,sAddrCT2 As String,sAddrCP As String,sAddrVille As String,sAddrLoc1 As String,sAddrLoc2 As String,sAddrLocCP As String,sAddrLocVille As String
	Dim sTypePorte As String,sTypePlancher As String,sTypeLocal As String,sTypeCloison As String,sNatureIsolant As String,sTypeIsolantComble As String
	Dim nbBatiment As Int,nbLogement As Int,HSPsansIsolant As Int,HSPavecIsolant As Int,chkAccesPC As Int,chkBatCollectif As Int,chkCombleAmenag As Int,chkPareVapeur As Int
	Dim chkRehausseVMC As Int,chkRehausseTrappe As Int,chkEvacIsolant As Int,chkSpot As Int,chkConduitCheminee As Int,chkEcartFeu As Int,chkCombleEncombre As Int,chkMage As Int,chkcnxInternet As Int
	Dim sTypeToit As String,sTypeLogement As String,sTypeIsolantCBL As String,sMateriauPrevu As String,sNatureIsolantCBL As String,sTypeEtatPlafond As String,sTypePlancherCBL As String
	Dim dHsousGouttiere As Int,nNbNiveaux As Int,dSurfComble As Int,dSurfReelle As Int,dSurfPLB As Int,sTypeReseauPlafond As String,sEtatIsolantPLB As String,sComment As String,sComment2 As String
	Dim nNbCageEsc As Int,sResistanceIsolant As String,sAccesPCommunes As String,HSPPorteIsolant As Int
	
	Try
		'ID campagne force a 1
		Main.fCursor.SetCurrentRowValue("IDCampagne",1)
		cSite="Site"
		Main.fCursor.SetCurrentRowValue("Site",cSite)
		'cLocalisation=spnArea.SelectedItem
		cLocalisation="Localisation"
		Main.fCursor.SetCurrentRowValue("Localisation",cLocalisation)
		
		ctimestamp = getTimestamp
		Main.fCursor.SetCurrentRowValue("timestamp",ctimestamp)
		
		'--------------- INFOS GENERALES / RECEPTION / ETAT DES LIEUX -----------------------------------------------------------------------------------------------------------------
		'nom / prenom / n° telephones
		If txtNom.Text<>Null And txtNom.Text<>"" Then sNom=txtNom.Text Else sNom=""
		Main.fCursor.SetCurrentRowValue("NomChantier",sNom)
		If txtPrenom.Text<>Null And txtPrenom.Text<>"" Then sPrenom=txtPrenom.Text Else sPrenom=""
		Main.fCursor.SetCurrentRowValue("PrenomChantier",sPrenom)
		If txtTel1.Text<>Null And txtTel1.Text<>"" Then sTel1=txtTel1.Text Else sTel1=""
		Main.fCursor.SetCurrentRowValue("TelChantier",sTel1)
		If txtTel2.Text<>Null And txtTel2.Text<>"" Then sTel2=txtTel2.Text Else sTel2=""
		Main.fCursor.SetCurrentRowValue("Tel2Chantier",sTel2)
		
		If txtAddrCT1.Text<>Null And txtAddrCT1.Text<>"" Then sAddrCT1=txtAddrCT1.Text Else sAddrCT1=""
		Main.fCursor.SetCurrentRowValue("addrChantier1",sAddrCT1)
		If txtAddrCT2.Text<>Null And txtAddrCT2.Text<>"" Then sAddrCT2=txtAddrCT2.Text Else sAddrCT2=""
		Main.fCursor.SetCurrentRowValue("addrChantier2",sAddrCT2)
		If txtAddrCTCP.Text<>Null And txtAddrCTCP.Text<>"" Then sAddrCP=txtAddrCTCP.Text Else sAddrCP=""
		Main.fCursor.SetCurrentRowValue("addrChantierCP",sAddrCP)
		If txtAddrCTVille.Text<>Null And txtAddrCTVille.Text<>"" Then sAddrVille=txtAddrCTVille.Text Else sAddrVille=""
		Main.fCursor.SetCurrentRowValue("addrChantierVille",sAddrVille)
		'TODO Refus locataire / avis passage 1-2
		Main.fCursor.SetCurrentRowValue("EDLAvisPassage1",cbAvis1.Checked)
		Main.fCursor.SetCurrentRowValue("EDLAvisPassage2",cbAvis2.Checked)
		Main.fCursor.SetCurrentRowValue("EDLRefusLocataire",cbRefusResident.Checked)
		
		'--------------- COMBLES -----------------------------------------------------------------------------------------------------------------------------------------------------
		'Spinners
		If spnTypeToit.SelectedItem<>Null And spnTypeToit.SelectedItem<>"" Then sTypeToit=spnTypeToit.SelectedItem Else sTypeToit=""
		Main.fCursor.SetCurrentRowValue("CBLTypeToit",sTypeToit)
		If spnTypeLogement.SelectedItem<>Null And spnTypeLogement.SelectedItem<>"" Then sTypeLogement=spnTypeLogement.SelectedItem Else sTypeLogement=""
		Main.fCursor.SetCurrentRowValue("CBLTypeLogement",sTypeLogement)
		'If spnTypeIsolantComble.SelectedItem<>Null And spnTypeIsolantComble.SelectedItem<>"" Then sTypeIsolantComble=spnTypeIsolantComble.SelectedItem Else sTypeIsolantComble=""
		'Main.fCursor.SetCurrentRowValue("CBLTypeIsolantcomble",sTypeIsolantComble)
		If spnNatureIsolant.SelectedItem<>Null And spnNatureIsolant.SelectedItem<>"" Then sNatureIsolantCBL=spnNatureIsolant.SelectedItem Else sNatureIsolantCBL=""
		Main.fCursor.SetCurrentRowValue("CBLNatureIsolantComble",sNatureIsolantCBL)
		If spnMateriauPrevu.SelectedItem<>Null And spnMateriauPrevu.SelectedItem<>"" Then sMateriauPrevu=spnMateriauPrevu.SelectedItem Else sMateriauPrevu=""
		Main.fCursor.SetCurrentRowValue("CBLtypeMateriauPrevu",sMateriauPrevu)
		If spnEtatPlafond.SelectedItem<>Null And spnEtatPlafond.SelectedItem<>"" Then sTypeEtatPlafond=spnEtatPlafond.SelectedItem Else sTypeEtatPlafond=""
		Main.fCursor.SetCurrentRowValue("CBLtypeEtatPlafond",sTypeEtatPlafond)
		If spnTypePlancherCBL.SelectedItem<>Null And spnTypePlancherCBL.SelectedItem<>"" Then sTypePlancherCBL=spnTypePlancherCBL.SelectedItem Else sTypePlancherCBL=""
		Main.fCursor.SetCurrentRowValue("CBLtypePlancher",sTypePlancherCBL)
		
		'TextBoxs
		If txtHsousGoutiere.Text<>Null And txtHsousGoutiere.Text<>"" Then dHsousGouttiere=txtHsousGoutiere.Text Else dHsousGouttiere=0
		Main.fCursor.SetCurrentRowValue("CBLhtsousGouttiere",dHsousGouttiere)
		If txtNbNiveaux.Text<>Null And txtNbNiveaux.Text<>"" Then nNbNiveaux=txtNbNiveaux.Text Else nNbNiveaux=0
		Main.fCursor.SetCurrentRowValue("CBLnbNiveaux",nNbNiveaux)
		If txtSurfComble.Text<>Null And txtSurfComble.Text<>"" Then dSurfComble=txtSurfComble.Text Else dSurfComble=0
		Main.fCursor.SetCurrentRowValue("CBLSurfaceComble",dSurfComble)
		If txtSurfReelle.Text<>Null And txtSurfReelle.Text<>"" Then dSurfReelle=txtSurfReelle.Text Else dSurfReelle=0
		Main.fCursor.SetCurrentRowValue("CBLSurfaceCombleReelle",dSurfReelle)
		If txtnbSacsUtilises.Text<>Null And txtnbSacsUtilises.Text<>"" Then dSacsUtilises=txtnbSacsUtilises.Text Else dSacsUtilises=0
		Main.fCursor.SetCurrentRowValue("CBLnbSacsUtilises",dSacsUtilises)
		If commentaire.Text<>Null And commentaire.Text<>"" Then sComment=commentaire.Text Else sComment=""
		Main.fCursor.SetCurrentRowValue("Commentaires",sComment)
		If txtCommentApres.Text<>Null And txtCommentApres.Text<>"" Then sComment2=txtCommentApres.Text Else sComment2=""
		Main.fCursor.SetCurrentRowValue("commentaires_apres",sComment2)
		
		'CheckBoxs
		Main.fCursor.SetCurrentRowValue("CBLchkCombleAmenageable",cbCombleAmenag.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkPareVapeur",cbPareVapeur.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkTrappeCombles",cbTrappeComble.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkRehausseVMC",cbRehausseVMC.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkRehausseTrappe",cbRehausseTrappe.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkEvacIsolant",cbEvacIsolant.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkSpot",cbSpot.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkConduitCheminee",cbConduitCheminee.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkEcartAuFeu",cbEcartFeu.Checked)
		Main.fCursor.SetCurrentRowValue("CBLchkComblesEncombres",cbCombleEncombre.Checked)
		Main.fCursor.SetCurrentRowValue("CBLProgrammeMAGE",cbPrgMage.Checked)
		Main.fCursor.SetCurrentRowValue("CBLConnexionInternet",cbcnxInternet.Checked)
		Main.fCursor.SetCurrentRowValue("CBLIsolable",cbIsolable.Checked)
		Main.fCursor.SetCurrentRowValue("CBLPresenceVMC",cbPresenceVMC.Checked)
			
		'--------------- PLANCHER BAS -----------------------------------------------------------------------------------------------------------------------------------------------------
		'Spinners
		If spnTypePorte.SelectedItem<>Null And spnTypePorte.SelectedItem<>"" Then sTypePorte=spnTypePorte.SelectedItem Else sTypePorte=""
		Main.fCursor.SetCurrentRowValue("PLBTypePorte",sTypePorte)
		If spnTypePlancher.SelectedItem<>Null And spnTypePlancher.SelectedItem<>"" Then sTypePlancher=spnTypePlancher.SelectedItem Else sTypePlancher=""
		Main.fCursor.SetCurrentRowValue("PLBTypePlancher",sTypePlancher)
		If spnTypeCloison.SelectedItem<>Null And spnTypeCloison.SelectedItem<>"" Then sTypeCloison=spnTypeCloison.SelectedItem Else sTypeCloison=""
		Main.fCursor.SetCurrentRowValue("PLBTypeCaveCloisonnement",sTypeCloison)
		If spnTypeLocal.SelectedItem<>Null And spnTypeLocal.SelectedItem<>"" Then sTypeLocal=spnTypeLocal.SelectedItem Else sTypeLocal=""
		Main.fCursor.SetCurrentRowValue("PLBtypeLocal",sTypeLocal)
		If spnTypeIsolant.SelectedItem<>Null And spnTypeIsolant.SelectedItem<>"" Then sNatureIsolant=spnTypeIsolant.SelectedItem Else sNatureIsolant=""
		Main.fCursor.SetCurrentRowValue("PLBNatureIsolant",sNatureIsolant)
		If spnTypeReseauPlafond.SelectedItem<>Null And spnTypeReseauPlafond.SelectedItem<>"" Then sTypeReseauPlafond=spnTypeReseauPlafond.SelectedItem Else sTypeReseauPlafond=""
		Main.fCursor.SetCurrentRowValue("PLBTypeReseauPlafond",sTypeReseauPlafond)
		If spnEtatIsolantPLB.SelectedItem<>Null And spnEtatIsolantPLB.SelectedItem<>"" Then sEtatIsolantPLB=spnEtatIsolantPLB.SelectedItem Else sEtatIsolantPLB=""
		Main.fCursor.SetCurrentRowValue("PLBEtatIsolant",sEtatIsolantPLB)
		If spnResistanceIsolant.SelectedItem<>Null And spnResistanceIsolant.SelectedItem<>"" Then sResistanceIsolant=spnResistanceIsolant.SelectedItem Else sResistanceIsolant=""
		Main.fCursor.SetCurrentRowValue("PLBResistanceIsolant",sResistanceIsolant)
		If spnAccesPCommunes.SelectedItem<>Null And spnAccesPCommunes.SelectedItem<>"" Then sAccesPCommunes=spnAccesPCommunes.SelectedItem Else sAccesPCommunes=""
		Main.fCursor.SetCurrentRowValue("PLBAccessPCommunes",sAccesPCommunes)
		
		'Textboxs
		If txtHSPAvecIsolant.Text<>Null And txtHSPAvecIsolant.Text<>"" Then HSPavecIsolant=txtHSPAvecIsolant.Text Else HSPavecIsolant=0
		Main.fCursor.SetCurrentRowValue("PLBHSPAvecIsolant",HSPavecIsolant)
		If txtHSPsansIsolant.Text<>Null And txtHSPsansIsolant.Text<>"" Then HSPsansIsolant=txtHSPsansIsolant.Text Else HSPsansIsolant=0
		Main.fCursor.SetCurrentRowValue("PLBHSPSansIsolant",HSPsansIsolant)
		If txtHSPPorteIsolant.Text<>Null And txtHSPPorteIsolant.Text<>"" Then HSPPorteIsolant=txtHSPPorteIsolant.Text Else HSPPorteIsolant=0
		Main.fCursor.SetCurrentRowValue("PLBHSPPorteIsolant",HSPPorteIsolant)
		If txtNbBat.Text<>Null And txtNbBat.Text<>"" Then nbBatiment=txtNbBat.Text Else nbBatiment=0
		Main.fCursor.SetCurrentRowValue("PLBnbBatiment",nbBatiment)
		If txtNbBatImm.Text<>Null And txtNbBatImm.Text<>"" Then nbLogement=txtNbBatImm.Text Else nbLogement=0
		Main.fCursor.SetCurrentRowValue("PLBnbLogementparBatiment",nbLogement)
		If txtNbCageEsc.Text<>Null And txtNbCageEsc.Text<>"" Then nNbCageEsc=txtNbCageEsc.Text Else nNbCageEsc=0
		Main.fCursor.SetCurrentRowValue("PLBnbCageEscalier",nNbCageEsc)
		
		'CheckBoxs
		'Main.fCursor.SetCurrentRowValue("PLBAccessPCommunes",cbAccesPCommunes.Checked)
		Main.fCursor.SetCurrentRowValue("PLBbatCollectif",cbCollectif.Checked)
		Main.fCursor.SetCurrentRowValue("PLBPointEauProche",cbPointEauProche.Checked)
		Main.fCursor.SetCurrentRowValue("PLBAltLuminaires",cbAltLuminaires.Checked)
		Main.fCursor.SetCurrentRowValue("PLBEnlevementNeon",cbEnlevementNeon.Checked)
		Main.fCursor.SetCurrentRowValue("PLBCaveEncombree",cbCaveEncombree.Checked)
				
		'Photos
		If curphoto1<>Null And curphoto1<>"" Then Main.fCursor.SetCurrentRowValue("photo1",curphoto1)
		If curphoto2<>Null And curphoto2<>"" Then Main.fCursor.SetCurrentRowValue("photo2",curphoto2)
		If curphoto3<>Null And curphoto3<>"" Then Main.fCursor.SetCurrentRowValue("photo3",curphoto3)
		If curphoto4<>Null And curphoto4<>"" Then Main.fCursor.SetCurrentRowValue("photo4",curphoto4)
		If curphoto5<>Null And curphoto5<>"" Then Main.fCursor.SetCurrentRowValue("photo5",curphoto5)
		If curphoto6<>Null And curphoto6<>"" Then Main.fCursor.SetCurrentRowValue("photo6",curphoto6)
		If curphoto7<>Null And curphoto7<>"" Then Main.fCursor.SetCurrentRowValue("photo7",curphoto7)
		If curphoto8<>Null And curphoto8<>"" Then Main.fCursor.SetCurrentRowValue("photo8",curphoto8)
'		If curphoto9<>Null And curphoto9<>"" Then Main.fCursor.SetCurrentRowValue("photo9",curphoto9)
'		If curphoto10<>Null And curphoto10<>"" Then Main.fCursor.SetCurrentRowValue("photo10",curphoto10)

		If curphotop1<>Null And curphotop1<>"" Then Main.fCursor.SetCurrentRowValue("photo1apres",curphotop1)
		If curphotop2<>Null And curphotop2<>"" Then Main.fCursor.SetCurrentRowValue("photo2apres",curphotop2)
		If curphotop3<>Null And curphotop3<>"" Then Main.fCursor.SetCurrentRowValue("photo3apres",curphotop3)
		If curphotop4<>Null And curphotop4<>"" Then Main.fCursor.SetCurrentRowValue("photo4apres",curphotop4)
		If curphotop5<>Null And curphotop5<>"" Then Main.fCursor.SetCurrentRowValue("photo5apres",curphotop5)
		If curphotop6<>Null And curphotop6<>"" Then Main.fCursor.SetCurrentRowValue("photo6apres",curphotop6)
'		If curphotop7<>Null And curphotop7<>"" Then Main.fCursor.SetCurrentRowValue("photo7apres",curphotop7)
'		If curphotop8<>Null And curphotop8<>"" Then Main.fCursor.SetCurrentRowValue("photo8apres",curphotop8)
'		If curphotop9<>Null And curphotop9<>"" Then Main.fCursor.SetCurrentRowValue("photo9apres",curphotop9)
'		If curphotop10<>Null And curphotop10<>"" Then Main.fCursor.SetCurrentRowValue("photo10apres",curphotop10)
		
		'infos operateur
		If Main.manager.Getstring("user1")<>Null And Main.manager.Getstring("user1")<>"" Then Main.fCursor.SetCurrentRowValue("Operateur",Main.manager.Getstring("user1"))
		
		ToastMessageShow("Fiche SAVE OK",False)
		IDMsg("SAVE")
	Catch
		ToastMessageShow("Fiche SAVE ERROR",False)
		Log("SaveFiche Update failed")
	End Try	
End Sub

Sub getTimestamp As String
	Dim dt As String
	DateTime.DateFormat = "dd/MM/yyyy HH:mm:ss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	Return dt
End Sub

Sub displayFiche
	Dim sPhoto As String
	Try
		'afficher les infos fiche (hors photo)
		Dim buf As Object,sAddr1 As String,sAddr2 As String
		'If Main.fCursor.GetColumnValue("Commentaires")=Null Then
		'	commentaire.Text=" "
		'Else
		'	commentaire.Text=Main.fCursor.GetColumnValue("Commentaires")
		'End If
		curphoto1="":curphoto2="":curphoto3="":curphoto4="":curphoto5="":curphoto6=""
		curphotop1="":curphotop2="":curphotop3="":curphotop4="":curphotop5="":curphotop6=""
		
		'spnArea.SelectedIndex=spnArea.IndexOf(Main.fCursor.GetColumnValue("Localisation"))
		'curArea=spnArea.SelectedItem
		If Main.fCursor.GetColumnValue("Commentaires")<>Null Then commentaire.Text=Main.fCursor.GetColumnValue("Commentaires") Else commentaire.Text=""
		If Main.fCursor.GetColumnValue("commentaires_apres")<>Null Then txtCommentApres.Text=Main.fCursor.GetColumnValue("commentaires_apres") Else txtCommentApres.Text=""
		
		'--------------- RECEPTION / ETAT DES LIEUX (infos generales resident) -------------------------------------------------------------------------------------------------------
		
		sAddr1=Chr(0xF015) & " ":sAddr2=""
		If Main.fCursor.GetColumnValue("addrChantier1")<>Null Then 
			txtAddrCT1.Text=Main.fCursor.GetColumnValue("addrChantier1") 
			sAddr1=sAddr1 & Main.fCursor.GetColumnValue("addrChantier1")
		Else 
			txtAddrCT1.Text=""
		End If
		If Main.fCursor.GetColumnValue("addrChantier2")<>Null Then 
			txtAddrCT2.Text=Main.fCursor.GetColumnValue("addrChantier2") 
			sAddr1=sAddr1 & " " & Main.fCursor.GetColumnValue("addrChantier2")
		Else 
			txtAddrCT2.Text=""
		End If
		If Main.fCursor.GetColumnValue("addrChantierCP")<>Null Then 
			txtAddrCTCP.Text=Main.fCursor.GetColumnValue("addrChantierCP") 
			sAddr2=sAddr2 & Main.fCursor.GetColumnValue("addrChantierCP")
		Else
			txtAddrCTCP.Text=""
		End If
		If Main.fCursor.GetColumnValue("addrChantierVille")<>Null Then 
			txtAddrCTVille.Text=Main.fCursor.GetColumnValue("addrChantierVille") 
			sAddr2=sAddr2 & " " & Main.fCursor.GetColumnValue("addrChantierVille")
		Else
			txtAddrCTVille.Text=""
		End If
		lblcAddr1.Text=sAddr1
		lblcAddr2.Text=sAddr2
		
		If Main.fCursor.GetColumnValue("NomChantier")<>Null Then
			'lblcNom.Text=Main.fCursor.GetColumnValue("NomChantier")
			txtNom.Text=Main.fCursor.GetColumnValue("NomChantier")
			'sAddr2=sAddr2 & " " & Main.fCursor.GetColumnValue("addrChantierVille")
		Else
			'lblcNom.Text=""
			txtNom.Text=""
		End If
		If Main.fCursor.GetColumnValue("PrenomChantier")<>Null Then
			'lblcPrenom.Text=Main.fCursor.GetColumnValue("PrenomChantier")
			txtPrenom.Text=Main.fCursor.GetColumnValue("PrenomChantier")
			'sAddr2=sAddr2 & " " & Main.fCursor.GetColumnValue("addrChantierVille")
		Else
			'lblcPrenom.Text=""
			txtPrenom.Text=""
		End If
		If Main.fCursor.GetColumnValue("TelChantier")<>Null Then
			'lblcTel1.Text=Main.fCursor.GetColumnValue("TelChantier")
			txtTel1.Text=Main.fCursor.GetColumnValue("TelChantier")
			'sAddr2=sAddr2 & " " & Main.fCursor.GetColumnValue("addrChantierVille")
		Else
			'lblcTel1.Text=""
			txtTel1.Text=""
		End If
		If Main.fCursor.GetColumnValue("Tel2Chantier")<>Null Then
			'lblcTel1.Text=Main.fCursor.GetColumnValue("TelChantier")
			txtTel2.Text=Main.fCursor.GetColumnValue("Tel2Chantier")
			'sAddr2=sAddr2 & " " & Main.fCursor.GetColumnValue("addrChantierVille")
		Else
			'lblcTel1.Text=""
			txtTel2.Text=""
		End If
		If Main.fCursor.GetColumnValue("EDLRefusLocataire")<>Null And Main.fCursor.GetColumnValue("EDLRefusLocataire")<>0 Then cbRefusResident.Checked=True Else cbRefusResident.Checked=False
		If Main.fCursor.GetColumnValue("EDLAvisPassage1")<>Null And Main.fCursor.GetColumnValue("EDLAvisPassage1")<>0 Then cbAvis1.Checked=True Else cbAvis1.Checked=False
		If Main.fCursor.GetColumnValue("EDLAvisPassage2")<>Null And Main.fCursor.GetColumnValue("EDLAvisPassage2")<>0 Then cbAvis2.Checked=True Else cbAvis2.Checked=False
		
		'---------------- COMBLES ------------------------------------------------------------------------------------------------------------
		'Spinners
		'spnTypeIsolantComble.SelectedIndex=spnTypeIsolantComble.IndexOf(Main.fCursor.GetColumnValue("CBLTypeIsolantComble"))
		spnTypeLogement.SelectedIndex=spnTypeLogement.IndexOf(Main.fCursor.GetColumnValue("CBLTypeLogement"))
		spnTypeToit.SelectedIndex=spnTypeToit.IndexOf(Main.fCursor.GetColumnValue("CBLTypeToit"))
		spnNatureIsolant.SelectedIndex=spnNatureIsolant.IndexOf(Main.fCursor.GetColumnValue("CBLNatureIsolantComble"))
		spnMateriauPrevu.SelectedIndex=spnMateriauPrevu.IndexOf(Main.fCursor.GetColumnValue("CBLTypeMateriauPrevu"))
		spnEtatPlafond.SelectedIndex=spnEtatPlafond.IndexOf(Main.fCursor.GetColumnValue("CBLTypeEtatPlafond"))
		spnTypePlancherCBL.SelectedIndex=spnTypePlancherCBL.IndexOf(Main.fCursor.GetColumnValue("CBLTypePlancher"))
		'TextBoxs
		If Main.fCursor.GetColumnValue("CBLhtsousGouttiere")<>Null Then txtHsousGoutiere.Text=Main.fCursor.GetColumnValue("CBLhtsousGouttiere") Else txtHsousGoutiere.Text=""
		If Main.fCursor.GetColumnValue("CBLnbNiveaux")<>Null Then txtNbNiveaux.Text=Main.fCursor.GetColumnValue("CBLnbNiveaux") Else txtNbNiveaux.Text=""
		buf=Main.fCursor.GetColumnValue("CBLsurfaceComble")
		If Main.fCursor.GetColumnValue("CBLsurfaceComble")<>Null Then txtSurfComble.Text=Main.fCursor.GetColumnValue("CBLsurfaceComble") Else txtSurfComble.Text=""
		If Main.fCursor.GetColumnValue("CBLsurfaceCombleReelle")<>Null Then txtSurfReelle.Text=Main.fCursor.GetColumnValue("CBLsurfaceCombleReelle") Else txtSurfReelle.Text=""
		If Main.fCursor.GetColumnValue("CBLnbSacsUtilises")<>Null Then txtnbSacsUtilises.Text=Main.fCursor.GetColumnValue("CBLnbSacsUtilises") Else txtnbSacsUtilises.Text=""
		
		'CheckBoxs
		If Main.fCursor.GetColumnValue("CBLchkCombleAmenageable")<>Null And Main.fCursor.GetColumnValue("CBLchkCombleAmenageable")<>0 Then cbCombleAmenag.Checked=True Else cbCombleAmenag.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkPareVapeur")<>Null And Main.fCursor.GetColumnValue("CBLchkPareVapeur")<>0 Then cbPareVapeur.Checked=True Else cbPareVapeur.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkTrappeCombles")<>Null And Main.fCursor.GetColumnValue("CBLchkTrappeCombles")<>0 Then cbTrappeComble.Checked=True Else cbTrappeComble.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkRehausseVMC")<>Null And Main.fCursor.GetColumnValue("CBLchkRehausseVMC")<>0 Then cbRehausseVMC.Checked=True Else cbRehausseVMC.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkRehausseTrappe")<>Null And Main.fCursor.GetColumnValue("CBLchkRehausseTrappe")<>0 Then cbRehausseTrappe.Checked=True Else cbRehausseTrappe.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkEvacIsolant")<>Null And Main.fCursor.GetColumnValue("CBLchkEvacIsolant")<>0 Then cbEvacIsolant.Checked=True Else cbEvacIsolant.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkSpot")<>Null And Main.fCursor.GetColumnValue("CBLchkSpot")<>0 Then cbSpot.Checked=True Else cbSpot.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkConduitCheminee")<>Null And Main.fCursor.GetColumnValue("CBLchkConduitCheminee")<>0 Then cbConduitCheminee.Checked=True Else cbConduitCheminee.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkEcartAuFeu")<>Null And Main.fCursor.GetColumnValue("CBLchkEcartAuFeu")<>0 Then cbEcartFeu.Checked=True Else cbEcartFeu.Checked=False
		If Main.fCursor.GetColumnValue("CBLchkComblesEncombres")<>Null And Main.fCursor.GetColumnValue("CBLchkComblesEncombres")<>0 Then cbCombleEncombre.Checked=True Else cbCombleEncombre.Checked=False		
		If Main.fCursor.GetColumnValue("CBLProgrammeMAGE")<>Null And Main.fCursor.GetColumnValue("CBLProgrammeMAGE")<>0 Then cbPrgMage.Checked=True Else cbPrgMage.Checked=False
		If Main.fCursor.GetColumnValue("CBLConnexionInternet")<>Null And Main.fCursor.GetColumnValue("CBLConnexionInternet")<>0 Then cbcnxInternet.Checked=True Else cbcnxInternet.Checked=False
		If Main.fCursor.GetColumnValue("CBLIsolable")<>Null And Main.fCursor.GetColumnValue("CBLIsolable")<>0 Then cbIsolable.Checked=True Else cbIsolable.Checked=False
		If Main.fCursor.GetColumnValue("CBLPresenceVMC")<>Null And Main.fCursor.GetColumnValue("CBLPresenceVMC")<>0 Then cbPresenceVMC.Checked=True Else cbPresenceVMC.Checked=False
		
		'---------------- PLAFOND BAS ------------------------------------------------------------------------------------------------------------		
		'spinners
		spnTypePorte.SelectedIndex=spnTypePorte.IndexOf(Main.fCursor.GetColumnValue("PLBTypePorte"))
		spnTypePlancher.SelectedIndex=spnTypePlancher.IndexOf(Main.fCursor.GetColumnValue("PLBTypePlancher"))
		spnTypeCloison.SelectedIndex=spnTypeCloison.IndexOf(Main.fCursor.GetColumnValue("PLBTypeCaveCloisonnement"))
		spnTypeLocal.SelectedIndex=spnTypeLocal.IndexOf(Main.fCursor.GetColumnValue("PLBtypeLocal"))
		spnTypeIsolant.SelectedIndex=spnTypeIsolant.IndexOf(Main.fCursor.GetColumnValue("PLBNatureIsolant"))
		spnEtatIsolantPLB.SelectedIndex=spnEtatIsolantPLB.IndexOf(Main.fCursor.GetColumnValue("PLBEtatIsolant"))
		spnTypeReseauPlafond.SelectedIndex=spnTypeReseauPlafond.IndexOf(Main.fCursor.GetColumnValue("PLBTypeReseauPlafond"))
		'accesParties communes
		spnAccesPCommunes.SelectedIndex=spnAccesPCommunes.IndexOf(Main.fCursor.GetColumnValue("PLBAccessPCommunes"))
		'resistanceIsolant
		spnResistanceIsolant.SelectedIndex=spnResistanceIsolant.IndexOf(Main.fCursor.GetColumnValue("PLBResistanceIsolant"))
		
		'TextBoxs
		If Main.fCursor.GetColumnValue("PLBHSPSansIsolant")<>Null Then txtHSPsansIsolant.Text=Main.fCursor.GetColumnValue("PLBHSPSansIsolant") Else txtHSPsansIsolant.Text=""
		If Main.fCursor.GetColumnValue("PLBHSPAvecIsolant")<>Null Then txtHSPAvecIsolant.Text=Main.fCursor.GetColumnValue("PLBHSPAvecIsolant") Else txtHSPAvecIsolant.Text=""
		If Main.fCursor.GetColumnValue("PLBnbBatiment")<>Null Then txtNbBat.Text=Main.fCursor.GetColumnValue("PLBnbBatiment") Else txtNbBat.Text=""
		If Main.fCursor.GetColumnValue("PLBnbLogementparBatiment")<>Null Then txtNbBatImm.Text=Main.fCursor.GetColumnValue("PLBnbLogementparBatiment") Else txtNbBatImm.Text=""
		If Main.fCursor.GetColumnValue("PLBnbCageEscalier")<>Null Then txtNbCageEsc.Text=Main.fCursor.GetColumnValue("PLBnbCageEscalier") Else txtNbCageEsc.Text=""
		'TODO HSPPorteIsolant
		If Main.fCursor.GetColumnValue("PLBHSPPorteIsolant")<>Null Then txtHSPPorteIsolant.Text=Main.fCursor.GetColumnValue("PLBHSPPorteIsolant") Else txtHSPPorteIsolant.Text=""
				
		'Checkboxs
		'If Main.fCursor.GetColumnValue("PLBAccessPCommunes")<>Null And Main.fCursor.GetColumnValue("PLBAccessPCommunes")<>0 Then cbAccesPCommunes.Checked=True Else cbAccesPCommunes.Checked=False
		If Main.fCursor.GetColumnValue("PLBbatCollectif")<>Null And Main.fCursor.GetColumnValue("PLBbatCollectif")<>0 Then cbCollectif.Checked=True Else cbCollectif.Checked=False
		If Main.fCursor.GetColumnValue("PLBEnlevementNeon")<>Null And Main.fCursor.GetColumnValue("PLBEnlevementNeon")<>0 Then cbEnlevementNeon.Checked=True Else cbEnlevementNeon.Checked=False
		If Main.fCursor.GetColumnValue("PLBPointEauProche")<>Null And Main.fCursor.GetColumnValue("PLBPointEauProche")<>0 Then cbPointEauProche.Checked=True Else cbPointEauProche.Checked=False
		If Main.fCursor.GetColumnValue("PLBAltLuminaires")<>Null And Main.fCursor.GetColumnValue("PLBAltLuminaires")<>0 Then cbAltLuminaires.Checked=True Else cbAltLuminaires.Checked=False
		If Main.fCursor.GetColumnValue("PLBCaveEncombree")<>Null And Main.fCursor.GetColumnValue("PLBCaveEncombree")<>0 Then cbCaveEncombree.Checked=True Else cbCaveEncombree.Checked=False
		
		'TODO : refus locataire / avis de passage 1-2 / PLBChkSurfaceIsolation / PLBIsolation / PLBAltLuminaires / PLBPointEauProche / Neon / CaveEncombree
		
		'-------- PHOTOS -------------------------------------------------------------------------------------------------------------------
		sPhoto=Main.fCursor.GetColumnValue("photo1")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView1.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto1=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo2")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView2.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto2=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo3")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView3.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto3=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo4")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView4.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto4=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo5")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView5.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto5=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo6")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView6.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto6=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo7")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView7.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto7=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo8")
		If sPhoto<>"null" And sPhoto<>"" Then ImageView8.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto8=sPhoto
' 		sPhoto=Main.fCursor.GetColumnValue("photo9")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageView9.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto9=sPhoto
'		sPhoto=Main.fCursor.GetColumnValue("photo10")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageView10.Bitmap=LoadBitmap(CNSPath, sPhoto):curphoto10=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo1apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp1.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop1=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo2apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp2.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop2=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo3apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp3.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop3=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo4apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp4.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop4=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo5apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp5.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop5=sPhoto
		sPhoto=Main.fCursor.GetColumnValue("photo6apres")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp6.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop6=sPhoto
'		sPhoto=Main.fCursor.GetColumnValue("photo7apres")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp7.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop7=sPhoto
'		sPhoto=Main.fCursor.GetColumnValue("photo8apres")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp8.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop8=sPhoto
' 		sPhoto=Main.fCursor.GetColumnValue("photo9apres")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp9.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop9=sPhoto
'		sPhoto=Main.fCursor.GetColumnValue("photo10apres")
'		If sPhoto<>"null" And sPhoto<>"" Then ImageViewp10.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotop10=sPhoto		
		
		sPhoto=Main.fCursor.GetColumnValue("photoSignature")
		If sPhoto<>"null" And sPhoto<>"" Then ImageViewSignature.Bitmap=LoadBitmap(CNSPath, sPhoto):curphotosignature=sPhoto
	Catch
		ToastMessageShow("Erreur affichage fiche",False)
	End Try
	Try
		'afficher la photo
		Log("DisplayFiche - Photo enter")
		Dim sphoto2 As String,sPhoto As String
		sPhoto=Main.fCursor.GetColumnValue("photo" & photoIdx)
		'sphoto1=Main.fCursor.GetColumnValue("photo1")
		sphoto2=Main.fCursor.GetColumnValue("photo2")
		'IDMsg("PREV")
		If (File.Exists(CNSPath,sPhoto)) And sPhoto<>""	 Then 
			Log("DisplayFiche - PhotoExists 1 enter")
			curPhoto=sPhoto		
			ImageView1.Bitmap=LoadBitmap(CNSPath, sPhoto)
			ImageView1.BringToFront
			canTakePic=True
		Else
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
			ImageView1.BringToFront
			canTakePic=True
		End If	
		If (File.Exists(CNSPath,sphoto2)) Then curphoto2=sphoto2
	Catch
		ToastMessageShow("Erreur affichage photo",False)
		ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		If iscamera Then 
			canTakePic=True
		End If
	End Try	
	'remettre picture1 par defaut
	InitP1
End Sub


Sub btnP1_Click
	InitP1
End Sub

Sub btnP2_Click
	photoIdx=2
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo2")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)
			canTakePic=True
		Else			
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		End If		
	Catch
		Log(LastException)
	End Try
End Sub

Sub gotoLastRow
	Dim nbrow As Int
	nbrow=Main.fTable.GetRowCount
	Log("STEP2")
	If nbrow>=1 Then
		'afficher la derniere fiche
		Main.fCursor.Reset
		For i=0 To nbrow -1
			Main.fCursor.GetNextRow
		Next
		displayFiche
	End If
	Log("STEP3")
	If nbrow<1 Then
		Main.fTable.addrow(Array As Object(1,1,1,2,"","","","",getTimestamp))
		'AddFiche		
		Main.fCursor.Reset
	End If
	Log("STEP4 Init end")
	'IDMsg("INIT")
End Sub

Sub gotoRowPosition(pos As Int)
	Dim nbrow As Int
	nbrow=Main.fTable.GetRowCount
	If pos>nbrow Then Return
	If nbrow>=1 Then
		Main.fCursor.Reset
		For i=0 To pos-1
			Main.fCursor.GetNextRow
		Next
		displayFiche
	End If
End Sub

Sub ftp_UploadCompleted (ServerPath As String, Success As Boolean)
    Log(ServerPath & ", Success=" & Success)
    If Success = False Then 
		Log(LastException.Message)
		ToastMessageShow("Envoi ZIP ECHEC : " & LastException.Message,True)
	Else
		ToastMessageShow("Envoi ZIP REUSSI",True)
	End If
End Sub	


Sub ParseUri(s As String) As Object
    Dim r As Reflector
    Return r.RunStaticMethod("android.net.Uri", "parse", Array As Object(s), Array As String("java.lang.String"))
End Sub

Sub OpenCam(dir As String, fn As String)
    Dim camintent As Intent
    camintent.Initialize("android.media.action.IMAGE_CAPTURE", "")
    camintent.PutExtra("output",ParseUri("file://" & File.Combine(dir,fn)))
    StartActivity(camintent)
End Sub


Sub LoadbitmapSample2(Dir As String,Filename As String,MaxWidth As Int,MaxHeight As Int,AutoRotate As Boolean) As Bitmap
   Dim bm As Bitmap
   bm=LoadBitmapSample(Dir,Filename,100%x,100%y)
   If AutoRotate Then
      Dim bm2 As BitmapExtended
      Dim exifdata1 As ExifData
      Try
         exifdata1.Initialize(Dir,Filename)
         'http://sylvana.net/jpegcrop/exif_orientation.html
         ' 1) transform="";;
         ' 2) transform="-flip horizontal";;
         ' 3) transform="-rotate 180";;
         ' 4) transform="-flip vertical";;
         ' 5) transform="-transpose";;
         ' 6) transform="-rotate 90";;
         ' 7) transform="-transverse";;
         ' 8) transform="-rotate 270";;
         Select Case exifdata1.getAttribute(exifdata1.TAG_ORIENTATION)
         Case exifdata1.ORIENTATION_ROTATE_180 '3
            bm=bm2.rotateBitmap(bm,180)
         Case exifdata1.ORIENTATION_ROTATE_90 '6
            bm=bm2.rotateBitmap(bm,90)
         Case exifdata1.ORIENTATION_ROTATE_270 '8
            bm=bm2.rotateBitmap(bm,270)			
         End Select
      Catch
			Log("LoadBitmapSample failed")
      End Try
   End If
   Return bm
End Sub

Sub LoadbitmapRotated(Dir As String,Filename As String,bmprotation As Float) As Bitmap
  Dim bm As Bitmap
  bm=LoadBitmapSample(Dir,Filename,100%x,100%y)   
  Dim bm2 As BitmapExtended
  Try
     bm=bm2.rotateBitmap(bm,bmprotation)
  Catch
  	Log("LoadBitmapRotated failed")
  End Try
  Return bm
End Sub

Sub InitP1
	Try 
		photoIdx=1
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo1")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 1 dans Imageview1
			ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)
			canTakePic=True
		Else			
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		End If		
	Catch
		Log(LastException)
	End Try
End Sub


public Sub updatePhoto(tpath As String,tPic As String)
	Try
		ImageView1.Bitmap=LoadBitmap(tpath, tPic)
	Catch
		Log(LastException)
	End Try
	
End Sub


Sub RunCamera
	Dim dt As String,filepic As String
	DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	If curphotoName="" Or curphotoName=Null Or curphotoName="null" Then curphotoName="Unknown"
	filepic=curphotoName & getPhotoPrefix & ".jpg"
	expectedPic=filepic
	If curphotoName="photo1" Then curphoto1=expectedPic
	If curphotoName="photo2" Then curphoto2=expectedPic
	If curphotoName="photo3" Then curphoto3=expectedPic
	If curphotoName="photo4" Then curphoto4=expectedPic
	If curphotoName="photo5" Then curphoto5=expectedPic
	If curphotoName="photo6" Then curphoto6=expectedPic
	If curphotoName="photo7" Then curphoto7=expectedPic
	If curphotoName="photo8" Then curphoto8=expectedPic
'	If curphotoName="photo9" Then curphoto9=expectedPic
'	If curphotoName="photo10" Then curphoto10=expectedPic
	
	
	If curphotoName="photo1apres" Then curphotop1=expectedPic
	If curphotoName="photo2apres" Then curphotop2=expectedPic
	If curphotoName="photo3apres" Then curphotop3=expectedPic
	If curphotoName="photo4apres" Then curphotop4=expectedPic
	If curphotoName="photo5apres" Then curphotop5=expectedPic
	If curphotoName="photo6apres" Then curphotop6=expectedPic
'	If curphotoName="photo7apres" Then curphotop7=expectedPic
'	If curphotoName="photo8apres" Then curphotop8=expectedPic
'	If curphotoName="photo9apres" Then curphotop9=expectedPic
'	If curphotoName="photo10apres" Then curphotop10=expectedPic	
	OpenCam(CNSPath,filepic)		
	'Stocker coordonnées GPS
	
End Sub


Sub UpdateSpnArea
	'Recree la liste des lieux pour le spinner
	spnArea.Clear
	Main.fTableLieux.Reset
	Dim i As Int
	For i=0 To Main.fTableLieux.GetRowCount - 1
		Main.fTableLieux.GetNextRow
		spnArea.Add(Main.fTableLieux.GetColumnValue("Libelle"))
	Next	
End Sub


Sub UpdateSpnTypePorte
	Dim i As Int
	spnTypePorte.Clear
	Main.fTableTypePorte.reset
	For i=0 To Main.fTableTypePorte.GetRowCount -1
		Main.fTableTypePorte.GetNextRow
		spnTypePorte.Add(Main.fTableTypePorte.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypePlancher
	Dim i As Int
	spnTypePlancher.Clear
	Main.ftableTypePlancher.reset
	For i=0 To Main.ftableTypePlancher.GetRowCount -1
		Main.ftableTypePlancher.GetNextRow
		spnTypePlancher.Add(Main.ftableTypePlancher.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeLocal
	Dim i As Int
	spnTypeLocal.Clear
	Main.ftableTypeLocal.reset
	For i=0 To Main.ftableTypeLocal.GetRowCount -1
		Main.ftableTypeLocal.GetNextRow
		spnTypeLocal.Add(Main.ftableTypeLocal.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeIsolant
	Dim i As Int
	spnTypeIsolant.Clear
	Main.ftableTypeIsolant.reset
	For i=0 To Main.ftableTypeIsolant.GetRowCount -1
		Main.ftableTypeIsolant.GetNextRow
		spnTypeIsolant.Add(Main.ftableTypeIsolant.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeCloisonnement
	Dim i As Int
	spnTypeCloison.Clear
	Main.fTableTypeCloisonnement.reset
	For i=0 To Main.fTableTypeCloisonnement.GetRowCount -1
		Main.fTableTypeCloisonnement.GetNextRow
		spnTypeCloison.Add(Main.fTableTypeCloisonnement.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeReseauPlafond
	Dim i As Int
	spnTypeReseauPlafond.Clear
	Main.fTabletypeReseauPlafond.reset
	For i=0 To Main.fTabletypeReseauPlafond.GetRowCount -1
		Main.fTabletypeReseauPlafond.GetNextRow
		spnTypeReseauPlafond.Add(Main.fTabletypeReseauPlafond.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnEtatIsolantPLB
	Dim i As Int
	spnEtatIsolantPLB.Clear
	Main.fTableEtatIsolant.reset
	For i=0 To Main.fTableEtatIsolant.GetRowCount -1
		Main.fTableEtatIsolant.GetNextRow
		spnEtatIsolantPLB.Add(Main.fTableEtatIsolant.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnResistanceThermique
	Dim i As Int
	spnResistanceIsolant.Clear
	Main.fTableResistanceThermique.reset
	For i=0 To Main.fTableResistanceThermique.GetRowCount -1
		Main.fTableResistanceThermique.GetNextRow
		spnResistanceIsolant.Add(Main.fTableResistanceThermique.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnAccesPCommunes
	Dim i As Int
	spnAccesPCommunes.Clear
	Main.fTableAccesPCommunes.reset
	For i=0 To Main.fTableAccesPCommunes.GetRowCount -1
		Main.fTableAccesPCommunes.GetNextRow
		spnAccesPCommunes.Add(Main.fTableAccesPCommunes.GetColumnValue("Libelle"))
	Next
End Sub

'UPDATE SPINNERS COMBLES
Sub UpdateSpnTypeLogementCBL
	Dim i As Int
	spnTypeLogement.Clear
	Main.fTableTypeLogement.reset
	For i=0 To Main.fTableTypeLogement.GetRowCount -1
		Main.fTableTypeLogement.GetNextRow
		spnTypeLogement.Add(Main.fTableTypeLogement.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeToitCBL
	Dim i As Int
	spnTypeToit.Clear
	Main.fTableTypeToit.reset
	For i=0 To Main.fTableTypeToit.GetRowCount -1
		Main.fTableTypeToit.GetNextRow
		spnTypeToit.Add(Main.fTableTypeToit.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypePlancherCBL
'	Dim i As Int
	spnTypePlancherCBL.Clear
	Main.fTableCBLPlancher.reset
	For i=0 To Main.fTableCBLPlancher.GetRowCount -1
		Main.fTableCBLPlancher.GetNextRow
		spnTypePlancherCBL.Add(Main.fTableCBLPlancher.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeIsolantCBL
	Dim i As Int
	spnTypeIsolantComble.Clear
	Main.fTableCBLIsolant.reset
	For i=0 To Main.fTableCBLIsolant.GetRowCount -1
		Main.fTableCBLIsolant.GetNextRow
		spnTypeIsolantComble.Add(Main.fTableCBLIsolant.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnNatureIsolantCBL
	Dim i As Int
	spnNatureIsolant.Clear
	Main.fTableNatureIsolant.reset
	For i=0 To Main.fTableNatureIsolant.GetRowCount -1
		Main.fTableNatureIsolant.GetNextRow
		spnNatureIsolant.Add(Main.fTableNatureIsolant.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnMateriauPrevuCBL
	Dim i As Int
	spnMateriauPrevu.Clear
	Main.fTableMateriauPrevu.reset
	For i=0 To Main.fTableMateriauPrevu.GetRowCount -1
		Main.fTableMateriauPrevu.GetNextRow
		spnMateriauPrevu.Add(Main.fTableMateriauPrevu.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnEtatPlafondCBL
	Dim i As Int
	spnEtatPlafond.Clear
	Main.fTableEtatPlafond.reset
	For i=0 To Main.fTableEtatPlafond.GetRowCount -1
		Main.fTableEtatPlafond.GetNextRow
		spnEtatPlafond.Add(Main.fTableEtatPlafond.GetColumnValue("Libelle"))
	Next
End Sub


Sub btnAddArea_Click
	Dim dlgArea As InputDialog,selArea As String, ret As Int
	dlgArea.Hint = "Entrez la zone"
	dlgArea.HintColor = Colors.ARGB(196, 255, 140, 0)
	ret = DialogResponse.CANCEL
	ret = dlgArea.Show("Entrez le nom de la zone", "Relevé Matelas", "Yes", "No", "Maybe",Null)
	selArea=dlgArea.Input
	If selArea<>"" Then
		Main.fTableLieux.GetNextRow
		Main.fTableLieux.AddRow(Array As Object(selArea,1))
		curArea=selArea
		UpdateSpnArea
		spnArea.SelectedIndex=spnArea.IndexOf(selArea)
	End If
End Sub

Sub spnArea_ItemClick (Position As Int, Value As Object)
	curArea=spnArea.SelectedItem
End Sub


Sub btn_prev_LongClick()
	'retourner a la premiere fiche
Try 
		expectedPic=""
		Main.fCursor.Reset()
		If Main.fCursor.IsBeforeFirst=False Then
			RAZ_fiche
			displayFiche
			IDMsg("FIRST")
		Else
			'ToastMessageShow("Debut de liste atteint !",False)
			Main.fCursor.GetNextRow
			RAZ_fiche
			displayFiche
			'Sleep(0)
		End If	
	Catch
		'ToastMessageShow("Debut de liste atteint !",False)
		Sleep(0)
	End Try			
End Sub

Sub btn_next_LongClick()
	gotoLastRow
End Sub

Sub btnAddMateriel_Click
	Dim dlgMateriel As InputDialog,selMateriel As String,ret As Int
	dlgMateriel.Hint = "Equipement"
	dlgMateriel.HintColor = Colors.ARGB(196, 255, 140, 0)
	ret = dlgMateriel.Show("Entrez l'équipement", "Relevé Matelas", "Yes", "No", "Maybe",Null)
	selMateriel=dlgMateriel.Input
	If selMateriel<>"" Then
		Main.fTableMateriel.GetNextRow
		Main.fTableMateriel.AddRow(Array As Object(selMateriel,1))
		curMateriel=selMateriel
		RefreshspnType
	End If
End Sub



Sub RefreshspnType
	'rajoute le nouvel equipement dans la liste des spinners (tout en gardant les valeurs en cours)
	Dim selEquipment As String
	'------------------------------------------------------------------
	'selEquipment=spnType1.SelectedItem
	'UpdateSpnType1
	'spnType1.SelectedIndex=spnType1.IndexOf(selEquipment)
	'------------------------------------------------------------------
	

End Sub

Sub ImageView1_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo1")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub imgFullScreen_LongClick

End Sub

Public Sub GetAllTabLabels (tabstrip As TabStrip) As List
	Dim jo As JavaObject = tabstrip
	Dim r As Reflector
	r.Target = jo.GetField("tabStrip")
	Dim tc As Panel = r.GetField("tabsContainer")
	Dim res As List
	res.Initialize
	For Each v As View In tc
		If v Is Label Then res.Add(v)
	Next
	Return res
   
End Sub

Sub ImageView1_Click
	canTakePic=True
	curphotoName="photo1"
	RunCamera
	'lancer le gps pour stocker dans la base
	If gpsClient.GPSEnabled=True Then gpsClient.Start(0,0)
End Sub

Sub ImageView6_Click
	canTakePic=True
	curphotoName="photo6"
	RunCamera
End Sub

Sub ImageView6_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo6")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView5_Click
	canTakePic=True
	curphotoName="photo5"
	RunCamera
End Sub

Sub ImageView5_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo5")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView4_Click
	canTakePic=True
	curphotoName="photo4"
	RunCamera
End Sub

Sub ImageView4_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo4")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView3_Click
	canTakePic=True
	curphotoName="photo3"
	RunCamera
End Sub

Sub ImageView3_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo3")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView2_Click
	canTakePic=True
	curphotoName="photo2"
	RunCamera
End Sub

Sub ImageView2_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo2")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp6_Click
	canTakePic=True
	curphotoName="photo6apres"
	RunCamera
End Sub

Sub ImageViewp6_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo6apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp5_Click
	canTakePic=True
	curphotoName="photo5apres"
	RunCamera
End Sub

Sub ImageViewp5_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo5apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp4_Click
	canTakePic=True
	curphotoName="photo4apres"
	RunCamera
End Sub

Sub ImageViewp4_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo4apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp3_Click
	canTakePic=True
	curphotoName="photo3apres"
	RunCamera
End Sub

Sub ImageViewp3_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo3apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp2_Click
	canTakePic=True
	curphotoName="photo2apres"
	RunCamera
End Sub

Sub ImageViewp2_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo2apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo dans Imageview
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageViewp1_Click
	canTakePic=True
	curphotoName="photo1apres"
	'enregistrer la date dans le timestamp après
	Main.fCursor.SetCurrentRowValue("timestamp_apres",getTimestamp)
	RunCamera
End Sub

Sub ImageViewp1_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo1apres")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo dans Imageview
			imvFSApres.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFSApres.Visible=True
			imvFSApres.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ValideCBL(showMsg As Boolean) As Int
	'Valider la saisie des champs COMBLES
	'renvoie 1->OK ,   0->Erreur, 2->sauvegarde forcee sur Erreur
	Dim isValide As Int ,sMsg As String
	isValide=1
	sMsg=""
	'Spinners
	If spnTypeLogement.SelectedItem=Null Or spnTypeLogement.SelectedItem="" Or spnTypeLogement.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type logement non renseigné"
	End If
	If spnNatureIsolant.SelectedItem=Null Or spnNatureIsolant.SelectedItem="" Or spnNatureIsolant.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Isolant en place non renseigné"
	End If
	If spnTypeToit.SelectedItem=Null Or spnTypeToit.SelectedItem="" Or spnTypeToit.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type toit non renseigné"
	End If
	If spnMateriauPrevu.SelectedItem=Null Or spnMateriauPrevu.SelectedItem="" Or spnMateriauPrevu.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Matériau prévu non renseigné"
	End If
	If spnEtatPlafond.SelectedItem=Null Or spnEtatPlafond.SelectedItem="" Or spnEtatPlafond.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Etat plafond non renseigné"
	End If
	If spnTypePlancherCBL.SelectedItem=Null Or spnTypePlancherCBL.SelectedItem="" Or spnTypePlancherCBL.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type plancher non renseigné"
	End If

	'TextBoxs
	If txtNbNiveaux.Text=Null Or txtNbNiveaux.Text="" Or txtNbNiveaux.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Nb niveaux vide ou 0"
	End If
	If txtHsousGoutiere.Text=Null Or txtHsousGoutiere.Text="" Or txtHsousGoutiere.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Ht ss gouttière vide ou 0"
	End If
	If txtSurfComble.Text=Null Or txtSurfComble.Text="" Or txtSurfComble.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Surface combles vide ou 0"
	End If
	If txtSurfReelle.Text=Null Or txtSurfReelle.Text="" Or txtSurfReelle.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Surface réelle vide ou 0"
	End If
	If isValide=0 And showMsg=True Then
		Dim i As Int
		i = Msgbox2("Certains champs sont erronés : " & sMsg & Chr(10) & "Voulez-vous enregistrer quand même ?", "Validation du relevé", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then isValide=2
	End If
	Return isValide
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean 'return true if you want to consume the event
	Dim sMeg As String
	If KeyCode = KeyCodes.KEYCODE_BACK Then
		'verifier si la fiche Combles est saisie
		If Main.fCursor.getColumnValue("timestamp")=Null Or Main.fCursor.getColumnValue("timestamp")="" Then
			If Msgbox2("La fiche n'a pas été sauvegardée, voulez-vous sortir quand même ?", "", "OUI", "", "NON", Null) = DialogResponse.POSITIVE Then
				'ToastMessageShow("Please wait...",True)
				Return False
				'ExitApplication 'App is exiting
				'ToastMessageShow("Exit Activity",True)
			Else
				Return True
			End If
		End If
	End If
End Sub


Sub RotateImage(original As Bitmap, degree As Float) As Bitmap
	Dim matrix As JavaObject
	matrix.InitializeNewInstance("android.graphics.Matrix", Null)
	matrix.RunMethod("postRotate", Array(degree))
	Dim bmp As JavaObject
	bmp.InitializeStatic("android.graphics.Bitmap")
	Dim NewImage As Bitmap = bmp.RunMethod("createBitmap", Array(original, 0, 0, original.Width, original.Height, _
     matrix, True))
	Return NewImage
End Sub


Sub btnSign_Click
	'lancer l'ecran de signature
	Dim filepic As String,sID As String
	sID=Main.fCursor.getColumnValue("ID")
	filepic="Signature_" & sID & getPhotoPrefix & ".jpg"
	gPic=filepic
	expectedPic=filepic
	curphotoName="photoSignature"
	StartActivity("SignatureScreen")
End Sub

Sub imvFS_LongClick
	imvFS.Visible=False
	imvFS.SendToBack
End Sub

Sub imvFSApres_LongClick
	imvFSApres.Visible=False
	imvFSApres.SendToBack
End Sub

Sub lblP6apres_LongClick
Try
	Dim i As Int
	i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 6 après", "Oui", "", "Non", Null)
	If i=DialogResponse.POSITIVE Then
		Main.fCursor.SetCurrentRowValue("photo6apres","")
		ImageViewp6.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
	End If
Catch
	Log(LastException)
End Try
End Sub

Sub lblP3apres_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 3 après", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo3apres","")
			ImageViewp3.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP5apres_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 5 après", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo5apres","")
			ImageViewp5.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP2apres_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 2 après", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo2apres","")
			ImageViewp2.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP4apres_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 4 après", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo4apres","")
			ImageViewp4.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP1apres_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 1 après", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo1apres","")
			ImageViewp1.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP6_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 6", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo6","")
			ImageView6.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP5_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 5", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo5","")
			ImageView5.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
	
End Sub

Sub lblP4_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 4", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo4","")
			ImageView4.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
	
End Sub

Sub lblP3_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 3", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo3","")
			ImageView3.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
	
End Sub

Sub lblP2_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 2", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo2","")
			ImageView2.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
	
End Sub

Sub lblP1_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 1", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo1","")
			ImageView1.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView8_Click
	canTakePic=True
	curphotoName="photo8"
	RunCamera
End Sub

Sub ImageView8_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo8")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo dans Imageview
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP8_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 8", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo8","")
			ImageView8.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub ImageView7_Click
	canTakePic=True
	curphotoName="photo7"
	RunCamera
End Sub

Sub ImageView7_LongClick
	'Affiche la photo (après) en plein écran
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo7")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo dans Imageview
			imvFS.Bitmap=LoadBitmap(CNSPath, sphoto)
			imvFS.Visible=True
			imvFS.BringToFront
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub lblP7_LongClick
	Try
		Dim i As Int
		i = Msgbox2("Voulez-vous supprimer cette photo  ?", "Supprimer photo 7", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then
			Main.fCursor.SetCurrentRowValue("photo7","")
			ImageView7.Bitmap=LoadBitmap(CNSPath,"vide.jpg")
		End If
	Catch
		Log(LastException)
	End Try
End Sub

Sub gpsclient_LocationChanged(Location1 As Location)
	Dim lat As Double,lng As Double
	userLocation=Location1
	'stocker latitude et longitude dans la base (trigger=clic photo 1)
	lat=userLocation.Latitude
	lng=userLocation.Longitude
	Main.fCursor.SetCurrentRowValue("addrLat",lat)
	Main.fCursor.SetCurrentRowValue("addrLng",lng)
	gpsClient.Stop
	'Msgbox(Location1.Latitude & " Lat " & Location1.Longitude & " long1", "Loc")
End Sub

Sub ValidePLB(showMsg As Boolean) As Int
	'Valider la saisie des champs PLANCHER BAS
	'renvoie 1->OK ,   0->Erreur, 2->sauvegarde forcee sur Erreur
	Dim isValide As Int ,sMsg As String
	isValide=1
	sMsg=""
	'Spinners
	If spnTypePorte.SelectedItem=Null Or spnTypePorte.SelectedItem="" Or spnTypePorte.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type porte non renseigné"
	End If
	If spnTypePlancher.SelectedItem=Null Or spnTypePlancher.SelectedItem="" Or spnTypePlancher.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type plancher non renseigné"
	End If
	If spnTypeCloison.SelectedItem=Null Or spnTypeCloison.SelectedItem="" Or spnTypeCloison.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type cloison non renseigné"
	End If
	If spnTypeLocal.SelectedItem=Null Or spnTypeLocal.SelectedItem="" Or spnTypeLocal.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type local non renseigné"
	End If
	If spnTypeIsolant.SelectedItem=Null Or spnTypeIsolant.SelectedItem="" Or spnTypeIsolant.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Type isolant non renseigné"
	End If
	If spnEtatIsolantPLB.SelectedItem=Null Or spnEtatIsolantPLB.SelectedItem="" Or spnEtatIsolantPLB.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Etat isolant non renseigné"
	End If
	If spnResistanceIsolant.SelectedItem=Null Or spnResistanceIsolant.SelectedItem="" Or spnResistanceIsolant.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Résistance isolant non renseignée"
	End If
	If spnAccesPCommunes.SelectedItem=Null Or spnAccesPCommunes.SelectedItem="" Or spnAccesPCommunes.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Parties communes non renseigné"
	End If
	If spnTypeReseauPlafond.SelectedItem=Null Or spnTypeReseauPlafond.SelectedItem="" Or spnTypeReseauPlafond.SelectedItem=" " Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - Réseau plafond non renseigné"
	End If
	
	'TextBoxs
	If txtHSPsansIsolant.Text=Null Or txtHSPsansIsolant.Text="" Or txtHSPsansIsolant.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - HSP sans isolant vide ou 0"
	End If
	If txtHSPAvecIsolant.Text=Null Or txtHSPAvecIsolant.Text="" Or txtHSPAvecIsolant.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - HSP avec isolant vide ou 0"
	End If
	If txtHSPPorteIsolant.Text=Null Or txtHSPPorteIsolant.Text="" Or txtHSPPorteIsolant.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - HSP plafond porte vide ou 0"
	End If
	If txtNbCageEsc.Text=Null Or txtNbCageEsc.Text="" Or txtNbCageEsc.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - nb cages escalier vide ou 0"
	End If
	If txtNbBat.Text=Null Or txtNbBat.Text="" Or txtNbBat.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - nb batiments vide ou 0"
	End If
	If txtNbBatImm.Text=Null Or txtNbBatImm.Text="" Or txtNbBatImm.Text="0" Then
		isValide=0
		sMsg=sMsg & Chr(10) & " - nb logements/batiment vide ou 0"
	End If
	If isValide=0 And showMsg=True Then
		Dim i As Int
		i = Msgbox2("Certains champs sont erronés : " & sMsg & Chr(10) & "Voulez-vous enregistrer quand même ?", "Validation du relevé", "Oui", "", "Non", Null)
		If i=DialogResponse.POSITIVE Then isValide=2
	End If
	Return isValide
End Sub

Sub getPhotoPrefix As String
	Dim result As String, dt As String
	'renvoie la fin du nom de la photo (avec Android Device ID et Date aaaammjjhhnnss)
	Try
		DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
		dt = DateTime.Date(DateTime.Now)
		result="_" & Main.DeviceID & "_" & dt 
		Return result		
	Catch
		Log(LastException)
		Return ""
	End Try
End Sub

Sub tsPreca_PageSelected (Position As Int)
	If Position=4 Then
		'empeche la selection du dernier onglet (infos autre appli) et repasse au premier
		tsPreca.ScrollTo(0,False)
	End If
End Sub