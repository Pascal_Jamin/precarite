﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim rp As RuntimePermissions
	Type Address(ID As Long, Adress1 As String, lat As Double, lng As Double, dist As Float)
	Dim gps As GPS,gLocation As Location
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private gmap As GoogleMap
	Private MapFragment1 As MapFragment
	Private rlvMode As String,sAddr1 As String,sName1 As String
	Private lstProximity As ListView
	Private lblRlvMode As Label
	Private spnRlvMode As Spinner
	Private lblSearchProximity As Label
	Dim lstAdresses As List,ListLimit As Int = 10
	Private ProgressBar1 As ProgressBar
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	'Activity.LoadLayout("Layout1")
	Activity.LoadLayout("gMaps")
	If MapFragment1.IsGooglePlayServicesAvailable = False Then
		ToastMessageShow("Please install Google Play Services.", True)
	End If
	
	'lstProximity.TwoLinesLayout.Background=cd
	lstProximity.TwoLinesLayout.ItemHeight=135
	lstProximity.ScrollingBackgroundColor = Colors.DarkGray
	lstProximity.TwoLinesLayout.Label.TextSize=13
	lstProximity.TwoLinesLayout.Label.TextColor=Colors.Blue
	lstProximity.TwoLinesLayout.SecondLabel.TextSize=12
	lstProximity.TwoLinesLayout.SecondLabel.TextColor=Colors.DarkGray
	lstProximity.TwoLinesLayout.SecondLabel.Typeface=Typeface.FONTAWESOME
	If FirstTime Then
		spnRlvMode.Add("Avant")
		spnRlvMode.Add("Après")
		spnRlvMode.SelectedIndex=spnRlvMode.IndexOf("Avant")
		gps.Initialize("gps")

	End If
	gLocation.Initialize
	lstAdresses.Initialize
	'gps.Start(0,0)
	'creer la liste des adresses proches
	CreeListeProximity
End Sub

Sub CreeListeProximity
	'creer la liste des adresses proches
	Dim curDoneAvant As String,curDoneApres As String,curLng As Double,curLat As Double,lstLng As Double,lstLat As Double,slstLng As Double,slstLat As Double
	Dim ecart As Double,nbrow As Long,curAdresse As String,curID As Long,curMode As String,curDist As Double, Distance As Float,cpt As Int
	Dim pList As  Location
	
	'Attendre le fix du gps et stocker la position actuelle 
	gps.Start(0,0)
	Wait For gps_LocationChanged (Location1 As Location)
	
	curMode="Avant"     'TODO : se baser sur le spinner 
	Main.ProximCursor.Reset
	nbrow=Main.fTable.GetRowCount
	ProgressBar1.Visible=True
	cpt=0
	For i=0 To nbrow -1
		Main.ProximCursor.GetNextRow
		ProgressBar1.progress=(i/nbrow)*100
		ProgressBar1.Invalidate
		curID=Main.ProximCursor.GetColumnValue("ID")
		Log(curID)
		curDoneAvant=Main.ProximCursor.GetColumnValue("timestamp")
		curDoneApres=Main.ProximCursor.GetColumnValue("timestamp_apres")
		If Main.ProximCursor.GetColumnValue("addrLat")<>Null Then lstLat=Main.ProximCursor.GetColumnValue("addrLat") Else lstLat=0
		If Main.ProximCursor.GetColumnValue("addrLng")<>Null Then lstLng=Main.ProximCursor.GetColumnValue("addrLng") Else lstLng=0
		'Si fiche adresse non remplie : on ajoute à la liste
		If (lstLat<>0 And lstLng<>0) And (curDoneAvant="null" Or curDoneAvant="") Then  'TODO : gerer Apres
			slstLat=lstLat
			slstLng=lstLng
			pList.Initialize2(slstLat,slstLng)
			'constituer l'adresse a afficher (addr1 + Ville)
			If Main.ProximCursor.GetColumnValue("addrChantier1")<>Null And Main.ProximCursor.GetColumnValue("addrChantier1")<>"" Then curAdresse=Main.ProximCursor.GetColumnValue("addrChantier1") Else curAdresse="(NoAdress)"
			If Main.ProximCursor.GetColumnValue("addrChantierVille")<>Null And Main.ProximCursor.GetColumnValue("addrChantierVille")<>"" Then curAdresse=curAdresse & " - " & Main.ProximCursor.GetColumnValue("addrChantierVille") Else curAdresse=curAdresse & " (NoVille)"
			'calculer distance
			Distance = gLocation.DistanceTo(pList)
			Dim adr As Address
			adr.Adress1=curAdresse
			adr.ID=curID
			adr.lat=lstLat
			adr.lng=lstLng
			adr.dist=Distance
			lstAdresses.Add(adr)
			cpt=cpt+1	
		End If
	Next
	
	ProgressBar1.Visible=False
	'Afficher les n premieres adresses triées par distance
	lstAdresses.SortType("dist", True) 'Sort the list based on the Age field.
	For j = 0 To lstAdresses.Size - 1
		Dim a As Address
		If i>=ListLimit Then Exit
		a = lstAdresses.Get(i)
		'Log(p)
		lstProximity.AddTwoLines2(a.Adress1, a.dist & " m",a.ID)
	Next
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub MapFragment1_Ready
	gmap = MapFragment1.GetMap
	rp.CheckAndRequest(rp.PERMISSION_ACCESS_FINE_LOCATION)
	Wait For Activity_PermissionResult (Permission As String, Result As Boolean)
	gmap.MyLocationEnabled = Result
	Dim m1 As Marker = gmap.AddMarker(10, 30, "test")
	m1.Snippet = "Test CNS"
End Sub

Sub gps_LocationChanged(Location1 As Location)
	gLocation=Location1
	gps.Stop
	Msgbox(Location1.Latitude & " Lat " & Location1.Longitude & " long1", "Loc")
End Sub