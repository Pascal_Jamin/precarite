﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private btnRet As Button
	Private ImageView2 As ImageView,imageview3 As ImageView
	Dim SD As SignatureData 'This object holds the data required for SignatureCapture
	Dim Canvas1 As Canvas, canvas2 As Canvas
	Private Panel2 As Panel
	Private btnSave As IconButton
	Private btnClear As IconButton
	
	
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Dessin")
	Try
		If Detail.gPic<>"" Then
			ImageView2.Bitmap=LoadBitmap(Detail.gPath,Detail.gPic)
		End If
	Catch
		Log(LastException)
	End Try

	'init pour Canvas
	Canvas1.Initialize(Panel2)	
	SD.Initialize
	SD.Canvas = Canvas1
	SD.Panel = Panel2
	SD.SignatureColor = Colors.Red
	SD.SignatureWidth = 4dip 'Stroke width
	
	btnSave.Text="SAVE"
	btnClear.Text="CLEAR"
	
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub


Sub btnRet_Click
	Activity.Finish
	StartActivity(Detail)
End Sub

Sub Panel2_Touch (Action As Int, X As Float, Y As Float)
	SignatureCapture.Panel_Touch(SD, X, Y, Action)
	Panel2.Invalidate
End Sub


Sub btnSave_Click
	'src code
	SignatureCapture.Save(SD, File.DirRootExternal, "sign.png")
	'ToastMessageShow("Signature saved to: " & File.Combine(File.DirRootExternal, "sign.png"), False)

	'merge images
	DoEvents
	Try
		Dim imgForeGround As BitmapDrawable
	   	imgForeGround.Initialize(LoadBitmap(File.DirRootExternal,"sign.png"))
	   
		Dim imgBackGround As Bitmap
		imgBackGround.Initialize(Detail.gPath,Detail.gPic)

		Dim obj1 As Reflector
		obj1.Target = imgForeGround
		obj1.RunMethod2("setAlpha", 255, "java.lang.int")

		Dim displayImg As ImageView
		displayImg.Initialize("displayImg")
		displayImg.Bitmap = imgBackGround
		displayImg.Gravity = Gravity.FILL
		displayImg.Visible=False
		Activity.AddView(displayImg,0dip,50dip,100%x-0dip,100%y-50dip)

		Dim destRect As Rect
		destRect.Initialize(0,0,displayImg.Width,displayImg.Height)

		Dim canNewImg As Canvas
		canNewImg.Initialize(displayImg)
		canNewImg.DrawDrawable(imgForeGround,destRect)
		displayImg.Invalidate
		
		obj1.RunMethod2("setAlpha", 255, "java.lang.int")
	    canNewImg.DrawBitmap(imgBackGround,Null,destRect)
	    canNewImg.DrawDrawable(imgForeGround,destRect)
	    displayImg.Bitmap = canNewImg.Bitmap

		'Resizer l'image en 1080x608
		'Dim bmpr As Bitmap
		'bmpr=CreateScaledBitmap(canNewImg.Bitmap,1080,608)

		Dim Out As OutputStream
		Out = File.OpenOutput(Detail.gPath, Detail.gPic, False)
		canNewImg.Bitmap.WriteToStream(Out, 100, "JPEG")
		'bmpr.WriteToStream(Out, 100, "JPEG")
		ToastMessageShow("Picture saved to: " & File.Combine(Detail.gPath, Detail.gPic), False)
		CallSubDelayed3(Detail,"updatePhoto",Detail.gPath, Detail.gPic)
		Activity.Finish
	Catch
		Log(LastException)
		ToastMessageShow("Save Image failed",True)
	End Try

End Sub

Sub btnClear_Click
	SignatureCapture.Clear(SD)
	Panel2.Invalidate
End Sub

Sub CreateScaledBitmap(Original As Bitmap, NewWidth As Int, NewHeight As Int) As Bitmap
    Dim r As Reflector
    Dim b As Bitmap
   
    b = r.RunStaticMethod("android.graphics.Bitmap", "createScaledBitmap", _
        Array As Object(Original, NewWidth, NewHeight, True), _
        Array As String("android.graphics.Bitmap", "java.lang.int", "java.lang.int", "java.lang.boolean"))
    Return b
End Sub
