﻿Type=Activity
Version=7.3
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim ftp As FTP,gPath As String,gPic As String
	Dim manager As PreferenceManager
	Dim screen As PreferenceScreen
End Sub

Sub Globals
	Dim camera1 As CameraExClass
	Dim btnTakePicture As IconButton
	Dim photoIdx As Int
	Dim db As JackcessDatabase
    Dim Found As Boolean
	Dim CampagneID As Int,iscamera As Boolean,canTakePic As Boolean
	Dim Panel3 As Panel
	Dim bmp1 As Bitmap,rotation As String,campagne As String
	Dim curPhoto As String,curID As Int,CNSPath As String,curTC As String,curSite As String,curMateriel As String
	Dim curphoto2 As String,expectedPic As String,curArea As String,curUser As String,curphoto3 As String,curphoto4 As String,curphoto5 As String,curphoto6 As String
	Private commentaire As EditText
	Private btn_next As IconButton,btn_prev As IconButton, btnSave As Button, btnSaverec As IconButton
	Private ImageView1 As ImageView, imgFullScreen As ImageView
	Private spnArea As Spinner
	Private btnAddArea As Button,btnAddMateriel As Button
	Private NB1 As EditText,NB2 As EditText,NB3 As EditText, NB4 As EditText, NB5 As EditText, NB6 As EditText, NB7 As EditText, NB8 As EditText, NB9 As EditText
	Private spnType1 As Spinner,spnType2 As Spinner,spnType3 As Spinner,spnType4 As Spinner,spnType5 As Spinner,spnType6 As Spinner,spnType7 As Spinner, spnType8 As Spinner, spnType9 As Spinner
	Private DN1 As EditText,DN2 As EditText,DN3 As EditText, DN4 As EditText, DN5 As EditText, DN6 As EditText, DN7 As EditText,DN8 As EditText, DN9 As EditText
	Private spnFluide1 As Spinner,spnFluide2 As Spinner,spnFluide3 As Spinner,spnFluide4 As Spinner,spnFluide5 As Spinner,spnFluide6 As Spinner,spnFluide7 As Spinner,spnFluide8 As Spinner,spnFluide9 As Spinner
	Private spnVal1 As Spinner,spnVal2 As Spinner,spnVal3 As Spinner,spnVal4 As Spinner,spnVal5 As Spinner,spnVal6 As Spinner,spnVal7 As Spinner,spnVal8 As Spinner,spnVal9 As Spinner
	Private Options2 As Label
	Private lblPorte As Label
	Private lblPlancher As Label
	Private lblCloison As Label
	Private lblLocal As Label
	Private lblIsolant As Label
	Private cbAccesPCommunes As CheckBox
	Private spnTypePorte As Spinner
	Private spnTypePlancher As Spinner
	Private spnTypeCloison As Spinner
	Private spnTypeLocal As Spinner
	Private spnTypeIsolant As Spinner
	Private ImageView2 As ImageView
	Private ImageView3 As ImageView
	Private ImageView4 As ImageView
	Private ImageView5 As ImageView
	Private ImageView6 As ImageView
	Private lblcbAcces As Label
	Private lblHSPAvec As Label
	Private lblcbCollectif As Label
	Private cbCollectif As CheckBox
	Private txtHSPAvecIsolant As EditText
	Private txtHSPsansIsolant As EditText
	Private lblHSPsans As Label
	Private lblNbBat As Label
	Private txtNbBat As EditText
	Private lblNBBatImm As Label
	Private txtNbBatImm As EditText
	Private txtAddrCT1 As EditText
	Private txtAddrCT2 As EditText
	Private txtAddrCTCP As EditText
	Private txtAddrCTVille As EditText
	Private txtNbCageEsc As EditText
	Private lblnbCageEsc As Label
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Activity.LoadLayout("Detail_CBL")
	Activity.Title="Relevé Précarité CNS (Détail)"
	Activity.AddMenuItem("Options","Options")
	Activity.AddMenuItem("Supprimer et quitter","ResetDB")
	CNSPath=File.DirRootExternal & "/RelevePrecarite"
	iscamera=False
	canTakePic=True
	expectedPic=""
	photoIdx=1	
	'init des boutons de controle
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"313-arrow-left.png"))
	btn_prev.Text = ""
	btn_prev.IconPadding = 0
	btn_prev.setIcon(True,bm)
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"309-arrow-right.png"))
	btn_next.Text = ""
	btn_next.IconPadding = 0
	btn_next.setIcon(True,bm)
	Dim bm As BitmapDrawable
	bm.Initialize(LoadBitmap(File.DirAssets,"099-floppy-disk.png"))
	btnSaverec.Text = ""
	btnSaverec.IconPadding = 0
	btnSaverec.setIcon(True,bm)
	
	'Ouvrir base Access et initialiser fichiers
	If FirstTime Then
		If Not(File.Exists(CNSPath,"")) Then File.MakeDir(File.DirRootExternal, "/RelevePrecarite")
		If Not(File.Exists(CNSPath, "precarite.accdb")) Then File.Copy(File.DirAssets, "precarite.accdb", CNSPath, "precarite.accdb")
		If Not(File.Exists(CNSPath, "vide.jpg")) Then File.Copy(File.DirAssets, "vide.jpg", CNSPath, "vide.jpg") 
		db.Open(CNSPath & "/precarite.accdb")
		
		ftp.Initialize("ftp", "ftp.barrault-recherche.com", 21, "barrault", "TH62JX14dL")
		Main.fTable.Initialize(db.GetTable("Detail"))
		Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
		Main.fTableLieux.Initialize(db.GetTable("Lieux"))
		Main.fTableTypeCapteur.Initialize(db.GetTable("TypeCapteur"))
		Main.fTableSite.Initialize(db.GetTable("Site"))
		Main.fTableMateriel.Initialize(db.GetTable("Materiel"))
		Main.fTableFluide.Initialize(db.GetTable("TypeFluide"))
		Main.fTableTemperature.Initialize(db.GetTable("Temperature"))
		CampagneID=1  'A changer pour pouvoir selectionner la campagne en amont
		curArea="Localisation"
		
		'Afficher l'ordre des colonnes
		Dim colonnes() As String,i As Int
		colonnes=Main.fTable.GetColumnNames
		For i=0 To colonnes.Length -1
			Log("Colonne " & i & " - " & colonnes(i))
		Next

		
		CreatePreferenceScreen
		If manager.GetAll.Size = 0 Then SetDefaults
	End If		
	
	'TEST : Afficher Libelle de la campagne 1
	If db.IsOpen=False Then
		db.Open(CNSPath & "/precarite.accdb")
		Main.fTable.Initialize(db.GetTable("Detail"))
		Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
		Main.fTableLieux.Initialize(db.GetTable("Lieux"))
		Main.fTableTypeCapteur.Initialize(db.GetTable("TypeCapteur"))
		Main.fTableSite.Initialize(db.GetTable("Site"))
		Main.fTableMateriel.Initialize(db.GetTable("Materiel"))
		Main.fTableFluide.Initialize(db.GetTable("TypeFluide"))
	End If
	CampagneID=1
	Main.fCursor.Initialize(db.GetTable("Detail"),Main.ftable.GetIndex("PrimaryKey"))
	
	'initialiser les spinners
	UpdateSpnTypePorte:UpdateSpnTypePlancher:UpdateSpnTypeCloisonnement:UpdateSpnTypeLocal:UpdateSpnTypeIsolant


	'initialiser infos generales (rotation,user,campagne,...)
	rotation=manager.Getstring("Rotation")
	If rotation="" Then rotation="0"
	curUser=manager.Getstring("user1")
	If curUser="" Then curUser="CNS"
	campagne=manager.Getstring("cp1")
	If campagne="" Then campagne="Campagne1"

	'aller à la dernière fiche (ou position dans la liste)
	Dim position As Int
	position=Main.gPosition
	If position=0 Then gotoLastRow Else gotoRowPosition(position)	
	
End Sub

Sub Camera1_Ready (Success As Boolean)
	If Success Then
		camera1.StartPreview
		'limiter la taille des photos
		'camera1.GetSupportedPicturesSizes
	Else
		ToastMessageShow("Cannot open camera.", False)
	End If
End Sub

Sub Activity_Resume
	iscamera=True
	
	'si retour app photo : sauvegarder photo
	If expectedPic<>"" And File.Exists(CNSPath,expectedPic) Then
		'Gerer rotation auto 
		Try
			Dim rot As String,degre As Float
			rot=manager.GetString("Rotation")
			Dim exif As ExifData
			exif.Initialize(CNSPath,expectedPic)
			Select rot
				Case "90"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_90)
				Case "180"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_180)
				Case "270"
					exif.setAttribute(exif.TAG_ORIENTATION,exif.ORIENTATION_ROTATE_270)
			End Select
			degre=rot
			bmp1.Initialize(CNSPath,expectedPic)
			Dim bmp2 As Bitmap = LoadbitmapRotated(CNSPath,expectedPic,degre)
			Dim out As OutputStream
			out=File.OpenOutput(CNSPath,expectedPic,False)
			bmp2.WriteToStream(out,80,"JPEG")
			out.Close
		Catch
			Log(LastException)
			ToastMessageShow("Photo rotate error", False)
		End Try
		
		Main.fCursor.SetCurrentRowValue("photo" & photoIdx,expectedPic)
		ImageView1.Bitmap=LoadBitmap(CNSPath, expectedPic)
		ImageView1.BringToFront
		If photoIdx=1 Then curPhoto=expectedPic Else curphoto2=expectedPic
		SaveFiche
		expectedPic=""
	End If
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	'//camera1.Release
	Try
		StateManager.SetSetting("CameraRotation",manager.GetString("Rotation"))
		StateManager.SetSetting("SurveyName",manager.GetString("cp1"))
		StateManager.SaveSettings
	Catch
		Log(LastException)
	End Try

End Sub

Sub Camera1_PictureTaken (Data() As Byte)
	'camera1.StartPreview
	Dim out As OutputStream,filePic As String,filePicF As String
	Dim dt As String
	DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	'photoIdx=photoIdx+1
	
	filePicF="matelas_" & dt & "F.jpg"
	filePic="matelas_" & dt & ".jpg"
	out = File.OpenOutput(CNSPath, filePicF, False)
	out.WriteBytes(Data, 0, Data.Length)
	out.Close
	'ToastMessageShow("Image saved: " & File.Combine(CNSPath, filePic),False)
	btnTakePicture.Enabled = True

	'recompresse l'image
	Dim BitMap_Source As Bitmap
	Dim BitMap_Travail As BitmapExtended
	out = File.OpenOutput (CNSPath, filePic, False)
	BitMap_Source = LoadBitmap(CNSPath,filePicF)
	'BitMap_Travail.compress (BitMap_Source, "JPEG", 75, out)
	'BitMap_Travail.
	
	BitMap_Source.WriteToStream(out, 75, "JPEG")
	out.Close


	'HasPhoto=True
	If photoIdx=1 Then curPhoto=filePic
	If photoIdx=2 Then curphoto2=filePic	
	Try
		SaveFiche
	Catch
		Log(LastException)
	End Try
	'Associe l'image au canvas	
	ImageView1.Bitmap=LoadbitmapSample2(CNSPath, filePic,100%x,100%y,True)
End Sub

Sub btnTakePicture_Click
	RunCamera
End Sub

Sub RAZ_fiche
	commentaire.Text=""
	'--------------------------------------------------------------------
	'NB1.Text=""
	'spnType1.SelectedIndex=0
	'DN1.Text=""
	'Valeur1.Text=""
	'spnVal1.SelectedIndex=spnVal1.IndexOf(" ")
	'spnFluide1.SelectedIndex=spnFluide1.IndexOf(" ")
	'--------------------------------------------------------------------

	'UpdateSpnType1:UpdateSpnType2:UpdateSpnType3:UpdateSpnType4:UpdateSpnType5:UpdateSpnType6:UpdateSpnType7:UpdateSpnType8:UpdateSpnType9
	
	curPhoto=""
	curphoto2=""
	expectedPic=""	
	spnArea.SelectedIndex=spnArea.IndexOf("Localisation")
End Sub

Sub btn_next_Click
	'passe à la fiche suivante / nouvelle fiche
	Dim NextID As Int
	Try
		'essayer d'enregistrer la fiche
		SaveFiche
	Catch
		Log(LastException)
	End Try
	RAZ_fiche
	curID=Main.fCursor.GetColumnValue("ID")
	NextID=curID+1	
	Main.fCursor.GetNextRow
	If Main.fCursor.IsAfterLast=True Then
		RAZ_fiche
		curPhoto=""
		curphoto2=""
		AddFiche
		Main.fCursor.FindFirstRow("ID",NextID)
		IDMsg("NEXT NOFOUND")
		ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		ImageView1.BringToFront
		canTakePic=True
		'displayFiche
		'reprendre la precedente localisation
		spnArea.SelectedIndex=spnArea.IndexOf(curArea)
	Else
		displayFiche
		IDMsg("NEXT FOUND")
	End If	
End Sub


Sub btnSaverec_Click
SaveFiche
End Sub

Sub btn_prev_Click
	'passe à la fiche suivante / nouvelle fiche
	Try
		'essayer d'enregistrer la fiche
		SaveFiche
	Catch
		Log(LastException)
	End Try
	
	Try 
		expectedPic=""
		Main.fCursor.GetPreviousRow
		If Main.fCursor.IsBeforeFirst=False Then
			RAZ_fiche
			displayFiche
			IDMsg("PREV")
		Else
			'ToastMessageShow("Debut de liste atteint !",False)
			Main.fCursor.GetNextRow
			RAZ_fiche
			displayFiche
			'Sleep(0)
		End If	
	Catch
		'ToastMessageShow("Debut de liste atteint !",False)
		Sleep(0)
	End Try		
End Sub

Sub IDMsg(act As String)
	Dim msg As String
	Try
		If Main.fCursor.IsAfterLast=False And Main.fCursor.IsBeforeFirst=False Then
			msg=Main.fCursor.GetColumnValue("ID")
		Else
			msg="#UNDEF#"
		End If	
	Catch
		msg="ERROR"
	End Try	
	'ToastMessageShow("ACTION=" & act & " ID=" & msg,False)
End Sub

Sub AddFiche
	'Ajouter une fiche vierge dans un nouvel enregistrement
	RAZ_fiche
	AddRecord
	spnArea.SelectedIndex=spnArea.IndexOf(curArea)
	Main.fCursor.GetNextRow
End Sub

Sub AddRecord
	'se baser sur la liste des champs en commentaire plus haut
	Dim Record(39) As Object, i As Int
	Record(0)=CampagneID
	Record(1)=1   'ID
	Record(2)=curArea
	Record(3)=""  'commentaire
	Record(4)=""  'Site
	Record(5)=""   'Operateur
	For i=6 To 13
		Record(i)=""  'Adresses chantier et Locataire
	Next
	'Checkboxs et valeurs diverses
	Record(14)=0:Record(15)=0:Record(16)=0:Record(17)=0:Record(18)=0
	Record(19)="":Record(20)=0:Record(21)="":Record(22)="":Record(23)="":Record(24)=0:Record(25)=0:Record(26)=0:Record(27)=0:Record(28)="":Record(29)=""
	Record(30)=0:Record(31)=0:Record(32)=0
	For i=33 To 44
		Record(i)=""  'photos 1 a 6 (avant + apres)
	Next
	Record(45)=getTimestamp
	Main.fTable.AddRow(Record)
End Sub
Sub ImageView1_Click
	canTakePic=True
	RunCamera
End Sub

Sub SaveFiche
	'enregistrer la fiche
	Dim ccomment As String ,cphoto1 As String,cphoto2 As String,ctimestamp As String
	Dim cSite As String,cLocalisation As String,cFluide As String,cValeurn As String,cMateriel As String,cNB As Int,cDN As Int
	Dim sAddrCT1 As String,sAddrCT2 As String,sAddrCP As String,sAddrVille As String,sAddrLoc1 As String,sAddrLoc2 As String,sAddrLocCP As String,sAddrLocVille As String
	Dim sTypePorte As String,sTypePlancher As String,sTypeLocal As String,sTypeCloison As String,sNatureIsolant As String
	Dim nbBatiment As Int,nbLogement As Int,HSPsansIsolant As Int,HSPavecIsolant As Int,chkAccesPC As Int,chkBatCollectif As Int
	
	Try
	'ID campagne force a 1
	Main.fCursor.SetCurrentRowValue("IDCampagne",1)
	cSite="Site"
	Main.fCursor.SetCurrentRowValue("Site",cSite)
	cLocalisation=spnArea.SelectedItem
	Main.fCursor.SetCurrentRowValue("Localisation",cLocalisation)
	
	If curPhoto=Null Then cphoto1="vide.jpg" Else cphoto1=curPhoto
	Main.fCursor.SetCurrentRowValue("photo1",cphoto1)
	If curphoto2=Null Or curphoto2="" Then cphoto2="vide.jpg" Else cphoto2=curphoto2
	Main.fCursor.SetCurrentRowValue("photo2",cphoto2)
	
	'ccomment=commentaire.Text
	'Main.fCursor.SetCurrentRowValue("Commentaires",ccomment)
	
	
	'cphoto3=""
	ctimestamp = getTimestamp
	Main.fCursor.SetCurrentRowValue("timestamp",ctimestamp)
	'---------------------------------------------------------------
	If txtAddrCT1.Text<>Null And txtAddrCT1.Text<>"" Then sAddrCT1=txtAddrCT1.Text Else sAddrCT1=""
	Main.fCursor.SetCurrentRowValue("addrChantier1",sAddrCT1)
	If txtAddrCT2.Text<>Null And txtAddrCT2.Text<>"" Then sAddrCT2=txtAddrCT2.Text Else sAddrCT2=""
	Main.fCursor.SetCurrentRowValue("addrChantier2",sAddrCT2)
	If txtAddrCTCP.Text<>Null And txtAddrCTCP.Text<>"" Then sAddrCP=txtAddrCTCP.Text Else sAddrCP=""
	Main.fCursor.SetCurrentRowValue("addrChantierCP",sAddrCP)
	If txtAddrCTVille.Text<>Null And txtAddrCTVille.Text<>"" Then sAddrVille=txtAddrCTVille.Text Else sAddrVille=""
	Main.fCursor.SetCurrentRowValue("addrChantierVille",sAddrVille)
	If spnTypePorte.SelectedItem<>Null And spnTypePorte.SelectedItem<>"" Then sTypePorte=spnTypePorte.SelectedItem Else sTypePorte=""
	Main.fCursor.SetCurrentRowValue("TypePorte",sTypePorte)
	If spnTypePlancher.SelectedItem<>Null And spnTypePlancher.SelectedItem<>"" Then sTypePlancher=spnTypePlancher.SelectedItem Else sTypePlancher=""
	Main.fCursor.SetCurrentRowValue("TypePlancher",sTypePlancher)
	If spnTypeCloison.SelectedItem<>Null And spnTypeCloison.SelectedItem<>"" Then sTypeCloison=spnTypeCloison.SelectedItem Else sTypeCloison=""
	Main.fCursor.SetCurrentRowValue("TypeCaveCloisonnement",sTypeCloison)
	If spnTypeLocal.SelectedItem<>Null And spnTypeLocal.SelectedItem<>"" Then sTypeLocal=spnTypeLocal.SelectedItem Else sTypeLocal=""
	Main.fCursor.SetCurrentRowValue("TypeLocal",sTypeLocal)
	If spnTypeIsolant.SelectedItem<>Null And spnTypeIsolant.SelectedItem<>"" Then sNatureIsolant=spnTypeIsolant.SelectedItem Else sNatureIsolant=""
	Main.fCursor.SetCurrentRowValue("NatureIsolant",sNatureIsolant)
	If txtHSPAvecIsolant.Text<>Null Then HSPavecIsolant=txtHSPAvecIsolant.Text Else HSPavecIsolant=0
	Main.fCursor.SetCurrentRowValue("HSPAvecIsolant",HSPavecIsolant)
	If txtHSPsansIsolant.Text<>Null Then HSPsansIsolant=txtHSPsansIsolant.Text Else HSPsansIsolant=0
	Main.fCursor.SetCurrentRowValue("HSPSansIsolant",HSPsansIsolant)
	If txtNbBat.Text<>Null Then nbBatiment=txtNbBat.Text Else nbBatiment=0
	Main.fCursor.SetCurrentRowValue("nbBatiment",nbBatiment)
	If txtNbBatImm.Text<>Null Then nbLogement=txtNbBatImm.Text Else nbLogement=0
	Main.fCursor.SetCurrentRowValue("nbLogementParBatiment",nbLogement)
	
	IDMsg("SAVE")
	Catch
		Log("SaveFiche Update failed")
	End Try	
End Sub

Sub getTimestamp As String
	Dim dt As String
	DateTime.DateFormat = "dd/MM/yyyy HH:mm:ss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	Return dt
End Sub

Sub displayFiche
	Try
		'afficher les infos fiche (hors photo)
		'If Main.fCursor.GetColumnValue("Commentaires")=Null Then
		'	commentaire.Text=" "
		'Else
		'	commentaire.Text=Main.fCursor.GetColumnValue("Commentaires")
		'End If
		
		spnArea.SelectedIndex=spnArea.IndexOf(Main.fCursor.GetColumnValue("Localisation"))
		curArea=spnArea.SelectedItem
		'----------------------------------------------------------------------------------------------------------------------------
		If Main.fCursor.GetColumnValue("addrChantier1")<>Null Then txtAddrCT1.Text=Main.fCursor.GetColumnValue("addrChantier1") Else txtAddrCT1.Text=""
		If Main.fCursor.GetColumnValue("addrChantier2")<>Null Then txtAddrCT2.Text=Main.fCursor.GetColumnValue("addrChantier2") Else txtAddrCT2.Text=""
		If Main.fCursor.GetColumnValue("addrChantierCP")<>Null Then txtAddrCTCP.Text=Main.fCursor.GetColumnValue("addrChantierCP") Else txtAddrCTCP.Text=""
		If Main.fCursor.GetColumnValue("addrChantierVille")<>Null Then txtAddrCTVille.Text=Main.fCursor.GetColumnValue("addrChantierVille") Else txtAddrCTVille.Text=""
		spnTypePorte.SelectedIndex=spnTypePorte.IndexOf(Main.fCursor.GetColumnValue("TypePorte"))
		spnTypePlancher.SelectedIndex=spnTypePlancher.IndexOf(Main.fCursor.GetColumnValue("TypePlancher"))
		spnTypeCloison.SelectedIndex=spnTypeCloison.IndexOf(Main.fCursor.GetColumnValue("TypeCaveCloisonnement"))
		spnTypeLocal.SelectedIndex=spnTypeLocal.IndexOf(Main.fCursor.GetColumnValue("TypeLocal"))
		spnTypeIsolant.SelectedIndex=spnTypeIsolant.IndexOf(Main.fCursor.GetColumnValue("NatureIsolant"))
		If Main.fCursor.GetColumnValue("HSPSansIsolant")<>Null Then txtHSPsansIsolant.Text=Main.fCursor.GetColumnValue("HSPSansIsolant") Else txtHSPsansIsolant.Text=""
		If Main.fCursor.GetColumnValue("HSPAvecIsolant")<>Null Then txtHSPAvecIsolant.Text=Main.fCursor.GetColumnValue("HSPAvecIsolant") Else txtHSPAvecIsolant.Text=""
		If Main.fCursor.GetColumnValue("nbBatiment")<>Null Then txtNbBat.Text=Main.fCursor.GetColumnValue("nbBatiment") Else txtNbBat.Text=""
		If Main.fCursor.GetColumnValue("nbLogementParBatiment")<>Null Then txtNbBatImm.Text=Main.fCursor.GetColumnValue("nbLogementParBatiment") Else txtNbBatImm.Text=""
 		
		'----------------------------------------------------------------------------------------------------------------------------
	Catch
		ToastMessageShow("Erreur affichage fiche",False)
	End Try
	Try
		'afficher la photo
		Log("DisplayFiche - Photo enter")
		Dim sphoto2 As String,sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo" & photoIdx)
		'sphoto1=Main.fCursor.GetColumnValue("photo1")
		sphoto2=Main.fCursor.GetColumnValue("photo2")
		'IDMsg("PREV")
		If (File.Exists(CNSPath,sphoto)) And sphoto<>""	 Then 
			Log("DisplayFiche - PhotoExists 1 enter")
			curPhoto=sphoto		
			ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)
			ImageView1.BringToFront
			canTakePic=True
		Else
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
			ImageView1.BringToFront
			canTakePic=True
		End If	
		If (File.Exists(CNSPath,sphoto2)) Then curphoto2=sphoto2
	Catch
		ToastMessageShow("Erreur affichage photo",False)
		ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		If iscamera Then 
			canTakePic=True
		End If
	End Try	
	'remettre picture1 par defaut
	InitP1
End Sub


Sub btnP1_Click
	InitP1
End Sub

Sub btnP2_Click
	photoIdx=2
	Try
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo2")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)
			canTakePic=True
		Else			
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		End If		
	Catch
		Log(LastException)
	End Try
End Sub

Sub gotoLastRow
	Dim nbrow As Int
	nbrow=Main.fTable.GetRowCount
	Log("STEP2")
	If nbrow>=1 Then
		'afficher la derniere fiche
		Main.fCursor.Reset
		For i=0 To nbrow -1
			Main.fCursor.GetNextRow
		Next
		displayFiche
	End If
	Log("STEP3")
	If nbrow<1 Then
		Main.fTable.addrow(Array As Object(1,1,1,2,"","","","",getTimestamp))
		'AddFiche		
		Main.fCursor.Reset
	End If
	Log("STEP4 Init end")
	'IDMsg("INIT")
End Sub

Sub gotoRowPosition(pos As Int)
	Dim nbrow As Int
	nbrow=Main.fTable.GetRowCount
	If pos>nbrow Then Return
	If nbrow>=1 Then
		Main.fCursor.Reset
		For i=0 To pos-1
			Main.fCursor.GetNextRow
		Next
		displayFiche
	End If
End Sub

Sub ftp_UploadCompleted (ServerPath As String, Success As Boolean)
    Log(ServerPath & ", Success=" & Success)
    If Success = False Then 
		Log(LastException.Message)
		ToastMessageShow("Envoi ZIP ECHEC : " & LastException.Message,True)
	Else
		ToastMessageShow("Envoi ZIP REUSSI",True)
	End If
End Sub	


Sub ParseUri(s As String) As Object
    Dim r As Reflector
    Return r.RunStaticMethod("android.net.Uri", "parse", Array As Object(s), Array As String("java.lang.String"))
End Sub

Sub OpenCam(dir As String, fn As String)
    Dim camintent As Intent
    camintent.Initialize("android.media.action.IMAGE_CAPTURE", "")
    camintent.PutExtra("output",ParseUri("file://" & File.Combine(dir,fn)))
    StartActivity(camintent)
End Sub


Sub LoadbitmapSample2(Dir As String,Filename As String,MaxWidth As Int,MaxHeight As Int,AutoRotate As Boolean) As Bitmap
   Dim bm As Bitmap
   bm=LoadBitmapSample(Dir,Filename,100%x,100%y)
   If AutoRotate Then
      Dim bm2 As BitmapExtended
      Dim exifdata1 As ExifData
      Try
         exifdata1.Initialize(Dir,Filename)
         'http://sylvana.net/jpegcrop/exif_orientation.html
         ' 1) transform="";;
         ' 2) transform="-flip horizontal";;
         ' 3) transform="-rotate 180";;
         ' 4) transform="-flip vertical";;
         ' 5) transform="-transpose";;
         ' 6) transform="-rotate 90";;
         ' 7) transform="-transverse";;
         ' 8) transform="-rotate 270";;
         Select Case exifdata1.getAttribute(exifdata1.TAG_ORIENTATION)
         Case exifdata1.ORIENTATION_ROTATE_180 '3
            bm=bm2.rotateBitmap(bm,180)
         Case exifdata1.ORIENTATION_ROTATE_90 '6
            bm=bm2.rotateBitmap(bm,90)
         Case exifdata1.ORIENTATION_ROTATE_270 '8
            bm=bm2.rotateBitmap(bm,270)			
         End Select
      Catch
			Log("LoadBitmapSample failed")
      End Try
   End If
   Return bm
End Sub

Sub LoadbitmapRotated(Dir As String,Filename As String,bmprotation As Float) As Bitmap
  Dim bm As Bitmap
  bm=LoadBitmapSample(Dir,Filename,100%x,100%y)   
  Dim bm2 As BitmapExtended
  Try
     bm=bm2.rotateBitmap(bm,bmprotation)
  Catch
  	Log("LoadBitmapRotated failed")
  End Try
  Return bm
End Sub

Sub InitP1
	Try 
		photoIdx=1
		Dim sphoto As String
		sphoto=Main.fCursor.GetColumnValue("photo1")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 1 dans Imageview1
			ImageView1.Bitmap=LoadBitmap(CNSPath, sphoto)
			canTakePic=True
		Else			
			ImageView1.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		End If		
	Catch
		Log(LastException)
	End Try
End Sub


public Sub updatePhoto(tpath As String,tPic As String)
	Try
		ImageView1.Bitmap=LoadBitmap(tpath, tPic)
	Catch
		Log(LastException)
	End Try
	
End Sub


Sub ResetDB_click
	'Supprimer la base et le repertoire
	Try
		If db.IsOpen Then db.Close
		'supprimer tous les fichiers du repertoire, puis le repertoire
		Dim FilesToDelete As List
		FilesToDelete.Initialize
		FilesToDelete.AddAll(File.ListFiles(CNSPath))
		For I = 0 To FilesToDelete.Size -1		    
		    File.Delete(CNSPath, FilesToDelete.Get(I))
		Next
		File.Delete(CNSPath,"")
		ToastMessageShow("Relevé Matelas supprimé",True)
		ExitApplication
	Catch
		Log(LastException)
	End Try	
End Sub

Sub Options2_click
	StartActivity(screen.CreateIntent)
End Sub


Sub RunCamera
	Dim dt As String,filepic As String
	DateTime.DateFormat = "yyMMddHHmmss" ' See this page regarding uppercase letters.
	dt = DateTime.Date(DateTime.Now)
	filepic="Matelas_" & dt & ".jpg"
	expectedPic=filepic
	OpenCam(CNSPath,filepic)		
End Sub


Sub SetDefaults
	'defaults are only set on the first run.
	manager.SetString("cp1", "Campagne1")
	manager.SetString("Rotation", "0")
	manager.SetString("user1", "CNS")
End Sub

Sub CreatePreferenceScreen
	screen.Initialize("Options", "")
	'create two categories
	Dim cat1, cat2 As PreferenceCategory
	cat1.Initialize("Application")
	cat1.AddEditText("cp1", "Campagne", "Nom de la campagne","")
	cat1.AddEditText("user1", "Trigramme", "Trigramme de l'utilisateur","CNS")
	
	cat2.Initialize("Photos")
	cat2.AddList("Rotation", "Rotation", "Rotation auto des photos", "", Array As String("0", "90", "180", "270"))
		
	'add the categories to the main screen
	screen.AddPreferenceCategory(cat1)
	screen.AddPreferenceCategory(cat2)
End Sub


Sub UpdateSpnArea
	'Recree la liste des lieux pour le spinner
	spnArea.Clear
	Main.fTableLieux.Reset
	Dim i As Int
	For i=0 To Main.fTableLieux.GetRowCount - 1
		Main.fTableLieux.GetNextRow
		spnArea.Add(Main.fTableLieux.GetColumnValue("Libelle"))
	Next	
End Sub

Sub UpdateSpnType1
	Dim i As Int
	spnType1.Clear
	Main.fTableMateriel.reset
	For i=0 To Main.fTableMateriel.GetRowCount -1
		Main.fTableMateriel.GetNextRow
		spnType1.Add(Main.fTableMateriel.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypePorte
	Dim i As Int
	spnTypePorte.Clear
	Main.fTableTypePorte.reset
	For i=0 To Main.fTableTypePorte.GetRowCount -1
		Main.fTableTypePorte.GetNextRow
		spnTypePorte.Add(Main.fTableTypePorte.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypePlancher
	Dim i As Int
	spnTypePlancher.Clear
	Main.ftableTypePlancher.reset
	For i=0 To Main.ftableTypePlancher.GetRowCount -1
		Main.ftableTypePlancher.GetNextRow
		spnTypePlancher.Add(Main.ftableTypePlancher.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeLocal
	Dim i As Int
	spnTypeLocal.Clear
	Main.ftableTypeLocal.reset
	For i=0 To Main.ftableTypeLocal.GetRowCount -1
		Main.ftableTypeLocal.GetNextRow
		spnTypeLocal.Add(Main.ftableTypeLocal.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeIsolant
	Dim i As Int
	spnTypeIsolant.Clear
	Main.ftableTypeIsolant.reset
	For i=0 To Main.ftableTypeIsolant.GetRowCount -1
		Main.ftableTypeIsolant.GetNextRow
		spnTypeIsolant.Add(Main.ftableTypeIsolant.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdateSpnTypeCloisonnement
	Dim i As Int
	spnTypeCloison.Clear
	Main.fTableTypeCloisonnement.reset
	For i=0 To Main.fTableTypeCloisonnement.GetRowCount -1
		Main.fTableTypeCloisonnement.GetNextRow
		spnTypeCloison.Add(Main.fTableTypeCloisonnement.GetColumnValue("Libelle"))
	Next
End Sub


Sub UpdateSpnFluide1
	Dim i As Int
	spnFluide2.Clear
	Main.fTableFluide.Reset
	For i=0 To Main.fTableFluide.GetRowCount - 1
		Main.fTableFluide.GetNextRow
		spnFluide2.Add(Main.fTableFluide.GetColumnValue("Libelle"))
	Next
End Sub

Sub UpdatespnVal1
	Dim i As Int
	spnVal1.Clear
	Main.fTableTemperature.Reset
	For i=0 To Main.fTableTemperature.GetRowCount - 1
		Main.fTableTemperature.GetNextRow
		spnVal1.Add(Main.fTableTemperature.GetColumnValue("Libelle"))
	Next
End Sub


Sub btnAddArea_Click
	Dim dlgArea As InputDialog,selArea As String, ret As Int
	dlgArea.Hint = "Entrez la zone"
	dlgArea.HintColor = Colors.ARGB(196, 255, 140, 0)
	ret = DialogResponse.CANCEL
	ret = dlgArea.Show("Entrez le nom de la zone", "Relevé Matelas", "Yes", "No", "Maybe",Null)
	selArea=dlgArea.Input
	If selArea<>"" Then
		Main.fTableLieux.GetNextRow
		Main.fTableLieux.AddRow(Array As Object(selArea,1))
		curArea=selArea
		UpdateSpnArea
		spnArea.SelectedIndex=spnArea.IndexOf(selArea)
	End If
End Sub

Sub spnArea_ItemClick (Position As Int, Value As Object)
	curArea=spnArea.SelectedItem
End Sub


Sub btn_prev_LongClick()
	'retourner a la premiere fiche
Try 
		expectedPic=""
		Main.fCursor.Reset()
		If Main.fCursor.IsBeforeFirst=False Then
			RAZ_fiche
			displayFiche
			IDMsg("FIRST")
		Else
			'ToastMessageShow("Debut de liste atteint !",False)
			Main.fCursor.GetNextRow
			RAZ_fiche
			displayFiche
			'Sleep(0)
		End If	
	Catch
		'ToastMessageShow("Debut de liste atteint !",False)
		Sleep(0)
	End Try			
End Sub

Sub btn_next_LongClick()
	gotoLastRow
End Sub

Sub btnAddMateriel_Click
	Dim dlgMateriel As InputDialog,selMateriel As String,ret As Int
	dlgMateriel.Hint = "Equipement"
	dlgMateriel.HintColor = Colors.ARGB(196, 255, 140, 0)
	ret = dlgMateriel.Show("Entrez l'équipement", "Relevé Matelas", "Yes", "No", "Maybe",Null)
	selMateriel=dlgMateriel.Input
	If selMateriel<>"" Then
		Main.fTableMateriel.GetNextRow
		Main.fTableMateriel.AddRow(Array As Object(selMateriel,1))
		curMateriel=selMateriel
		RefreshspnType
	End If
End Sub



Sub RefreshspnType
	'rajoute le nouvel equipement dans la liste des spinners (tout en gardant les valeurs en cours)
	Dim selEquipment As String
	'------------------------------------------------------------------
	selEquipment=spnType1.SelectedItem
	UpdateSpnType1
	spnType1.SelectedIndex=spnType1.IndexOf(selEquipment)
	'------------------------------------------------------------------
	

End Sub

Sub ImageView1_LongClick
	'Affiche la photo en plein écran
	Try
		Dim sphoto As String
		If photoIdx=2 Then sphoto=Main.fCursor.GetColumnValue("photo2") Else sphoto=Main.fCursor.GetColumnValue("photo1")
		If sphoto<>"" And File.Exists(CNSPath,sphoto) Then
			'affiche la photo 2 dans Imageview1
			imgFullScreen.Bitmap=LoadBitmap(CNSPath, sphoto)
			'canTakePic=True
		Else
			imgFullScreen.Bitmap=LoadBitmap(CNSPath, "vide.jpg")
		End If
		imgFullScreen.Visible=True
		imgFullScreen.BringToFront
	Catch
		Log(LastException)
	End Try

End Sub

Sub imgFullScreen_LongClick
	imgFullScreen.Visible=False
	imgFullScreen.SendToBack
End Sub